﻿var express = require('express'),
    bodyParser = require('body-parser'),
    context = require('./server-src/context'),
    fs = require('fs'),
    compression = require('compression'),
    app = express(),
    http = require('http').Server(app),
    io = require('socket.io')(http),

    // Controllers
    userController = require('./server-src/controllers/userController.js'),
    mapController = require('./server-src/controllers/mapController.js'),
    planetController = require('./server-src/controllers/planetcontroller.js'),
    shipController = require('./server-src/controllers/shipController.js'),
    missionController = require('./server-src/controllers/missionController.js'),
    diplomacyController = require('./server-src/controllers/diplomacyController.js'),
    scienceController = require('./server-src/controllers/scienceController.js'),
    techController = require('./server-src/controllers/techController.js'),
    asteroidController = require('./server-src/controllers/asteroidController.js'),
    adminController = require('./server-src/controllers/adminController.js'),
    guildController = require('./server-src/controllers/guildController.js'),
    fleetController = require('./server-src/controllers/fleetController.js'),
    buildingController = require('./server-src/controllers/buildingcontroller.js');

// Setup the parsing engine.
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// Setup the static files.
app.use(compression());

var oneDay = 86400000 / 4;
app.use('/', express.static('./compiled/', { maxAge: oneDay }));

if ( process.argv[2] == 'DEBUG' ) console.log("This server was started in DEBUG mode.");

// Register the DEBUG check.
app.get('/debug', function (req, res) {
    if (process.argv[2] == 'DEBUG')
        res.send({ debug: true });
    else
        res.send({ debug: false });

    res.end();
});

// Register the root url.
app.get('/', function (res, res) {
    fs.readFile('./compiled/login.htm', function (err, file) {
        res.send(file.toString()); 
    });
});

// Create the game context and pass it everywhere.
var gameContext = new context();

// Error handling
app.use(function (err, req, res, next) {
    if (gameContext)
        gameContext.Logger.logError(err, err.message);
    res.status(500).send("Something broke!");
});

// Setup socket.io
io.on('connection', function (socket) {
    gameContext.SocketManager.bindClient(socket);
    socket.on('disconnect', function () {
        gameContext.SocketManager.disconnect(socket);
    });
});

// Register the controllers.
userController(app, gameContext);
mapController(app, gameContext);
buildingController(app, gameContext);
planetController(app, gameContext);
shipController(app, gameContext);
missionController(app, gameContext);
diplomacyController(app, gameContext);
scienceController(app, gameContext);
techController(app, gameContext);
asteroidController(app, gameContext);
adminController(app, gameContext);
guildController(app, gameContext);
fleetController(app, gameContext);

// Start the server.
var server = http.listen(process.env.PORT || 1337, function () { });

// Spawn the worker threads.
if (process.argv[2] == 'DEBUG' && process.argv[3] !== "THREADED") {
    // NODEJS VS can't handle forking.
    require('./server-src/logic/queueWorker.js');
} else {
    gameContext.QueueManager.spawnThreads();
}