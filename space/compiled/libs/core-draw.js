var Drawable = function(config) {
  config = config || {};
  this.x = config.x || 0;
  this.y = config.y || 0;
  this.fx = config.fx || 1.0;
  this.fy = config.fy || 1.0;
  this.vx = config.vx || 0;
  this.vy = config.vy || 0;
  this.tx = config.tx || 0; // textX (offset)
  this.ty = config.ty || 0; // textY (offset)
  this.width = config.width || 0;
  this.height = config.height || 0;
  this.tinted = config.tinted || 0;
  this.angle = config.angle || 0;
  this.visible = config.visible || true;
  this.current_angle = config.angle || 0;
  this.ox = 0;
  this.oy = 0;
  this.cx = 0;
  this.cy = 0;
  this.zoom = 0;
  this.zoomMod = config.zoomMod || 1.0;
  this.scale = config.scale || 1.0;
  this.planet = config.planet;
  this.context = config.context;
  this.name = config.name || '';
  this.children = [];

  this.hasAngles = config.hasAngles || false;
  this.image = null;
  this.text = config.text || '';
  this.font = config.font || '';
  this.textColor = config.textColor;
  this.drawLogic = [];
  this.updateLogic = [];
  this.mm = config.mm;
  this.md = config.md;
  this.mu = config.mu || function() {};
  this.altmd = [];
  this.altmu = [];
  this.clicktime = 0;

  // If we have an image, we need to get the image object.
  if ( $.trim(config.image).length > 0 ) {
    this.image = Assets.Get(config.image);
    this.width = this.image.width * this.scale;
    this.height = this.image.height * this.scale;
  }
};

Drawable.prototype.onMouseMove = function(x,y) {
  if ( !this.mm ) return false;
  this.mm.call(this, x, y);
  for ( var i =0; i < this.children.length; i++ ) {
    this.children[i].onMouseMove.call(this.children[i],x,y);
  }
};

Drawable.prototype.onMouseDown = function(x,y) {
  delete this.altmd;
  this.altmd = [];

  if (!this.md) return false;
  var res = this.md.call(this, x, y);
  if ( typeof(res) == 'function' ) {
    this.altmd.push(res);
  } else if (res){
    return true;
  }

  for ( var i = 0; i < this.children.length; i++ ) {
    if (this.children[i].onMouseDown.call(this.children[i],x,y)) return true;
  }
  return false;
};

Drawable.prototype.onMouseUp = function(x,y) {
  delete this.altmu;
  this.altmu = [];

  if (!this.mu) return false;
  var res = this.mu.call(this, x, y);
  if ( typeof(res) == 'function' ) {
    this.altmu.push(res);
  } else if (res){
    return true;
  }

  for ( var i = 0; i < this.children.length; i++ ) {
    if (this.children[i].onMouseUp.call(this.children[i],x,y)) return true;
  }
  return false;
};

Drawable.prototype.cascade = function(fn) {
  if ( fn ) {
    fn.call(this);
    for ( var i = 0; i < this.children.length; i++ ) {
      this.children[i].cascade.call(this.children[i], fn);
    }
  }
};

Drawable.prototype.distance = function(x,y) {
  // Calculate the center.
  var cx = (this.x + 15) * (this.gZoom || 1.0);
  var cy = (this.y + 15) * (this.gZoom || 1.0);
  return Math.sqrt(Math.pow(x - cx, 2) + Math.pow(y - cy, 2));
};

Drawable.prototype.addDrawingLogic = function(fn) {
  this.drawLogic.push(fn.bind(this));
};

Drawable.prototype.addLogic = function(ctx, fn) {
  this.updateLogic.push(fn.bind(this,ctx));
  fn.call(this, ctx);
};

function stepScale(ctx, scale, x, y, w, img) {
  var oc = document.createElement('canvas'),
      oc2 = document.createElement('canvas'),
      octx = oc.getContext('2d'),
      octx2 = oc2.getContext('2d');

  if ( w < img.width * 0.5 ) {
    var factor = 0.4;
    factor = scale;

    oc.width = img.width * factor;
    oc.height = img.height * factor;
    octx.drawImage(img, 0, 0, oc.width, oc.height);

    // Step 2
    octx2.drawImage(oc, 0, 0, oc.width * factor, oc.height * factor);

    // Step 3
    ctx.drawImage(oc2, 0, 0, oc.width * factor, oc.height * factor, x, y, w, w);
  } else {
    ctx.drawImage(img, x, y, w, w);
  }
}

Drawable.prototype.onDraw = function(ctx, gZoom) {
  // If it's an image.
  this.gZoom = gZoom;
  if (!this.visible) return;
  if ( this.image ) {
    ctx.save();
    var w = this.width;
    var h = this.height;
    var x = this.x * this.fx - (w/2) + this.ox + (this.zoom * this.zoomMod);
    var y = this.y * this.fy - (h/2) + this.oy + (this.zoom * this.zoomMod);

    // Update the center position since we just calculated.
    this.nx = x;
    this.ny = y;
    this.cx = x - (this.zoom * this.zoomMod) / 2;
    this.cy = y - (this.zoom * this.zoomMod) / 2;

    if ( this.angle != 0 || this.hasAngles ) {
      // rotation logic
      ctx.translate(this.cx, this.cy);
      ctx.rotate(this.angle);
      ctx.drawImage(this.image, -w/2, -h/2, w - (this.zoom * this.zoomMod), h - (this.zoom * this.zoomMod));
    } else {
      // normal logic
      ctx.drawImage(this.image, x - (this.zoom * this.zoomMod) / 2, y- (this.zoom * this.zoomMod)/2, w - (this.zoom * this.zoomMod), h - (this.zoom * this.zoomMod));
    }

    ctx.restore();

    // Draw the text.
    if ( $.trim(this.text).length > 0 ) {
      ctx.save();
      ctx.font = this.font || '20px Exo2';
      ctx.fillStyle = this.textColor || "rgba(255,255,255,0.5)";
      ctx.fillText(this.text, this.cx - (ctx.measureText(this.text).width / 2) + (this.width/2), this.cy - this.ty);
      ctx.restore();
    }

    if ( this.tinted ) {
      ctx.save();
      ctx.fillStyle = "rgba(0,0,0,0.45)";
      ctx.beginPath();
      ctx.arc(this.cx + this.width/2,this.cy+this.height/2,this.width/2,0,2*Math.PI);
      ctx.fill();
      ctx.closePath();
      //ctx.fillRect(x,y,w,h);
      ctx.restore();
    }
  }

  for ( var i = 0; i < this.drawLogic.length; i++ ) {
    ctx.save();
    this.drawLogic[i](ctx, gZoom);
    ctx.restore();
  }

  for ( var i = 0; i < this.children.length; i++ ) {
    this.children[i].onDraw(ctx, gZoom);
  }
};

Drawable.prototype.onUpdate = function() {
  this.x += this.vx;
  this.y += this.vy;

  // Update the angle.
  for ( var i = 0; i < this.updateLogic.length; i++ ) {
    this.updateLogic[i]();
  }
  for ( var i = 0; i < this.children.length; i++ ) {
    this.children[i].onUpdate();
  }
};

var Line = function(config) {
  var obj = new Drawable(config);
  obj.x1 = config.x1;
  obj.x2 = config.x2;
  obj.y1 = config.y1;
  obj.y2 = config.y2;
  obj.addDrawingLogic(function(ctx){
    ctx.save();
    ctx.beginPath();
    ctx.moveTo(this.x1,this.y1);
    ctx.lineTo(this.x2, this.y2);
    ctx.strokeStyle = "#5BF4FF";
    ctx.stroke();
    ctx.closePath();
    ctx.restore();
  });
  return obj;
};

var Hexagon = function(config) {
  var obj = new Drawable(config);
  obj.radius = config.radius;
  obj.bx = config.bx;
  obj.by = config.by;
  obj.explored = config.explored || false;
  obj.colonized = config.colonized || false;
  obj.home = config.home;
  obj.mdc = config.md;
  obj.ctx = config.ctx;
  obj.mm = function(x,y) {
    var gZoom = this.gZoom || 1.0;
    var distance = Math.sqrt(Math.pow((x - 15) - this.cx * gZoom,2) + Math.pow((y - 15) - this.cy * gZoom,2));
    if (distance < (this.radius * 0.8 * gZoom)) {
      this.hovered = true;
    } else {
      this.hovered = false;
    }
  };
  obj.md = function(x,y) {
    var gZoom = this.gZoom || 1.0;
    var distance = Math.sqrt(Math.pow((x - 15) - this.cx * gZoom,2) + Math.pow((y - 15) - this.cy * gZoom,2));
    if (distance < (this.radius * 0.8 * gZoom)) {
      this.hovered = true;
      this.mdc.call(this, x, y);
    } else {
      this.hovered = false;
    }
  };
  obj.addDrawingLogic(function(ctx, gZoom){
    this.gZoom = gZoom;
    ctx.save();
    ctx.beginPath();

    // All these hexagon variables.
    var alpha = 0.25;
    if ( this.hovered ) alpha = 0.75;
    var color = "rgba(37,91,99," + alpha + ")";
    var border = "rgb(37,91,99)";

    if ( !this.explored && !this.colonized) {
      // grayed out.
      color = "rgba(255,43,57," + alpha + ")";
      border = "rgba(255,43,57, 0.5)";
    }

    // Grayish
    if ( this.explored ) {
      var gray = 107;
      color = "rgba(" + gray +"," + gray + "," + gray +"," + alpha + ")";
      border = "rgba(" + gray +"," + gray + "," + gray +",0.5)";
    }

    if ( this.home ) {
      color = "rgba(0,253,246," + alpha + ")";
      border = "rgba(0,253,246,0.5)";
    }

    var hexagonAngle = 0.523598776;
    var sideLength = this.radius;
    var hexHeight = Math.sin(hexagonAngle) * sideLength;
    var hexRadius = Math.cos(hexagonAngle) * sideLength;
    var hexRectangleHeight = sideLength + 2 * hexHeight;
    var hexRectangleWidth = hexRadius * 2;

    var x = this.x + (this.bx * hexRectangleWidth + ((this.by % 2) * hexRadius));
    var y = this.y + (this.by * (sideLength + hexHeight));

    ctx.moveTo(x + hexRadius, y);
    ctx.lineTo(x + hexRectangleWidth, y + hexHeight);
    ctx.lineTo(x + hexRectangleWidth, y + hexHeight + sideLength);
    ctx.lineTo(x + hexRadius, y + hexRectangleHeight);
    ctx.lineTo(x, y + sideLength + hexHeight);
    ctx.lineTo(x, y + hexHeight);
    ctx.closePath();
    ctx.strokeStyle = border;
    ctx.fillStyle = color;
    ctx.stroke();
    ctx.fill();

    // Update the center.
    this.cx = x + hexRectangleWidth / 2;
    this.cy = y + hexRectangleWidth / 2;

    // If we've explored this piece, draw a circle.
    if ( this.explored || this.colonized ) {
      ctx.beginPath();
      ctx.fillStyle = "#5BF4FF";
      ctx.strokeStyle = ctx.fillStyle;
      ctx.arc(x + hexRectangleWidth / 2,y + hexRectangleHeight/2,5,0,2*Math.PI);
      ctx.fill();
      ctx.closePath();
      ctx.beginPath();
      ctx.lineWidth = 2;
      ctx.arc(x + hexRectangleWidth / 2,y + hexRectangleHeight/2,10,0,2*Math.PI);
      ctx.stroke();
      ctx.closePath();
    }

    ctx.fillStyle = "rgba(255,255,255,0.5)";
    var txtWidth = ctx.measureText(this.text).width;
    ctx.fillText(this.text, x + hexRectangleWidth / 2 - (txtWidth/2),y + (hexRectangleHeight/2) - (txtWidth/2));

    ctx.restore();
  });
  return obj;
};

var Orbit = function(config) {
  config = config || {};
  this.color = config.color || '#ADADAD';
  this.x = config.x || 0;
  this.y = config.y || 0;
  this.ox = 0;
  this.oy = 0;
  this.mm = config.mm || function() {};
  this.md = config.md || function() {};
  this.mu = config.mu || function() {};
  this.visible = config.visible || true;

  // Deferred md. A list of functions to run onClick
  // if nothing else claims the event.
  this.altmd = [];
  this.altmu = [];

  // factorX / factorY determines isometric look.
  this.fx = config.fx || 1.0;
  this.fy = config.fy || 1.0;
  this.zoom = 0;
  this.radius = config.radius || 10;
  this.children = [];
};

Orbit.prototype.cascade = function(fn) {
  if ( fn ) {
    fn.call(this);
    for ( var i = 0; i < this.children.length; i++ ) {
      this.children[i].cascade.call(this.children[i], fn);
    }
  }
};

Orbit.prototype.onDraw = function(ctx, gZoom) {
  if ( !this.visible ) return;
  function drawOrbit(clip) {
    ctx.scale(this.fx, this.fy);
    ctx.strokeStyle = this.color;
    ctx.setLineDash([5]);
    ctx.beginPath();
    ctx.arc(this.x + this.ox, this.y + this.oy, this.radius - (this.zoom * 5), 0, 2 * Math.PI, false);
    ctx.lineWidth = 1;
    ctx.closePath();
    ctx.stroke();

  }

  // Draw first pass.
  ctx.save();
  drawOrbit.call(this);
  ctx.restore();

  // Draw the orbital bodies.
  this.gZoom = gZoom;
  for ( var i = 0; i < this.children.length; i++ ) {
    this.children[i].fx = this.fx;
    this.children[i].fy = this.fy;
    this.children[i].gZoom = gZoom;
    ctx.save();
    this.children[i].onDraw(ctx, gZoom);
    ctx.restore();
  }
};

Orbit.prototype.onMouseDown = function(x,y) {
  var gZoom = this.gZoom || 1.0;
  delete this.altmd;
  this.altmd = [];

  for ( var i = 0; i < this.children.length; i++ ) {
    var distance = Math.sqrt(Math.pow(x- (this.children[i].x * gZoom),2) + Math.pow(y - (this.children[i].y*gZoom),2));
    if ( distance < (35 * this.gZoom || 1.0 )) {
      if (this.children[i].md) {
        var res = this.children[i].md.call(this.children[i], x, y);
        if ( typeof(res) == 'function' ) {
          this.altmd.push(res);
        } else if ( res ) {
          return true;
        }
        return false;
      }
    }
  }
  return false;
};

Orbit.prototype.onMouseMove = function(x,y) {
  for ( var i = 0; i < this.children.length; i++ ) {
    if (this.children[i].mm) {
      if ( this.children[i].mm.call(this.children[i], x, y)) {
        return true;
      }
    }
  }
  return false;
};

Orbit.prototype.onMouseUp = function(x,y) {
  for ( var i = 0; i < this.children.length; i++ ) {
    if (this.children[i].mu) {
      var res = this.children[i].mu.call(this.children[i], x, y);
      if ( typeof(res) == 'function' ) {
        this.altmu.push(res);
      } else if ( res ) {
        return true;
      }
    }
  }
  return false;
};

Orbit.prototype.onUpdate = function() {
  for ( var i = 0; i < this.children.length; i++ ) {
    this.children[i].onUpdate();
  }
};

Orbit.prototype.addObject = function(drawable, angle) {
  // Place it.
  var ox = this.x;
  var oy = this.y;
  var rad = this.radius;

  // Add the logic module.
  drawable.addLogic({
    ox: ox,
    oy: oy,
    angle: angle,
    rad: rad,
    parent: this,
    speed: Math.random()
  },function(obj) {
    // Calculate the x/y of the planet.
    this.theta = this.theta || ((obj.angle / 180) * Math.PI);
    this.x = obj.ox - (Math.cos(this.theta) * (obj.rad - (obj.parent.zoom * 5)));
    this.y = obj.oy - (Math.sin(this.theta) * (obj.rad - (obj.parent.zoom * 5)));
  });

  // Push this thing to the children list.
  this.children.push(drawable);
};

var Viewport = function(config){
  config = config || {};
  this.ox = config.ox || 0;
  this.oy = config.oy || 0;
  this.zoom = 0;
  this.name = config.name || '';
  this.children = [];
};

Viewport.prototype.mm = function(x,y) {
  for ( var i = 0; i < this.children.length; i++ ) {
    if ( this.children[i].onMouseMove )
      this.children[i].onMouseMove.call(this.children[i],x,y);
    else if ( this.children[i].mm )
      this.children[i].mm.call(this.children[i],x,y);
  }
};

Viewport.prototype.md = function(x,y) {
  // Clear the altmd.
  for ( var i = 0; i < this.children.length; i++ ) {
    this.children[i].cascade(function() { delete this.altmd; this.altmd = []; });
  }

  for ( var i = 0; i < this.children.length; i++ ) {
    if ( this.children[i].onMouseDown ) {
      if ( this.children[i].onMouseDown.call(this.children[i],x,y) ) return;
    }
    else if ( this.children[i].md )
      if (this.children[i].md.call(this.children[i],x,y)) return;
  }

  // If nothing returned a success, then cascade the invokation of
  // altmd.
  for ( var i = 0; i < this.children.length; i++ ) {
    this.children[i].cascade(function() {
      for ( var i = 0; i < this.altmd.length; i++ ) {
        this.altmd[i].call(this);
      }
    });
  }
};

Viewport.prototype.mu = function(x,y) {
  // Clear the altmd.
  for ( var i = 0; i < this.children.length; i++ ) {
    this.children[i].cascade(function() { delete this.altmu; this.altmu = []; });
  }

  for ( var i = 0; i < this.children.length; i++ ) {
    if ( this.children[i].onMouseUp )
      if ( this.children[i].onMouseUp.call(this.children[i],x,y) ) return;
    else
      if (this.children[i].mu.call(this.children[i],x,y)) return;
  }

  // If nothing returned a success, then cascade the invokation of
  // altmu.
  for ( var i = 0; i < this.children.length; i++ ) {
    this.children[i].cascade(function() {
      for ( var i = 0; i < this.altmu.length; i++ ) {
        this.altmu[i].call(this);
      }
    });
  }
};

Viewport.prototype.addAsset = function(obj) {
  this.children.push(obj);
};

Viewport.prototype.onDraw = function(ctx, gZoom) {
  for ( var i = 0; i < this.children.length; i++ ) {
    ctx.save();
    this.children[i].onDraw(ctx, gZoom);
    ctx.restore();
  }
};

Viewport.prototype.onUpdate = function() {
  var ox = this.ox;
  var oy = this.oy;
  var zoom = this.zoom;
  for ( var i = 0; i < this.children.length; i++ ) {
    this.children[i].cascade(function(){
      this.ox = ox;
      this.oy = oy;
      this.zoom = zoom;
    });
    this.children[i].onUpdate();
  }
};

Viewport.prototype.find = function(name) {
  if ( this.name == name ) return this;
  for ( var i = 0; i < this.children.length; i++ ) {
    if ( this.children[i].name == name ) return this.children[i];
  }
  return null;
};

Viewport.prototype.cascade = function(fn) {
  for ( var i = 0; i < this.children.length; i++ )
    this.children[i].cascade(fn);
};

var JSCanvas = function(canvasId) {
  this.canvas = document.getElementById(canvasId);
  this.ctx = this.canvas.getContext('2d');
  this.scenes = [];
  this.assets = [];
  this.gZoom = 1.0; // global zoom.
  this.fadeDirection = 0;
  this.fadeCallback = null;
  this.activeScene = null;
  this.onDragChange = function() {};
  this.onDraw();
  this.onUpdate();
};

JSCanvas.prototype.cascade = function(fn) {
  if ( this.activeScene ) this.activeScene.cascade(fn);
}

JSCanvas.prototype.addScene = function(name, drawable) {
  drawable["sceneName"] = name;
  this.scenes.push(drawable);
  if ( this.activeScene == null ) this.activeScene = drawable;
};

JSCanvas.prototype.getActiveScene = function() {
  return (this.activeScene || {}).sceneName || '';
};

JSCanvas.prototype.openScene = function(name) {
  if ( this.activeScene && this.activeScene.sceneName == name ) return;
  for ( var i = 0; i < this.scenes.length; i++ ) {
    if ( this.scenes[i].sceneName == name ) {
      this.activeScene = this.scenes[i];
    }
  }
};

JSCanvas.prototype.fade = function(fadeOut, callback) {
  if ( this.fadeDirection !== 0 ) return;

  if (!fadeOut) {
    this.ctx.globalAlpha = 0.0;
  } else {
    this.ctx.globalAlpha = 1.0;
  }

  this.fadeDirection = (fadeOut) ? 1 : -1;
  setTimeout(callback, 250);
};

JSCanvas.prototype.clearAll = function(callback) {
  this.activeScene = new Drawable();
  delete this.assets;
  delete this.scenes;
  this.assets = [];
  this.scenes = [];
  if ( callback ) callback.call();
};

JSCanvas.prototype.find = function(name) {
  if ( this.activeScene ) {
    return this.activeScene.find(name);
  }
  return null;
}

JSCanvas.prototype.addAsset = function(asset) {
  if ( asset.onDraw && asset.onUpdate ) {
    this.assets.push(asset);
  }
};

JSCanvas.prototype.onMouseMove = function(x,y) {
  if ( this.activeScene ) {
    this.activeScene.mm(x,y);
  }
};

JSCanvas.prototype.onMouseDown = function(x,y) {
  if ( this.activeScene ) {
    this.activeScene.md(x,y);
  }
};

JSCanvas.prototype.onMouseUp = function(x,y) {
  if ( this.activeScene ) {
    this.activeScene.mu(x,y);
  }
}

JSCanvas.prototype.setOnDragChange = function(fn) {
  this.onDragChange = fn;
}

JSCanvas.prototype.changeDragState = function(dd) {
  this.dd = dd;
  if ( this.onDragChange ) {
    this.onDragChange.call(this, dd);
  }
}

JSCanvas.prototype.onDraw = function() {
  var ctx = this.ctx;
	this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  if ( this.activeScene ) {
    if ( this.fadeDirection < 0 ) {
      ctx.globalAlpha = Math.min(ctx.globalAlpha + 0.2, 1.0);
      if ( ctx.globalAlpha == 1.0 ) {
        this.fadeDirection = 0;
      }
    } else if ( this.fadeDirection > 0) {
      ctx.globalAlpha = Math.max(ctx.globalAlpha - 0.2, 0.0);
      if ( ctx.globalAlpha == 0.0 ) {
        this.fadeDirection = 0;
      }
    }

    this.ctx.save();
    this.ctx.scale(this.gZoom,this.gZoom);
    this.activeScene.onDraw(this.ctx, this.gZoom);
    this.ctx.restore();
  }

  setTimeout(this.onDraw.bind(this), 30);
};

JSCanvas.prototype.onUpdate = function() {
  if ( this.activeScene ) {
    this.activeScene.onUpdate();
  }
  setTimeout(this.onUpdate.bind(this), 30);
};
