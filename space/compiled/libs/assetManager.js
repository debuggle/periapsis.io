// 2014-08-22
// SharpCoder
// This is the asset manager for my ludum dare 30 entry.
Assets = (function() {
	var images = [
		'anomaly2.png',
    'asteroid.png',
    'planet_home.png',
    'planet_silver.png',
    'planet-silmar.png',
    'planet0.png',
    'planet2.png',
    'planet3.png',
    'planet4.png',
    'planet5.png',
    'planet6.png',
    'planet7.png',
    'planet8.png',
		't-1.png',
		't-2.png',
		't-3.png',
		't-4.png',
    'star.png',
		'star2.png',
		'pirate-ship.png',
		'ship-icon.png',
		'ship-enemy-icon.png',
		'ship-single-big.png',
		'ship-single-move.png',
		'ship-single-big-enemy.png',
		'ship-single-move-enemy.png',
		'star3.png'
	];

	var loaded = {};
	var loaded_sound = {};
	var index = 0;

	function process( i, callback ) {
		if ( i >= images.length ) {
      if ( callback ) callback.call();
			return;
		}

		var image = new Image();
		image.onload = function() {
			process( i + 1, callback );
		};
		image.src = '../assets/' + images[i];
		loaded[images[i]] = image;
	}

	return {
		Initialize: function( callback ) {
			process(0, callback);
		},
		Get: function(asset) {
			if ( loaded[asset] !== undefined && loaded[asset] !== null ) {
				return loaded[asset];
			}
		}
	}
})();
