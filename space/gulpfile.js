var gulp = require('gulp'),
    concat = require('gulp-concat'),
    react = require('gulp-react'),
    less = require('gulp-less');

gulp.task('default', function() {
  gulp.src(['./web-src/*.htm'])
    .pipe(gulp.dest('./compiled/'));

  gulp.src(['./web-src/*.less'])
    .pipe(less())
    .pipe(gulp.dest('./compiled/'));

  gulp.src(['./web-src/assets/*.*'])
    .pipe(gulp.dest('./compiled/assets/'));

  gulp.src('./web-src/libs/*.js')
   .pipe(gulp.dest('./compiled/libs/'));

  gulp.src('./web-src/js/*.js')
     .pipe(react())
     .pipe(concat('tetra.js'))
     .pipe(gulp.dest('./compiled/'));
});
