﻿var uuid = require('node-uuid');
// Action is the follow up action to call after the fleet arrives.
// Requires -----
// user,
// fleet,
// sx,
// sy,
// x,
// y,
// angle
module.exports = function (action, gameContext, actionObj, req, res, tCoeff, callback) {
    var user = req.body.user;
    var fleet = req.body.fleet;
    
    // DestinationX, DestinationY
    // tCoeff - alternate arrival time.
    var sx = req.body.sx; // StartX
    var sy = req.body.sy; // StartY
    var dmx = req.body.x; // DestinationX
    var dmy = req.body.y; // DestinationY
    var angle = req.body.angle;
    
    function continue_process(objUser) {
        gameContext.FleetManager.getFleet(user, fleet, function (objFleet) {
            
            // Update the map properties
            if (Math.abs(sx - objFleet.x) > 930 || Math.abs(sy - objFleet.y) > 930) {
                res.send({ success: false, msg: 'Invalid starting coordinates.' });
                res.end();
                return;
            }
            
            // Get the ship count.
            var shipCount = 0;
            for (var i = 0; i < objFleet.ships.length; i++) {
                shipCount += parseFloat(objFleet.ships[i][1]) || 0;
            }
            
            objFleet.angle = angle;
            objFleet.x = sx;
            objFleet.y = sy;
            objFleet.sx = sx;
            objFleet.sy = sy;
            objFleet.dx = dmx;
            objFleet.dy = dmy;
            objFleet.moved = true;
            objFleet.guid = uuid.v1();
            
            // Check if we were protecting an asteroid.
            if (objFleet.asteroidX && objFleet.asteroidY) {
                // Update the asteroid.
                gameContext.AsteroidManager.abandon(objFleet.asteroidX, objFleet.asteroidY, function () {
                    gameContext.SocketManager.updateMap(user);
                });
                
                objFleet.asteroidX = null;
                objFleet.asteroidY = null;
            }
            
            // Check if we were defending a planet.
            if (objFleet.planetX && objFleet.planetY) {
                var px = objFleet.planetX, py = objFleet.planetY;
                gameContext.Map.updateProperty(objFleet.planetX, objFleet.planetY, "defendingFleet", null, function () {
                    gameContext.SocketManager.refreshMap(user, gameContext.Map.getQuadrant(px,py));
                });
                
                objFleet.planetX = null;
                objFleet.planetY = null;
            }
            
            // Calculate the time to move.
            var distance = Math.sqrt(Math.pow(sx - dmx, 2) + Math.pow(sy - dmy, 2));
            var time = (distance * gameContext.getSettings(1).fleetMoveTime);
            time = Math.min(tCoeff || time, time);

            // Arbitrarily assume all maps are 800 pixels.
            objFleet.thrustStart = Math.round(gameContext.now());
            objFleet.thrustEnd = Math.round(objFleet.thrustStart + time);
            
            // Verify we have enough h3.
            var h3cost = (distance / 7) * (shipCount * 3);
            
            // Verify the user has enough.
            if (res && objUser && parseFloat(objUser.h3) < h3cost) {
                res.send({ success: false, msg: 'You do not have enough H3 to move that distance.' });
                res.end();
            } else {
                // Debit the h3.
                if (objUser)
                    gameContext.UserManager.debitResources(user, 0, 0, h3cost, 0);
                
                // Invoke the callback so things that are thrusting can spawn off timers if they want.
                if (callback) callback.call(this, distance, time / 1000);
            
                // Save the change.
                // Convert time to seconds.
                time = Math.round((time - 1000) / 1000);
                gameContext.FleetManager.updateFleet(user, fleet, objFleet, function () {
                    gameContext.QueueManager.enqueue('moveFleet', { action: action, actionObj: actionObj, x: dmx, y: dmy, user: user, fleet: fleet, guid: objFleet.guid }, time, function () {
                        gameContext.SocketManager.updateFleet(user);
                        if (res) {
                            res.send({ success: true });
                            res.end();
                        }
                    });
                });
            }
        });
    }

    // Update the variables.
    gameContext.UserManager.getUser(user, function (objUser) {
        continue_process(objUser);
    }, function () {
        continue_process()
    });
    
};