﻿// Easing function.
function ease(t) { return t < .5 ? 2 * t * t : -1 + (4 - 2 * t) * t };
var thrust = require('./thrust.js');
module.exports = function (gameContext, req, res) {
    var attackingUser = req.body.attackingUser;
    var attackingFleet = req.body.attackingFleet;
    var defendingUser = req.body.defendingUser;
    var defendingFleet = req.body.defendingFleet;
    var capturingPlanet = req.body.capturingPlanet || false;
    
    // Get the fleets.
    gameContext.FleetManager.getFleet(attackingUser, attackingFleet, function (objFleetAttacking) {
        gameContext.FleetManager.getFleet(defendingUser, defendingFleet, function (objFleetDefending) {
            
            // Get the positions.
            var f1 = {
                sector: objFleetAttacking.sector,
                startTime: objFleetAttacking.thrustStart,
                endTime: objFleetAttacking.thrustEnd,
                startX: objFleetAttacking.x,
                startY: objFleetAttacking.y,
                endX: objFleetAttacking.dx,
                endY: objFleetAttacking.dy,
                currentX: 0,
                currentY: 0
            };
            
            var f2 = {
                sector: objFleetDefending.sector,
                startTime: objFleetDefending.thrustStart,
                endTime: objFleetDefending.thrustEnd,
                startX: objFleetDefending.x,
                startY: objFleetDefending.y,
                endX: objFleetDefending.dx,
                endY: objFleetDefending.dy,
                currentX: 0,
                currentY: 0
            };
            
            // Calculate the current positions.
            function calculatePosition(sector, startX, startY, endX, endY, startTime, endTime, now) {
                now = now || gameContext.now();
                if (endTime > now) {
                    var sectorCoords = gameContext.Map.normalizeSector(sector);
                    var offsetX = gameContext.FleetManager.mapToFleet(sectorCoords[0] * 10, sectorCoords[1] * 10)[0];
                    var offsetY = gameContext.FleetManager.mapToFleet(sectorCoords[0] * 10, sectorCoords[1] * 10)[1];
                    var diffX = Math.round((offsetX - endX) + (offsetX - startX));
                    var diffY = Math.round((offsetY - endY) + (offsetY - startY));
                    var totalTime = endTime - startTime;
                    var elapsed = now - startTime;
                    var traveled = (elapsed / totalTime);
                    var eased = ease(traveled);
                    return [offsetX + (diffX * eased), offsetY + (diffY * eased)];
                } else {
                    return [endX, endY];
                }
            }
            
            // Calculate the current position of each fleet.
            var f1Current = calculatePosition(f1.sector, f1.startX, f1.startY, f1.endX, f1.endY, f1.startTime, f1.endTime);
            var f2Current = calculatePosition(f2.sector, f2.startX, f2.startY, f2.endX, f2.endY, f2.startTime, f2.endTime);
            f1.currentX = f1Current[0];
            f1.currentY = f1Current[1];
            f2.currentX = f2Current[0];
            f2.currentY = f2Current[1];
            
            // Check how long it will take to get to their current position.
            var distance = Math.sqrt(Math.pow(f1.currentX - f2.currentX, 2) + Math.pow(f1.currentY - f2.currentY, 2));
            var time = distance * gameContext.getSettings(1).fleetMoveTime;
            
            // Now just simulate the time component along the original fleets vector.
            var destination = calculatePosition(f2.sector, f2.startX, f2.startY, f2.endX, f2.endY, f2.startTime, f2.endTime, gameContext.now() + (time / 2));
            
            // Now, for thrust, we need the following variables:
            // user, fleet, sx, sy, x, y, angle
            req.body.user = objFleetAttacking.user;
            req.body.fleet = objFleetAttacking.title;
            req.body.sx = f1.startX;
            req.body.sy = f1.startY;
            req.body.x = destination[0];
            req.body.y = destination[1];
            req.body.angle = Math.atan2(destination[1] - f1.currentY, destination[0] - f1.currentX);
            
            // Thrust.
            var objCombat = {
                attackingUser: attackingUser,
                defendingUser: defendingUser,
                attackingFleet: attackingFleet,
                defendingFleet: defendingFleet,
                interceptX: destination[0],
                interceptY: destination[1]
            };
            
            // Tell the fleets they can't change course now.
            //gameContext.FleetManager.putProperty(attackingUser, attackingFleet, "war", true, function () {
            //    gameContext.FleetManager.putProperty(defendingUser, defendingFleet, "war", true, function () {
            
            // Do the thrust.
            thrust('fleetCombat', gameContext, objCombat, req, res, time, function (distance, time) {
                gameContext.TimerManager.spawn("combat", objFleetAttacking.user, time, "Until your fleet '" + objFleetAttacking.displayTitle + "' arrives to intercept an enemy player.", "red", {});
            });

                //    });                    
                //});

        }, function () {
            // Failure condition.
            if (res) {
                res.send({ success: false });
                res.end();
            }
        });
    }, function () {
        // Failure condition.
        if (res) {
            res.send({ success: false });
            res.end();
        }
    });
};