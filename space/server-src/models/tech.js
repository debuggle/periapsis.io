﻿var Tech = function (config) {
    this.title = config.title || '';
    this.description = config.description || '';
    this.cost = config.cost || 0;
    this.icon = config.icon || 'fa-flask';
    this.category = config.category || 'Science';

    this.children = [];
};

Tech.prototype.addChild = function (config) {
    var child = new Tech(config);
    this.children.push(child);
    return child;
};

module.exports = Tech;