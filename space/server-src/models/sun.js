﻿var Sun = function (config) {
    this.x = config.x || 0;
    this.y = config.y || 0;
    this.owner = config.owner;
    
    // Cataclysmic Events
    this.solarFlareTime = 0;
};

module.exports = Sun;