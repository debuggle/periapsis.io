﻿Quest = function (title, index, issuer, description) {
    this.title = title;
    this.index = index;
    this.issuer = issuer;
    this.description = description;
};

module.exports = Quest;