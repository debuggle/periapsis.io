﻿Ship = function (config) {
    config = config || {};
    this.timeMultiplier = config.timeMultiplier || 1.0;
    this.cost = config.cost || 0;
    this.name = config.title || '';
    this.attack = config.attack || 0;
    this.defense = config.defense || 0;
    this.hp = config.hp || 0;
    this.description = config.description || '';
    this.pirate = config.pirate || false;
    this.tech = config.tech || '';
    this.explorationDistance = config.explorationDistance || 0;
    this.miningIronMultiplier = config.miningIronMultiplier || 0;
    this.miningGoldMultiplier = config.miningGoldMultiplier || 0;
    this.miningH3Multiplier = config.miningH3Multiplier || 0;
};

module.exports = Ship;