﻿// This is different from a building. For some reason.
var Structure = function (config) {
    this.h3Cost = config.h3Cost || 0;
    this.goldCost = config.goldCost || 0;
    this.ironCost = config.ironCost || 0;
    this.creditCost = config.creditCost || 0;
    this.icon = config.icon || 'fa-flask';
    this.name = config.name || '';
    this.description = config.description || '';
};

module.exports = Structure;