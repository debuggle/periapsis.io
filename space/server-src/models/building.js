﻿Building = function (timeManipulator, cost, category, gold, iron, h3, science, spaceDock, title, description) {
    this.timeManipulator = timeManipulator || 1.0;
    this.cost = cost;
    this.gold = gold;
    this.iron = iron;
    this.h3 = h3;
    this.science = science;
    this.title = title;
    this.category = category;
    this.spaceDock = spaceDock;
    this.description = description;
};

module.exports = Building;