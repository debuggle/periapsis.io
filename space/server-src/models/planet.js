﻿var NameHelper = require('../nameHelper');

Planet = function (config) {
    config = config || {};
    this.type = config.type;
    this.owner = config.owner; // For colonization.
    this.climate = config.climate; // For fun.
    this.description = '';
    this.size = '';
    this.isPlanet = true;
    this.isHome = false;
    this.x = config.x || 0;
    this.y = config.y || 0;
    this.name = config.name || '';
    this.sector = config.sector || 0;
    this.layer = config.layer || 0;
    
    // Attack information.
    this.shields = 100;
    this.defense = 5;

    // Store the timer of a currently underway colonizing action.
    this.colonizeAction = null
    
    // Building construction
    this.MineAction = null;
    this.GasExtractorAction = null;
    this.ScienceLabAction = null;
    this.ShipYardAction = null;
    this.SpacePortAction = null;
    
    // Building Ownage
    this["Iron MineOwned"] = 0;
    this["Gas ExtractorOwned"] = 0;
    this["Science LabOwned"] = 0;
    this["Ship YardOwned"] = 0;
    this["Space PortOwned"] = 0;
    
    // Actions
    this.beingColonized = false;
    this.attackTimers = [];

    // Randomize applicable properties.
    if (config.randomize)
        this.randomize();
};

Planet.prototype.randomize = function () {
    var climates = ['Temperate', 'Icey', 'Rainforest', 'Rocky'];
    var sizes = ['Large', 'Small', 'Goliath', 'Bohemith', 'Tiny', 'Micro'];
    var defenses = [8, 8, 8, 8, 8, 8];

    var index = Math.round(Math.random() * (climates.length - 1));
    var size_index = Math.round(Math.random() * (sizes.length - 1));

    // Randomize the properties.
    this.climate = climates[index];
    this.size = sizes[size_index];
    this.defense = defenses[size_index];
    this.name = NameHelper.generatePlanetName(this.sector);

    var adjSize = this.size.toLowerCase();
    
    switch (this.type) {
        case 'planet-1':
            this.description = "An aquatic world parimarily covered with a single, giant ocean spanning the entirety of this " + adjSize + " planet.";
            break;
        case 'planet-2':
            this.description = "A rocky, hilly world with jutting mountains that create many dips and peaks."
            break;
        case 'planet-3':
            this.description = "A very habitable world with large oceans interrupting equally magnificient land masses.";
            break;
        case 'planet-6':
            this.description = "A gasseous planet that seems to be made of many different elements. Who knows what mystical creatures may be surviving on this " + adjSize + " world.";
            break;
        case 'planet-7':
            this.description = "A gasseous planet that seems to be made of many different elements. Who knows what mystical creatures may be surviving on this " + adjSize + " world.";
            break;
        case 'planet-8':
            this.description = "A gasseous planet that seems to be made of many different elements. Who knows what mystical creatures may be surviving on this " + adjSize + " world.";
            break;
        case 'planet_silver':
            this.description = "A mysterious planet defined by the oddly silver tint. Almost as if the entire world was put into stasis.";
            break;
        case 'planet-sylmar':
            this.description = "The sylmar have left this once beautiful planet a husk of its former self.";
            break;
        default:
            this.description = "A common world.";
            break;
    }
};

module.exports = Planet;