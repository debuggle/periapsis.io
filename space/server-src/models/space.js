﻿Space = function (config) {
    config = config || {};
    this.x = config.x || 0;
    this.y = config.y || 0;
    this.planet = false || config.planet;
    this.type = config.type || 0;
    this.infected = false || config.infected;
};

module.exports = Space;