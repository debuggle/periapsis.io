﻿Log = function (title, category, text, critical) {
    this.title = title;
    this.category = category;
    this.text = text;
    this.critical = false || critical;
    this.date = Date.now();
};

module.exports = Log;