﻿Timer = function (start, duration, action_name) {
    this.start = start;
    this.duration = duration;
    this.action_name = action_name;
};

module.exports = Timer;