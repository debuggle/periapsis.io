﻿var uuid = require('node-uuid');

var Fleet = function (config) {
    config = config || {};
    this.user = config.user || '';
    this.displayUser = config.displayUser || config.user || '';
    this.title = uuid.v1();
    this.displayTitle = config.title || '';
    this.guid = uuid.v1();
    
    // These are NOT sector coordinates. These are FLEET coordinates
    // optimized for rendering on a game map.
    this.x = config.x || 0;
    this.y = config.y || 0;
    this.dx = config.dx || 0; // Destination X
    this.dy = config.dy || 0; // Destination Y
    this.angle = config.angle || 0;
    
    this.sector = config.sector || 0;
    this.thrustStart = 0;
    this.thrustEnd = 0;

    // The ship list.
    this.ships = [];

    // Formation.
    this.formation = '';
    this.miners = 0;
    this.planetX = config.planetX;
    this.planetY = config.planetY;
    this.ai = config.ai || false;
    this.moved = false;
};

module.exports = Fleet;