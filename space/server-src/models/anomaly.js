﻿var uuid = require('node-uuid'),
    NameHelper = require('../nameHelper.js');
var Anomaly = function (config) {
    this.type = 'anomaly';
    this.x = config.x || 0;
    this.y = config.y || 0;
    this.sector = config.sector || 133.7;
    this.science = config.science || 500;
    this.stabilizeAction = [];
    this.protectAction = [];
    
    // Generate a GUID to refer to (for ship placement purposes).
    this.guid = uuid.v1();

    // Calculate the decay time.
    this.decay = Date.now() + ((config.decay || 0) * 1000);
    
    // Create some bs properties to make it look interesting.
    this.properties = {
        size: 0,
        ionization: 0,
        magnetization: 0,
        temperature: 0,
        pressure: 0
    };

    // Randomize the properties.
    this.randomize();
};

Anomaly.prototype.randomize = function () {
    for (var prop in this.properties) {
        this.properties[prop] = Math.round(Math.random() * 100000);
    }

    // Select a description.
    var descriptions = [
        'This temporal disturbance seems to indicate correlations between space and time previously misunderstood.',
        'This temporal disturbance might provide some scientific insight we were not aware of.',
        'This black hole definitely helps our best and brightest comprehend the secrets of event horizons.',
        'This black hole has sparked a bunch of theories in the scientific community.',
        'This Asteroid field does not seem to follow the laws of gravity.',
        'This Asteroid field has given our scientists new theories about how the game asteroids should work.',
        'This wrecked science station appears to have been working on something big. If only we could resume their research.',
        'This wrecked space station might have been attacked by giants bugs. We are not sure.',
        'This interpolated hydrogen cloud might help us understand the meaning of life, the universe, and all things.',
        'This bioluminscent space entity has inspired our scientists on how to make better sci-fi shows.',
        'This inverted magnetic field is disrupting our sense of direction. Maybe it will help us build a better compass.'
    ];
    
    this.title = NameHelper.generateAnomalyName(this.sector);
    this.description = descriptions[Math.round(Math.random() * (descriptions.length - 1))];
};

module.exports = Anomaly;