﻿var uuid = require('node-uuid');

var Asteroid = function (config) {
    config = config || {};
    
    this.type = 'asteroid';
    this.layer = config.layer;
    this.structures = [];
    this.ships = [];
    this.protector = '';
    this.x = config.x || 0;
    this.y = config.y || 0;
    this.guid = uuid.v1();
    this.density = 'normal';

    // Determine the modifiers.
    this.goldModifier = 0.5 + Math.random();
    this.ironModifier = 0.5 + Math.random();
    this.h3Modifier = 0.5 + Math.random();
    
    // Add up the modifiers.
    var density = this.goldModifier + this.ironModifier + this.h3Modifier;
    
    // 1.5 -> 4.5
    if (density > 3)
        this.density = 'rich';
    else if (density > 2)
        this.density = 'normal';
    else
        this.density = 'sparse';

    if (this.goldModifier > this.ironModifier && this.goldModifier > this.h3Modifier) {
        this.resource = "gold";
    } else if (this.ironModifier > this.goldModifier && this.ironModifier > this.h3Modifier) {
        this.resource = "iron";
    } else {
        this.resource = "h3";
    }

    // Determine the mass.
    this.mass = 250 + (1500 * Math.random());
}

module.exports = Asteroid;