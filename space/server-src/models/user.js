﻿var uuid = require('node-uuid');
User = function (config) {
    config = config || {};
    this.difficulty = config.difficulty || 1;
    this.username = config.username;
    this.password = config.password;
    this.x = config.x || 0;
    this.y = config.y || 0;
    this.science = config.science || 1000;
    this.gold = config.gold || 3500;
    this.iron = config.iron || 3500;
    this.h3 = config.h3 || 3500;
    this.credits = config.credits || 50;
    this.docks = [];
    this.planets = config.planets || [];
    this.relocated = false || config.relocated;
    
    // Used to invalidate past orders.
    this.generation = uuid.v1();

    this.sector = {};
    this.explored = {};

    // Meta properties.
    this.tag = '';
    this.email = config.email || '';
    this.asteroidsMined = 0;
    this.anomaliesStabilized = 0;
    this.score = 0;

    // Alignments
    this.alignment_Sylmar = -100;
    this.alignment_Pirates = -10;
    this.alignment_Mythos = 0;
    
    // Check if we're AI.
    this.AI = (config.username == "Pirates" || config.username == "Mythos" || config.username == "Sylmar");

    // Wars.
    if (!this.AI)
        this.protection = Date.now() + (60 * 60 * 24 * 1 * 1000);
    else
        this.protection = 0;
    
    if (config.username == "Sylmar") {
        this.gold += 30000;
        this.iron += 30000;
        this.h3 += 30000;
    }

    this.wars = [];
    this.guild = config.guild || '';
    
    // Relocation buffs.
    if (config.relocated) {
        this.gold += 1000;
        this.iron += 1000;
        this.h3 += 1000;
        this.science += 500;
    }
};

module.exports = User;