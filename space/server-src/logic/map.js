﻿var space = require('../models/space'),
    Planet = require('../models/planet'),
    stream = require('stream'),
    redis = require('../redis-layer.js');

var quadrant_size = 10;
var probability = {
    "planet-1": 2, // Planet 1
    "planet-2": 1, // Planet 3
    "planet-3": 3, // Planet 4
    "planet-6": 0.5,
    "planet-7": 0.5,
    "planet-8": 0.25,
    //"planet_silver": 0.15,
    "space": 60 // Open Space
};

function getQuadrant(x, y) {
    var result = (Math.trunc(x / quadrant_size));
    result += (Math.trunc(y / quadrant_size) / 1000);
    result = Math.trunc(result * 1000);
    result = result / 1000;
    return result;
}

function normalize(sector) {
    var x = Math.trunc(sector);
    var y = Math.trunc(Math.round((sector - x) * 1000));
    return [x, y];
}

function getNormalizedSector(x, y) {
    return normalize(getQuadrant(x, y));
}

function getType() {
    var max = 0, target = 0, current = 0;
    for (var prop in probability) {
        max += probability[prop];
    }
    
    target = Math.random() * max;
    for (var prop in probability) {
        current += probability[prop];
        if (current >= target)
            return prop;
    }

    return "space";
}

function populateMap(quadrant, planets) {
    var map = {};
    for (var prop in planets) {
        map[prop] = planets[prop];
    }

    var n = normalize(quadrant);

    for (var y = 0; y < quadrant_size; y++) {
        for (var x = 0; x < quadrant_size; x++) {
            var index = ((n[1] * quadrant_size) + y) + '-' + ((n[0] * quadrant_size) + x);
            if (planets.hasOwnProperty(index)) {
                map[index] = planets[index];
            } else {
                map[index] = { type: 'space', x: (n[0] * quadrant_size) + x, y: (n[1] * quadrant_size) + y };
            }
        }
    }
    return map;
};

Map = function (gameContext) {
    // Load initial settings.
    this.gameContext = gameContext;
    this.db0 = gameContext.db0;
    this.lock = gameContext.lock;
    this.biggestQuadrant = 0;
    this.loadQuadrant();
};

Map.prototype.normalizeSector = function (sector) {
    return normalize(sector);
}

Map.prototype.recordQuadrant = function (sector, success) {
    var self = this;
    self.db0.create('map_furthest', { sector: sector }, success);    
};

Map.prototype.loadQuadrant = function (success) {
    var self = this;
    this.db0.retrieve('map_furthest', function (obj) {
        if ( success ) success.call(self, obj.sector || 133.700);
    }, function () {
        if ( success ) success.call(self, 133.700);
    });
}

Map.prototype.scanSector = function (quadrant, success) {
    var map = this;
    // See if this quadrant has been generated before.
    this.db0.retrieve("quadrant_" + quadrant, function (obj) {
        // Obj = the map.
        if (success) {
            success.call(this, populateMap(quadrant, obj));
        }
    }, function () {
        // Need to generate the quadrant.
        var x = normalize(quadrant)[0] * quadrant_size;
        var y = normalize(quadrant)[1] * quadrant_size;
        map.generate(x, y, function (result) {
            if (success)
                success.call(this, populateMap(quadrant, result)); 
        });
    });
}

Map.prototype.scan = function (x, y, success) {
    // Determine the quadrant
    var quadrant = getQuadrant(x, y);
    this.scanSector(quadrant, success);
};

Map.prototype.getQuadrantSize = function () {
    return quadrant_size;
};

Map.prototype.generateAsteroidField = function (x, y) {
    var map = {};
    var quadrant = getQuadrant(x, y);
    var self = this;
    
    var sector = getNormalizedSector(x, y);
    var qx = sector[0];
    var qy = sector[1];
    var count = 8 + Math.round(Math.random() * 12);
    
    // Add a single pirate planet to the map.
    var px = ((qx * quadrant_size) + 5);
    var py = ((qy * quadrant_size) + 5);
    var planet = new Planet({ x: px, y: py, type: "planet-infected", layer: 0, sector: quadrant, randomize: true });
    planet.owner = "Pirates";
    planet.type = "planet-infected";
    
    // Add it to the map.
    map[py + "-" + px] = planet;
    
    // Save the map.
    self.db0.create("quadrant_" + quadrant, populateMap(quadrant, map), function () {
        // Don't generate any planets, just generate asteroids.
        self.gameContext.QueueManager.enqueue('sectorHelper', { x: x, y: y, asteroidCount: count }, 1);
    });
    
    // Return the map.
    return map;
};

Map.prototype.generate = function (x, y, callback) {
    var map = {};
    var quadrant = getQuadrant(x, y);
    
    var planets = [];
    var hasPirates = false;
    var self = this;
    var gameContext = this.gameContext;

    var sector = getNormalizedSector(x, y);
    var qx = sector[0];
    var qy = sector[1];
    var l = 0;

    for (var spy = 1; spy < quadrant_size - 1; spy++) {
        for (var spx = 1; spx < quadrant_size - 1; spx++) {

            var px = ((qx * quadrant_size) + spx);
            var py = ((qy * quadrant_size) + spy);
            var index = py + '-' + px;
            var type = getType();
            if (type == 'space') continue;

            // Create the planet.
            var planet = new Planet({ x: px, y: py, type: type, layer: l++, sector: quadrant, randomize: true });
            if (planets.length < 5) {
                planets.push(index);
                map[index] = planet;
            }
        }
    }
    
    if (planets.length < 4) {
        // Generate again! This is an unacceptable sector.
        return this.generate(x, y, callback);
    }
    

    // If there were no pirates, then find a planet randomly and assign pirates there.
    if (!hasPirates) {
        var pIndex = planets[0];
        
        // Don't allocate a pirate on a planet that already has something. Somehow.
        if (map[pIndex].owner != undefined)
            return this.generate(x, y, callback);

        map[pIndex].owner = 'Pirates';
        map[pIndex].type = 'planet-infected';
    }
    
    // Store this quadrant.
    this.db0.create("quadrant_" + quadrant, populateMap(quadrant, map), function () {
        self.gameContext.QueueManager.enqueue('sectorHelper', { x: x, y: y, asteroidCount: 3 }, 1);
        self.gameContext.QueueManager.enqueue('cataclysmicEvent', { x: x, y: y }, self.gameContext.getSettings(1).cataclysmicEvent);
        if (callback) callback.call(this, map);
    });
    return map;
};

Map.prototype.getUsersInQuadrant = function (x, y, success) {
    this.scan(x, y, function (map) {
        var users = [];
        for (var prop in map) {
            if (map[prop].owner || map[prop].protector) {
                users.push(map[prop].owner || map[prop].protector);
            }
        }
        if (success) success.call(this, users);        
    });
};

Map.prototype.sectorAvailable = function (x, y, callback) {
    this.scan(x, y, function (map) {
        var res = true;
        for (var prop in map) {
            if ((map[prop].owner 
                && map[prop].owner !== "Pirates")
                 || map[prop].beingColonized == true) {
                res = false;
            }
        }
        if (callback) callback.call(this, res, true);
    });
};

Map.prototype.nextSpiral = function (sector, success, failure) {
    var self = this;
    var normalized = normalize(sector);
    var x = quadrant_size * normalized[0], y = quadrant_size * normalized[1];
    
    // The step determines what padding we give for random anomalous sectors.
    var step = 2;

    // Check which directions we can go.
    self.sectorAvailable(x, y, function (canStay, sHome) {
        self.sectorAvailable(x + quadrant_size * step, y, function (canGoRight, grHome) {
            self.sectorAvailable(x - quadrant_size * step, y, function (canGoLeft, glHome) {
                self.sectorAvailable(x, y - quadrant_size * step, function (canGoUp, guHome) {
                    self.sectorAvailable(x, y + quadrant_size * step, function (canGoDown, gdHome) {
                        var returnValue;
                        
                        if (canStay) returnValue = [x, y, sHome];
                        else if (canGoUp && canGoDown && canGoRight) returnValue = [x, y - quadrant_size * step, guHome];
                        else if (canGoLeft && !canGoDown) returnValue = [x - quadrant_size * step, y, glHome];
                        else if (canGoDown && !canGoRight) returnValue = [x, y + quadrant_size * step, gdHome];
                        else if (canGoUp && canGoRight) returnValue = [x, y - quadrant_size * step, guHome];
                        else if (canGoRight) returnValue = [x + quadrant_size * step, y, grHome];
                        else {
                            if (failure) failure.call();
                            return;
                        }
                        
                        if (returnValue) {
                            self.recordQuadrant(self.getQuadrant(returnValue[0], returnValue[1]), function () {
                                if (success) success.call(this, returnValue, returnValue[2]);                                                         
                            });
                        }
                    });
                });
            });
        });
    });
};

Map.prototype.getNextPlanet = function (success, failure, customQ, error) {
    var self = this;
    // If error is at 100, well, we've got a problem.
    if ((error || 0) >= 100) {
        if (failure) failure.call();
    }
    else
        this.loadQuadrant(function (lastQ) {
            var sector = parseFloat(customQ || lastQ || 133.700);
            self.nextSpiral(sector, function (pos, home) {
                // Available!
                // Now generate the actual sector.
                self.scan(pos[0], pos[1], function (map) {
                    var planets = [];
                    var error = false;

                    for (var prop in map) {
                        if (map[prop].owner !== undefined && map[prop].owner !== "Pirates") error = true;
                        if (map[prop].isPlanet 
                            && map[prop].owner !== "Pirates" 
                            && map[prop].type !== "planet_silver" 
                            && !map[prop].beingColonized)
                            planets.push(map[prop]);
                    }
                    
                    if (planets.length == 0 || error) {
                        var n = normalize(sector);
                        var x = quadrant_size * n[0], y = quadrant_size * n[1];
                        y -= quadrant_size * 2;
                        self.getNextPlanet(success, failure, self.getQuadrant(x, y), (error || 0) + 1);
                        return;
                    }
                    
                    // Generate asteroid fields around this position.
                    self.generateAsteroidField(pos[0] - quadrant_size, pos[1]);
                    self.generateAsteroidField(pos[0] + quadrant_size, pos[1]);
                    self.generateAsteroidField(pos[0], pos[1] - quadrant_size);
                    self.generateAsteroidField(pos[0], pos[1] + quadrant_size);
                    
                    // Good to go.
                    var index = Math.round(Math.random() * (planets.length - 1));
                    if (success) success.call(this, planets[index]);
                });
            }, function () {
                // Not available. So let's just go up and try again.
                var n = normalize(sector);
                var x = quadrant_size * n[0], y = quadrant_size * n[1];
                y -= quadrant_size * 2;
                self.getNextPlanet(success, failure, self.getQuadrant(x, y), (error || 0) + 1);
                return;
            });
        });
};

Map.prototype.changeOwner = function (x, y, owner, success) {
    var gameContext = this.gameContext;
    var self = this;
    this.getInfo(x, y, function (planet) {
        // Check if this planet has an owner.
        if (planet.owner) {
            
            // Remove any timers the player may have had.
            var remove = [];
            if (planet.productionBoost !== undefined) remove.push(planet.productionBoost);
            
            // Iterate over the remove list and do so.
            gameContext.db0.getList("user_" + planet.owner + "_timers", function (timers) {
                for (var i = 0; i < remove.length; i++) {
                    for (var r = 0; r < timers.length; r++) {
                        if (timers[r] == remove[i]) {
                            // Add it to the conquering player.
                            gameContext.db0.addList("user_" + owner + "_timers", timers[r]);
                            // Remove it from the owner.
                            gameContext.db0.remAt("user_" + planet.owner + "_timers", r--);
                            break;
                        }
                    }
                }
            });

            gameContext.UserManager.getUser(planet.owner, function (user) {
                user.planets = user.planets || [];
                user.docks = user.docks || [];
                for (var i = 0; i < user.planets.length; i++) {
                    if (user.planets[i][0] == planet.x && user.planets[i][1] == planet.y) {
                        user.planets.splice(i, 1);
                        break;
                    }
                }
                
                // Also update docks, if applicable.
                for (var i = 0; i < user.docks.length; i++) {
                    if (user.docks[i].x == planet.x && user.docks[i].y == planet.y) {
                        user.docks.splice(i, 1);
                        break;
                    }
                }

                gameContext.UserManager.putValue(planet.owner || 'Pirates', "planets", user.planets);
            });
        }
        
        gameContext.UserManager.getUser(owner, function (objUser) {
            if (objUser) {
                objUser.planets = objUser.planets || [];
                objUser.planets.push([x, y]);
                objUser.docks = objUser.docks || [];

                // Check if the planet has any docks.
                if (parseFloat(planet["Ship YardOwned"] || 0) >= 1) {
                    var has = false;
                    for (var i = 0; i < objUser.docks.length; i++) {
                        if (objUser.docks[i].x == x && objUser.docks[i].y == y) {
                            has = true;
                            break;
                        }
                    }
                    if (!has) {
                        objUser.docks.push({
                            x: planet.x,
                            y: planet.y,
                            name: planet.name
                        });
                    }
                }
                
                gameContext.UserManager.putValue(owner, "docks", objUser.docks, function () {
                    gameContext.UserManager.putValue(owner || 'Pirates', "planets", objUser.planets, function () {
                        gameContext.Map.updateProperty(x, y, "owner", owner, success, success);                           
                    });         
                });
            }
        }, function () {
            gameContext.Map.updateProperty(x, y, "owner", owner, success, success);
        });
    });
};

Map.prototype.setInfo = function (x, y, planet, callback) {
    var quadrant = getQuadrant(x, y);
    var index = y + '-' + x;
    var self = this;
    self.db0.put("quadrant_" + quadrant, index, planet, callback);
};

// This will push a new value to a list data type on a planet
// in an atomic way.
Map.prototype.pushProperty = function (x, y, property, value, success, failure) {
    var sector = getQuadrant(x, y);
    var index = y + '-' + x;
    var self = this;
    self.db0.retrieve("quadrant_" + sector, function (map) {
        if (map.hasOwnProperty(index)) {
            map[index][property] = map[index][property] || [];
            map[index][property].push(value);
            self.db0.create("quadrant_" + sector, map, function () {
                if (success) success.call();
            });
        } else {
            if (failure) failure.call();
        }
    }, function () {
        if (failure) failure.call();   
    });
};

// Wrap map things in a transaction, essentially.
Map.prototype.updateProperty = function (x, y, property, value, success, failure) {
    var sector = getQuadrant(x, y);
    var index = y + "-" + x;
    var self = this;
    self.db0.retrieve("quadrant_" + sector, function (map) {
        if (map.hasOwnProperty(index)) {
            map[index][property] = value;
            self.db0.create("quadrant_" + sector, map, function () {
                if (success) success.call();
            }, function () {
                if (failure) failure.call();
            });
        } else {
            if (failure) failure.call();
        }
    }, function () {
        if (failure) failure.call();
    });
};

Map.prototype.addAttackTimer = function (x, y, uid) {
    var self = this;
    this.pushProperty(x, y, "attackTimers", uid);
}

Map.prototype.getQuadrant = function (x, y) {
    return getQuadrant(x, y);
};

Map.prototype.getInfo = function (x, y, success) {
    var quadrant = getQuadrant(x, y);
    var map = this;
    
    // Retrieve the quadrant.
    this.scan(x, y, function (map) {
        var index = y + '-' + x;
        if (map[index]) {
            success.call(this, map[index]);
        }
    });
};

module.exports = Map;