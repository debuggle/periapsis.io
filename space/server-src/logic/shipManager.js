﻿var Ship = require('../models/ship'),
    redis = require('../redis-layer'),
    copy = require('deep-copy');

ShipManager = function (gameContext) {
    this.ships = [];
    this.gameContext = gameContext;
};

ShipManager.prototype.find = function (name) {
    for (var i = 0; i < this.ships.length; i++) {
        if (this.ships[i].name == name) {
            return this.ships[i];
        }
    }
    return null;
};

ShipManager.prototype.findCost = function (name) {
    var ship = this.find(name);
    if (ship) {
        return ship.cost;
    }
    return 0;
};

ShipManager.prototype.getBuildTime = function (user, shipName, qty, x, y, ctx, callback) {
    var gameContext = this.gameContext;
    gameContext.Map.getInfo(x, y, function (planet) {
        var shipYardLevel = parseFloat(planet["Ship YardOwned"] || 1);
        gameContext.db0.getList("user_" + user + "_ships_order", function (orders) {
            // Iterate the list, looking for past orders that are still active.
            // [0] = shipName, [1] = shipQty, [2] = endTime
            var qtyInProduction = 0;
            for (var i = 0; i < orders.length; i++) {
                if (orders[i][0] == shipName) {
                    if (parseFloat(orders[i][2] || 0) > gameContext.now()) {
                        // Determine how many have been "produced" already to 
                        // essentially prorate the build time.
                        var orderQty = parseFloat(orders[i][1]);
                        var start = parseFloat(orders[i][3]);
                        var end = parseFloat(orders[i][2]);
                        var now = gameContext.now();
                        
                        // how many ships have been produced?
                        var shipIteration = (end - start) / orderQty;
                        var iterations = (end - now);
                        var stillQueued = Math.round(iterations / shipIteration);
                        qtyInProduction += stillQueued;
                    } else {
                        orders.splice(i, 1);
                        gameContext.db0.remAt("user_" + user + "_ships_order", i--);
                    }
                }
            }
            
            // Add the order to the list.
            var gShip = gameContext.ShipManager.find(shipName);
            if (gShip) {
                var buildTime = gameContext.getSettings(1).shipConstructionTime * gShip.timeMultiplier * (qtyInProduction + qty);
                
                // Ship Yard Level bonus.
                buildTime = Math.round(buildTime / Math.max(shipYardLevel, 1));

                var uid = gameContext.TimerManager.spawn('fabricate', user, buildTime, "Fabricating " + qty + "x " + shipName, "yellow");
                var end = gameContext.TimerManager.find(uid).end;
                
                gameContext.db0.addList("user_" + user + "_ships_order", [shipName, qty, end, gameContext.now()], function () {
                    if (callback) callback.call(this, buildTime, ctx);
                });
            } else {
                if (callback) callback.call(this, 0, ctx);
            }
        });
    });
};

ShipManager.prototype.getAll = function (user, callback) {
    var self = this;
    var ships = [];
    self.ships.forEach(function (item) {
        if (item.pirate) return;
        if (item.hidden) return;
        var ship = copy(item);
        ships.push(ship);
    });
    if (callback) callback.call(this, ships);

};


ShipManager.prototype.allocateNewShip = function (shipName, qty, x, y, user, callback) {
    var gameContext = this.gameContext;
    var db0 = gameContext.db0;
    var key = y + '-' + x;

    db0.retrieve("user_" + user + "_vessels", function (objShips) {
        objShips[key] = objShips[key] || {};
        objShips[key][shipName] = objShips[key][shipName] || 0;
        objShips[key][shipName] += parseFloat(qty);
        db0.update("user_" + user + "_vessels", objShips, function () {
            gameContext.SocketManager.updateShips(user);
            if ( callback ) callback.call();   
        });
    }, function () {
        var objShips = {};
        objShips[key] = {};
        objShips[key][shipName] = qty;
        db0.create("user_" + user + "_vessels", objShips, function () {
            gameContext.SocketManager.updateShips(user);
            if (callback) callback.call();
        });
    });
};

// This removes the ship from the vessels list because it's being assigned elsewhere.
ShipManager.prototype.assignShip = function (shipName, qty, x, y, user, callback) {
    var gameContext = this.gameContext;
    var db0 = gameContext.db0;
    var key = y + '-' + x;
    
    gameContext.lock("user_" + user + "_vessels", 2500, function (done) {
        db0.retrieve("user_" + user + "_vessels", function (objShips) {
            objShips[key] = objShips[key] || {};
            objShips[key][shipName] = parseFloat(objShips[key][shipName]) || 0;
            objShips[key][shipName] = Math.max(objShips[key][shipName] - qty, 0);
            db0.update("user_" + user + "_vessels", objShips, function () {
                done();
                gameContext.SocketManager.updateShips(user);
                if (callback) callback.call();
            }, function () {
                done();           
            });
        }, function () {
            done();
            if (callback) callback.call();
        }); 
    });
};

ShipManager.prototype.getUserDeployedShips = function (user, callback) {
    // DEPRECATE THIS
};

ShipManager.prototype.getUserInventory = function (user, callback) {
    var gameContext = this.gameContext;
    var db0 = gameContext.db0;
        
    db0.retrieve("user_" + user + "_vessels", function (objShips) {
        if (callback) callback.call(this, objShips);
    }, function () {
        if (callback) callback.call(this, {});
    });
};

ShipManager.prototype.getUserShips = function(user, x, y, callback) {
    var gameContext = this.gameContext;
    var db0 = gameContext.db0;
    var key = y + '-' + x;

    db0.retrieve("user_" + user + "_vessels", function (objShips) {
        objShips[key] = objShips[key] || {};
        if (callback) callback.call(this, objShips[key]);
    }, function () {
        if (callback) callback.call(this, {});
    });
}


ShipManager.prototype.killShip = function (shipName, deployed, mission, user, callback, qty) {
    // DEPRECATE THIS

};

// action - attack / return
// ships -
//   array with { nam, qty }
// x - target x of temporary destination
// y - target y of temporary destination
ShipManager.prototype.deployFleet = function (action, mission, ships, x, y, user, success) {
    // DEPRECATE THIS
};

ShipManager.prototype.getCombatReport = function (user1Ships, user2Ships, shields, defenseOwned) {
    var gameContext = this.gameContext;
    var u1s = 0, u1d = 0, u2s = 0, u2d = 0;
    user1Ships = user1Ships || [];
    user2Ships = user2Ships || [];
    for (var i = 0; i < user1Ships.length; i++) {
        if (user1Ships[i] !== undefined && user1Ships[i] !== null) {
            var s = gameContext.ShipManager.find(user1Ships[i][0]);
            if (s) {
                u1s += (s.attack) * parseFloat(user1Ships[i][1]);
                u1d += (s.defense) * parseFloat(user1Ships[i][1]);
            }
        }
    }

    for (var i = 0; i < user2Ships.length; i++) {
        if (user2Ships[i] !== undefined && user2Ships[i] !== null) {
            var s = gameContext.ShipManager.find(user2Ships[i][0]);
            if (s) {
                u2s += (s.attack) * parseFloat(user2Ships[i][1]);
                u2d += (s.defense) * parseFloat(user2Ships[i][1]);
            }
        }
    }
    
    // Add any planetary defenses.
    if (shields && defenseOwned) {
        var pAttack = 250 + Math.pow(128, defenseOwned);
        var pDefense = (250 + Math.pow(128, defenseOwned)) * (shields / 100);
        // Multiply by 10 because that's how many times it goes in a single round.
        // And I think it produces more accurate results.
        pDefense *= 10; 
        u2d += pDefense;
    }
    
    // User 1 attack vs User 2 defense.
    return Math.round(((u1s / u2d) * 100) * 100) / 100;
};

// Helper function
// A fleet will be an object that contains a property, ships.
// array of [shipName, qty]
function flatten(gameContext, list) {
    var result = [];
    for (var i = 0; i < list.length; i++) {
        for (var r = 0; r < parseFloat(list[i][1]); r++) {
            var g_ship = gameContext.ShipManager.find(list[i][0]);
            if (g_ship) result.push(copy(g_ship));
        }
    }
    return result;
}

function d(val) {
    return 1 + Math.round(Math.random() * parseFloat(val));
}

ShipManager.prototype.combatPlanet = function (x, y, user, fleet, success) {
    // See if the fleet is capable of defeating the planetary defenses.
    var gameContext = this.gameContext;
    gameContext.Map.getInfo(x, y, function (planet) {
        // Check out the orbital defenses.
        var orbitalLevel = parseFloat(planet["Suborbital ArrayOwned"] || 0) + 1;
        var attack = 250 + Math.pow(128, orbitalLevel);
        var maxdefense = 250 + Math.pow(128, orbitalLevel);
        var defense = maxdefense;
        var shields = parseFloat(planet.shields);
        var maxhp = Math.round(10000 + defense);
        var hp = maxhp;

        // Iterate over the fleet.
        gameContext.FleetManager.getFleet(user, fleet, function (objFleet) {
            var flat = flatten(gameContext, objFleet.ships || []);
            do {
                // Recalculate defense.
                defense = (maxdefense * (shields / 100));

                // Iterate over each ship.
                for (var i = 0; i < flat.length; i++) {
                    var ship = flat[i];
                    if (d(ship.attack) > d(defense)) {
                        hp -= (d(ship.attack) + d(ship.attack) + d(ship.attack));
                    }
                }

                // Now let the planet takes 5 volleys.
                for (var i = 0; i < 5; i++) {
                    // Randomly select a ship.
                    var index = Math.round(Math.random() * (flat.length - 1));
                    var target = flat[index];
                    if (d(attack) > d(target.defense)) {
                        target.hp -= (d(attack) + d(attack) + d(attack));
                    }
                }
                
                // Calculate shields.
                shields = Math.round((hp / maxhp) * 100) / 100;

                // Check for ship deaths.
                for (var i = 0; i < flat.length; i++) {
                    if (flat[i].hp <= 0) {
                        flat.splice(i--, 1);
                    }
                }
            } while (flat.length > 0 && hp > 0);
            
            // Reup the shields.
            shields = Math.max((Math.round(shields * 100 * 100) / 100), 100);

            // Determine the outcome.
            if (flat.length == 0) {
                // The planet won.
                gameContext.FleetManager.disband(user, fleet, function () {
                    // Update the planet.
                    gameContext.Map.updateProperty(x, y, "shields", shields, function () {
                        gameContext.SocketManager.updateMap(user);
                        if (success) success.call(this, false, shields);
                    });
                });
            } else if (hp <= 0) {
                // The fleet won.
                gameContext.Map.updateProperty(x, y, "shields", shields, function () {
                    gameContext.Map.changeOwner(x, y, user, function () {
                        gameContext.SocketManager.updateMap(user);
                        if (success) success.call(this, true);
                    });
                });
            }

        });

    });
};

// ships = [[shipName, qty]]
// user1Fleets / user2Fleets = an array of fleet names.
ShipManager.prototype.combat = function (sector, user1, user1Fleet, user2, user2Fleet, success) {
    var gameContext = this.gameContext;
    
    if (user1 == user2) {
        // Invalid condition.
        if (success) success.call();
        return;
    }

    // Get the fleets.
    gameContext.FleetManager.getFleet(user1, user1Fleet, function (objUser1Fleet) {
        gameContext.FleetManager.getFleet(user2, user2Fleet, function (objUser2Fleet) {

            function rebuild(list) {
                var obj = {};
                var result = [];
                for (var i = 0; i < list.length; i++) {
                    obj[list[i].name] = (obj[list[i].name] || 0) + 1;
                }

                // Iterate over the object.
                for (var prop in obj) {
                    result.push([prop, obj[prop]]);
                }

                // Return the result.
                return result;
            };

            // Flatten the list of ships.
            var fleet1Flat = flatten(gameContext, objUser1Fleet.ships);
            var fleet2Flat = flatten(gameContext, objUser2Fleet.ships);
            var fleet1Destroyed = 0;
            var fleet2Destroyed = 0;
            var rounds = 15;

            do {
                // Attacking fleet will do just that. Attack.
                for (var i = 0; i < fleet1Flat.length; i++) {
                    var target = Math.round(Math.random() * (fleet2Flat.length - 1));
                    var ship1 = fleet1Flat[i];
                    var ship2 = fleet2Flat[0];
                    
                    // Roll against it.
                    if (d(ship1.attack) > d(ship2.defense)) {
                        ship2.hp -= (d(ship1.attack) + d(ship1.attack) + d(ship1.attack));
                    }
                }
                
                // Defending fleet gets a round.
                for (var i = 0; i < fleet2Flat.length; i++) {
                    var target = Math.round(Math.random() * (fleet1Flat.length - 1));
                    var ship1 = fleet2Flat[i];
                    var ship2 = fleet1Flat[0];

                    if (d(ship1.attack) > d(ship2.defense)) {
                        ship2.hp -= (d(ship1.attack) + d(ship1.attack) + d(ship1.attack));
                    }
                }

                // Remove the dead ships.
                for (var i = 0; i < fleet1Flat.length; i++) {
                    if (fleet1Flat[i].hp <= 0) {
                        fleet1Destroyed++;
                        fleet1Flat.splice(i--, 1);
                    }
                }

                for (var i = 0; i < fleet2Flat.length; i++) {
                    if (fleet2Flat[i].hp <= 0) {
                        fleet2Destroyed++;
                        fleet2Flat.splice(i--, 1);
                    }
                }

            } while (fleet1Flat.length > 0 && fleet2Flat.length > 0);// && rounds-- > 0);
            
            var user1DisplayName = objUser1Fleet.displayUser || user1;
            var user2DisplayName = objUser2Fleet.displayUser || user2;
            
            // Now we have the survivors.
            if (fleet1Flat.length <= 0) {
                user1Destroyed = true;
                gameContext.FleetManager.disband(user1, user1Fleet, function () {
                    gameContext.UserManager.addNotification(user1DisplayName, "red", "You have lost the '" + objUser1Fleet.displayTitle + "' fleet fighting " + user2DisplayName + " in sector " + sector + ".");
                    gameContext.SocketManager.updateNotifications(user1DisplayName);
                });
            } else {
                if (fleet1Destroyed > 0) {
                    gameContext.UserManager.addNotification(user1DisplayName, "red", "You have lost " + fleet1Destroyed + " ships from the fleet '" + objUser1Fleet.displayTitle + "' during the attack against " + user2DisplayName + " in sector " + sector + ".");
                } else {
                    gameContext.UserManager.addNotification(user1DisplayName, "red", "You did not lose any ships during the attack against " + user2DisplayName + " in sector " + sector + ".");
                }
                // Otherwise we need to update the ships.
                objUser1Fleet.ships = rebuild(fleet1Flat);
            }
            
            if (fleet2Flat.length <= 0) {
                user2Destroyed = true;
                gameContext.FleetManager.disband(user2, user2Fleet, function () {
                    gameContext.UserManager.addNotification(user2DisplayName, "red", "You have lost the '" + objUser2Fleet.displayTitle + "' fleet fighting " + user1DisplayName + " in sector " + sector + ".");
                    gameContext.SocketManager.updateNotifications(user2DisplayName);
                });
            } else {
                if (fleet2Destroyed > 0) {
                    gameContext.UserManager.addNotification(user2DisplayName, "red", "You have lost " + fleet2Destroyed + " ships from the fleet '" + objUser2Fleet.displayTitle + "' during the attack against " + user1DisplayName + " in sector " + sector + ".");
                } else {
                    gameContext.UserManager.addNotification(user2DisplayName, "red", "You did not lose any ships during the attack against " + user1DisplayName  + " in sector " + sector + ".");
                }
                // We need to update the ships in the fleet.
                objUser2Fleet.ships = rebuild(fleet2Flat);
            }
            
            // Figure out who the winner is.
            var winnerUser = '', winnerFleet = '', winnerFleetObj;
            if (fleet2Flat.length > 0) {
                winnerUser = user2;
                winnerFleet = user2Fleet;
                winnerFleetObj = objUser2Fleet;
            } else if ( fleet1Flat.length > 0) {
                winnerUser = user1;
                winnerFleet = user1Fleet;
                winnerFleetObj = objUser1Fleet;
            }
            
            // Update the ship count of the fleet in question.
            gameContext.FleetManager.updateFleet(winnerUser, winnerFleet, winnerFleetObj, function () {
                if (success) success.call(this, winnerUser, winnerFleet);
                gameContext.SocketManager.updateFleet(winnerUser);
            });
            

        });
    });
};

ShipManager.prototype.init = function () {
    
    // Mining ships
    this.ships.push(new Ship({
        title: 'Quarry',
        description: 'A Class-I mining vessel. Tattered and old, but she\'ll get the job done!',
        cost: 2000,
        attack: 0,
        defense: 20,
        hp: 100,
        miningIronMultiplier: 1.0,
        miningGoldMultiplier: 1.0,
        miningH3Multiplier: 1.0,
        timeMultiplier: 1.0
    }));
    
    this.ships.push(new Ship({
        title: 'Prodigy',
        tech: 'Prodigy',
        description: 'A Class-II mining vessel that specializes in iron harvesting.',
        miningIronMultiplier: 2.5,
        miningGoldMultiplier: 1.0,
        miningH3Multiplier: 1.0,
        cost: 8000,
        attack: 0,
        defense: 50,
        hp: 400,
        timeMultiplier: 5.0
    }));
    
    this.ships.push(new Ship({
        title: 'Skytrader',
        tech: 'Skytrader',
        description: 'A Class-II mining vessel that specializes in gold harvesting.',
        miningIronMultiplier: 1.0,
        miningGoldMultiplier: 2.5,
        miningH3Multiplier: 1.0,
        cost: 8000,
        attack: 0,
        defense: 200,
        hp: 400,
        timeMultiplier: 5.0
    }));
    
    // War vessels
    this.ships.push(new Ship({
        title: 'Delta Wing',
        description: 'A Class-I attack vessel useful in large groups. Great attack but poor defense',
        cost: 250,
        attack: 100,
        defense: 50,
        hp: 1000,
        timeMultiplier: 2.0
    }));
    
    this.ships.push(new Ship({
        title: 'Orbiter',
        description: 'A Class-I attack vessel with great defense but poor attack.',
        cost: 250,
        attack: 70,
        defense: 80,
        hp: 1000,
        timeMultiplier: 2.0
    }));
    
    this.ships.push(new Ship({
        title: 'Warbird',
        tech: 'Improved Attack I',
        description: 'A Class-II attack vessel capable of providing basic firepower.',
        cost: 800,
        attack: 300,
        defense: 200,
        hp: 1500,
        timeMultiplier: 4.0
    }));
    
    this.ships.push(new Ship({
        title: 'Shadow',
        tech: 'Improved Attack I',
        description: 'A Class-II attack vessel capable of providing massive firepower with severely reduced shields.',
        cost: 800,
        attack: 450,
        defense: 50,
        hp: 1500,
        timeMultiplier: 4.0
    }));
    
    this.ships.push(new Ship({
        title: 'Bloodletter',
        tech: 'Improved Attack II',
        description: 'A Class-III attack vessel capable of providing massive firepower.',
        cost: 1600,
        attack: 800,
        defense: 400,
        hp: 2200,
        timeMultiplier: 6.0
    }));
    
    this.ships.push(new Ship({
        title: 'Apollo',
        tech: 'Improved Attack II',
        description: 'A Class-III attack vessel capable of providing massive firepower with reduced defenses.',
        cost: 1600,
        attack: 500,
        defense: 700,
        hp: 2200,
        timeMultiplier: 6.0
    }));

    this.ships.push(new Ship({
        title: 'Crusader',
        tech: 'Improved Attack III',
        description: 'A Class-IV attack vessel capable of providing massive firepower.',
        cost: 3200,
        attack: 2500,
        defense: 100,
        hp: 3000,
        timeMultiplier: 10.0
    }));
    
    
    this.ships.push(new Ship({
        title: 'Andromeda',
        tech: 'Improved Attack III',
        description: 'A Class-IV attack vessel capable of providing massive firepower with reduced defense.',
        cost: 3200,
        attack: 1600,
        defense: 1500,
        hp: 3000,
        timeMultiplier: 10.0
    }));

    // Science Vessels
    this.ships.push(new Ship({
        title: 'Mythos',
        description: 'A basic science vessel capable of investigating anomalies.',
        cost: 2000,
        attack: 0,
        defense: 20,
        hp: 100,
        timeMultiplier: 1.0
    }));

    // Technology ships.    
    this.ships.push(new Ship({
        title: 'Stratos',
        tech: 'Space Exploration I',
        description: 'An advanced exploration vessel capable of leading a fleet to new sectors.',
        cost: 6000,
        attack: 0,
        defense: 100,
        hp: 100,
        explorationDistance: 5.0,
        timeMultiplier: 2.0
    }));

    this.ships.push(new Ship({
        title: 'Terraformer',
        tech: 'Terraform Planets',
        description: 'A basic science vessel capable of colonizing planets.',
        cost: 10000,
        attack: 0,
        defense: 20,
        hp: 100,
        timeMultiplier: 1.0
    }));

    // A.I. Ships
    this.ships.push(new Ship({
        title: 'Pirate Ship',
        description: 'A basic science vessel capable of colonizing planets.',
        attack: 110,
        defense: 80,
        hp: 80,
        pirate: true,
        timeMultiplier: 1.0
    }));
    
    this.ships.push(new Ship({
        title: 'Voracious',
        description: 'A basic science vessel capable of colonizing planets.',
        attack: 100,
        defense: 50,
        hp: 100,
        pirate: true,
        timeMultiplier: 1.0
    }));
};

module.exports = ShipManager;