﻿var uuid = require('node-uuid'),
    thrust = require('../common/thrust'),
    combat = require('../common/combat'),
    nameHelper = require('../nameHelper.js');

PirateManager = function (gameContext) {
    this.gameContext = gameContext;
};

var pirateName = "Pirates";

// Return the list of users re-mapped with their dispositions towards the Pirate faction.
PirateManager.prototype.getAlignments = function (users, success) {
    var result = [];
    var gameContext = this.gameContext;
    function next(index) {
        if (index > (users.length - 1)) {
            success.call(this, result);
        } else {
            gameContext.UserManager.getUser(users[index], function (objUser) {
                result.push([objUser.username, objUser.alignment_Pirates || -10]);
                next(index + 1);
            });
        }
    }

    next(0);
};

PirateManager.prototype.iterate = function (map, pirateGUID) {
    var gameContext = this.gameContext;
    
    
    // Iterate the map, and try to find a target to attack.
    var targets = [];
    var users = [];

    for (var prop in map) {
        var planet = map[prop];
        if (planet && planet.isPlanet && planet.owner !== undefined && planet.owner !== null && planet.owner !== pirateName) {
            users.push(planet.owner);
            targets.push(planet);
        }
    }
        
    // Figure out who to attack
    gameContext.PirateManager.getAlignments(users, function (alignments) {
        alignments.sort(function (a,b) { return a[1] - b[1]; });
        if (alignments.length > 0) {
                
            // Find the target players lowest planet.
            var targetPlayer = alignments[0][0];
            var target = null;

            for (var i = 0; i < targets.length; i++) {
                if (targets[i].owner == targetPlayer) {
                    if (target == null) target = targets[i];
                    else if (targets[i].shields < target.shields) target = targets[i];
                }
            }

            // If we have a target, make it so.
            gameContext.UserManager.getUser(pirateName, function (objUser) {
                
                var fleetPlanet;
                for (var prop in map) {
                    var planet = map[prop];
                    if (planet && planet.isPlanet) {
                        // Check for ownership.
                        if (planet.owner == pirateName) {
                            function attemptUpgrade(type, x, y) {
                                var b = gameContext.BuildingManager.find(type);
                                var l = parseFloat(planet[type + "Owned"] || 0);
                                    
                                // Cap it at 4.
                                if (l > 2) return;
                                if (b && (b.cost * l) < parseFloat(objUser.gold)) {
                                    gameContext.UserManager.debitResources(pirateName, (b.cost * l), 0, 0, 0);
                                    gameContext.QueueManager.enqueue('upgrade', { x: x, y: y, type: type, level: l, user: pirateName }, 1);
                                }
                            }
                            
                            fleetPlanet = planet;

                            // This is the home base for the quadrant in question.
                            // Check for a suborbital array.
                            if (parseFloat(planet["Suborbital Array"]) <= 0) {
                                gameContext.QueueManager.enqueue('construct', { x: planet.x, y: planet.y, type: 'Suborbital Array', user: pirateName }, 1);
                            } else if ( parseFloat(planet["Suborbital Array"]) <= 2) {
                                attemptUpgrade("Suborbital Array", planet.x, planet.y);
                            }

                            // Verify they have an Iron Mine.
                            if (parseFloat(planet["Iron MineOwned"]) <= 0) {
                                gameContext.QueueManager.enqueue('construct', { x: planet.x, y: planet.y, type: 'Iron Mine', user: pirateName }, 1);
                            } else {
                                attemptUpgrade("Iron Mine", planet.x, planet.y);
                            }
                                
                            // Verify they have a gas extractor.
                            if (parseFloat(planet["Gas ExtractorOwned"]) <= 0) {
                                gameContext.QueueManager.enqueue('construct', { x: planet.x, y: planet.y, type: 'Gas Extractor', user: pirateName }, 1);
                            } else {
                                attemptUpgrade("Gas Extractor", planet.x, planet.y);
                            }

                            if (parseFloat(planet["Ship YardOwned"]) <= 0) {
                                gameContext.QueueManager.enqueue('construct', { x: planet.x, y: planet.y, type: 'Ship Yard', user: pirateName }, 1);
                            } else {
                                attemptUpgrade("Ship Yard", planet.x, planet.y);
                            }
                        }
                    }
                }
                
                // Check if we have a fleet.
                if (fleetPlanet) {
                    gameContext.FleetManager.getUserFleets(pirateGUID, function (fleets) {
                        if (fleets.length == 0) {
                            // Let's create a fleet.
                            // Calculate the sector coordinates.
                            var scoords = gameContext.Map.normalizeSector(gameContext.Map.getQuadrant(planet.x, planet.y));
                            var coords = gameContext.FleetManager.mapToFleet(scoords[0] * 10, scoords[1] * 10);
                            gameContext.FleetManager.createFleet(pirateGUID, "Raptor Squad I", coords[0], coords[1], fleetPlanet.x, fleetPlanet.y, { ai: true, displayTitle: nameHelper.generatePirateFleetName(), dx: coords[0], dy: coords[1], displayUser: pirateName }, function (title) {
                                gameContext.Map.updateProperty(fleetPlanet.x, fleetPlanet.y, "defendingFleet", null);
                                
                                // Figure out how many ships.
                                var qty = 3 + Math.round(Math.random() * 8);
                                gameContext.FleetManager.addShips(pirateGUID, title, [["Pirate Ship", qty]], null, null, function () {
                                    gameContext.SocketManager.updateFleet(pirateName + '2');
                                });
                            });
                        } else {
                            
                            // Determine if we're going to move.
                            var prob = Math.random();
                            if (prob > 0.8) {
                                // Get all the fleets and see if one is somewhere in our sector.
                                var sector = gameContext.Map.getQuadrant(planet.x, planet.y);
                                gameContext.FleetManager.getAllFleets(function (allFleets) {
                                    for (var i = 0; i < allFleets.length; i++) {
                                        if (allFleets[i].sector == sector && allFleets[i].ai == false) {
                                            
                                            // If the player has been active recently, they're up for grabs.
                                            if (gameContext.UserManager.isActive(allFleets[i].user)) {
                                                
                                                // Fresh meat!
                                                var targetFleet = allFleets[i];
                                                var req = {
                                                    body: {
                                                        attackingUser: pirateGUID,
                                                        attackingFleet: fleets[0].title,
                                                        defendingUser: targetFleet.user,
                                                        defendingFleet: targetFleet.title
                                                    }
                                                };
                                                
                                                combat(gameContext, req, null);
                                                break;
                                            }
                                        }
                                    }
                                });
                            } else if (prob > 0.3) {
                                // We're just going to move randomly for effect.
                                var scoords = gameContext.Map.normalizeSector(gameContext.Map.getQuadrant(planet.x, planet.y));
                                var coords = gameContext.FleetManager.mapToFleet(scoords[0] * 10, scoords[1] * 10);
                                var req = {
                                    body: {
                                        user: pirateGUID,
                                        fleet: fleets[0].title,
                                        sx: fleets[0].x,
                                        sy: fleets[0].y,
                                        x: coords[0] + 100 + Math.round(Math.random() * 350),
                                        y: coords[1] + 100 + Math.round(Math.random() * 550),
                                        angle: 0
                                    }
                                };
                                
                                req.body.angle = Math.atan2(req.body.y - req.body.sy, req.body.x - req.body.sx);
                                thrust(null, gameContext, null, req, null, null, function () {
                                    gameContext.SocketManager.updateFleet(pirateName + '2');
                                });
                            }
                        }
                    });
                } 
            });
        }
    });
};

module.exports = PirateManager;