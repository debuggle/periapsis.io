﻿var storage = require('azure-storage'),
    // Threading
    child_process = require('child_process'),
    numCPUs = Math.max(require('os').cpus().length, 1),
    // Service stuff.
    service = storage.createQueueService(
    'periapsisdata', 
        'n2ypat5e9vZpSmhYEpFHTlWwqOJmioX+MHqjsfbOCjls4A4kaq/iXZXPLxn8yzpu7VfrqGXKY43iVOaRR1NbPQ==', 
        'https://periapsisdata.queue.core.windows.net/');

// Derive the queue name.
var queue_name = 'periapsis-task';
if (process.argv[2] == 'DEBUG') {
    queue_name += '-debug';
}
// Queue Control Block
QCB = function (name, object, guid) {
    this.action = name;
    this.obj = object;
    this.generation = guid;
};

QueueManager = function (gameContext, callback) {
    this.service = storage.createQueueService(
        'periapsisdata', 
        'n2ypat5e9vZpSmhYEpFHTlWwqOJmioX+MHqjsfbOCjls4A4kaq/iXZXPLxn8yzpu7VfrqGXKY43iVOaRR1NbPQ==', 
        'https://periapsisdata.queue.core.windows.net/');
    
    // Setup the queue.
    var self = this;
    this.gameContext = gameContext;
    this.service.createQueueIfNotExists(queue_name, function (err) {
        if (callback) callback.call();
    });

    // Setup other properties.
    this.threads = [];
};

QueueManager.prototype.enqueue = function (action, obj, duration, callback, q_name) {
    // Enqueue.
    q_name = q_name || queue_name;
    var self = this;
    var details = new QCB(action, obj, this.generation);
    duration = Math.max(duration, 1);
    this.service.createMessage(q_name, JSON.stringify(details), {
        visibilityTimeout: Math.round(duration)
    }, function (error) {
        if ( error )
            self.gameContext.Logger.logError(error, action);
    });
    
    if (callback) callback.call();
};

function initProcess(qm, ps) {
    ps.on('error', function (err) {
        if (err)
            qm.gameContext.Logger.logError(err, "A thread died to bring you this information.");
    });
    
    ps.on("disconnect", function () {
        qm.gameContext.Logger.logEvent("A thread has been disconnected.");
    });
    
    ps.on("exit", function () {
        qm.gameContext.Logger.logEvent("A thread has exited");
    });
    
    // Setup the thread bubbling stuff.
    ps.on('message', function (m) {
        // Check if it's a net table.
        if (m.tablefield && m.tablevalue) {
            qm.emitToThreads(m);
        }else if (m.user && m.evt) {
            qm.gameContext.SocketManager.emitSilent(m.user, m.evt, m.arg1, m.arg2);
            qm.emitToThreads(m);
        } else if (m.ping && m.threadId) {
            for (var i = 0; i < qm.threads.length; i++) {
                if (i == m.threadId) {
                    qm.threads[i].ping = qm.gameContext.now();
                    qm.threads[i].action = m.action || '';
                    qm.threads[i].ctx = m.ctx || '';
                }
            }
        }
    });
};

QueueManager.prototype.spawnThreads = function () {
    var qm = this;
    for (var i = 0; i < numCPUs; i++) {
        // Build the argument list.
        var args = [];
        if (process.argv[2] == 'DEBUG') args.push('DEBUG');
        else args.push('RELEASE');
        
        // Include the threadId
        args.push(i);
        
        // Spawn the worker.
        var ps = child_process.fork(__dirname + '/queueWorker.js', args);
        this.threads.push(ps);
        
        // Setup the error handlers.
        initProcess(qm, ps);
    }

    // Health checker.
    function beHealthy() {
        for (var i = 0; i < qm.threads.length; i++) {
            var thread = qm.threads[i];
            var lastPing = qm.gameContext.now() - thread.ping;

            // If the last ping was > some time ago, we need to 
            // jumpstart it.
            if (lastPing > 7000) {
                // Kill the thread and start a new one.
                qm.gameContext.Logger.logEvent("restarting thread " + i, "failed to process action '" + (qm.threads[i].action || '') + "'", qm.threads[i].ctx || {})
                qm.threads[i].kill();
                
                // Build the args.
                var args = [];
                if (process.argv[2] == 'DEBUG') args.push('DEBUG');
                else args.push('RELEASE');
                args.push(i);
                
                // Start the thread again.
                qm.threads[i] = child_process.fork(__dirname + '/queueWorker.js', args);

                // Setup the error handlers.
                initProcess(qm, qm.threads[i]);
            }
        }
        setTimeout(beHealthy, 8000);
    }
    beHealthy();
};

QueueManager.prototype.emitToThreads = function (message) {
    // Associate the threadId
    if (message.user == "Pirates") return;
    if (process.send) {
        this.gameContext.NetTable.set(message.tablefield, message.tablevalue, true);
        process.send(message);
    } else {
        for (var i = 0; i < this.threads.length; i++) {
            this.threads[i].send(message);
        }
        
        if (message.tablefield && message.tablevalue) {
            this.gameContext.NetTable.set(message.tablefield, message.tablevalue, true);
        } else if (message.evt) {
            if (message.evt == 'updateFleet') {
                this.gameContext.SocketManager.emitToAllSilent(message.user, message.evt, message.arg1, message.arg2);
            } else {
                this.gameContext.SocketManager.emitSilent(message.user, message.evt, message.arg1, message.arg2);
            }
        }
    }
}

QueueManager.prototype.init = function () {

};

module.exports = QueueManager;