﻿var Building = require('../models/building.js');

BuildingManager = function () {
    this.buildings = [];
    var self = this;
};

BuildingManager.prototype.getBuildings = function () {
    return this.buildings;
};

BuildingManager.prototype.find = function (title) {
    for (var i = 0; i < this.buildings.length; i++) {
        if (this.buildings[i].title == title) {
            return this.buildings[i];
        }
    }
    return null;
};

BuildingManager.prototype.findCost = function (title) {
    var building = this.find(title);
    if (building == null) return 0;
    return building.cost;
}

BuildingManager.prototype.init = function () {
    // Create all of the buildings.
    this.buildings.push(new Building(1.0, 500, 'resources', 25, 40, 0, 0, false, 'Iron Mine', 'Use this building to periodically gather more iron. Collect tax money as well.'));
    this.buildings.push(new Building(1.0, 500, 'resources', 25, 0, 40, 0, false, 'Gas Extractor', 'Harvest the valuable gas H-3 from your planet. Collect tax money as well.'));
    this.buildings.push(new Building(2.0, 500, 'research', 0, 0, 0, 12, false, 'Science Lab', 'Encourage your best and brightest to develop advanced technologies.'));
    this.buildings.push(new Building(3.0, 1000, 'space', 0, 0, 0, 0, true, 'Ship Yard', 'Setup a ship yard for developing faster-than-light vessels.'));
    //this.buildings.push(new Building(4.0, 1500, 'space', 0, 0, 0, 0, true, 'Space Port', 'Create a headquarters for establishing trade routes from this planet.'));
    this.buildings.push(new Building(5.0, 3500, 'defense', 0, 0, 0, 0, false, 'Suborbital Array', 'Protect your planet with a network of suborbital defense platforms.'));
    this.buildings.push(new Building(25.0, 5000, "array", 0, 0, 0, 0, false, "Von Hoffstad Array", "Advanced warning station capable of detecting catastrophic events long before they hit."));
    this.buildings.push(new Building(50.0, 10000, "array", 0, 0, 0, 0, false, "Solar Defense Program", "Improve your planetary defenses against natural disasters. Reduce the amount of damage solar flares cause."));
};

module.exports = BuildingManager;