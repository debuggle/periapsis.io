﻿// This class represents a bunch of helper methods to use when
// constructing the AI for a given race or NPC.
var AI = function (gameContext) {
    this.gameContext = gameContext;
};

AI.prototype.build = function (user, x, y, building) {
    var gameContext = this.gameContext;
    // Get the user
    gameContext.UserManager.getUser(user, function (objUser) {
        // Get the building
        var b = gameContext.BuildingManager.find(building);
        if (b) {
            
            // Check if we've build this already on this planet.
            gameContext.Map.getInfo(x, y, function (planet) {
                var level = parseFloat(planet[building + "Owned"] || 0);
                if (level == 0) {
                    // Build
                    if (objUser.gold >= b.cost) {
                        gameContext.UserManager.debitResources(user, b.cost, 0, 0, 0);
                        gameContext.QueueManager.enqueue('construct', { x: x, y: y, user: user, type: building }, gameContext.getSettings(1).constructTime);
                    }
                } else {
                    if (level > 6) return;
                    // Upgrade
                    if (objUser.gold >= (b.cost * (level + 1))) {
                        gameContext.UserManager.debitResources(user, b.cost * (level + 1), 0, 0, 0);
                        gameContext.QueueManager.enqueue('upgrade', { x: x, y: y, user: user, level: level, type: building }, gameContext.getSettings(1).upgradeTime * level);
                    }
                }
            });
        }
    });
};

AI.prototype.terraform = function (x, y, user, success) {
    var gameContext = this.gameContext;
    var self = this;
    this.shipAvailable(user, "Terraformer", function (has) {
        if (has) {
            gameContext.ShipManager.deployFleet('colonize', null, [['Terraformer', 1]], x, y, user, function () {
                var uid = gameContext.TimerManager.spawn('colonize', user, gameContext.getSettings(1).colonizeTime, "", "blue", { x: x, y: y });
                gameContext.Map.updateProperty(x, y, "beingColonized", true, function () {
                    gameContext.Map.updateProperty(x, y, "colonizeStart", gameContext.now(), function () {
                        gameContext.Map.updateProperty(x, y, "colonizeEnd", gameContext.TimerManager.find(uid).end, function () {
                            gameContext.QueueManager.enqueue('updatePlanet', { x: x, y: y, prop: 'colonizeAction', val: uid }, 0);
                            gameContext.QueueManager.enqueue('colonize', { x: x, y: y, user: user, }, gameContext.getSettings(1).colonizeTime);
                        });
                    });
                });
            });
        }
    });
};

AI.prototype.fabricate = function (x, y, qty, user, ship, success) {
    var gameContext = this.gameContext;
    var s = gameContext.ShipManager.find(ship);
    gameContext.UserManager.getUser(user, function (objUser) {
        if (s) {
            var cost = s.cost * parseFloat(qty);
            if (objUser.iron > cost) {
                gameContext.UserManager.debitResources(user, 0, cost, 0, 0);
                gameContext.ShipManager.allocateNewShip(ship, qty, x, y, user);
                if (success) success.call();
            }
        }
    });
};

AI.prototype.shipAvailable = function (user, ship, callback) {
    var gameContext = this.gameContext;
    gameContext.ShipManager.getUserShips(user, function (ships) {
        var has = false;
        for (var i = 0; i < ships.length; i++) {
            if (ships[i][0] == ship) {
                has = true;
            }
        }
        if (callback) callback.call(this, has);
    });
};

AI.prototype.shipQuantity = function (user, ship, callback) {
    var gameContext = this.gameContext;
    gameContext.ShipManager.getUserShips(user, function (ships) {
        var count = 0;
        for (var i = 0; i < ships.length; i++) {
            if (ships[i][0] == ship)
                count++;
        }
        if (callback) callback.call(this);
    });
};

module.exports = AI;