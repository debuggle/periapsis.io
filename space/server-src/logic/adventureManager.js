﻿var Adventure = function (title, info) {
    info = info || {};
    this.title = title;
    this.npc = info.npc || 'Unknown';
    this.parts = [];
};

// OPTIONS
// - description
// - componentDidFinish
// - applyReward
var AdventureComponent = function (options) {
    options = options || {};
    this.description = options.description;
    this.componentDidFinish = options.componentDidFinish;
    this.applyReward = options.applyReward;
};

Adventure.prototype.addComponent = function (obj) {
    this.parts.push(new AdventureComponent(obj));
}

var AdventureManager = function (gameContext) {
    this.adventures = [];
    this.gameContext = gameContext;
};

AdventureManager.prototype.find = function (title) {
    for (var i = 0; i < this.adventures.length; i++) {
        if (this.adventures[i].title == title) {
            return this.adventures[i];
        }
    }
    return null;
};

AdventureManager.prototype.processEvent = function (user, obj) {
    var gameContext = this.gameContext;
    var self = this;
    gameContext.UserManager.getQuests(user, function (quests) {
        for (var prop in quests) {
            var quest = self.find(prop);
            var index = quests[prop].index;
            
            if (quest.parts.length > index) {
                quest.parts[index].componentDidFinish(user, obj, function () {
                    // If they completed it, apply the reward and upgrade index.
                    quest.parts[index].applyReward.call(this, user);
                    index++;
                    gameContext.UserManager.updateQuest(user, prop, index, null);
                });
            }
        }
    });
};

AdventureManager.prototype.getAll = function (user, callback) {
    // Look at the adventures the user has completed.
    var gameContext = this.gameContext;
    var self = this;
    
    // Resulting object structure.
    // title - quest title.
    // index - current part
    // description - current description of part
    // accepted - status
    // issuer - npc who issued

    gameContext.UserManager.getQuests(user, function (quests) {
        var result = [];
        for (var i = 0; i < self.adventures.length; i++) {
            var adventure = self.adventures[i];
            var item = {
                title: adventure.title,
                index: 0,
                description: adventure.parts[0].description,
                accepted: false,
                completed: false,
                issuer: adventure.npc
            };

            if (quests[adventure.title]) {
                item.index = quests[adventure.title].index;
                item.accepted = true;

                if (item.index >= adventure.parts.length) {
                    item.completed = true;
                } else {
                    item.description = adventure.parts[item.index].description;
                    item.completed = false;
                }
            }

            result.push(item);
        }

        // Return the results.
        if (callback) callback.call(this, result);
    });
};

AdventureManager.prototype.init = function () {
    var entryLevelNpc = "Parlay Jack";
    var gameContext = this.gameContext;
    
    function is(obj) {
        return obj !== null && obj !== undefined;
    }

    var upgradeBuildings = new Adventure("Upgrade Buildings", { npc: entryLevelNpc });
    upgradeBuildings.addComponent({
        description: 'What is this, old earth? If yer going to survive in this universe you need to upgrade some buildings! Get yer Iron Mine and Gas Extractor to level 3.',
        componentDidFinish: function (user, obj, success, failure) {
            if (is(user) && is(obj) && is(obj.x) && is(obj.y)) {
                var buildings = gameContext.BuildingManager.getBuildings();
                gameContext.Map.getInfo(obj.x, obj.y, function (planet) {
                    if (parseFloat(planet["Iron MineOwned"]) >= 3 && parseFloat(planet["Gas ExtractorOwned"]) >= 3) {
                        if (success)
                            success.call();
                    } else {
                        if (failure)
                            failure.call();
                    }
                });
            } else {
                if (failure)
                    failure.call();
            }
        },
        applyReward: function (user) {
            gameContext.UserManager.addResources(user, 2000, 2000, 0);
            gameContext.UserManager.addNotification(user, "green", "You have completed the 'Upgrade Buildings' quest! Here's 2000 Iron and 2000 Gold.");
        }
    });

    var attackPirates = new Adventure("Attack Pirates", { npc: entryLevelNpc });
    attackPirates.addComponent({
        description: 'There be pirates in our sector. This is unacceptable! Construct 5 Warbirds and strike while the iron is hot!',
        componentDidFinish: function (user, obj, success, failure) {
            if (is(user) && is(obj) && is(obj.target)) {
                if (obj.target == "Pirates") {
                    if (success)
                        success.call();
                } else {
                    if (failure) failure.call();
                }
            } else {
                if (failure) failure.call();
            }
        },
        applyReward: function (user) {
            gameContext.UserManager.addResources(user, 0, 0, 2500);
            gameContext.UserManager.addNotification(user, "green", "You have completed the 'Attack Pirates' quest! Here's 2500 H3. Keep the fight going!");
        }
    });

    var exploreNewSector = new Adventure("Explore new Sector", { npc: entryLevelNpc });
    exploreNewSector.addComponent({
        description: 'You know there are other sectors out there to explore, yeah? Build up a space probe and send her out into the deep unknown.',
        componentDidFinish: function (user, obj, success, failure) {
            if (is(user) && is(obj) && is(obj.newSector)) {
                if (obj.newSector) {
                    if (success) success.call();
                } else {
                    if (failure) failure.call();
                }
            } else { 
                if (failure) failure.call();
            }
        },
        applyReward: function (user) {
            gameContext.UserManager.addResources(user, 5000, 5000, 5000);
            gameContext.UserManager.addNotification(user, "green", "You have completed the 'Explore new Sector' quest! Here's 5000 of each resource. Go conquer the universe!");
        }
    });

    var colonizePlanet = new Adventure("Colonize Planet", { npc: entryLevelNpc });
    colonizePlanet.addComponent({
        description: 'We could always use more planets in this god forsaken sector. Go colonize another planet by using a Terraformer.',
        componentDidFinish: function (user, obj, success, failure) {
            if (is(user) && is(obj) && is(obj.x) && is(obj.y) && is(obj.colonized)) {
                gameContext.Map.getInfo(obj.x, obj.y, function (planet) {
                    if (planet.type !== 'planet-home' && planet.owner == user) {
                        if (success) success.call();
                    } else {
                        if (failure) failure.call();
                    }
                });
            } else {
                if (failure) failure.call();
            }
        },
        applyReward: function (user) {
            gameContext.UserManager.addResources(user, 0, 2500, 0);
            gameContext.UserManager.addNotification(user, "green", "You have completed the 'Colonize Planet' quest! Here's 2500 gold. Hopefully that helps with the construction.");
        }
    });

    // Setup each adventure.


    // Register each adventure.
    this.adventures.push(upgradeBuildings);
    this.adventures.push(attackPirates);
    this.adventures.push(exploreNewSector);
    this.adventures.push(colonizePlanet);

};

module.exports = AdventureManager;