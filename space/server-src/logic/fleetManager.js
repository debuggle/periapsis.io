﻿var Fleet = require('../models/fleet');

function getKey(user, name){ 
    return "user_" + user + "_fleet_" + name;
}

var FleetManager = function (gameContext) {
    this.gameContext = gameContext;
};

FleetManager.prototype.createFleet = function (user, name, x, y, sx, sy, obj, success) {
    var gameContext = this.gameContext;
    var db0 = this.gameContext.db0;
    var fleet = new Fleet({
        user: user,
        title: name,
        displayUser: user,
        x: x - 25,
        y: y - 25,
        dx: x - 25,
        dy: y - 25,
        planetX: sx,
        planetY: sy,
        sector: gameContext.Map.getQuadrant(sx,sy)
    });
    
    // Apply the other properties.
    for (var prop in obj)
        fleet[prop] = obj[prop];

    gameContext.Map.getInfo(sx, sy, function (planet) {
        // Check if this fleet should be defending the planet.
        if (!planet.defendingFleet) {
            gameContext.Map.updateProperty(sx, sy, "defendingFleet", name, function () {
                gameContext.SocketManager.refreshMap(user, gameContext.Map.getQuadrant(sx, sy));
            });
        } else {
            fleet["planetX"] = null;
            fleet["planetY"] = null;
        }

        // Create the fleet.
        db0.addList("world_fleets", fleet, function () {
            gameContext.SocketManager.updateFleet(user);
            if (success) success.call(this, fleet.title);
        });
    });
};

FleetManager.prototype.getFleet = function (user, name, success, failure) {
    var db0 = this.gameContext.db0;
    db0.getList("world_fleets", function (fleets) {
        for (var i = 0; i < fleets.length; i++) {
            if (fleets[i].title == name && fleets[i].user == user) {
                if (success) success.call(this, fleets[i]);
                return;
            }
        }
        if (failure) failure.call();
    });
};

FleetManager.prototype.getUserFleets = function (user, success) {
    var db0 = this.gameContext.db0;
    db0.getList("world_fleets", function (objFleets) {
        var result = [];
        for (var r = 0; r < objFleets.length; r++) {
            if (objFleets[r].user == user) {
                result.push(objFleets[r]);
            }
        }
        if (success) success.call(this, result);
    });
};

FleetManager.prototype.getFleets = function (user, fleets, success) {
    var db0 = this.gameContext.db0;
    db0.getList("world_fleets", function (objFleets) {
        var result = [];
        for (var r = 0; r < fleets.length; r++) {
            for (var i = 0; i < objFleets.length; i++) {
                if (objFleets[i].title == fleets[r] && objFleets[i].user == user) {
                    result.push(objFleets[i]);
                    break;
                }
            }
        }

        if (success) success.call(this, result);
    });
};

FleetManager.prototype.updateFleet = function (user, name, fleetObj, success, failure) {
    var db0 = this.gameContext.db0;
    // Check if this new fleet object has no ships.
    var ships = fleetObj.ships || [];
    var shipQty = 0;
    for (var i = 0; i < ships.length; i++) {
        shipQty += parseFloat(ships[i][1]);
    }
    
    if (shipQty > 0) {
        this.gameContext.lock("world_fleets", 2500, function (done) {
            db0.getList("world_fleets", function (fleets) {
                for (var i = 0; i < fleets.length; i++) {
                    if (fleets[i].title == name && fleets[i].user == user) {
                        db0.remAt("world_fleets", i, function () {
                            db0.addList("world_fleets", fleetObj, function () {
                                done();
                                if (success) success.call();
                            });
                        });
                        return;
                    }
                }
                if (failure) failure.call();
                done();
                return;
            });
        });
    } else {
        // Disband the fleet.
        this.disband(user, name, success);
    }
};

FleetManager.prototype.calcThrustCoords = function (x, y) {
    return [x * 800, y * 800];
};

FleetManager.prototype.mapToFleet = function (x, y) {
    return [Math.round((x / 10) * 800), Math.round((y / 10) * 800)];
};

FleetManager.prototype.fleetToMap = function (x, y) {
    return [Math.round(x / 800), Math.round(y / 800)];
};

FleetManager.prototype.moveFleet = function (user, name, x, y, success) {
    var gameContext = this.gameContext;
    var db0 = this.gameContext.db0;
    var sector = this.gameContext.Map.getQuadrant(x, y);
    
    // Get the current fleet position.
    this.getFleet(user, name, function (fleet) {
        // Update the relevant properties.
        fleet.sector = sector;
        fleet.dx = x;
        fleet.dy = y;

        // Commit the changes.
        gameContext.FleetManager.updateFleet(user, name, fleet, function () {
            gameContext.SocketManager.updateFleet(user);
            if (success) success.call();
        });
    });
};

FleetManager.prototype.changeFormation = function (user, name, formation, success) {
    var db0 = this.gameContext.db0;
    var gameContext = this.gameContext;
    this.getFleet(user, name, function (fleet) {
        fleet.formation = formation;
        gameContext.FleetManager.updateFleet(user, name, fleet, success);
    });
};

FleetManager.prototype.changeName = function (user, name, newTitle, success) {
    var gameContext = this.gameContext;
    var db0 = this.gameContext.db0;
    this.getFleet(user, name, function (fleet) {
        fleet.displayTitle = newTitle;
        gameContext.FleetManager.updateFleet(user, name, fleet, function () {
            gameContext.SocketManager.updateFleet(user);
            if (success) success.call();   
        });
    });
};

FleetManager.prototype.userHasFleet = function (user, callback) {
    var db0 = this.gameContext.db0;
    db0.getList("world_fleets", function (fleets) {
        for (var i = 0; i < fleets.length; i++) {
            if (fleets[i].user == user) {
                if (callback) callback.call(this, true);
                return;
            }
        }
        if (callback) callback.call(this, false);
    });
};

FleetManager.prototype.exists = function (user, name, callback) {
    var db0 = this.gameContext.db0;
    this.getFleet(user, name, function () {
        if (callback) callback.call(this, true);
    }, function () {
        if (callback) callback.call(this, false);
    });
};

FleetManager.prototype.putProperty = function (user, fleet, property, value, callback) {
    var gameContext = this.gameContext;
    var db0 = this.gameContext.db0;
    gameContext.lock("user_" + user + "_fleets", 2500, function (done) {
        gameContext.FleetManager.getFleet(user, fleet, function (objFleet) {
            objFleet[property] = value;
            gameContext.FleetManager.updateFleet(user, fleet, objFleet, function () {
                done();
                gameContext.SocketManager.updateFleet(user);
                if (callback) callback.call();
            });
        }, function () {
            done();   
        });
    });
};

// X - where the ship is currently stationed (sector coordinates).
// Y - where the ship is currently stationed (sector coordinates).
FleetManager.prototype.addShips = function (user, name, ships, x, y, callback) {
    var gameContext = this.gameContext;
    var db0 = this.gameContext.db0;
    if (ships.length == 0) {
        if (callback) callback.call();
        return;
    }
    
    gameContext.FleetManager.getFleet(user, name, function (objFleet) {
        
        // Build the fleet.
        objFleet.ships = objFleet.ships || [];
        objFleet.miners = objFleet.miners || 0;
        var shiphash = {};
        for (var i = 0; i < objFleet.ships.length; i++) {
            var shipName = objFleet.ships[i][0];
            var shipQty = parseFloat(objFleet.ships[i][1]) || 0;
            
            if (shipQty > 0) {
                shiphash[shipName] = (shiphash[shipName] || 0) + shipQty;
            }
        }
        
        // Iterate over the ships and kill them.
        for (var i = 0; i < ships.length; i++) {
            var s = ships[i];
            if (s.length >= 2) {
                var objShip = gameContext.ShipManager.find(s[0]);
                // Add to miners if this is a mining vessel.
                if (objShip.miningGoldMultiplier > 0 || objShip.miningH3Multiplier > 0 || objShip.miningIronMultiplier > 0) objFleet.miners++;
                // Assign the ship if it comes from somewhere.
                if (x && y) gameContext.ShipManager.assignShip(objShip.name, parseFloat(s[1]), x, y, user);
                shiphash[s[0]] = (shiphash[s[0]] || 0) + parseFloat(s[1] || 0);
            }
        }
        
        // Construct the proper list.
        delete objFleet.ships;
        objFleet.ships = [];
        for (var prop in shiphash) {
            objFleet.ships.push([prop, shiphash[prop]]);
        }
        
        // Commit the changes.
        gameContext.FleetManager.updateFleet(user, name, objFleet, function () {
            if (callback) callback.call(this);
        })
    });
};

// Fleet 1 -> Fleet 2
FleetManager.prototype.merge = function (user, fleet1, fleet2, callback) {
    var gameContext = this.gameContext;
    var db0 = gameContext.db0;

    gameContext.FleetManager.getFleet(user, fleet1, function (objFleet1) {
        gameContext.FleetManager.getFleet(user, fleet2, function (objFleet2) {
           // Just to make sure it still exists.
            gameContext.FleetManager.addShips(user, fleet2, objFleet1.ships, null, null, function () {
                gameContext.FleetManager.disband(user, fleet1);
                if (callback) callback.call();
            }); 
        });
    });
};

FleetManager.prototype.disband = function (user, name, callback) {
    var gameContext = this.gameContext;
    var db0 = this.gameContext.db0;
        
    // Return the ships and remove the fleet object.
    db0.getList("world_fleets", function (fleets) {
        for (var i = 0; i < fleets.length; i++) {
            if (fleets[i].title == name && fleets[i].user == user) {
                db0.remAt("world_fleets", i, function () {
                    gameContext.SocketManager.updateFleet(user);
                    // Check if we were protecting an asteroid.
                    if (fleets[i].asteroidX && fleets[i].asteroidY) {
                        gameContext.AsteroidManager.abandon(fleets[i].asteroidX, fleets[i].asteroidY, callback);
                    } else {
                        if (callback) callback.call();   
                    }
                }, callback);
                return;
            }
        }
        if (callback) callback.call();
    });
};

FleetManager.prototype.getAllFleets = function (callback) {
    var db0 = this.gameContext.db0;
    db0.getList("world_fleets", function (fleets) {
        if (callback) callback.call(this, fleets);
    });
};

FleetManager.prototype.capturePlanet = function (user, fleet, defenderUser, x, y) {
    // Get the planet.
    var gameContext = this.gameContext;
    gameContext.Map.getInfo(x, y, function (planet) {
        gameContext.Map.changeOwner(x, y, user, function () {
            // Set this fleet to defend the planet.
            gameContext.Map.updateProperty(x, y, "defendingFleet", fleet, function () {
                gameContext.FleetManager.getFleet(user, fleet, function (objFleet) {
                    objFleet.planetX = x;
                    objFleet.planetY = y;
                    gameContext.FleetManager.updateFleet(user, fleet, objFleet, function () {
                        // Send out the notifications.
                        gameContext.UserManager.addNotification(user, "yellow", "You have successfully captured the planet " + planet.name + " in sector " + gameContext.Map.getQuadrant(x, y) + ".", { x: x, y: y });
                        gameContext.UserManager.addNotification(defenderUser, "red", "You have lost the planet " + planet.name + " in sector " + gameContext.Map.getQuadrant(x, y) + ".", { x: x, y: y });
                        
                        // Check for the relocation condition.
                        gameContext.UserManager.relocate(defenderUser, planet.x, planet.y);
                        
                        // Tell everyone to update stuff.
                        gameContext.SocketManager.updateFleet(user);
                        gameContext.SocketManager.updateFleet(defenderUser);
                        
                        var sector = gameContext.Map.getQuadrant(planet.x, planet.y);
                        gameContext.SocketManager.refreshMap(user, sector);
                        gameContext.SocketManager.refreshMap(defenderUser, sector);
                    });
                });
            });
        });
    });
};

FleetManager.prototype.init = function () {

};

module.exports = FleetManager;