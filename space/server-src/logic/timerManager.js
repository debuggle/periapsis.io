﻿var redis = require('../redis-layer.js'),
    uuid = require('node-uuid');

TimerManager = function (gameContext) {
    this.gameContext = gameContext;
    this.actions = {};
    this.queue = [];
};

TimerManager.prototype.find = function (uid) {
    for (var i = 0; i < this.queue.length; i++) {
        if (this.queue[i].uid == uid) {
            return this.queue[i];
        }
    }
    return null;
};

TimerManager.prototype.spawn = function (actionName, username, duration, note, color, context) {
    var uid = uuid.v1();
    var duration = duration;
    var gameContext = this.gameContext;
    var now = gameContext.now();
    color = color || 'blue';

    // Enqueue the work.
    var obj = {
        owner: username,
        color: color,
        context: context || {},
        uid: uid,
        start: now,
        note: note,
        completed: false,
        end: now + Math.round(duration) * 1000
    };
    
    // Add to the queue
    this.queue.push(obj);
    
    // Register the timer with the user.
    this.gameContext.db0.addList("user_" + username + "_timers", obj, function () {
        setTimeout(function () {
            gameContext.SocketManager.updateNotifications(username);
        }, 150);
    });
    return uid;
};

module.exports = TimerManager