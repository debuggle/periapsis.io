﻿var VictoryLog = function (gameContext) {
    this.gameContext = gameContext;
};

VictoryLog.prototype.rankToTitle = function (rank) {
    switch (rank) {
        case 1:
            return "Crescent Civilization";
        case 2:
            return "Adept Civilization";
        case 3:
            return "Multi-Planetary Civilization";
        case 4:
            return "Hive Civilization";
        case 5:
            return "Deep Space Civilization";
        case 6:
            return "Asimovian Civilization";
        default:
            return "Primitive Civilization";
    }
};

VictoryLog.prototype.getRank = function (user, success, failure) {
    var db0 = this.gameContext.db0;
    db0.getList("user_" + user + "_victory_log", function (list) {
        var rank = 0;
        var colonized = 0;
        var constructed = 0;
        var upgraded = 0;
        var quarries = 0;
        var fleet = 0;

        for (var i = 0; i < list.length; i++) {
            var qty = Math.max(list[i][1], 1);
            if (list[i][0] == "colonized") colonized += qty;
            else if (list[i][0] == "quarry") quarries += qty;
            else if (list[i][0] == "upgraded") upgraded += qty;
            else if (list[i][0] == "constructed") constructed += qty;
            else if (list[i][0] == "fleet") fleet += qty;
        }
        
        // Calculate the rank.
        if (colonized >= 15) rank = 6;
        else if (constructed >= 12 && colonized >= 3 && upgraded >= 32) rank = 4;
        else if (colonized >= 2 && constructed >= 8 && upgraded >= 28) rank = 3;
        else if (quarries >= 2 && fleet >= 2 && constructed >= 4 && upgraded >= 8) rank = 2;
        else if (constructed >= 4 && upgraded >= 4) rank = 1;

        // Return the rank.
        if (success) success.call(this, rank);

    }, failure);
};

VictoryLog.prototype.fireEvent = function (user, evt, qty, callback) {
    var self = this;
    var gameContext = this.gameContext;
    var db0 = this.gameContext.db0;
    // Get the current user rank.
    gameContext.UserManager.getUser(user, function (objUser) {
        var currentRank = objUser.rank || 0;
        db0.addList("user_" + user + "_victory_log", [evt, qty], function () {
            // Get the new rank.
            self.getRank(user, function (rank) {
                // if the rank is different, update it and alert the player.
                if (rank > currentRank) {
                    gameContext.UserManager.putValue(user, "rank", rank, function () {
                        gameContext.UserManager.addNotification(user, "yellow", "You have earned the new title of '" + self.rankToTitle(rank) + "'", {});
                        if (callback) callback.call(this, rank);
                    });
                }
            }, function () {
                if (callback) callback.call(this, 0);           
            });
        });
    });

};

module.exports = VictoryLog;