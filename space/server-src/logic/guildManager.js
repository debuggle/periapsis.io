﻿var GuildManager = function (gameContext) {
    this.gameContext = gameContext;
    this.color = "orange";
};

GuildManager.prototype.create = function (name, founder, success, failure) {
    var db0 = this.gameContext.db0;
    var gameContext = this.gameContext;
    db0.retrieve("guild_" + name, function () {
        if (failure) failure.call();
    }, function () {
        db0.create("guild_" + name, {
            name: name,
            founder: founder,
            admins: [founder],
            members: [founder],
            ships: []
        }, success);
        gameContext.UserManager.putValue(founder, "guild", name, function () {
            gameContext.UserManager.addNotification(founder, gameContext.GuildManager.color, "You have founded the guild '" + name + "'");
        });
    });
};

GuildManager.prototype.fireUpdateEvent = function (guildName) {
    var gameContext = this.gameContext;
    this.getGuild(guildName, function (objGuild) {
        var members = objGuild.members || [];
        for (var i = 0; i < members.length; i++) {
            gameContext.SocketManager.updateGuild(members[i]);
        }
    });
};

GuildManager.prototype.getGuild = function (guildName, success, failure) {
    var db0 = this.gameContext.db0;
    db0.retrieve("guild_" + guildName, function (result) {
        if (success) success.call(this, result);
    }, failure);
}

GuildManager.prototype.join = function (guildName, playerName, success) {
    var db0 = this.gameContext.db0;
    var gameContext = this.gameContext;
    var self = this;
    self.getGuild(guildName, function (guild) {
        var members = guild.members || [];
        if (members.indexOf(playerName) < 0) {
            
            // First, tell all the members that this player has joined (before we modify that list).
            for (var i = 0; i < members.length; i++) {
                gameContext.UserManager.addNotification(members[i], gameContext.GuildManager.color, "The player '" + playerName + "' has joined your guild.");
            }

            members.push(playerName);
            db0.put("guild_" + guildName, "members", members, function () {
                gameContext.UserManager.putValue(playerName, "guild", guildName, success);
                gameContext.UserManager.addNotification(playerName, gameContext.GuildManager.color, "You have joined the guild '" + guildName + "'");
                self.fireUpdateEvent(guildName);
            });
        } else {
            if (success) success.call();
        }
    });
};

GuildManager.prototype.leave = function (guildName, playerName, success) {
    var db0 = this.gameContext.db0;
    var gameContext = this.gameContext;
    var self = this;
    this.getGuild(guildName, function (guild) {
        var members = guild.members || [];
        if (members.indexOf(playerName) >= 0) {

            // First, tell all the members that this player has left (before we modify that list).
            for (var i = 0; i < members.length; i++) {
                gameContext.UserManager.addNotification(members[i], gameContext.GuildManager.color, "The player '" + playerName + "' has left your guild.");
            }

            members.splice(members.indexOf(playerName), 1);
            db0.put("guild_" + guildName, "members", members, function () {
                gameContext.UserManager.putValue(playerName, "guild", "", success);
                gameContext.UserManager.addNotification(playerName, gameContext.GuildManager.color, "You have left the guild '" + guildName + "'");
                self.fireUpdateEvent(guildName);
            });
        } else {
            if (success) success.call();
        }
    });
};

GuildManager.prototype.disband = function (guildName, playerName, success, failure) {
    // Make sure the founder is doing this.
    var gameContext = this.gameContext;
    var self = this;
    this.getGuild(guildName, function (objGuild) {
        if (objGuild.admins.indexOf(playerName) >= 0) {
            // They are an admin, so this is acceptable.
            // Iterate over each member and tell them it has been disbanded, then udpate that member
            // and remove the guild from their association.
            for (var i = 0; i < objGuild.members.length; i++) {
                gameContext.UserManager.addNotification(objGuild.members[i], gameContext.GuildManager.color, "Your guild '" + guildName + "' has been disbanded.");
                gameContext.UserManager.putValue(objGuild.members[i], "guild", "");
                gameContext.SocketManager.updateGuild(objGuild.members[i]);
            }

            // Now delete the guild object.
            gameContext.db0.delete("guild_" + guildName, function () {
                self.fireUpdateEvent(guildName);
                if (success) success.call();
            });
        } else {
            if (failure) failure.call();
        }
    });
};

GuildManager.prototype.message = function (guildName, playerName, message, success) {
    var db0 = this.gameContext.db0;
    var gameContext = this.gameContext;
    this.getGuild(guildName, function (guild) {
        var members = guild.members || [];
        for (var i = 0; i < members.length; i++) {
            gameContext.UserManager.addNotification(members[i], gameContext.GuildManager.color, message);
        }
        if (success) success.call();
    });
};

GuildManager.prototype.isSameGuild= function (user1, user2, success) {
    var db0 = this.gameContext.db0;
    var gameContext = this.gameContext;
    gameContext.UserManager.getUser(user1, function (objUser1) {
        gameContext.UserManager.getUser(user2, function (objUser2) {
            var same = (objUser1.guild == objUser2.guild);
            if (success) success.call(this, same);
        });
    });
};

GuildManager.prototype.getShips = function (guild, success) {
    this.getGuild(guild, function (objGuild) {
        if (success) success.call(this, objGuild.ships);
    });
};

GuildManager.prototype.donateShips = function (user1, ships, success) {
    var db0 = this.gameContext.db0;
    var gameContext = this.gameContext;
    var self = this;
    gameContext.UserManager.getUser(user1, function (objUser) {
        if (user1.guild !== null && user1.guild !== undefined && user1.guild.length > 0) {
            // They have a guild, so that's who we're donating this to.
            // Make sure they have the ships in question.
            gameContext.ShipManager.getUserShips(user1, function (userShips) {
                var donation = [];
                for (var r = 0; r < ships.length; r++) {
                    if (!ships[r]) continue;
                    var ship = ships[r][0];
                    var qty = parseFloat(ships[r][1] || 0);
                    var actual = 0;
                    for (var i = 0; i < userShips.length; i++) {
                        if (userShips[i].name == ship && userShips[i].deployed == false) {
                            gameContext.ShipManager.killShip(ship, false, userShips[i].mission, user1);
                            actual++;
                        }
                    }
                    
                    // Donate "actual" to the cause.
                    donation.push([ship, actual]);
                }
                // Update the ships in the guild.
                self.getGuild(user1.guild, function (guild) {
                    var g_ships = guild.ships || [];
                    for (var i = 0; i < donation.length; i++) {
                        for (var r = 0; r < donation[i][1]; r++) {
                            g_ships.push({ name: donation[i][0], x: 0, y: 0, deployed: false, mission: '' });
                        }
                    }
                    // Update ships.
                    db0.put("guild_" + objUser.guild, "ships", g_ships, success);
                    gameContext.UserManager.addNotification(user1, gameContext.GuildManager.color, "You have donated ships to '" + objUser.guild + "'");
                });
            });
        }
    });
};

GuildManager.prototype.init = function () {

};

module.exports = GuildManager;