﻿var CataclysmicManager = function (gameContext) {
    this.gameContext = gameContext;
};

CataclysmicManager.prototype.chance = function () {
    return Math.random() > 0.98;
};

// In this case, X/Y refer to a sector, NOT a planet.
CataclysmicManager.prototype.generateEvent = function (x, y, callback) {
    if (this.chance()) {
        // Then we need to generate some kind of event.
        var gameContext = this.gameContext;
        var self = this;
        gameContext.Map.scan(x, y, function (map) {
            for (var prop in map) {
                if (map[prop].isPlanet) {
                    if (self.chance()) {
                        // This planet is targetted.
                        self.generateSolarFlare(map[prop].x, map[prop].y, false);
                    }
                }
            }            
        });
    }
};

// In this case, X/Y refer to a specific planet, NOT a sector.
CataclysmicManager.prototype.generateSolarFlare = function (x, y, alerted, success, failure) {
    var gameContext = this.gameContext;
    gameContext.Map.getInfo(x, y, function (planet) {
        if (planet.owner && planet.isPlanet) {
            // Check if this planet has a Solar Defense Program
            var level = parseFloat(planet["Solar Defense ProgramOwned"] || 0);
            var array = parseFloat(planet["Von Hoffstad ArrayOwned"] || 0);
            
            // If they have an array, and have not been alerted... Defer the action.
            if (array > 0 && !alerted) {
                gameContext.UserManager.addNotification(planet.owner, "yellow", "There is an impending solar flare which will strike " + planet.name + " within the next two hours.", { x: planet.x, y: planet.y });
                gameContext.QueueManager.enqueue("solarFlare", { x: planet.x, y: planet.y }, gameContext.getSettings(1).cataclysmicEvent * (.5 + Math.random()));
                if (success) success.call();
            } else {
                // If they do have a solar defense, then reduce the damage by 15 * level.
                var damage = Math.max(30 - (5 * level), 0);
                damage = Math.round(Math.random() * damage);

                // Send the notification. (Anything after a level of 5 will have no effect).
                if (level < 6) gameContext.UserManager.addNotification(planet.owner, "red", "There has been a solar flare which has affected planet " + planet.name + ".", { x: planet.x, y: planet.y });
                
                // Apply the damage.
                var shields = Math.max(parseFloat(planet.shields || 0) - damage, 0);
                gameContext.Map.updateProperty(x, y, "shields", shields, function () {
                    if (success) success.call();
                    gameContext.SocketManager.updateMap(planet.owner);
                    gameContext.SocketManager.updatePlanet(planet.owner);
                }, failure);
            }
        } else {
            if (failure) failure.call();
        }
    });
};

module.exports = CataclysmicManager;