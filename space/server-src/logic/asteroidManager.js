﻿var Asteroid = require('../models/asteroid'),
    Structure = require('../models/structure');

var AsteroidManager = function (gameContext) {
    this.gameContext = gameContext;
    this.structures = [];
}

AsteroidManager.prototype.getStructures = function () {
    return this.structures;
};

AsteroidManager.prototype.getAsteroid = function (x, y, success) {
    var gameContext = this.gameContext;
    gameContext.Map.scan(x, y, function (map) {
        // First check if the actual coordinates are an asteroid.
        var test = map[y + '-' + x];
        if (test && test.type == 'asteroid') {
            success.call(this, test);
        } else {
            // Check for all the empty spots on the map.
            for (var prop in map) {
                if (map[prop].type == 'asteroid') {
                    success.call(this, map[prop]);
                    return;
                }
            }
        }
    });
};

AsteroidManager.prototype.fireUpdateEvent = function (x, y) {
    var gameContext = this.gameContext;
    gameContext.Map.getUsersInQuadrant(x, y, function (users) {
        for (var i = 0; i < users.length; i++) {
            gameContext.SocketManager.updateMap(users[i]);
            gameContext.SocketManager.updateAsteroid(users[i], x, y);
        }
    });
}

AsteroidManager.prototype.find = function (name) {
    for (var i = 0; i < this.structures.length; i++) {
        if (this.structures[i].name == name)
            return this.structures[i];
    }
    return null;
};

AsteroidManager.prototype.spawnAsteroid = function (x, y, count, success) {
    var gameContext = this.gameContext;
    var self = this;
    gameContext.Map.scan(x, y, function (map) {
        
        // Check for all the empty spots on the map.
        var empty = [];
        var hasAsteroid = false;
        var asteroidCount = 0;

        for (var prop in map) {
            if (map[prop].type == 'space') {
                empty.push(map[prop]);
            } else if (map[prop].type == 'asteroid') {
                hasAsteroid = true;
                asteroidCount++;
            }
        }
        
        // If this sector has an asteroid, don't do anything.
        if (hasAsteroid && asteroidCount > (count || 1)) return;

        // Now randomly select an empty slot.
        var target = empty[Math.round(Math.random() * (empty.length - 1))];

        // It will become a new asteroid.
        gameContext.Map.setInfo(target.x, target.y, new Asteroid({ x: target.x, y: target.y, layer: asteroidCount % 5 }), function () {
            self.fireUpdateEvent(target.x, target.y);
            if (success) success.call();
        });
    });
};

AsteroidManager.prototype.abandon = function (x, y, success) {
    var gameContext = this.gameContext;
    this.getAsteroid(x, y, function (objAsteroid) {
        if (objAsteroid.x !== x || objAsteroid.y !== y) {
            if (success) success.call();
            return;
        } else {
            objAsteroid.defenderPlayer = null;
            objAsteroid.defenderFleet = null;
            gameContext.AsteroidManager.updateAsteroid(x, y, objAsteroid, success);
        }
    });
};

// Destroy the asteroid. Return it to empty space.
AsteroidManager.prototype.destroy = function (x, y, success) {
    var gameContext = this.gameContext;
    var self = this;
    this.getAsteroid(x, y, function (asteroid) {
        gameContext.Map.setInfo(asteroid.x, asteroid.y, { type: 'space', x: asteroid.x, y: asteroid.y }, function () {
            if (asteroid.defenderPlayer) {
                gameContext.SocketManager.updateMap(asteroid.defenderPlayer);
            }
            self.fireUpdateEvent(x, y);
            if (success) success.call();
        });       
    });
};

AsteroidManager.prototype.updateAsteroid = function (x, y, asteroid, success) {
    var gameContext = this.gameContext;
    gameContext.Map.setInfo(x, y, asteroid, success);
};

// Determine mass to subtract and then return that in the success callback.
AsteroidManager.prototype.mine = function (x, y, shipQty, user, success) {
    var gameContext = this.gameContext;
    var self = this;
    this.getAsteroid(x, y, function (asteroid) {
        var x = asteroid.x;
        var y = asteroid.y;
        var mass = asteroid.mass || 0;
        var hasRefinery = (asteroid.structures || []).indexOf('Refinery') >= 0;
        var hasQuantumRefinery = (asteroid.structures || []).indexOf('Quantum Refinery') >= 0;

        // Determine the amount to take.
        var sub = 0;
        // Iterate over each ship and simulate them all mining.
        for (var i = 0; i < shipQty; i++) {
            // The more ships you have, the less efficient some of them become.
            sub += Math.round((150 / (i + 1)) * Math.random());
        }
        if (sub > mass) sub = mass;

        // Subtract.
        gameContext.Map.updateProperty(x, y, "mass", mass - sub, function () {
            var destroyed = false;
            if (mass - sub <= 0) {
                // Need to destroy the asteroid.
                self.destroy(x, y, function () {
                    gameContext.SocketManager.refreshMap(asteroid.defenderPlayer, gameContext.Map.getQuadrant(x, y));
                });
                destroyed = true;
            }
            
            var totalYield = 0;

            // Calculate the total yield based on any modifiers.
            if (hasRefinery) totalYield = sub * 5;
            else totalYield = sub * 3;
                        
            // Check if they have a quantum refinery.
            if (hasQuantumRefinery) totalYield *= 2;

            self.fireUpdateEvent(x, y);
            if (success) success.call(this, asteroid.resource, totalYield, destroyed);
        });
    });
};

AsteroidManager.prototype.deployStructure = function (x, y, name, user, success) {
    var gameContext = this.gameContext;
    var self = this;
    this.getAsteroid(x, y, function (asteroid) {
        var target = self.find(name);
        if (target) {
            asteroid.structures = asteroid.structures || [];
            asteroid.structures.push(target.name);

            if (name == "Booby Trap") {
                gameContext.Map.updateProperty(asteroid.x, asteroid.y, "trapper", user, function () {
                    gameContext.Map.pushProperty(asteroid.x, asteroid.y, "structures", target.name, success);
                });
            } else {
                gameContext.Map.pushProperty(asteroid.x, asteroid.y, "structures", target.name, success);
            }
        }
    });
};

AsteroidManager.prototype.recall = function (x, y, user, success, failure) {
    var gameContext = this.gameContext;
    var self = this;
    this.getAsteroid(x, y, function (asteroid) {
        // Make sure the user calling recall actually owns the ships!
        if (asteroid.protector == user) {
            // Iterate over the ships.
            var mission = y + '-' + x;
            
            // Return the defense ships.
            gameContext.ShipManager.deployFleet('return', mission, asteroid.ships, x, y, asteroid.protector, function () {
                
                // Update the properties about this asteroid.
                gameContext.Map.updateProperty(x, y, "ships", [], function () {
                    gameContext.Map.updateProperty(x, y, "protector", null, function () {
                        if (success) success.call();
                    });
                });
            });
        } else {
            if (failure) failure.call();
        }
    });
};

AsteroidManager.prototype.protect = function (x, y, user, ships, success) {
    var gameContext = this.gameContext;
    var self = this;
    this.getAsteroid(x, y, function (asteroid) {
        // First, check if someone is already protecting this asteroid.
        if (asteroid.protector) { 
            // Check if it's us?
            if (asteroid.protector == user) return;
            
            // Check if this has the auto cannon upgrade.
            var hasAutoCannon = (asteroid.structures || []).indexOf('Auto Cannon') >= 0;
            // Otherwise... we need to space combat.
            var sector = gameContext.Map.getQuadrant(x, y);
            var player2Ships = asteroid.ships || [];
            var player1Ships = ships;
            
            // If there is an auto cannon, add to the ships.
            if (hasAutoCannon) {
                player2Ships.push(['Warbird', 5]);
            }

            gameContext.ShipManager.combat(sector, user, player1Ships, asteroid.protector, player2Ships, y + "-" + x, function (winner, winnerShips) {
                // TODO: Fix this
                // Update the database.
                gameContext.Map.updateProperty(asteroid.x, asteroid.y, "ships", winnerShips, function () {
                    gameContext.Map.updateProperty(asteroid.x, asteroid.y, "protector", winner, function () {
                        
                        // Send the notifications.
                        if (winner == asteroid.protector) {
                            gameContext.UserManager.addNotification(winner, "yellow", "You have maintained control of the asteroid in sector " + sector, { x: asteroid.x, y: asteroid.y });
                        } else {
                            gameContext.UserManager.addNotification(winner, "green", "You have taken control of the asteroid in sector " + sector, { x: asteroid.x, y: asteroid.y });
                        }
                        
                        self.fireUpdateEvent(x, y);
                        // Invoke the callback.
                        if (success) success.call();
                    });
                });
            });
                
        } else {
            // Check for the booby trap.            
            var hasBoobyTrap = (asteroid.structures || []).indexOf('Booby Trap') >= 0;
            
            // If there is one, they're going to space combat with it.
            if (hasBoobyTrap) {
                var player1Ships = ships;
                var player2Ships = [['Warbird', 5]];
                gameContext.ShipManager.combat(sector, user, player1Ships, asteroid.trapper, player2Ships, "asteroid-" + sector, function (winner, winnerShips) {
                    // TODO: Fix this
                    // Take off the booby trap.
                    var index = asteroid.structures.indexOf('Booby Trap');
                    asteroid.structures.splice(index, 1);
                    
                    // Update the property.
                    gameContext.Map.updateProperty(asteroid.x, asteroid.y, "structures", asteroid.structures, function () {
                        // Do the thing.
                        if (winner != user) {
                            gameContext.UserManager.addNotification(user, "red", "The asteroid in sector " + gameContext.Map.getQuadrant(x, y) + " was booby trapped! You have lost all your ships.", { x: asteroid.x, y: asteroid.y });
                        } else {
                            // User won.
                            gameContext.UserManager.addNotification(user, "red", "The asteroid in sector " + gameContext.Map.getQuadrant(x, y) + " was booby trapped! However, you managed to secure the asteroid.");
                            gameContext.Map.updateProperty(asteroid.x, asteroid.y, "ships", winnerShips, function () {
                                gameContext.Map.updateProperty(asteroid.x, asteroid.y, "protector", user, function () {
                                    self.fireUpdateEvent(x, y);
                                    if (success) success.call();
                                });
                            });
                        }
                    });
                });
            } else {
                // Otherwise, we need to become the protector.
                gameContext.Map.updateProperty(asteroid.x, asteroid.y, "ships", ships, function () {
                    gameContext.Map.updateProperty(asteroid.x, asteroid.y, "protector", user, function () {
                        gameContext.UserManager.addNotification(user, "brown", "You have taken control of an asteroid in sector " + gameContext.Map.getQuadrant(x, y), { x: asteroid.x, y: asteroid.y });
                        self.fireUpdateEvent(x, y);
                        if (success) success.call();
                    });
                });
            }

        }

    });
};

AsteroidManager.prototype.init = function () {
    this.structures.push(new Structure({
        name: 'Refinery',
        description: 'Improve the yield of each mining expedition.',
        ironCost: 500
    }));
    
    this.structures.push(new Structure({
        name: 'Quantum Refinery',
        description: 'Top of the line technology can offer you a super high yield on resources gathered.',
        creditCost: 5
    }));

    this.structures.push(new Structure({
        name: 'Auto Cannon',
        description: 'Deploy a self maintaning array of auto cannons to improve defenses.',
        icon: 'fa-fighter-jet',
        ironCost: 1000
    }));

    this.structures.push(new Structure({
        name: 'Booby Trap',
        description: 'Leave a booby trap for future miners.',
        icon: 'fa-bomb',
        h3Cost: 2000
    }));

};

module.exports = AsteroidManager;