﻿var fs = require('fs');
var Logger = function (gameContext) {
    this.gameContext = gameContext;
    this.threadId = process.argv[3] || 'Master';
};

Logger.prototype.logEvent = function (evt, msg, ctx) {
    var data = {
        date: new Date(),
        severity: 'minor',
        event: evt,
        data: msg,
        ctx: ctx
    };
    fs.appendFile(__dirname + '/basic-thread[' + this.threadId + '].log', JSON.stringify(data) + "\n", function (err) { });
};

Logger.prototype.logError = function (evt, msg) {
    var data = {
        date: new Date(),
        severity: 'major',
        event: 'error',
        additional: msg || '',
        data: JSON.stringify(evt)
    };
    fs.appendFile(__dirname + '/basic-thread[' + this.threadId + '].log', JSON.stringify(data) + "\n", function (err) { });
};

module.exports = Logger;