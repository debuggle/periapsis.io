﻿var SocketManager = function (gameContext) {
    this.gameContext = gameContext;
    this.connections = {};
    this.active = {};

    // When a socket manager is created, we want to synchronize it across all threads.
    var sm = this;
    process.on('message', function (obj) {
        if (obj.user != undefined && obj.evt != undefined) {
            sm.emitSilent(obj.user, obj.evt, obj.arg1, obj.arg2);
        }
    });
};

SocketManager.prototype.emit = function (user, evt, arg1, arg2) {
    // Propagate this event to all worker threads.
    this.gameContext.QueueManager.emitToThreads({ user: user, evt: evt, arg1: arg1, arg2: arg2, thread: process.argv[3] || -1 });
    
    // Then process it on our current thread.
    this.emitSilent(user, evt, arg1, arg2);
};

SocketManager.prototype.emitSilent = function (user, evt, arg1, arg2) {
    if (this.connections.hasOwnProperty(user)) {
        this.connections[user].emit(evt, { x: arg1, y: arg2 });
    }
};

SocketManager.prototype.emitToAllSilent = function (user, evt, arg1, arg2) {
    for (var prop in this.connections) {
        this.emitSilent(prop, evt, { x: arg1, y: arg2 });
    }
};

SocketManager.prototype.bindClient = function (socket) {
    var self = this;
    socket.on('identify', function (msg) {
        try {
            var obj = JSON.parse(msg);
            if (obj.user) {
                self.connections[obj.user] = socket;
                self.updateNotifications(obj.user);
            }
        } catch (ex) { }
    });
};

SocketManager.prototype.takeOffline = function () {
    for (var prop in this.connections) {
        try {
            this.connections[prop].emit('offline');
        } catch (ex) { }
    }
};

SocketManager.prototype.updateGuild = function (user) {
    this.emit(user, 'updateGuild');
};

SocketManager.prototype.updateResources = function (user) {
    this.emit(user, 'updateResources');
}

SocketManager.prototype.updateAsteroid = function (user, x, y) {
    this.emit(user, 'updateAsteroid', x, y);
}

SocketManager.prototype.updateAnomaly = function (user, x, y) {
    this.emit(user, 'updateAnomaly', x, y);
}

SocketManager.prototype.updateNotifications = function (user) {
    this.emit(user, 'updateNotifications');
}

SocketManager.prototype.updateTradeAgreements = function (user) {
    this.emit(user, 'updateTrade');
}

SocketManager.prototype.updateShips = function (user) {
    this.emit(user, 'updateShips');
}

SocketManager.prototype.updatePlanet = function (user) {
    this.emit(user, 'updatePlanet');
}

SocketManager.prototype.updateQuests = function (user) {
    this.emit(user, 'updateQuests');
}

SocketManager.prototype.updateMap = function (user) {
    this.emit(user, 'updateMap');
}

SocketManager.prototype.relocated = function (user) {
    this.emit(user, 'relocated');
}

SocketManager.prototype.updateFleet = function (user) {
    this.emitToAllSilent(user, 'updateFleet');
    
    // Send to all threads.
    this.gameContext.QueueManager.emitToThreads({ user: user, evt: 'updateFleet', arg1: null, arg2: null, thread: process.argv[3] || -1 });
};

SocketManager.prototype.refreshMap = function (user, sector) {
    this.emit(user, 'refreshMap', sector);
}

SocketManager.prototype.disconnect = function (socket) {
    for (var prop in this.connections) {
        if (this.connections[prop] == socket) {
            delete this.connections[prop];
            return;
        }
    }
};

module.exports = SocketManager;