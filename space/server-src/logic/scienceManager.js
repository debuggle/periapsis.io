﻿var Anomaly = require('../models/anomaly'),
    Building = require('../models/building');

var ScienceManager = function (gameContext) {
    this.gameContext = gameContext;
    this.structures = [];
};

ScienceManager.prototype.getStructures = function () {
    return this.structures;
};

ScienceManager.prototype.findStructure = function (name) {
    for (var i = 0; i < this.structures.length; i++) {
        if (this.structures[i].type == name) {
            return this.structures[i];
        }
    }
    return null;
};

ScienceManager.prototype.updateAnomaly = function (x, y, property, value, success) {
    this.gameContext.Map.updateProperty(x, y, property, value, success);
};

ScienceManager.prototype.push = function (x, y, property, value, success, failure) {
    this.gameContext.Map.pushProperty(x, y, property, value, success, failure);
};

// Decay is seconds until dissipation.
ScienceManager.prototype.spawnAnomaly = function (sector, decay, success) {
    var gameContext = this.gameContext;
    gameContext.Map.scanSector(sector, function (map) {
        // Find some space tiles.
        var space = [];
        var users = [];
        var hasAnomaly = false;

        for (var prop in map) {
            if (map[prop].type == 'space')
                space.push(map[prop]);
            if (map[prop].owner)
                users.push(map[prop].owner);
            if (map[prop].type == 'anomaly')
                hasAnomaly = true;
        }
        
        if (hasAnomaly) return;

        // Find a random one.
        if (space.length > 0) {
            var target = space[Math.round(Math.random() * (space.length - 1))];
            gameContext.Map.setInfo(target.x, target.y, new Anomaly({ sector: sector, x: target.x, y: target.y, decay: decay }), function () {
                if (success) success();
                for (var i = 0; i < users.length; i++) {
                    gameContext.SocketManager.updateMap(users[i]);
                }
            });
        }
    });
};

// ships = [[shipName,qty]]
ScienceManager.prototype.protectAnomaly = function (sector, user, ships, success, failure) {
    var gameContext = this.gameContext;
    gameContext.Map.scanSector(sector, function (map) {
        // Get the users who own stuff in this sector.
        var users = [];
        for (var prop in map) {
            if (map[prop].owner || map[prop].protector)
                users.push(map[prop].owner || map[prop].protector);
        }

        // Get the anomaly and then update the protector.
        for (var prop in map) {
            if (map[prop].type == 'anomaly') {
                if (map[prop].protector && map[prop].protector !== user) {
                    // BATTLE!
                    gameContext.ShipManager.combat(map[prop].protector, map[prop].fleet, user, ships, "protect-" + sector, function (winner, remainingFleet) {
                        gameContext.Map.updateProperty(map[prop].x, map[prop].y, "fleet", remainingFleet, function () {
                            gameContext.Map.updateProperty(map[prop].x, map[prop].y, "protector", winner, function () {
                                if (winner != map[prop].protector) {
                                    gameContext.UserManager.addNotification(winner, "purple", "You have taken control of the anomaly.");
                                    gameContext.UserManager.addNotification(map[prop].protector, "red", "You have lost control of the anomaly.");
                                    
                                    // Destroy any mythos currently deployed.
                                    gameContext.ShipManager.getUserDeployedShips(map[prop].protector, function (ships) {
                                        var destroyed = 0;
                                        for (var i = 0; i < ships.length; i++) {
                                            if (ships[i][0] == "Mythos" && ships[i][2] == "mythos-" + sector) {
                                                gameContext.ShipManager.killShip('Mythos', true, "mythos-" + sector, map[prop].protector);
                                                destroyed++;
                                            }
                                        }
                                        if (destroyed > 0) {
                                            gameContext.UserManager.addNotification(map[prop].protector, "red", "You have lost " + destroyed + " Mythos ships in the battle for the anomaly in sector " + sector + " while fighting " + winner);
                                        }
                                    });

                                    for (var i = 0; i < users.length; i++) {
                                        gameContext.SocketManager.updateAnomaly(users[i], map[prop].x, map[prop].y);
                                        //gameContext.SocketManager.updateShips(users[i]);
                                    }                                    
                                } else {
                                    gameContext.UserManager.addNotification(winner, "purple", "You maintain control of the anomaly.");
                                }
                                if (success) success.call();
                            });
                        });
                    });
                } else {
                    gameContext.Map.updateProperty(map[prop].x, map[prop].y, "fleet", ships, function () {
                        gameContext.Map.updateProperty(map[prop].x, map[prop].y, "protector", user, function () {
                            gameContext.UserManager.addNotification(user, "purple", "You have taken control of the anomaly.");
                            if (success) success.call();
                            for (var i = 0; i < users.length; i++) {
                                gameContext.SocketManager.updateAnomaly(users[i], map[prop].x, map[prop].y);
                                //gameContext.SocketManager.updateShips(users[i]);
                            }
                        });
                    });
                }
                return;
            }
        }

    });
};

ScienceManager.prototype.stabilizeAnomaly = function (sector, success, failure) {
    var gameContext = this.gameContext;
    gameContext.Map.scanSector(sector, function (map) {
        var users = [];
        for (var prop in map) {
            if (map[prop].owner)
                users.push(map[prop].owner);
        }

        for (var prop in map) {
            if (map[prop].type == 'anomaly') {
                // This is an anomaly, so we need to stabilize it.
                gameContext.Map.updateProperty(map[prop].x, map[prop].y, "decay", gameContext.now() + gameContext.getSettings(1).anomalyStabilizeTotal * 1000, function () {
                    for (var i = 0; i < users.length; i++) {
                        gameContext.SocketManager.updateMap(users[i]);
                        gameContext.SocketManager.updatePlanet(users[i]);
                    }
                    if (success) success.call();
                });
                return;
            }
        }

        if (failure) failure.call();
    });
};

ScienceManager.prototype.destroyAnomaly = function (sector, success, failure) {
    var gameContext = this.gameContext;
    var failed = false;

    gameContext.Map.scanSector(sector, function (map) {
        var users = [];
        var props = [];
        var remaining = 0;

        for (var prop in map) {
            props.push(prop);
            if (map[prop].owner)
                users.push(map[prop].owner);
        }
        
        function next(index, map, props) {
            if (index > props.length - 1) {
                if (success) success.call();
                for (var i = 0; i < users.length; i++)
                    gameContext.SocketManager.updateMap(users[i]);
                return;
            }
            
            var prop = props[index];
            if ( !map[prop] || map[prop].type !== 'anomaly') {
                next(index + 1, map, props);
            } else {
                var time = gameContext.now();
                if (map[prop].decay && map[prop].decay > time) {
                    // If it hasn't decayed yet, calculate when it should decay and then send that to the failure function.
                    failed = true;
                    remaining = (map[prop].decay - time) / 1000;
                    return;
                }
                
                // Send the fleet home.
                if (map[prop].protector && map[prop].fleet) {
                    gameContext.ShipManager.deployFleet('return', "protect-" + sector, map[prop].fleet || [], 0, 0, map[prop].protector, function () {
                        //gameContext.SocketManager.updateShips(map[prop].protector);
                    });
                }

                // See if they have any deployed Mythos.
                gameContext.ShipManager.getUserDeployedShips(map[prop].protector, function (mythos) {
                    var returning = 0;
                    for (var i = 0; i < mythos.length; i++) {
                        if (mythos[i][0] == "Mythos" && mythos[i][2] == "mythos-" + sector) {
                            returning += parseFloat(mythos[i][1] || 1);
                        }
                    }
                    // If we have any to return, do so.
                    if (returning > 0) {
                        gameContext.ShipManager.deployFleet('return', 'mythos-' + sector, [['Mythos', returning]], 0, 0, map[prop].protector);
                    }
                });

                gameContext.Map.setInfo(map[prop].x, map[prop].y, { type: 'space', x: map[prop].x, y: map[prop].y }, function () {
                    next(index + 1, map, props);
                });
            }
        }

        next(0, map, props);
        if (failed) {
            if (failure) failure.call(this, remaining);
        }
    });
};

ScienceManager.prototype.init = function () {
    this.structures.push({ type: 'Gasseous Converter', h3Cost: 1000, reward: 500, description: 'Analyze the effects of h3 relative to this scientific phenomena. (Trade H3 for Science)' });
    this.structures.push({ type: 'Iron Matter Converter', ironCost: 1000, reward: 500, description: 'Investigate how iron interacts with this scientific phenomena. (Trade Iron for Science)' });
    this.structures.push({ type: 'Scientific Method', goldCost: 1000, reward: 500, description: 'Throw tons of scientists at this thing to determine what is going on. (Trade Gold for Science)' });
};

module.exports = ScienceManager;