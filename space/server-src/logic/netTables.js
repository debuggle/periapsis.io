﻿// NetTables will be a distributed data structure that operates
// across threads.
var NetTable = function (gameContext) {
    this.gameContext = gameContext;
    this.data = {};
    var self = this;
    process.on('message', function (obj) {
        if (obj.tablefield && obj.tablevalue) {
            self.data[obj.tablefield] = obj.tablevalue;
        }
    });
};

NetTable.prototype.set = function (field, value, supress) {
    var packet = {
        tablefield: field,
        tablevalue: value
    };

    this.data[field] = value;

    if (!supress)
        this.gameContext.QueueManager.emitToThreads(packet);
};

NetTable.prototype.get = function (field) {
    return this.data[field];
};

module.exports = NetTable;