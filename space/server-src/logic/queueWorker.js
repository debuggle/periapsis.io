﻿var storage = require('azure-storage'),
    uuid = require('node-uuid'),

    // Context
    gameSystemContext = require('../context');

function isNull(obj) {
    return obj === undefined || obj === null;
};

var queue_name = 'periapsis-task';
if (process.argv[2] == 'DEBUG') {
    queue_name += '-debug';
}

// Initialize the queueWorker
QueueWorker = function (ctx) {
    this.service = storage.createQueueService(
        'periapsisdata', 
        'n2ypat5e9vZpSmhYEpFHTlWwqOJmioX+MHqjsfbOCjls4A4kaq/iXZXPLxn8yzpu7VfrqGXKY43iVOaRR1NbPQ==', 
        'https://periapsisdata.queue.core.windows.net/');
    
    var self = this;
    this.gameContext = new gameSystemContext(function () {
        self.init();
    });
    this.generation = uuid.v1();
    this.started = false;
    this.actions = {};
    
    process.on('message', function (obj) {
        if (obj.jumpstart) {
            self.gameContext.Logger.logEvent("jumpstarted thread");
            self.processEvent();
        }
    });
};

QueueWorker.prototype.registerAction = function (title, action, callback) {
    this.actions[title] = action;
};

QueueWorker.prototype.invoke = function (action, obj, done, mid, prc) {
    if (this.actions[action]) {
        try {
            this.actions[action].call(this, obj, function () {
                if (done) done.call(this, mid, prc);
            });
        } catch (ex) {
            this.gameContext.Logger.logError(ex, "Failed invoking " + action);
        }
    } else {
        this.gameContext.Logger.logEvent("missing queue action", action);
        if (done) done.call(this, mid, prc);
    }
};

QueueWorker.prototype.processEvent = function () {
    var self = this;
    var Logger = this.gameContext.Logger;
    
    function ping(action, obj) {
        if (process.send) {
            process.send({
                ping: true,
                action: action,
                threadId: process.argv[3] || -1,
                ctx: JSON.stringify(obj)
            });
        }
    }

    self.service.getMessages(queue_name, { numOfMessages: 32 }, function (error, result, response) {
        if (!error) {
            var complete = false;
            function next(index) {
                if (index >= result.length) {
                    ping();
                    complete = true;
                    self.processEvent();
                    return;
                }
                
                // Process the job.
                var msg = result[index];
                var obj = JSON.parse(msg.messagetext);
                ping(obj.action, obj);
                try {
                    self.invoke(obj.action, obj.obj, function (messageid, popreceipt) {
                        self.service.deleteMessage(queue_name, messageid, popreceipt, function (er) {
                            next(index + 1);
                        });
                    }, msg.messageid, msg.popreceipt);
                } catch (ex) {
                    Logger.logError(ex);
                    next(index + 1);
                }
            }

            next(0);
        } else {
            self.processEvent();
        }
    });   
};

QueueWorker.prototype.init = function () {
    // Register all queue handling methods.
    var gameContext = this.gameContext;
    var queue = this;
    
    // OBJ should have 
    // x - coord of planet.
    // y - coord of planet.
    // fleet - holding the terraformer.
    // user - of new owner.
    this.registerAction('colonize', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.x)) return;
        if (isNull(obj.y)) return;
        if (isNull(obj.user)) return;
        if (isNull(obj.fleet)) return;

        gameContext.Map.changeOwner(obj.x, obj.y, obj.user, function () {
            gameContext.VictoryManager.fireEvent(obj.user, "colonized", 1);
            gameContext.SocketManager.refreshMap(obj.user, gameContext.Map.getQuadrant(obj.x, obj.y));
            done();
        });
    });
    
    // OBJ should have
    // fleet - name
    // user - name
    // x - destination x
    // y - destination y
    // guid - of the fleet object originally.
    this.registerAction('moveFleet', function (obj, done) {
        
        // Invalid object.
        if (!obj || isNull(obj.x) || isNull(obj.y) || isNull(obj.fleet) || isNull(obj.user) || isNull(obj.guid)) {
            done();
            return;    
        }
        
        // Get the fleet.
        gameContext.FleetManager.getFleet(obj.user, obj.fleet, function (fleet) {
            // Check the guid. if it matches, we can update this thing.
            if (fleet.guid != obj.guid) {
                done();
                return;
            }

            // Update the coordinates.
            fleet.x = obj.x;
            fleet.y = obj.y;
            fleet.dx = obj.x;
            fleet.dy = obj.y;
            fleet.sector = gameContext.Map.getQuadrant(Math.round(obj.x / 80), Math.round(obj.y / 80));

            // Create a new guid so any other movement operations will be discarded automatically.
            fleet.guid = uuid.v1();
            gameContext.FleetManager.updateFleet(obj.user, obj.fleet, fleet, function () {
                // Tell the player to update the map.
                gameContext.SocketManager.updateFleet(obj.user);

                // If we have an action, then preform it.
                if ((obj.action || '').length > 0) {
                    gameContext.QueueManager.enqueue(obj.action, obj.actionObj || obj, 1, function () {
                        done();
                    });
                } else {
                    done();
                }
            });
        }, function () {
            done(); 
        });
    });
    
    this.registerAction('fleetDeployTerraformer', function (obj, done) {
        if (!obj || isNull(obj.user) || isNull(obj.fleet) || isNull(obj.x) || isNull(obj.y)) {
            done();
            return;
        }
        
        var user = obj.user;
        var fleet = obj.fleet;
        var x = obj.x;
        var y = obj.y;

        // Remove the terraformer.
        gameContext.FleetManager.getFleet(user, fleet, function (objFleet) {
            // Verify they have a terraformer.
            var proceed = false;
            for (var i = 0; i < objFleet.ships.length; i++) {
                if (objFleet.ships[i][0] == "Terraformer") {
                    // Here's one!
                    if (parseFloat(objFleet.ships[i][1]) > 0) {
                        objFleet.ships[i][1] = parseFloat(objFleet.ships[i][1]) - 1;
                        proceed = true;
                    }
                }
            }

            if (!proceed) {
                done();
            } else {
                // We can proceed.
                gameContext.FleetManager.updateFleet(user, fleet, objFleet, function () {
                    gameContext.SocketManager.updateFleet(user);
                    gameContext.Map.getInfo(obj.x, obj.y, function (planet) {
                        // We have subtracted the terraformer. Now we can begin the colonization action.
                        gameContext.TimerManager.spawn("colonize", user, gameContext.getSettings(1).colonizeTime, "Terraforming the planet " + planet.name + " in sector " + gameContext.Map.getQuadrant(x, y) + ".", "yellow", { x: x, y: y });
                        gameContext.QueueManager.enqueue("colonize", obj, gameContext.getSettings(1).colonizeTime, function () {
                            done();
                        });
                    });
                }, function () {
                    done();         
                });
            }

        }, function () {
            done();
        });
    });
    
    // OBJ should have
    // attackingUser
    // defendingUser
    // attackingFleet
    // defendingFleet
    // interceptX
    // interceptY
    this.registerAction('fleetCombat', function (obj, done) {
        if (!obj || isNull(obj.attackingUser) || isNull(obj.defendingUser) || isNull(obj.attackingFleet) || isNull(obj.defendingFleet) || isNull(obj.interceptX) || isNull(obj.interceptY)) {
            done();
            return;
        }
        
        var attackingUser = obj.attackingUser;
        var attackingFleet = obj.attackingFleet;
        var defendingUser = obj.defendingUser;
        var defendingFleet = obj.defendingFleet;
        var interceptX = obj.interceptX;
        var interceptY = obj.interceptY;
        
        // Get the sector this is happening in.
        var scoords = gameContext.FleetManager.fleetToMap(interceptX, interceptY);
        var sector = gameContext.Map.getQuadrant(scoords[0] * 10, scoords[1] * 10);

        // Otherwise: TO WAR!
        gameContext.FleetManager.getFleet(attackingUser, attackingFleet, function (objFleetAttacking) {
            gameContext.FleetManager.getFleet(defendingUser, defendingFleet, function (objFleetDefending) {

                // Update some properties.
                objFleetAttacking.war = false;
                objFleetDefending.war = false;

                // Commit the changes, then calculate battle.
                gameContext.FleetManager.updateFleet(attackingUser, attackingFleet, objFleetAttacking, function () {
                    gameContext.FleetManager.updateFleet(defendingUser, defendingFleet, objFleetDefending, function () {
                        // Now that the war status is updated. We can calculate the damage.
                        gameContext.ShipManager.combat(sector, attackingUser, attackingFleet, defendingUser, defendingFleet, function (winningUser, winningFleet) {
                            // Check if someone conquered a planet.
                            if (winningUser == attackingUser) {
                                if (objFleetDefending.planetX && objFleetDefending.planetY) {
                                    // It looks like someone gets a new planet!
                                    gameContext.ShipManager.combatPlanet(objFleetDefending.planetX, objFleetDefending.planetY, attackingUser, attackingFleet, function (won, shields) {
                                        if (won) {
                                            gameContext.FleetManager.capturePlanet(attackingUser, attackingFleet, defendingUser, objFleetDefending.planetX, objFleetDefending.planetY);
                                        } else {
                                            gameContext.UserManager.addNotification(attackingUser, "red", "You have failed to capture the planet in sector " + sector + ", however, you brought their shields down to " + shields + "%");
                                        }
                                    });
                                }
                            } else {
                                if (objFleetDefending.planetX && objFleetDefending.planetY) {
                                    gameContext.UserManager.addNotification(attackingUser, "red", "You have failed to capture the planet in sector " + sector + ".");
                                    gameContext.UserManager.addNotification(defendingUser, "blue", "A planet in sector " + sector + " has been attacked, however, you have mainted control of it.", { x: objFleetDefending.planetX, y: objFleetDefending.planetY });
                                }
                            }
                        });
                    }, function () {
                        done();                                       
                    });
                }, function () {
                    done();                       
                });
            }, function () {
                done();           
            });
        }, function () {
            done();   
        });
    });
    
    // OBJ should have
    // user
    // x of the planet to defend
    // y of the planet to defend
    // fleet doing the defending
    this.registerAction('defendPlanet', function (obj, done) {
        if (!obj || isNull(obj.x) || isNull(obj.y) || isNull(obj.user) || isNull(obj.fleet)) {
            done();
            return;
        }

        var x = obj.x;
        var y = obj.y;
        var user = obj.user;
        var fleet = obj.fleet;
        gameContext.Map.getInfo(x, y, function (objPlanet) {
            if (objPlanet.owner == user) {
                gameContext.Map.updateProperty(x, y, "defendingFleet", fleet, function () {
                    gameContext.SocketManager.updateMap(user);
                    // Check if they already had one that might have gotten overwritten.
                    if (objPlanet.defendingFleet) {
                        // Update the fleet.
                        gameContext.FleetManager.putProperty(user, objPlanet.defendingFleet, "planetX", null, function () {
                            gameContext.FleetManager.putProperty(user, objPlanet.defendingFleet, "planetY", null, function () {
                                // yay we have officially abandoned.
                            });                        
                        });
                    }
                    
                    // Update the fleet in question and tell them where they're defending.
                    gameContext.FleetManager.putProperty(user, fleet, "planetX", x, function () {
                        gameContext.FleetManager.putProperty(user, fleet, "planetY", y, function () {
                            gameContext.SocketManager.updateFleet(user);
                            gameContext.SocketManager.refreshMap(user, gameContext.Map.getQuadrant(x, y));
                            done();
                        });
                    });
                }, function () {
                    done();
                });
            }
        });
    });
    
    // OBJ should have
    // user
    // fleet1
    // fleet2
    this.registerAction('fleetMerge', function (obj, done) {
        if (!obj || isNull(obj.user) || isNull(obj.fleet1) || isNull(obj.fleet2)) {
            done();
            return;
        }

        gameContext.FleetManager.merge(obj.user, obj.fleet1, obj.fleet2, function () {
            gameContext.FleetManager.getFleet(obj.user, obj.fleet2, function (objFleet) {
                gameContext.SocketManager.updateFleet(objFleet.user);
                var coords = gameContext.FleetManager.fleetToMap(objFleet.x, objFleet.y);
                gameContext.SocketManager.refreshMap(objFleet.user, gameContext.Map.getQuadrant(coords[0] * 10, coords[1] * 10));
            });
        });
        done();
    });

    // OBJ should have
    // user
    // x of the planet to defend
    // y of the planet to defend
    // fleet doing the defending
    this.registerAction('capturePlanet', function (obj, done) {
        if (!obj || isNull(obj.x) || isNull(obj.y) || isNull(obj.user) || isNull(obj.fleet)) {
            done();
            return;
        }
        
        var x = obj.x;
        var y = obj.y;
        var user = obj.user;
        var fleet = obj.fleet;
        var sector = gameContext.Map.getQuadrant(x, y);

        // Check if the planet is currently being defended.
        gameContext.Map.getInfo(x, y, function (planet) {
            if (planet.defendingFleet) {
                // Oops, they are. Time for combat.
                var combatObj = {
                    attackingUser: user,
                    attackingFleet: fleet,
                    defendingUser: planet.owner,
                    defendingFleet: planet.defendingFleet,
                    
                    // These aren't used yet (probably never will be).
                    interceptX: 0,
                    interceptY: 0
                };

                gameContext.QueueManager.enqueue("fleetCombat", combatObj, 0, function () {
                    done();
                });
            } else {
                // We get the planet!
                gameContext.ShipManager.combatPlanet(x, y, user, fleet, function (won, shields) {
                    if (won) {
                        gameContext.FleetManager.capturePlanet(user, fleet, planet.owner, x, y);
                        done();
                    } else {
                        gameContext.UserManager.addNotification(user, "red", "You have lost your fleet attacking the planet " + planet.name + " in sector " + sector + ", however, you brought their shields down to " + shields + "%", { x: x, y: y });
                        done();
                    }
                });
            }
        });
    });

    // OBJ should have
    // user
    // x of the sector
    // y of the sector
    this.registerAction('revealSector', function (obj, done) {
        if (!obj || isNull(obj.x) || isNull(obj.y) || isNull(obj.user)) {
            done();
            return;
        };

        gameContext.UserManager.getUser(obj.user, function (objUser) {
            objUser.explored= objUser.explored || {};
            objUser.explored[gameContext.Map.getQuadrant(obj.x, obj.y)] = true;
            gameContext.UserManager.putValue(obj.user, "explored", objUser.explored, function () {
                gameContext.SocketManager.updateMap(obj.user);
                gameContext.SocketManager.updateFleet(obj.user);
                done();
            });
        }, function () {
            done(); 
        });
    });

    // OBJ should have
    // user
    // fleet protecting asteroid
    // x of asteroid
    // y of asteroid
    this.registerAction("defendAsteroid", function (obj, done) {
        if (!obj || isNull(obj.user) || isNull(obj.fleet) || isNull(obj.x) || isNull(obj.y)) {
            done();
            return;
        }
        
        var x = obj.x;
        var y = obj.y;
        var user = obj.user;
        var fleet = obj.fleet;

        // We simply need to mark the asteroid as protected
        // and specify which fleet is doing the protecting.
        gameContext.AsteroidManager.getAsteroid(x, y, function (asteroid) {
            
            // TODO: Check if someone is already defending this asteroid.

            asteroid.defenderPlayer = user;
            asteroid.defenderFleet = fleet;
            gameContext.AsteroidManager.updateAsteroid(x, y, asteroid, function () {
                
                // Update the fleet.
                gameContext.FleetManager.getFleet(user, fleet, function (objFleet) {
                    objFleet.asteroidX = x;
                    objFleet.asteroidY = y;
                    gameContext.FleetManager.updateFleet(user, fleet, objFleet, function () {
                        var timeEnd = gameContext.getSettings(1).asteroidMine1 * 1000;
                        gameContext.Map.updateProperty(x, y, "mineTimer", gameContext.now() + timeEnd, function () {
                            gameContext.QueueManager.enqueue("mineAsteroidContinuous", obj, gameContext.getSettings(1).asteroidMine1, function () {
                                gameContext.SocketManager.refreshMap(user, gameContext.Map.getQuadrant(x, y));
                                done();
                            });
                        });
                    });
                }, function () {
                    gameContext.SocketManager.refreshMap(user, gameContext.Map.getQuadrant(x, y));
                    done();                     
                });
            });
        });
        
    });

    // OBJ should have
    // user - of target.
    // gold - to take away.
    // iron - to take away.
    // h3 - to take away.
    this.registerAction('debit', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.user)) return;
        
        gameContext.UserManager.debitResources(obj.user, obj.gold, obj.iron, obj.h3, 0);
        done();
    });
    
    this.registerAction('solarFlare', function (obj, done) {
        if (!obj || isNull(obj.x) || isNull(obj.y)) {
            done();
            return;
        }
        
        gameContext.CataclysmicManager.generateSolarFlare(obj.x, obj.y, true);
        done();
    });
    
    this.registerAction('spawnAnomaly', function (obj, done) {
        done();
    });

    // OBJ should have
    // x, y, asteroidCount
    this.registerAction('sectorHelper', function (obj, done) {
        if (!obj || isNull(obj.x) || isNull(obj.y) || isNull(obj.asteroidCount)) {
            done();
            return;
        }
        
        var x = parseFloat(obj.x);
        var y = parseFloat(obj.y);
        
        // Handle the anomaly.
        var decay = gameContext.getSettings(1).anomalyDecayTime * Math.random();
        var sector = gameContext.Map.getQuadrant(x, y);
        gameContext.ScienceManager.spawnAnomaly(sector, decay, function () {
            gameContext.QueueManager.enqueue('destroyAnomaly', { sector: sector }, decay);
        });
        
        // Handle the planetary regeneration.
        gameContext.Map.scan(x, y, function (map) {
            // Iterate over each section.
            var asteroids = 0;
            var target_asteroids = obj.asteroidCount;
            for (var prop in map) {
                var planet = map[prop];
                if (planet.isPlanet) {
                    // Check if the owner has a field manipulator.
                    function twodec(x) {
                        return Math.round(x * 100) / 100;
                    }
                        
                    var val = Math.max(Math.min(twodec(parseFloat(planet.shields) + 1), 100), 0);                        
                    if (parseFloat(planet.shields) < 100) {
                        gameContext.Map.updateProperty(planet.x, planet.y, "shields", val, function () {
                            gameContext.SocketManager.refreshMap(planet.owner, gameContext.Map.getQuadrant(planet.x, planet.y));         
                        });
                    }
                }
                // Track asteroids.
                if (planet.type == "asteroid") {
                    asteroids++;
                }
            }
            
            // Handle the asteroid stuff.
            obj["astskip"] = obj["astskip"] || 0;
            obj["ast"] = (obj["ast"] || 0) + 1.5;
            
            // If we've skipped enough
            if (obj["ast"] >= obj["astskip"] && asteroids < target_asteroids) {
                // Reset skip.
                obj["ast"] = 0;
                obj["astskip"] = obj["astskip"] + 1;

                // Spawn an asteroid.
                gameContext.AsteroidManager.spawnAsteroid(x, y, parseFloat(obj.asteroidCount));
            }

            // Iterate the pirates.
            obj["pirateGUID"] = obj["pirateGUID"] || uuid.v1();
            gameContext.PirateManager.iterate(map, obj["pirateGUID"]);
            
            // Respawn the helper thread.
            gameContext.QueueManager.enqueue("sectorHelper", obj, gameContext.getSettings(1).sectorHelperTime, function () {
                done();
            });

        });
    });
    
    // OBJ should have
    // x, y
    this.registerAction('spawnAsteroid', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.x)) return;
        if (isNull(obj.y)) return;
        
        gameContext.AsteroidManager.spawnAsteroid(obj.x, obj.y, obj.count || 2);
        // Keep calling this periodically.
        gameContext.QueueManager.enqueue('spawnAsteroid', obj, gameContext.getSettings(1).asteroidSpawnTime * Math.random(), function () {
            done();
        });
    });
    
    // OBJ should have
    // x, y, user, type
    this.registerAction('deployAsteroid', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.x)) return;
        if (isNull(obj.y)) return;
        if (isNull(obj.user)) return;
        if (isNull(obj.type)) return;
        
        done();
        gameContext.AsteroidManager.deployStructure(obj.x, obj.y, obj.type, obj.user, function () {
        });
    });
    
    // OBJ should have
    // x, y, user
    this.registerAction('mineAsteroidContinuous', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.x)) return;
        if (isNull(obj.y)) return;
        if (isNull(obj.user)) return;
        if (isNull(obj.fleet)) return;
        
        var x = obj.x;
        var y = obj.y;
        var user = obj.user;
        var fleet = obj.fleet;
        
        gameContext.AsteroidManager.getAsteroid(x, y, function (asteroid) {
            // Verify it is still protected.
            if (asteroid.defenderFleet == fleet) {
                gameContext.FleetManager.getFleet(user, fleet, function (objFleet) {
                    gameContext.TechManager.userHasTechs(user, ["Mining I", "Mining II", "Mining III"], function (techs) {
                        // Figure out all of the mining ships in the fleet and how much they're going to harvest.
                        var techMultiplier = 1.0,
                            ironMultiplier = 0.0,
                            goldMultiplier = 0.0,
                            h3Multiplier = 0.0,
                            miningShips = 0;
                        
                        for (var i = 0; i < objFleet.ships.length; i++) {
                            var shipName = objFleet.ships[i][0];
                            var qty = parseFloat(objFleet.ships[i][1]);
                            var ship = gameContext.ShipManager.find(shipName);
                            if (ship) {
                                ironMultiplier += ship.miningIronMultiplier * qty;
                                goldMultiplier += ship.miningGoldMultiplier * qty;
                                h3Multiplier += ship.miningH3Multiplier * qty;
                                miningShips += qty;
                            }
                        }
                        
                        // Determine the bonus for tech.
                        if (techs.indexOf("Mining III") >= 0) techMultiplier = 2.8;
                        else if (techs.indexOf("Mining II") >= 0) techMultiplier = 2.2;
                        else if (techs.indexOf("Mining I") >= 0) techMultiplier = 1.5;
                        
                        // Now preform the actual mining operation.
                        gameContext.AsteroidManager.mine(x, y, miningShips, user, function (resource, value, destroyed) {
                            var resources = {
                                gold: Math.round(value * (asteroid.goldModifier || 0.5) * goldMultiplier * techMultiplier),
                                iron: Math.round(value * (asteroid.ironModifier || 0.5) * ironMultiplier * techMultiplier),
                                h3: Math.round(value * (asteroid.h3Modifier || 0.5) * h3Multiplier * techMultiplier)
                            };
                            
                            var duration = gameContext.getSettings(1).asteroidMine1;

                            // Give the player their reward.
                            obj["iron"] = (obj["iron"] || 0) + resources.iron;
                            obj["gold"] = (obj["gold"] || 0) + resources.gold;
                            obj["h3"] = (obj["h3"] || 0) + resources.h3;
                            gameContext.UserManager.addResources(user, resources.iron, resources.gold, resources.h3, 0);
                            if (!destroyed) {
                                gameContext.Map.updateProperty(x, y, "mineTimer", gameContext.now() + (duration * 1000), function () {
                                    gameContext.QueueManager.enqueue('mineAsteroidContinuous', obj, duration, function () {
                                        done();
                                    });
                                }, function () {
                                    done();                                                                      
                                });
                            } else {
                                gameContext.UserManager.addNotification(user, "brown", "Your mining ships have finished harvesting " + obj["iron"] + " iron, " + obj["gold"] + " gold, " + obj["h3"] + " h3 from an asteroid in sector in " + gameContext.Map.getQuadrant(x, y) + ".", { x: x, y: y });
                                done();
                            }
                        });
                    });
                }, function () {
                    done();
                });
            }
        });
    });

    // OBJ should have
    // x, y, user
    this.registerAction('mineAsteroid', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.x)) return;
        if (isNull(obj.y)) return;
        if (isNull(obj.user)) return;
        if (isNull(obj.fleet)) return;
        
        var x = obj.x;
        var y = obj.y;
        var user = obj.user;
        var fleet = obj.fleet;
        
        gameContext.AsteroidManager.getAsteroid(x, y, function (asteroid) {
            gameContext.FleetManager.getFleet(user, fleet, function (objFleet) {
                gameContext.TechManager.userHasTechs(user, ["Mining I", "Mining II", "Mining III"], function (techs) {
                    // Figure out all of the mining ships in the fleet and how much they're going to harvest.
                    var techMultiplier = 1.0,
                        ironMultiplier = 0.0,
                        goldMultiplier = 0.0,
                        h3Multiplier = 0.0,
                        miningShips = 0;
                    
                    for (var i = 0; i < objFleet.ships.length; i++) {
                        var shipName = objFleet.ships[i][0];
                        var qty = parseFloat(objFleet.ships[i][1]);
                        var ship = gameContext.ShipManager.find(shipName);
                        if (ship) {
                            ironMultiplier += ship.miningIronMultiplier * qty;
                            goldMultiplier += ship.miningGoldMultiplier * qty;
                            h3Multiplier += ship.miningH3Multiplier * qty;
                            miningShips += qty;
                        }
                    }

                    // Determine the bonus for tech.
                    if (techs.indexOf("Mining III") >= 0) techMultiplier = 2.8;
                    else if (techs.indexOf("Mining II") >= 0) techMultiplier = 2.2;
                    else if (techs.indexOf("Mining I") >= 0) techMultiplier = 1.5;

                    // Now preform the actual mining operation.
                    gameContext.AsteroidManager.mine(x, y, miningShips, user, function (resource, value) {
                        var resources = {
                            gold: Math.round(value * (asteroid.goldModifier || 0.5) * goldMultiplier * techMultiplier),
                            iron: Math.round(value * (asteroid.ironModifier || 0.5) * ironMultiplier * techMultiplier),
                            h3: Math.round(value * (asteroid.h3Modifier || 0.5) * h3Multiplier * techMultiplier)
                        };

                        // Give the player their reward.
                        gameContext.UserManager.addResources(user, resources.iron, resources.gold, resources.h3, 0);
                        gameContext.UserManager.addNotification(obj.user, "brown", "You have gathered " + resources.gold + " gold, " + resources.iron + " iron, " + resources.h3 + " h3 from the asteroid in sector " + gameContext.Map.getQuadrant(obj.x, obj.y), { x: obj.x, y: obj.y });
                        done();
                    });
                });
            }, function () {
                done();         
            });
        });
    });
    
    // OBJ should have
    // x, y, user
    this.registerAction('recallAsteroid', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.x)) return;
        if (isNull(obj.y)) return;
        if (isNull(obj.user)) return;
        
        var x = obj.x;
        var y = obj.y;
        var user = obj.user;
        
        gameContext.AsteroidManager.recall(x, y, user, function () {
            gameContext.UserManager.addNotification(user, "brown", "You have successfully recalled the mining operation in sector " + gameContext.Map.getQuadrant(x, y).toString(), { x: x, y: y });
            done();
        }, function () {
            done();
        });
    });
    
    
    // OBJ should have
    // sector - to clear of anomalies.
    this.registerAction('destroyAnomaly', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.sector)) return;
        
        gameContext.ScienceManager.destroyAnomaly(obj.sector, function () {
            // Time to spawn a new one.
            done();
        }, function (remaining) {
            gameContext.QueueManager.enqueue('destroyAnomaly', { sector: obj.sector }, Math.max(remaining, 0));
            done();
        });
    });
    
    // OBJ should have
    // sector - to stabilize the anomaly in.
    // x, y
    this.registerAction('stabilizeAnomaly', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.sector)) return;
        if (isNull(obj.x)) return;
        if (isNull(obj.y)) return;
        
        gameContext.ScienceManager.stabilizeAnomaly(obj.sector, function () {
            gameContext.Map.getUsersInQuadrant(obj.x, obj.y, function (users) {
                for (var i = 0; i < users.length; i++) {
                    var user = users[i];
                    gameContext.SocketManager.updateNotifications(user);
                    gameContext.SocketManager.updateAnomaly(user, obj.x, obj.y);
                    done();
                }
            });
        });
    });
    
    // OBJ should have
    // x, y, ships, user
    this.registerAction('protectAnomaly', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.x)) return;
        if (isNull(obj.y)) return;
        if (isNull(obj.ships)) return;
        if (isNull(obj.user)) return;
        
        var x = obj.x;
        var y = obj.y;
        var ships = obj.ships;
        var user = obj.user;
        var sector = gameContext.Map.getQuadrant(x, y);
        gameContext.ScienceManager.protectAnomaly(sector, user, ships, function () {
            done();
        }, function () {
            done();
        });
    });
    
    // OBJ should have
    // x, y, type
    this.registerAction('deployAnomaly', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.x)) return;
        if (isNull(obj.y)) return;
        if (isNull(obj.type)) return;
        
        // The protector will get the resources.
        gameContext.Map.scan(obj.x, obj.y, function (map) {
            var index = obj.y + '-' + obj.x;
            if (map.hasOwnProperty(index)) {
                var user = map[index].protector;
                if (user) {
                    var structure = gameContext.ScienceManager.findStructure(obj.type);
                    if (structure) {
                        gameContext.UserManager.addResources(user, 0, 0, 0, structure.reward);
                        gameContext.ScienceManager.updateAnomaly(obj.x, obj.y, obj.type + "Action", null, function () {
                            gameContext.SocketManager.updateNotifications(user);
                            gameContext.SocketManager.updateAnomaly(user, obj.x, obj.y);
                            done();
                        });
                    } else {
                        done();
                    }
                } else {
                    done();
                }
            }
        });

    });
    
    // OBJ should have
    // x - coords of the building
    // y - coords of the building
    // type - of building
    // user - of the building   
    this.registerAction('construct', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.x)) return;
        if (isNull(obj.y)) return;
        if (isNull(obj.type)) return;
        if (isNull(obj.user)) return;
        
        var x = obj.x;
        var y = obj.y;
        var type = obj.type;
        var user = obj.user;
        
        var building = gameContext.BuildingManager.find(type);
        gameContext.Map.getInfo(x, y, function (world) {
            // Give the player a dock.
            if (building.spaceDock) {
                gameContext.UserManager.getUser(user, function (usr) {
                    usr.docks = usr.docks || [];
                    // Make sure the user doesn't already have this dock.
                    var has = false;
                    for (var i = 0; i < usr.docks.length; i++) {
                        if (usr.docks[i].x == x && usr.docks[i].y == y) {
                            has = true;
                            break;
                        }
                    }
                    if (!has) {
                        usr.docks.push({ x: x, y: y, name: world.name || '' });
                        gameContext.UserManager.putValue(user, "docks", usr.docks);
                    }
                });
            }
            
            // Update the interface to show they now have a space dock, if applicable.
            if (building.spaceDock)
                gameContext.SocketManager.updateShips(user);
            
            gameContext.Map.updateProperty(x, y, type + "Owned", 1, function () {
                gameContext.VictoryManager.fireEvent(user, "constructed", 1);
                gameContext.QueueManager.enqueue('allocate', { user: user, level: 1, x: x, y: y, type: type }, 0, function () {
                    done();
                });
            });
        });
    });
    
    // OBJ should have
    // x - coord of the planet
    // y - coord of the planet
    // type - of building
    // level - of the current building (to prevent hacking)
    this.registerAction('upgrade', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.x)) return;
        if (isNull(obj.y)) return;
        if (isNull(obj.type)) return;
        
        var x = obj.x;
        var y = obj.y;
        var type = obj.type;
        
        gameContext.Map.getInfo(x, y, function (world) {
            // Verify the level.
            var current = parseFloat(world[type + "Owned"] || 0);
            if (current != obj.level) {
                done();
                return;
            }
            
            // Update the information as expected.
            gameContext.Map.updateProperty(x, y, type + "Owned", current + 1, function () {
                gameContext.VictoryManager.fireEvent(world.owner, "upgraded", 1);
                gameContext.AdventureManager.processEvent(world.owner, { x: x, y: y });
                done();
            });
        });
    });
    
    // Apply a cataclysmic event occasionally.
    // OBJ should have
    // x - coord of sector
    // y - coord of sector
    this.registerAction("cataclysmicEvent", function (obj, done) {
        if (!obj || isNull(obj.x) || isNull(obj.y)) {
            done();
            return;
        }
        
        gameContext.CataclysmicManager.generateEvent(obj.x, obj.y, function () {
            gameContext.QueueManager.enqueue("cataclysmicEvent", obj, gameContext.getSettings(1).cataclysmicEvent);
        });
        done();
    });
    
    // Buy ships and allocate them to the database somewhere.
    // OBJ should have
    // x - coord of ship location
    // y - coord of ship location
    // name - of ship
    // quantity - of ships
    // user - purchasing
    this.registerAction('buyShips', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.x)) return;
        if (isNull(obj.y)) return;
        if (isNull(obj.name)) return;
        if (isNull(obj.quantity)) return;
        if (isNull(obj.user)) return;
        
        if (obj.name == "Quarry") { gameContext.VictoryManager.fireEvent(obj.user, "quarry", parseFloat(obj.quantity)); }
        gameContext.ShipManager.allocateNewShip(obj.name, parseFloat(obj.quantity), parseFloat(obj.x), parseFloat(obj.y), obj.user);
        done();
    });
    
    // Allocate resources to the user.
    // OBJ should have
    // type - of building
    // level - of building
    // user - to allocate towards
    // x - of the planet this action belongs to
    // y - of the planet this action belongs to
    this.registerAction('allocate', function (obj, done) {
        
        if (!obj || isNull(obj.type) || isNull(obj.level) || isNull(obj.user) || isNull(obj.x) || isNull(obj.y)) {
            done();
            return;
        }
        
        // Re-queue this event 10 minutes x level.
        gameContext.QueueManager.enqueue('allocate', obj, gameContext.getSettings(1).allocateTime, function () {
            done();
        });
        
        // Replace the user with the user who actually owns this planet.
        gameContext.Map.getInfo(obj.x, obj.y, function (planet) {
            obj.user = planet.owner;
            var level = parseFloat(planet[obj.type + "Owned"] || 1);
            var boost = 1.0;
            
            // Check for production boost.
            if ((planet.productionBoostEnd || 0) > gameContext.now()) {
                boost = 3.0;
            }
            
            // Get the building and then do the logic.
            var building = gameContext.BuildingManager.find(obj.type);
            gameContext.UserManager.addResources(obj.user, building.iron * level * boost, building.gold * level * boost, building.h3 * level * boost, building.science * level * boost);
        });
    });
    
    // Start some kind of mission involving a fleet.
    // OBJ should have
    // action - to take. attack / explore / etc
    // ships - an array containing [shipName, qty]
    // x - of target location
    // y - of target location
    // user - who owns the ships
    this.registerAction('startMission', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.action)) return;
        if (isNull(obj.ships)) return;
        if (isNull(obj.x)) return;
        if (isNull(obj.y)) return;
        if (isNull(obj.user)) return;
        
        if (obj.action == "attack") {
            var mission = uuid.v1();
            gameContext.ShipManager.deployFleet(obj.action, mission, obj.ships, obj.x, obj.y, obj.user, function (ships) {
                // Fix ships.
                obj.ships = ships;
                obj.mission = mission;
                gameContext.QueueManager.enqueue(obj.action, obj, gameContext.getSettings(1).attackTime);
                done();
            });
        } else {
            done();
        }
        
    });
    
    // Attack a world.
    // OBJ should have
    // ships - an array containing [shipName, qty]
    // x - of target location
    // y - of target location
    // user - who owns the ships
    this.registerAction('attack', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.x)) return;
        if (isNull(obj.y)) return;
        if (isNull(obj.user)) return;
        if (isNull(obj.ships)) return;
        if (isNull(obj.mission)) return;
        
        done();
        //gameContext.MissionManager.attackPlanet(obj.ships, obj.x, obj.y, obj.mission, obj.user, function (handled) {
        //    done();
        //    if (!handled)
        //        gameContext.QueueManager.enqueue('return', obj, 0);
        //    else
        //        gameContext.SocketManager.updateMap(obj.user);
        //});
    });
    
    // Return a fleet after some other action.
    // OBJ should have
    // ships - an array containing [shipName, qty]
    // x - of target location
    // y - of target location
    // user - who owns the ships.
    this.registerAction('return', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.x)) return;
        if (isNull(obj.y)) return;
        if (isNull(obj.user)) return;
        
        gameContext.ShipManager.deployFleet('return', obj.mission || null, obj.ships, obj.x, obj.y, obj.user, function () {
            gameContext.SocketManager.updateMap(obj.user);
            done();
        });
    });
    
    // Set a players homeworld.
    // OBJ should have
    // x - of the planet.
    // y - of the planet.
    // user - to change owner to.
    this.registerAction('setHome', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.x)) return;
        if (isNull(obj.y)) return;
        if (isNull(obj.user)) return;
        
        gameContext.Map.getInfo(obj.x, obj.y, function (planet) {
            planet.type = 'planet-home';
            planet.owner = obj.user;
            planet.home = true;
            
            gameContext.Map.updateProperty(obj.x, obj.y, "owner", obj.user, function () {
                gameContext.Map.updateProperty(obj.x, obj.y, "home", true, function () {
                    gameContext.setHomeQueue.push(obj.user);
                    done();
                });
            });
        });
    });

    // OBJ should have
    // x - coord of planet
    // y - coord of planet
    // user1 - involved in trade agreement
    // user2 - involved in trade agreement
    this.registerAction('tradeAgreement', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.x)) return;
        if (isNull(obj.y)) return;
        if (isNull(obj.user1)) return;
        if (isNull(obj.user2)) return;
        
        gameContext.DiplomacyManager.getTradeRoutes(obj.user1, function (routes) {
            for (var i = 0; i < routes.length; i++) {
                if (routes[i].x == obj.x && routes[i].y == obj.y) {
                    var target = false;
                    if (routes[i].user1 == obj.user1) {
                        if (routes[i].user2 == obj.user2) {
                            target = true;
                        }
                    } else if (routes[i].user1 == obj.user2) {
                        if (routes[i].user2 == obj.user1) {
                            target = true;
                        }
                    }
                    
                    // If we found it regardless of the order in which things were...
                    if (target) {
                        // Check if it's still accepted.
                        if (routes[i].accepted == true) {
                            
                            // Give each party some gold.
                            gameContext.UserManager.addResources(routes[i].user1, 0, 50, 0);
                            gameContext.UserManager.addResources(routes[i].user2, 0, 50, 0);
                            
                            // Re-up the agreement. This should naturally stop happening if the agreement is cancelled.
                            gameContext.QueueManager.enqueue('tradeAgreement', obj, gameContext.getSettings(1).tradeRouteTime);
                            done();
                        }
                        break;
                    }
                }
            }
        });
    });
    
    
    // OBJ should have
    // x - coord of planet
    // y - coord of planet
    // user - who is scanning
    this.registerAction('scan', function (obj, done) {
        if (!obj) return;
        if (isNull(obj.x)) return;
        if (isNull(obj.y)) return;
        if (isNull(obj.user)) return;
        
        var max = 0, target = 0, current = 0;
        var effect = "Cache of Resources";
        var results = {
            //"Ancient Ship": {
            //    chance: 0.25,
            //    text: 'You have discovered an ancient ship on planet {planet}',
            //    action: function (user) {
            //        gameContext.ShipManager.allocateNewShip("Ancient Capital Ship", 1, user.x, user.y, user.username);
            //    }
            //},
            //"Archeology": {
            //    chance: 0.5,
            //    text: 'You have discovered an important relic on planet {planet}',
            //    action: function () {
            
            //    }
            //},
            //"Discover Civilization": {
            //    chance: 5,
            //    text: 'You have discovered an active civilization on planet {planet}',
            //    action: function () {
            
            //    }
            //},
            "Cache of Resources": {
                chance: 40,
                action: function (user) {
                    var gold = Math.round(1500 * Math.random());
                    var iron = Math.round(1500 * Math.random());
                    var h3 = Math.round(1500 * Math.random());
                    
                    var txt = 'You have discovered {gold} gold, {iron} iron, and {h3} h3 during the scan of {planet}.';
                    txt = txt.replace("{gold}", gold);
                    txt = txt.replace("{iron}", iron);
                    txt = txt.replace("{h3}", h3);
                    
                    this.text = txt;
                    gameContext.UserManager.addResources(user.username || 'Pirates', iron, gold, h3);
                }
            }
        };
        
        for (var prop in results)
            max += results[prop].chance;
        
        var target = Math.random() * max;
        for (var prop in results) {
            current += results[prop].chance;
            if (current >= target) {
                effect = prop;
                break;
            }
        }
        
        var x = obj.x;
        var y = obj.y;
        var user = obj.user;
        
        // Now do the thing.
        gameContext.Map.getInfo(x, y, function (planet) {
            var targetEffect = results[effect];
            gameContext.UserManager.getUser(user, function (objUser) {
                objUser.scanAction = (objUser.scanAction || []);
                objUser.scanAction.push({ x: x, y: y });
                objUser.scanUnderway = false;
                
                // Invoke the target action. Also this should allow the method to udpate properties
                // and they will automatically get saved.
                targetEffect.action(objUser);
                
                // Save the object.
                gameContext.UserManager.putValue(user, "scanAction", objUser.scanAction, function () {
                    gameContext.UserManager.putValue(user, "scanUnderway", objUser.scanUnderway, function () {
                        var planetName = planet.name;
                        var msg = targetEffect.text.replace("{planet}", planetName);
                        // Update the player about the action.
                        gameContext.UserManager.addNotification(user, "blue", msg);
                        gameContext.SocketManager.updateMap(user);
                        done();
                    });
                });
            });
        });
    });

    this.processEvent();
};


// Instantiate a gameContext
var thread = new QueueWorker();
