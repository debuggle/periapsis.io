﻿MissionManager = function (gameContext) {
    this.gameContext = gameContext;
};

// Ships - array of arrays. [[shipType,qty]]
// x - of target
// y - of target
MissionManager.prototype.getAttackReport = function (ships, x, y, callback) {
    var gameContext = this.gameContext;
    var fleetPower = 0;
    var h3 = 0;
    var bonusModifier = 1.0;
    
    ships.forEach(function (item) {
        if (!item) return;
        
        var shipName = item[0];
        if (shipName == "Aurorae") {
            bonusModifier += 0.5;
        } else if (shipName == "Ancient Capital Ship") {
            bonusModifier += 0.75;
        } else if (shipName == "Sunzi") {
            bonusModifier += 1.5;
        }
    });
    
    // Calculate the fleetPower.
    ships.forEach(function (item) {
        if (!item) return;
        
        var shipName = item[0];
        var qty = item[1];
        var ship = gameContext.ShipManager.find(shipName);
        
        // Calculate the power.
        if (ship)
            fleetPower += (ship.attack * parseFloat(qty));
        
        // Calculate the fuel usage.
        if (ship)
            h3 += Math.round((10 * parseFloat(qty)) + ((ship.attack / 10) * parseFloat(qty)));
    });
    
    // Apply the bonus modifier.
    fleetPower *= bonusModifier;
    
    // Calculate the planet power.
    gameContext.Map.getInfo(x, y, function (planet) {
        planet = planet || {};
        gameContext.TechManager.userHasTechs(planet.owner, ["Improved Shields III", "Improved Shields II", "Improved Shields I"], function (shields) {
            
            // Get the defense modifier which is the percentage of shields.
            var defenseModifier = parseFloat(planet.shields || 100) / 100;

            // Add in the beam weapon defense.
            if (planet["Suborbital ArrayOwned"] && parseFloat(planet["Suborbital ArrayOwned"]) > 0) {
                defenseModifier += parseFloat(planet["Suborbital ArrayOwned"]);
            }
            
            // Significantly upgrade shields.
            if (shields.indexOf("Improved Shields III") >= 0) defenseModifier += 3;
            else if (shields.indexOf("Improved Shields II") >= 0) defenseModifier += 2;
            else if (shields.indexOf("Improved Shields I") >= 0) defenseModifier += 1;
            
            // Calculate how many "warbirds" the planet defense power is.
            var planetShips = [["Warbird", 157 * defenseModifier]];
            
            // Calculate probability.
            var probability = Math.round(gameContext.ShipManager.getCombatReport(ships, planetShips));
            
            // Create a report.
            var report = {
                planetShips: planetShips,
                name: planet.name,
                owner: planet.owner,
                fleetPower: fleetPower,
                planetDefense: defenseModifier,
                planetShields: planet.shields || 100,
                attackProbability: probability,
                h3: h3
            };
            
            // If there's a callback, invoke it with the report.
            if (callback) callback.call(this, report);            
        });
    });
};

// This function actually applies the attack damage.
// Ships - array of arrays. [[shipType,qty]]
// x - of target
// y - of target
// user - who is attacking
MissionManager.prototype.attackPlanet = function (ships, x, y, mission, user, callback, failure) {
    var gameContext = this.gameContext;
    
    // First, get the damage estimates and all that.
    this.getAttackReport(ships, x, y, function (report) {
        
        // Do the combat.
        gameContext.ShipManager.combat(report.owner, report.planetShips, user, ships, mission, function (winner, shipsRemaining) {
            
            // The callback is going to be the thread the clears up the queue,
            // so invoke it early on before any critical points of failure.            
            callback.call(this, true);

            if (winner == user) {
                // Return the ships.
                gameContext.ShipManager.deployFleet('return', mission, shipsRemaining, 0, 0, user);

                // They captured the planet.
                // Notify the outcome of the attack and then perform the capture.
                // Check if the new occupier has guerilla shield upgrade.
                gameContext.TechManager.userHasTechs(user, ["Guerilla Shields I", "Guerilla Shields II"], function (techs){
                    var shields = 0.15;
                    if (techs.indexOf("Guerilla Shields II") >= 0) shields = 50;
                    else if (techs.indexOf("Guerilla Shields I") >= 0) shields = 25;
                    gameContext.Map.updateProperty(x, y, "shields", shields, function () {
                        gameContext.Map.changeOwner(x, y, user, function () {
                            
                            // Get the target user.
                            gameContext.UserManager.getUser(report.owner, function (userObj) {
                                
                                var iron = userObj.iron,
                                    h3 = userObj.h3,
                                    gold = userObj.gold,
                                    science = userObj.science,
                                    planets = (userObj.planets || []).length + 1;
                                
                                if (report.owner == "Pirates") {
                                    iron = 10000;
                                    h3 = 10000;
                                    gold = 10000;
                                    science = 0;
                                }
                                
                                // Calculate how much they are going to lose.
                                var percentage = (1 / planets) * Math.random();
                                
                                // Debit.
                                iron = Math.round(iron * percentage);
                                h3 = Math.round(h3 * percentage);
                                gold = Math.round(gold * percentage);
                                science = Math.round(science * percentage);
                                
                                // Give to the rich and steal from the poor!
                                gameContext.UserManager.debitResources(report.owner, gold, iron, h3, science);
                                gameContext.UserManager.addResources(user, iron, gold, h3, science);
                                
                                // Tell them the bad news.
                                gameContext.UserManager.addNotification(report.owner, "red", "You have lost control of the planet " + report.name + " and " + iron + " iron, " + gold + " gold, " + h3 + " h3 to " + user, { x: x, y: y });
                                    
                                // Check for relocation scenario.
                                gameContext.UserManager.relocate(report.owner, x, y);
                                
                                // Tell the good news.
                                gameContext.UserManager.addNotification(user, "blue", "You have gained control of the planet " + report.name + " and pillaged " + iron + " iron, " + gold + " gold, " + h3 + " h3");
                                
                                // Do the barrage of updates.
                                gameContext.SocketManager.updateNotifications(report.owner);
                                gameContext.SocketManager.updateNotifications(user);
                                gameContext.SocketManager.updateMap(user);
                                gameContext.SocketManager.updateMap(report.owner);
                                gameContext.SocketManager.updatePlanet(user);
                            });
                                                                         
                        });
                    }); 
                });
            } else if ( winner !== '') {
                gameContext.Map.getInfo(x, y, function (planet) {
                    // Calculate how much damage they did.
                    var damage = (shipsRemaining[0][1] / report.planetShips[0][1]);
                    var shields = Math.max(Math.round(planet.shields * damage), 0.15);
                    
                    // Notify the attacker of the outcome.
                    gameContext.UserManager.addNotification(user, "red", "You have lost all your ships during the battle with " + planet.owner + ", however, you managed to drop their shields to " + shields + "%");
                    gameContext.Map.updateProperty(x, y, "shields", shields, function () {
                        // Notify them of the shield change.
                        gameContext.UserManager.addNotification(planet.owner, "red", "You have been attacked by " + user + " and shields for planet " + planet.name + " have been dropped to " + shields + "%", { x: x, y: y });
                        
                        // Do the barrage of updates.
                        gameContext.SocketManager.updateNotifications(report.owner);
                        gameContext.SocketManager.updateNotifications(user);
                        gameContext.SocketManager.updatePlanet(user);
                        gameContext.SocketManager.updateMap(user);
                        gameContext.SocketManager.updateMap(report.owner);
                    });
                });
            } else {

            }
        }, true, true, false);
    });

};

module.exports = MissionManager;