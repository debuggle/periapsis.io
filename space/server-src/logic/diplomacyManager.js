﻿DiplomacyManager = function (gameContext) {
    this.gameContext = gameContext;
};

DiplomacyManager.prototype.getTradeRoutes = function (user, callback) {
    if (callback) callback.call(this, []);
};

DiplomacyManager.prototype.setTradeRoutes = function (user, routes, callback) {
    this.gameContext.db0.createStringified("user_" + user + "_trade", routes, callback);
};

// Establish a trade route.
DiplomacyManager.prototype.establishTradeRoute = function (x, y, user1, user2, callback) {

    // Create the trade route object.
    var gameContext = this.gameContext;
    var self = this;
    var obj = {
        x: x,
        y: y,
        creator: user1,
        user1: user1,
        user2: user2,
        pending: true,
        accepted: false
    };
    
    gameContext.lock("trade", 2000, function (done) {
        gameContext.Map.getInfo(x, y, function (planet) {
            obj["planet"] = planet.name;
            obj["planetType"] = planet.type;

            self.getTradeRoutes(user1, function (user1Routes) {
                self.getTradeRoutes(user2, function (user2Routes) {
                    // Update the trade routes.
                    user1Routes.push(obj);
                    user2Routes.push(obj);
                    
                    // Send the log.
                    gameContext.UserManager.addNotification(user2, "yellow", user1 + " has requested a trade agreement with the planet " + planet.name);
                    
                    // Commit the change.
                    self.setTradeRoutes(user1, user1Routes, function () {
                        self.setTradeRoutes(user2, user2Routes, function () {
                            done();
                            
                            // Update the interface if applicable.
                            gameContext.SocketManager.updateTradeAgreements(user1);
                            gameContext.SocketManager.updateTradeAgreements(user2);
                            
                            // Invoke the callback if applicable.
                            if (callback) callback.call();
                        });
                    });
                
                });
            });
        });        
    });
};

DiplomacyManager.prototype.updateTradeRequest = function (x, y, user1, user2, accepted, callback) {
    var self = this;
    var gameContext = this.gameContext;
    
    gameContext.lock("trade", 2000, function (done) {
        var sentLog = false;
        function updateStatus(routes, accepted) {
            for (var i = 0; i < routes.length; i++) {
                var obj = routes[i];
                if (obj.x == x && obj.y == y) {
                    var update = false;
                    
                    // Check if this is a trade route, regardless of the "order" in which
                    // users were passed to this method.
                    if (obj.user1 == user1) {
                        if (obj.user2 == user2) {
                            update = true;
                        }
                    } else if (obj.user1 == user2) {
                        if (obj.user2 == user1) {
                            update = true;
                        }
                    }
                    
                    // If we should update it, do so.
                    if (update) {
                        if (accepted == false) {
                            routes[i].accepted = false;
                            routes[i].pending = false;
                            routes.splice(i--, 1);
                        } else {
                            routes[i].accepted = accepted;
                            routes[i].pending = false;
                        }

                        // Trigger the log.
                        if (!sentLog) {
                            if (accepted) {
                                gameContext.UserManager.addNotification(user1, "green", "Trade agreement with " + user2 + " has been accepted for planet " + obj.planet);
                                gameContext.UserManager.addNotification(user2, "green", "Trade agreement with " + user1 + " has been accepted for planet " + obj.planet);
                            } else {
                                gameContext.UserManager.addNotification(user1, "red", "Trade agreement with " + user2 + " has been declined for planet " + obj.planet);
                                gameContext.UserManager.addNotification(user2, "red", "Trade agreement with " + user1 + " has been declined for planet " + obj.planet);
                            }
                            
                            sentLog = true;
                        }
                    }
                }
            }
            return routes;
        }

        self.getTradeRoutes(user1, function (user1Routes) {
            self.getTradeRoutes(user2, function (user2Routes) {
                user1Routes = updateStatus(user1Routes, accepted);
                user2Routes = updateStatus(user2Routes, accepted);

                self.setTradeRoutes(user1, user1Routes, function () {
                    self.setTradeRoutes(user2, user2Routes, function () {

                        // If they accepted the agreement. Start the queue.
                        if (accepted)
                            gameContext.QueueManager.enqueue('tradeAgreement', { x: x, y: y, user1: user1, user2: user2 }, gameContext.getSettings(1).tradeRouteTime);
                        
                        // Flag as done.
                        done();
                        
                        // Update trade agreements if applicable.
                        gameContext.SocketManager.updateTradeAgreements(user1);
                        gameContext.SocketManager.updateTradeAgreements(user2);

                        if (callback) callback.call();
                    });
                });
            });
        });
    });
};

DiplomacyManager.prototype.dissolveTradeRoute = function (x, y, user1, user2, callback) {
    var self = this;
    var sentLog = false;
    var gameContext = this.gameContext;

    gameContext.lock("trade", 2000, function (done) {
        self.getTradeRoutes(user1, function (user1Routes) {
            self.getTradeRoutes(user2, function (user2Routes) {
                
                function cleanRoute(routes) {
                    for (var i = 0; i < routes.length; i++) {
                        var obj = routes[i];
                        if (obj.x == x && obj.y == y) {
                            
                            var remove = false;
                            
                            // Check if this is a trade route, regardless of the "order" in which
                            // users were passed to this method.
                            if (obj.user1 == user1) {
                                if (obj.user2 == user2) {
                                    remove = true;
                                }
                            } else if (obj.user1 == user2) {
                                if (obj.user2 == user1) {
                                    remove = true;
                                }
                            }
                            
                            // If we should remove it, do so.
                            if (remove) {
                                routes.splice(i--, 1);
                                if (!sentLog) {
                                    gameContext.UserManager.addNotification(user1, "red", "A trade agreement with " + user2 + " has been dissolved");
                                    gameContext.UserManager.addNotification(user2, "red", "A trade agreement with " + user1 + " has been dissolved");
                                    sentLog = true;
                                }
                            }
                        }
                    }

                    return routes;
                }
                
                user1Routes = cleanRoute(user1Routes);
                user2Routes = cleanRoute(user2Routes);

                // Commit the change.
                self.setTradeRoutes(user1, user1Routes, function () {
                    self.setTradeRoutes(user2, user2Routes, function () {
                        done();
                        
                        // Update trade agreements.
                        gameContext.SocketManager.updateTradeAgreements(user1);
                        gameContext.SocketManager.updateTradeAgreements(user2);
                        
                        if (callback) callback.call();
                    });
                });
            });
        });
    });
};

module.exports = DiplomacyManager;