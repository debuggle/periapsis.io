﻿var Tech = require('../models/tech'),
    copy = require('deep-copy');

var TechManager = function (gameContext) {
    this.gameContext = gameContext;
    this.tree = [];
};

TechManager.prototype.find = function (tech, children) {
    var item = null;
    if ((children || []).length == 0) return null;

    for (var i = 0; i < children.length; i++) {
        if (children[i].title == tech) {
            return children[i];
        } else {
            item = this.find.call(this, tech, children[i].children);
        }

        if (item) return item;
    }
    return null;
};

TechManager.prototype.getAll = function (user, success) {
    var tree = copy(this.tree);
    // Get the user properties.
    this.gameContext.UserManager.getUser(user, function (objUser) {
        
        function hasTech(tech) {
            objUser.technologies = objUser.technologies || [];
            for (var i = 0; i < objUser.technologies.length; i++) {
                if (objUser.technologies[i] == tech)
                    return true;
            }
            return false;
        }

        function analyze(node, available) {
            if (hasTech(node.title)) {
                node.unlocked = true;
                node.available = true;
                for (var i = 0; i < node.children.length; i++) {
                    node.children[i] = analyze(node.children[i], true);
                }
            } else if (node.available || available) {
                node.available = true;
                for (var i = 0; i < node.children.length; i++) {
                    node.children[i] = analyze(node.children[i], false);
                }
            } else {
                node.available = false;
                for (var i = 0; i < node.children.length; i++) {
                    node.children[i] = analyze(node.children[i]);
                }
            }
            return node;
        }
        
        for (var i = 0; i < tree.length; i++) {
            tree[i] = analyze(tree[i], true);
        }

        // Return with the interpreted tree.
        if (success) success.call(this, tree);
    });
}

TechManager.prototype.giveTech = function (user, tech, success) {
    this.gameContext.UserManager.pushValue(user, "technologies", tech, success);
}

TechManager.prototype.userHasTechs = function (user, techs, success){
    this.gameContext.UserManager.getUser(user, function (objUser) {
        var technologies = objUser.technologies || [];
        var result = [];
        for (var i = 0; i < technologies.length; i++) {
            for (var r = 0; r < techs.length; r++) {
                if (technologies[i] == techs[r]) {
                    result.push(techs[r]);
                    break;
                }
            }
        }
        if (success) success.call(this, result);
    });
}

TechManager.prototype.userHasTech = function (user, tech, success){
    this.gameContext.UserManager.getUser(user, function (objUser) {
        objUser.technologies = objUser.technologies || [];
        for (var i = 0; i < objUser.technologies.length; i++) {
            if (objUser.technologies[i] == tech) {
                success.call(this, true);
                return;
            }
        }
        success.call(this, false);
    });
}

TechManager.prototype.init = function () {
    
    var shieldIcon = 'fa-wifi';
    var attackIcon = 'fa-rocket';
    var scienceIcon = 'fa-flask';
    var miningIcon = 'fa-diamond';

    // Create the tech tree.
    var Shield = new Tech({
        title: 'Improved Shields I',
        description: 'Improve shield effectiveness on all planets.',
        icon: shieldIcon,
        category: 'Shields',
        cost: 2000
    });
    
    var GuerillaShield = Shield.addChild({
        title: 'Guerilla Shields I',
        description: 'Upon capturing another planet, instantly bring back their shields to 25%',
        icon: shieldIcon,
        category: 'Shields',
        cost: 5000
    });
    
    var GuerillaShield2 = GuerillaShield.addChild({
        title: 'Guerilla Shields II',
        description: 'Upon capturing another planet, instantly bring back their shields to 50%',
        icon: shieldIcon,
        category: 'Shields',
        cost: 10000
    });

    var Shield2 = Shield.addChild({
        title: 'Improved Shields II',
        description: 'Improve shield effectiveness on all planets.',
        icon: shieldIcon,
        category: 'Shields',
        cost: 10000
    });
    
    var FieldManipulator = Shield2.addChild({
        title: 'Ion Field Manipulator',
        description: 'Improve shield recharge rate by 50%',
        icon: shieldIcon,
        category: 'Shields',
        cost: 5000
    });

    var Shield3 = Shield2.addChild({
        title: 'Improved Shields III',
        description: 'Improve shield effectiveness on all planets.',
        icon: shieldIcon,
        category: 'Shields',
        cost: 25000
    });
    
    var Attack = new Tech({
        title: 'Improved Attack I',
        description: 'Unlock the Warbird ship.',
        icon: attackIcon,
        category: 'Attack',
        cost: 1500
    });
    
    var Attack2 = Attack.addChild({
        title: 'Improved Attack II',
        description: 'Unlock the Bloodletter ship.',
        icon: attackIcon,
        category: 'Attack',
        cost: 10000
    });
    
    var Attack3 = Attack2.addChild({
        title: 'Improved Attack III',
        description: 'Unlock the Apollo ship.',
        icon: attackIcon,
        category: 'Attack',
        cost: 20000
    });
    
    var Science = new Tech({
        title: 'Science I',
        description: 'Unlock other science technologies.',
        icon: scienceIcon,
        category: 'Science',
        cost: 1000
    });

    var Terraform = Science.addChild({
        title: 'Terraform Planets',
        description: 'Unlock the Terraformer ship which allows colonization of other planets.',
        icon: scienceIcon,
        category: 'Science',
        cost: 3000
    });

    var SpaceProbe = Science.addChild({
        title: 'Space Exploration I',
        description: 'Unlock the Stratos exploration ship which allows traversal of other sectors.',
        icon: scienceIcon,
        category: 'Science',
        cost: 5000
    });

    var Science2 = Science.addChild({
        title: 'Science II',
        description: 'Unlock the ability to explore silver planets.',
        icon: scienceIcon,
        category: 'Science',
        cost: 10000
    });

    var Science3 = Science2.addChild({
        title: 'Science III',
        description: 'Unlock the ability to awaken silver planets.',
        icon: scienceIcon,
        category: 'Science',
        cost: 100000
    });
    
    var Mining = new Tech({
        title: 'Mining I',
        description: 'Improve the yield of mining operations',
        icon: miningIcon,
        category: 'Mining',
        cost: 1000
    });

    var Mining2 = Mining.addChild({
        title: 'Mining II',
        description: 'Mine resources faster and produce a higher yield.',
        icon: miningIcon,
        category: 'Mining',
        cost: 10000
    });
    
    var MiningProdigy = Mining2.addChild({
        title: 'Prodigy',
        description: 'Unlock the Prodigy ship which provides much higher yield for iron.',
        icon: miningIcon,
        category: 'Mining',
        cost: 8000
    });

    var MiningSkytrader = Mining2.addChild({
        title: 'Skytrader',
        description: 'Unlock the Skytrader ship which provides much higher yield for gold.',
        icon: miningIcon,
        category: 'Mining',
        cost: 8000
    });
    
    var Mining3 = Mining2.addChild({
        title: 'Mining III',
        description: 'Mine resources faster and produce a higher yield.',
        icon: miningIcon,
        category: 'Mining',
        cost: 25000
    });

    // Now add all the top level things.
    this.tree.push(Mining);
    this.tree.push(Shield);
    this.tree.push(Attack);
    this.tree.push(Science);
};

module.exports = TechManager;