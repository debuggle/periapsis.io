﻿var User = require('../models/user'),
    uuid = require('node-uuid');

SylmarManager = function (gameContext) {
    this.gameContext = gameContext;
};

SylmarManager.prototype.getTaunt = function (planet) {
    var taunts = [
        "We are going to burn the atmosphere of planet {name}.",
        "Your civilization will be remembered.",
        "Annihilation is inevitable for planet {name}.",
        "Our scholars look forward to piecing together your past.",
        "A race that needs oxygen? We are not bound by such limitations.",
        "Your peoples distinct languages will be assimilated into our culture.",
        "No Sylmar can be truly cultured if they have not witnessed the majestic effects of a civilization losing the powerful sanctity of atmosphere. Like that of planet {name}.",
        "We have been in The Solitude for hundreds of years. It is time to feast on the cultural distinctiveness of your empire.",
        "When one civilization collapses, their potential kinetic energy becomes a canvas from which all emotions may derive.",
        "The planet {name} currently lacks the defensive capabilities necessary to withstand Sylmar.",
        "Extermination has become a necessity.",
        "There is no star far enough to which you may retreat.",
        "Our military tacticians have concluded that battling for planet {name} will prove to be a decisive victory.",
        "War is a canvas and you are the paint which shall become immortal.",
        "Fastidious are those who seek to counter the majestic purpose we proffer.",
        "Do you think the outcome of this battle is not predetermined?",
        "You are but one of countless civilizations we have brought to ruins.",
        "You are a varied people. We cannot wait to sift through your cultural distinctiveness and unlock new perspectives.",
        "We have conquered far more advanced civilizations than your own. Do not be fooled by false hope."
    ];

    var selected = taunts[Math.round(Math.random() * (taunts.length - 1))];
    return selected.replace("{name}", planet);
};

SylmarManager.prototype.iterate = function (sector) {
    // Sylmar AI.
    var gameContext = this.gameContext;
    var buildings = gameContext.BuildingManager.getBuildings();
    var warShipName = "Voracious";
    var triggerQty = 31;
    var fleetSize = 30;
    var self = this;

    gameContext.UserManager.getUser("Sylmar", function (user) {
        
        // Don't do anything until they are awakened.
        if ((user.awakened || false).toString() != 'true') return;

        gameContext.Map.scanSector(sector, function (map) {
            var targets = [];
            var terraform = null;
            var target = { shields: 100 };

            for (var prop in map) {
                var planet = map[prop];
                if (planet.isPlanet) {
                    // Check if we own this planet.
                    if (planet.owner == "Sylmar") {
                        // Upgrade buildings.
                        gameContext.AIManager.build("Sylmar", planet.x, planet.y, "Iron Mine");
                        gameContext.AIManager.build("Sylmar", planet.x, planet.y, "Suborbital Array");
                        gameContext.AIManager.build("Sylmar", planet.x, planet.y, "Gas Extractor");
                        gameContext.AIManager.build("Sylmar", planet.x, planet.y, "Ship Yard");
                    } else if (planet.owner != undefined) {
                        if (planet.shields <= target.shields) {
                            target = planet;
                        }
                        targets.push(planet);
                    } else if (planet.owner == undefined && !planet.beingColonized) {
                        terraform = planet;
                    }
                } else if (planet.type == "asteroid") {
                    // Do a sylmar version of mining which is just extracting
                    // all the essense of the asteroid.
                    gameContext.AsteroidManager.getAsteroid(planet.x, planet.y, function (asteroid) {
                        gameContext.AsteroidManager.mine(asteroid.x, asteroid.y, "Sylmar", function (resource, value) {
                            // Sylmar technological advantage.
                            var modifier = 3.5;
                            var resources = {
                                gold: Math.round(value * (asteroid.goldModifier || 0.5) * modifier),
                                iron: Math.round(value * (asteroid.ironModifier || 0.5) * modifier),
                                h3: 0 // They don't need h3.
                            };

                            // Update the resources.
                            gameContext.UserManager.addResources("Sylmar", resources.iron, resources.gold, resources.h3, 0);
                        });
                    });
                }
            }
            
            var probe = gameContext.ShipManager.findCost("Space Probe");
            var terraformer = gameContext.ShipManager.findCost("Terraformer");
            var s = gameContext.ShipManager.findCost(warShipName);
            var availableIron = parseFloat(user.iron) - probe - terraformer;
            var qty = Math.floor(parseFloat(availableIron) / s);

            // Spend all our iron on Voracious.
            if (qty >= triggerQty) {
                gameContext.AIManager.fabricate(0, 0, qty - 1, "Sylmar", "Voracious");
            }

            // Calculate sectors controlled.
            var sectors = 0;
            for (var prop in user.sector) sectors++;

            // Now we have a list of targets.
            gameContext.ShipManager.getUserShips("Sylmar", function (ships) {
                var count = 0;
                for (var i = 0; i < ships.length; i++) {
                    if (ships[i][0] == warShipName)
                        count += parseFloat(ships[i][1]);
                }

                // Find out how many planets we can attack
                var usable = Math.round(count);// / sectors);

                // If we have more than 10 ships per sector, the time to strike is now!
                if (usable >= fleetSize && targets.length > 0) {
                    // Select a planet.
                    var toAttack = target;
                    // Calculate the time over distance.
                    var x1 = parseFloat(user.x), y1 = parseFloat(user.y);
                    var x2 = parseFloat(toAttack.x), y2 = parseFloat(toAttack.y);
                    var attackTime = gameContext.getSettings(1).attackTime;
                    var distance = Math.round(Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2)));
                    var warp = 0;
                    // Calculate the warp modifier.
                    warp = Math.round(distance / 10);
                    // Update attack time
                    attackTime = attackTime + (warp * gameContext.getSettings(1).warpTime);

                    // Create a mission guid.
                    var mission = uuid.v1();
                    
                    // Broadcast the taunt.
                    self.sendTaunt(target.owner, self.getTaunt(target.name), target.x, target.y);

                    // Generate the timers.
                    gameContext.ShipManager.deployFleet('attack', mission, [[warShipName, Math.floor(fleetSize)]], toAttack.x, toAttack.y, "Sylmar", function () {
                        gameContext.QueueManager.enqueue_other('attack', { x: toAttack.x, y: toAttack.y, user: "Sylmar", mission: mission, ships: [[warShipName, Math.floor(fleetSize)]] }, attackTime);
                    });
                }
            });
            
            // Terraform if possible.
            if (qty >= triggerQty && terraform) {
                gameContext.AIManager.fabricate(0, 0, 1, "Sylmar", "Terraformer", function () {
                    gameContext.AIManager.terraform(terraform.x, terraform.y, "Sylmar");
                });
            }

            if (qty >= triggerQty) {
                // Lastly, we need to build Space Probes and send them.
                gameContext.AIManager.fabricate(0, 0, 1, "Sylmar", "Space Probe", function () {
                    // Calculate the four corners of this sector and expand, if we don't already have them.
                    var normalize = gameContext.Map.normalizeSector(sector);
                    var x = normalize[0] * gameContext.Map.getQuadrantSize(), y = normalize[1] * gameContext.Map.getQuadrantSize();
                    
                    // Get the quadrants.
                    var quadrants = [];
                    var size = gameContext.Map.getQuadrantSize();
                    quadrants.push(gameContext.Map.getQuadrant(x + size, y));
                    quadrants.push(gameContext.Map.getQuadrant(x - size, y));
                    quadrants.push(gameContext.Map.getQuadrant(x, y - size));
                    quadrants.push(gameContext.Map.getQuadrant(x, y + size));
                    
                    // Randomize the list so they choose a non specific place
                    // to go to.
                    quadrants.sort(function () { return Math.random(); });
                    
                    // Check if we don't own any of them.
                    for (var i = 0; i < quadrants.length; i++) {
                        var q = quadrants[i];
                        if (user.sector.hasOwnProperty(q)) continue;
                        // Scan
                        gameContext.ShipManager.killShip("Space Probe", false, null, "Sylmar", function () {
                            user.sector[q] = true;
                            gameContext.UserManager.putValue("Sylmar", "sector", user.sector);
                            gameContext.QueueManager.enqueue_other('sylmar_ai', { sector: prop }, 0);
                        }, 1);
                        break;
                    }
                });
            }
        });
    });
};

SylmarManager.prototype.sendTaunt = function (user, msg, x, y) {
    this.gameContext.UserManager.addNotification(user, "black", msg, { x: x, y: y });
};

SylmarManager.prototype.broadcast = function (msg) {
    var gameContext = this.gameContext;
    gameContext.UserManager.getAllUsers(function (users) {
        for (var i = 0; i < users.length; i++) {
            var user = users[i];
            gameContext.UserManager.addNotification(user, "black", msg, {});
        }
    });
};

SylmarManager.prototype.awaken = function () {
    
    // Check if we're at the target time.
    var now = new Date();
    var target = new Date(2015, 11, 24, 15, 0, 0, 0);
    var condition = now.getTime() > target.getTime();
    if (!condition) return false;
    var gameContext = this.gameContext;
    var self = this;
    gameContext.SylmarManager.generate(function () {
        gameContext.UserManager.getUser("Sylmar", function (user) {
            if ((user.awakened || false).toString() != 'true') {
                gameContext.UserManager.putValue("Sylmar", "awakened", true, function () {
                    self.broadcast("Have you felt it? The awakening is upon us. We may be in our darkest hour.");
                    
                    // Turn all silver planets into Sylmar planets.
                    gameContext.db0.getList("silver_planets", function (coords) {
                        
                        // Give them 10,000 more gold per planet. 5,000 more iron per planet.
                        gameContext.UserManager.addResources("Sylmar", 5000 * coords.length, 10000 * coords.length, 0, 0);

                        var sectors = [];
                        for (var i = 0; i < coords.length; i++) {
                            var sector = gameContext.Map.getQuadrant(coords[i][0], coords[i][1]);
                            if (sectors.indexOf(sector) < 0) sectors.push(sector);
                            
                            // Awaken the planet.
                            var x = coords[i][0];
                            var y = coords[i][1];
                            gameContext.UserManager.pushValue("planets", coords[i]);
                            gameContext.Map.updateProperty(x, y, "type", "planet_sylmar");
                            gameContext.Map.updateProperty(x, y, "owner", "Sylmar");

                            gameContext.UserManager.getAllUsers(function (users) {
                                for (var i = 0; i < users.length; i++) {
                                    gameContext.SocketManager.updateMap(users[i]);
                                }
                            });
                        }
                        
                        // Now spawn the sylmar AI.
                        for (var i = 0; i < sectors.length; i++) {
                            gameContext.QueueManager.enqueue_other("sylmar_ai", { sector: sectors[i] }, 0);
                        }
                    });
                });
            }
        });
    });
    return true;
};

SylmarManager.prototype.registerSilverPlanet = function (x, y) {
    var gameContext = this.gameContext;
    gameContext.db0.addList("silver_planets", [x, y]);
};

SylmarManager.prototype.generate = function (callback) {
    var self = this;
    self.gameContext.UserManager.getUser("Sylmar", function (sylmar) {
        // Fix broken pirates.
        self.gameContext.UserManager.putValue("Sylmar", "username", "Sylmar");
        self.gameContext.UserManager.putValue("Sylmar", "protection", 0);
    }, function () {
        // Need to create the player.
        self.gameContext.Map.getNextPlanet(function (planet) {
            self.gameContext.Map.updateProperty(planet.x, planet.y, "type", "planet_silver", function () {
                self.gameContext.SylmarManager.registerSilverPlanet(planet.x, planet.y);
                self.gameContext.db0.create('user_Sylmar', new User({ username: 'Sylmar', protection: 0, x: planet.x, y: planet.y, user: 'Sylmar' }), function () {
                    var sector = {};
                    sector[self.gameContext.Map.getQuadrant(planet.x, planet.y)] = true;
                    self.gameContext.UserManager.putValue("Sylmar", "sector", sector, function () {
                        if (callback) callback.call();
                    });
                });
            });
        }, function () { }, 133.700);
    });
}

SylmarManager.prototype.initSylmar = function (callback) {
    var self = this;
    //self.gameContext.QueueManager.enqueue_other('sylmar_awaken', { once: true }, 0);
};

module.exports = SylmarManager;