﻿var redis = require('../redis-layer'),
    uuid = require('node-uuid'),
    User = require('../models/user');

UserManager = function (gameContext) {
    this.gameContext = gameContext;
};

UserManager.prototype.getUser = function (user, success, failure) {
    var gameContext = this.gameContext;
    gameContext.lock("update_user_" + user, 2000, function (taskComplete) {
        gameContext.db0.retrieve("user_" + user, function (objUser) {
            if (success) {
                success.call(this, objUser);
                taskComplete();
            }
        }, function () {
            if (failure) failure();
            taskComplete();
        });
    });
};

UserManager.prototype.putValue = function (username, field, value, success) {
    this.gameContext.db0.put("user_" + username, field, value, success);
};

UserManager.prototype.pushValue = function (username, field, value, success) {
    var gameContext = this.gameContext;
    gameContext.lock("update_user_" + username, 2000, function (done) {
        gameContext.db0.retrieve("user_" + username, function (objUser) {
            objUser[field] = objUser[field] || [];
            if (objUser[field].indexOf(value) < 0)
                objUser[field].push(value);
            gameContext.db0.create("user_" + username, objUser, function () {
                done();
                if (success) success.call();
            }, function () {
                done();
            });
        }, function () {
            done();
        });
    });
};

UserManager.prototype.removeValue = function (username, field, value, success) {
    var gameContext = this.gameContext;
    gameContext.lock("update_user_" + username, 2000, function (done) {
        gameContext.db0.retrieve("user_" + username, function (objUser) {
            objUser[field] = objUser[field] || [];
            if (objUser[field].indexOf(value) >= 0)
                objUser[field].splice(objUser[field].indexOf(value), 1);
            gameContext.db0.create("user_" + username, objUser, function () {
                done();
                if (success) success.call();
            }, function () {
                done();
            });
        }, function () {
            done();
        });
    });
}

UserManager.prototype.setWarStatus = function (user1, user2, atWar, callback) {
    var self = this;
    var gameContext = this.gameContext;
    this.getUser(user1, function (objUser1) {
        self.getUser(user2, function (objUser2) {
            var war1 = objUser1.wars || [];
            var war2 = objUser2.wars || [];

            if (atWar) {
                if (war1.indexOf(user2) < 0) {
                    war1.push(user2);
                    if (user1 != "Pirates" && user2 != "Pirates")
                        self.addNotification(user1, "red", "You are now in a war with " + user2);
                }

                if (war2.indexOf(user1) < 0) {
                    war2.push(user1);
                    if (user1 != "Pirates" && user2 != "Pirates")
                        self.addNotification(user2, "red", "You are now in a war with " + user1);
                }
            } else {
                if (war1.indexOf(user2) >= 0) {
                    war1.splice(war1.indexOf(user2), 1);
                    if (user1 != "Pirates" && user2 != "Pirates")
                        self.addNotification(user1, "blue", "You have made peace with " + user2);
                }
                if (war2.indexOf(user1) >= 0) {
                    war2.splice(war2.indexOf(user1), 1);
                    if (user1 != "Pirates" && user2 != "Pirates")
                        self.addNotification(user2, "blue", "You have made peace with " + user1);
                }
            }

            // Update the properties.
            self.putValue(user1, "wars", war1, function () {
                self.putValue(user2, "wars", war2, function () {
                    if (callback)
                        callback.call();

                    gameContext.SocketManager.updateMap(user1);
                    gameContext.SocketManager.updateMap(user2);
                });
            });
        });
    });
};

UserManager.prototype.angerFaction = function (faction, user, success) {
    var self = this;
    this.getUser(user, function (objUser) {
        var val = parseFloat(objUser["alignment_" + faction] || 0);
        val -= 1;
        self.putValue(user, "alignment_" + faction, val, success);
    });
};

UserManager.prototype.satisfyFaction = function (faction, user, success) {
    var self = this;
    this.getUser(user, function (objUser) {
        var val = parseFloat(objUser["alignment_" + faction] || 0);
        val += 1;
        self.putValue(user, "alignment_" + faction, val, success);
    });
};

UserManager.prototype.debitResources = function (user, gold, iron, h3, science) {
    var self = this;
    var gameContext = this.gameContext;
    this.getUser(user, function (objUser) {
        objUser.gold = Math.max(parseFloat(objUser.gold || 0) - parseFloat(gold || 0), 0);
        objUser.iron = Math.max(parseFloat(objUser.iron || 0) - parseFloat(iron || 0), 0);
        objUser.h3 = Math.max(parseFloat(objUser.h3 || 0) - parseFloat(h3 || 0), 0);
        objUser.science = Math.max(parseFloat(objUser.science || 0) - parseFloat(science || 0), 0);
        
        // Commit the changes.
        if ( gold || 0 > 0 )
            self.putValue(user, "gold", Math.round(objUser.gold));
        if ( iron || 0 > 0 )
            self.putValue(user, "iron", Math.round(objUser.iron));
        if ( h3 || 0 > 0 )
            self.putValue(user, "h3", Math.round(objUser.h3));
        if ( science || 0 > 0 )
            self.putValue(user, "science", Math.round(objUser.science));

        gameContext.SocketManager.updateNotifications(user);
    });
};

UserManager.prototype.debitCredits = function (user, credits, callback) {
    var self = this;
    this.getUser(user, function (objUser) {
        objUser.credits = Math.max(parseFloat(objUser.credits || 0) - credits, 0);
        self.putValue(user, "credits", objUser.credits, function () {
            self.gameContext.SocketManager.updateResources(user);
            if (callback) callback.call();
        });
    });
};

UserManager.prototype.addNotification = function (user, color, message, context) {
    if (user == "Pirates" || user == "Sylmar" || user == "Mythos") return;
    var self = this;
    var gameContext = this.gameContext;
    gameContext.db0.addList("user_" + user + "_notifications", {
        guid: uuid.v1(),
        color: color,
        context: context || {},
        message: message,
        date: gameContext.now(),
        dismissed: false
    }, function () {
        self.gameContext.SocketManager.updateNotifications(user);
    });
};

UserManager.prototype.addResources = function (user, iron, gold, h3, science) {
    var self = this;
    var gameContext = this.gameContext;

    this.getUser(user, function (objUser) {
        objUser.gold = parseFloat(objUser.gold || 0) + parseFloat(gold || 0);
        objUser.iron = parseFloat(objUser.iron || 0) + parseFloat(iron || 0);
        objUser.h3 = parseFloat(objUser.h3 || 0) + parseFloat(h3 || 0);
        objUser.science = parseFloat(objUser.science || 0) + parseFloat(science || 0);
        
        // Commit the changes.
        if ( gold || 0 > 0 )
            self.putValue(user, "gold", objUser.gold);
        if ( iron || 0 > 0 )
            self.putValue(user, "iron", objUser.iron);
        if ( h3 || 0 > 0 )
            self.putValue(user, "h3", objUser.h3);
        if ( science || 0 > 0 )
            self.putValue(user, "science", objUser.science);

        gameContext.SocketManager.updateNotifications(user);
    });
};

UserManager.prototype.getCivilizationScoreSync = function (objUser, ships) {
    var score = 0, 
        gameContext = this.gameContext,
        gold = parseFloat(objUser.gold || 0), 
        iron = parseFloat(objUser.iron || 0), 
        h3 = parseFloat(objUser.h3 || 0),
        planets = (objUser.planets || []).length;
    
    // Calculate the total score for all assets.
    for (var i = 0; i < (ships || []).length; i++) {
        score += (gameContext.ShipManager.find(ships[i].name) || {}).cost || 0;
    }
    
    score += gold + iron + h3 + (planets * 1500);
    return score;
}

UserManager.prototype.getCivilizationScore = function (user, callback) {
    var self = this;
    this.getUser(user, function (objUser) {
        self.getShips(user, function (ships) {
            var score = self.getCivilizationScoreSynchronous(userObj, ships.length);
            callback.call(score);
        });
    });
};

UserManager.prototype.relocate = function (user, x, y, success, failure) {
    // Check if the user is obliterated.
    var gameContext = this.gameContext;
    gameContext.UserManager.getUser(user, function (objUser) {
        // Get their home planet.
        var planets = objUser.planets || [];
        for (var i = 0; i < planets.length; i++) {
            // Remove the planet that was just attacked since that won't necessarily be current in the database.
            if (planets[i][0] == x && planets[i][1] == y) {
                planets.splice(i--, 1);
            }
        }

        if (planets.length == 0) {
            // Make sure their home planet isn't owned either.
            // Then relocation is applicable.
            gameContext.Map.getNextPlanet(function (newWorld) {
                gameContext.Map.updateProperty(newWorld.x, newWorld.y, "owner", user, function () {
                    var newUser = new User({ username: user, guild: objUser.guild, password: objUser.password, x: newWorld.x, y: newWorld.y, relocated: true });
                            
                    // Disband all fleets.
                    gameContext.FleetManager.getUserFleets(user, function (fleets) {
                        for (var i = 0; i < fleets.length; i++) {
                            if (fleets[i].user == user) {
                                gameContext.FleetManager.disband(user, fleets[i].title);
                            }
                        }
                                
                        // Delete all normal things.
                        gameContext.db0.delete('user_' + user + '_vessels', function () {
                            gameContext.db0.delete('user_' + user + '_timers', function () {
                                gameContext.db0.delete('user_' + user + '_victory_log', function () {
                                            
                                    // Update the sector information.
                                    newUser.sector = {};
                                    newUser.explored = {};
                                    newUser.sector[gameContext.Map.getQuadrant(newWorld.x, newWorld.y)] = true;
                                    newUser.explored[gameContext.Map.getQuadrant(newWorld.x, newWorld.y)] = true;
                                    newUser.planets = [];
                                    newUser.planets.push([newWorld.x, newWorld.y]);

                                    // Commit the changes.
                                    gameContext.db0.create('user_' + user, newUser, function () {
                                        // Notify them of the tragedy.
                                        gameContext.UserManager.addNotification(user, "red", "Your civilization has been wiped out.");
                                        gameContext.UserManager.addNotification(user, "blue", "You have been relocated to a new sector. Here are a few more resources to start your civilization anew!");
                                        gameContext.SocketManager.relocated(user);

                                        if (success) success.call();
                                    });
                                });
                            });
                        });

                    });
                });
            }, function () {
                if (failure) failure.call();
            });
        }
    });
}

UserManager.prototype.isActive = function (user) {
    var seen = this.gameContext.NetTable.get(user) || 0;
    if (seen) {
        return (this.gameContext.now() - seen) < 15 * 60 * 1000;
    }
    return false;
};

UserManager.prototype.registerUserOnce = function (user, callback) {
    this.gameContext.db0.addList("all_users", user, callback);
}

UserManager.prototype.findUser = function (user, success, failure) {
    var uname = user.toString().toLowerCase();
    this.gameContext.db0.getList("all_users", function (users) {
        for (var i = 0; i < users.length; i++) {
            if (users[i].toString().toLowerCase() == uname) {
                if (success) success.call(this, users[i]);
                return;
            }
        }
        if (failure) failure.call();
    });
};

UserManager.prototype.getAllUsers = function (success, failure) {
    this.gameContext.db0.getList("all_users", function (users) {
        if (success) success.call(this, users);
    });
};

UserManager.prototype.initPirates = function () {
    var self = this;
    this.getUser("Pirates", function (pirate) { 
        // Fix broken pirates.
        self.putValue("Pirates", "username", "Pirates");
        self.putValue("Pirates", "protection", 0);
    }, function () {
        // Need to create the player.
        self.gameContext.db0.create('user_Pirates', new User({ username: 'Pirates', x: 0, y: 0, user: 'Pirates'}));
    });
};

UserManager.prototype.getShips = function (username, callback) {
    this.gameContext.db0.getList("user_" + username + "_ships", function (ships) {
        if (callback) {
            callback.call(this, ships);
        }
    }, function () {
        if (callback) {
            callback.call(this, null);
        }   
    });
};

UserManager.prototype.getQuests = function (user, callback) {
    var self = this;
    this.getUser(user, function (objUser) {
        if (objUser.adventures) {
            if (callback) { callback.call(this, objUser.adventures); }
        } else {
            if (callback) callback.call(this, {});
        }           
    });
};

UserManager.prototype.acceptQuest = function (user, title, callback) {
    var self = this;
    var gameContext = this.gameContext;
    this.getUser(user, function (objUser) {
        objUser.adventures = objUser.adventures || {};
        objUser.adventures[title] = {
            title: title,
            index: 0,
            completed: false
        };
        self.putValue(user, "adventures", objUser.adventures, function () {
            if (callback) callback.call();
            gameContext.SocketManager.updateQuests(user);
        });
    });
};

UserManager.prototype.updateQuest = function (user, title, index, completed) {
    var self = this;
    var gameContext = this.gameContext;
    this.getUser(user, function (objUser) {
        objUser.adventures = objUser.adventures || {};
        var quest = objUser.adventures[title];

        if (index)
            quest.index = index;
        if (completed !== null && completed !== undefined)
            quest.completed = completed;

        objUser.adventures[title] = quest;
        self.putValue(user, "adventures", objUser.adventures, function () {
            gameContext.SocketManager.updateQuests(user);
        });
    });
}

module.exports = UserManager;