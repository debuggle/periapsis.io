﻿var User = require('../models/user'),
    copy = require('deep-copy');

module.exports = function (app, gameContext) {
    
    var db0 = gameContext.db0;
    app.post('/user/register', function (req, res) {
        var username = req.body.username;
        var password = req.body.password;
        
        if (username == undefined || username == null || username.length == 0) {
            res.send({ success: false, msg: 'Please supply a username.' });
            return;
        }
        
        if (password == undefined || password == null || password.length == 0) {
            res.send({ success: false, msg: 'Please supply a password.' });
            return;
        }
        
        gameContext.UserManager.findUser(username , function () {
            res.send({ success: false, msg: 'User already exists.' });
            return;
        }, function () {
            // Check if the user exists.
            db0.retrieve("user_" + username, function (obj) {
                // The user does exist. which is bad.
                res.send({ success: false, msg: 'User already exists.' });
                res.end();
            }, function () {
                // Failure condition means the user doesn't exist.
                // Which is good in this case...   
                var user = new User({ username: username, password: password });
                
                // Find an available planet.
                gameContext.Map.getNextPlanet(function (planet) {
                    user.x = planet.x;
                    user.y = planet.y;
                    user.sector = {};
                    user.sector[gameContext.Map.getQuadrant(planet.x, planet.y)] = true;
                    
                    gameContext.Map.changeOwner(planet.x, planet.y, username, function () {
                        gameContext.Map.updateProperty(planet.x, planet.y, "protection", (gameContext.now() + (60 * 60 * 24 * 1000)), function () {
                            gameContext.Map.updateProperty(planet.x, planet.y, "isHome", true, function () {
                                user.planets = [];
                                // Fix the planets.
                                user.planets.push([planet.x, planet.y]);
                                // Allocate the planet to the user.
                                db0.create("user_" + username, user, function () {
                                    gameContext.UserManager.registerUserOnce(username, function () {
                                        res.send({ success: true, user: user });
                                        res.end();
                                    });
                                });
                            });
                        });
                    });
                }, function () {
                    res.send({ success: false, msg: 'There was a failure generating the account. Please try again.' });
                    res.end();
                });
            });
        });
    });
    
    app.post('/user/notifications', function (req, res) {
        var username = req.body.username;
        db0.getList("user_" + username + "_notifications", function (notifications) {
            gameContext.NetTable.set(username, gameContext.now());
            res.send(notifications);
        }, function () {
            res.send([]);
        });
    });

    app.post('/user/login', function (req, res) {
        var uname = req.body.username;
        var password = req.body.password;
        var oldpass = req.body.oldpass || '';
        
        gameContext.UserManager.findUser(uname, function (username) {
            // Username now fixes the case.
            db0.retrieve("user_" + username, function (obj) {
                
                // Check if they are using the old password logic.
                if (obj.password == oldpass) {
                    // Update their password.
                    gameContext.UserManager.putValue(uname, "password", password);
                } else if (obj.password != password) {
                    res.send({ success: false, msg: 'Invalid password.' });
                    res.end();
                    return;
                }
                
                // Calculate civilization score.
                // Get all the ships (if any)
                db0.getList("user_" + username + "_ships", function (ships) {
                    obj.ships = ships;
                    
                    // Update the info about each ship.
                    for (var i = 0; i < obj.ships.length; i++) {
                        var gShip = gameContext.ShipManager.find(obj.ships[i].name);
                        if (gShip)
                            obj.ships[i].attack = gShip.attack;
                    }
                    
                    obj.score = gameContext.UserManager.getCivilizationScoreSync(obj, ships);
                    
                    // Update their score in the database.
                    gameContext.UserManager.putValue(username, "score", obj.score);
                    
                    res.send({ success: true, user: obj });
                    res.end();
                }, function () {
                    obj.score = gameContext.UserManager.getCivilizationScoreSync(obj, 0);
                    res.send({ success: true, user: obj });
                    res.end();
                });

            }, function () {
                res.send({ success: false, msg: 'Invalid username.' });
                res.end();
            });
        }, function () {
            res.send({ success: false, msg: 'Invalid username.' });
        });
    });
    
    app.post('/user/ackrelocation', function (req, res) {
        var user = req.body.user;
        gameContext.UserManager.putValue(user, "relocated", false, function () {
            res.send({ success: true });
        });
    });
    
    app.post('/user/update', function (req, res) {
        var user = req.body.user;
        var pass = req.body.pass || '';
        var newPass = req.body.newPass || '';
        var tag = req.body.tag || '';
        var email = req.body.email || '';
        
        gameContext.UserManager.getUser(user, function (objUser) {
            var cont = false;
            if (newPass.length > 0) {
                if (objUser.password == pass) {
                    gameContext.UserManager.putValue(user, "password", newPass);
                    cont = true;
                } else {
                    res.send({ success: false, msg: 'Invalid password.' });
                    res.end();
                }
            } else {
                cont = true;
            }
            
            if (!cont) return;
            if (email.length >= 0)
                gameContext.UserManager.putValue(user, "email", email);
            
            if (tag.length >= 0) {
                var ip = req.connection.remoteAddress;
                if (tag.toLowerCase() == "developer" || tag.toLowerCase() == "dev" || tag.toLowerCase() == "admin") {
                    // Check if it's me!
                    if (ip == '73.221.56.155') {
                        gameContext.UserManager.putValue(user, "tag", tag);
                    }
                } else {
                    gameContext.UserManager.putValue(user, "tag", tag);
                }
            }
            
            // Success!
            res.send({
                success: true
            });
        });
    });
    
    app.post('/user/dismiss/:guid', function (req, res) {
        var user = req.body.user;
        var guid = req.params.guid;
        var found = false;
        
                        res.send({ success: true });
                        res.end();
        db0.getList("user_" + user + "_notifications", function (notifications) {
            for (var i = 0; i < notifications.length; i++) {
                if (notifications[i].guid == guid) {
                    found = true;
                    db0.remAt("user_" + user + "_notifications", i, function () {
                    }, function () {
                    });
                }
            }
            
            if (!found) {
                db0.getList("user_" + user + "_timers", function (timers) {
                    for (var i = 0; i < timers.length; i++) {
                        if (timers[i].uid == guid) {
                            db0.remAt("user_" + user + "_timers", i, function () {
                            }, function () {
                            });
                            break;
                        }
                    }
                }, function () {
                });
            }
        });
    });
    
    app.post('/user/taunt', function (req, res) {
        var user = req.body.user;
        var target = req.body.target;
        var msg = req.body.msg;
        
        // Check if the user can taunt.
        gameContext.UserManager.getUser(user, function (objUser) {
            var taunts = objUser.taunts || {};
            // Check if the user can send this taunt.
            if (taunts[target] === undefined || gameContext.now() > taunts[target]) {
                // Send it, but create a timer.
                taunts[target] = gameContext.now() + gameContext.getSettings(1).tauntTime * 1000;
                gameContext.UserManager.addNotification(target, "yellow", "Message from " + user + ": '" + msg + "'");
                gameContext.UserManager.putValue(user, "taunts", taunts);
                res.send({ success: true });
            } else {
                res.send({ success: false });
            }
        }, function () {
            res.send({ success: false });
        });
    });
    
    app.post('/user/timers/:username', function (req, res) {
        var username = req.params.username;
        db0.getList("user_" + username + "_timers", function (timers) {
            // Construct details about the timers.
            res.send(timers);
            res.end();
        }, function () {
            res.send([]);
            res.end();
        });
    });

    app.get('/user/get/:username', function (req, res) {
        db0.retrieve("user_" + req.params.username, function (user) {
            
            // Check for offline status.
            if (!gameContext.db0.canWrite)
                user.offline = true;
            else
                user.offline = false;
            
            // Update relevant properties.
            user.rank = gameContext.VictoryManager.rankToTitle(user.rank || 0);
            user.now = gameContext.now();

            // Send the user object.
            res.send(user);
            res.end();

        }, function () {
            res.send({ success: false }); 
            res.end();
        });
    });
    
    app.post('/user/ping', function (req, res) {
        var username = req.body.username;
        gameContext.UserManager.getUser(username, function (objUser) {
            gameContext.ShipManager.getUserShips(username, function (ships) {
                objUser.score = gameContext.UserManager.getCivilizationScoreSync(objUser, ships);
                objUser.lastSeen = new Date();
                gameContext.UserManager.putValue(username, "score", objUser.score);
                gameContext.UserManager.putValue(username, "lastSeen", objUser.lastSeen);
                res.send({ success: true, user: objUser });
                res.end();
            });
        });
    });
    
    app.post('/user/peace', function (req, res) {
        var user = req.body.user;
        var target = req.body.target;

        gameContext.UserManager.setWarStatus(user, target, false, function () {
            res.send({ success: true });
            res.end();
        });
    });

    app.post('/user/orders', function (req, res) {
        var username = req.body.username;
        db0.retrieve("user_" + username + "_orders", function (pastOrders) {
            res.send({ orders: pastOrders.orders });
        }, function () {
            res.send({ orders: [] }); 
        });
    });
};