﻿var redis = require('../redis-layer.js'),
    uuid = require('node-uuid');

module.exports = function (app, gameContext) {
    var db0 = gameContext.db0;
    
    app.post('/ships/inventory', function (req, res) {
        var user = req.body.user;
        gameContext.UserManager.getUser(user, function (objUser) {
            gameContext.ShipManager.getUserInventory(user, function (ships) {
                var result = [];
                // Create the list.
                for (var prop in ships) {
                    var x = prop.substr(prop.indexOf('-') + 1);
                    var y = prop.substr(0, prop.indexOf('-'));
                    var sector = gameContext.Map.getQuadrant(x, y);
                    var planetName = '';

                    // Find the corresponding planet.
                    for (var i = 0; i < objUser.docks.length; i++) {
                        if (objUser.docks[i].x == x && objUser.docks[i].y == y) {
                            planetName = objUser.docks[i].name;
                            break;
                        }   
                    }

                    for (var ship in ships[prop]) {
                        var record = [];
                        record.push(sector);
                        record.push("Planet " + planetName);
                        record.push(ship);
                        record.push(ships[prop][ship]);
                        result.push(record);
                    }
                }

                // Now that we've created the data, send it back.
                res.send({
                    success: true,
                    ships: result
                });
                res.end();
            });
        }, function () {
            res.send({
                success: false,
                msg: 'Invalid user'
            });
            res.end();
        });
    });

    app.post('/ships/query', function (req, res) {
        var x = req.body.x;
        var y = req.body.y;
        var user = req.body.user;
        
        gameContext.ShipManager.getUserShips(user, x, y, function (ships) {
            // Normalize.
            var result = [];
            for (var prop in ships) {
                result.push([prop, ships[prop]]);
            }
            res.send({
                success: true,
                ships: result
            });
        });

    });

    app.get('/ships/getall/:user', function (req, res) {
        db0.retrieve("user_" + req.params.user, function (user) {
            gameContext.ShipManager.getAll(req.params.user, function (ships) {
                res.send({ docks: user.docks, ships: ships });
            });
        });
    });
    
    app.post('/ships/startMission', function (req, res) {
        var user = req.body.user;
        var x = req.body.x;
        var y = req.body.y;
        var ships = req.body.ships;
        var mission = req.body.mission;

        gameContext.UserManager.getUser(user, function (objAttackingUser) {
            gameContext.MissionManager.getAttackReport(ships, x, y, function (report) {
                gameContext.UserManager.getUser(report.owner, function (objUser) {
                    // Verify the owner is a good target.
                    if (objUser.protection > gameContext.now()) {
                        res.send({ success: false, msg: 'This is a new player and they are protected. Wait a few more hours before you conquer them!' });
                        res.end();
                    } else {
                        gameContext.UserManager.debitResources(user, 0, 0, report.h3, 0);
                        
                        // Calculate the time over distance.
                        var x1 = parseFloat(objAttackingUser.x), y1 = parseFloat(objAttackingUser.y);
                        var x2 = parseFloat(x), y2 = parseFloat(y);
                        var attackTime = gameContext.getSettings(1).attackTime;
                        var distance = Math.round(Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2)));
                        var warp = 0;

                        // Calculate the warp modifier.
                        warp = Math.round(distance / 10);
                        
                        // Update attack time
                        attackTime = attackTime + (warp * gameContext.getSettings(1).warpTime);
                        
                        // Declare war.
                        gameContext.UserManager.setWarStatus(user, report.owner, true, function () {
                            // Create a mission guid.
                            var mission = uuid.v1();

                            // Generate the timers.
                            gameContext.ShipManager.deployFleet('attack', mission, ships, x, y, user, function () {
                                gameContext.QueueManager.enqueue('attack', { x: x, y: y, user: user, mission: mission, ships: ships }, attackTime);
                                var uid = gameContext.TimerManager.spawn('attack', user, attackTime, "Attacking fleet arrival time against planet " + report.name, "red", { x: x, y: y });
                                
                                // Invalidate their new user shield.
                                gameContext.UserManager.putValue(user, "protection", 0);
                                gameContext.Map.updateProperty(objUser.x, objUser.y, "protection", 0);
                                
                                gameContext.Map.addAttackTimer(x, y, uid);
                                res.send({ uid: uid, success: true });
                            });
                        });
                    }
                });
            });
        });
    });

    app.post('/ships/fabricate', function (req, res) {
        var user = req.body.user;
        var order = req.body.items;
        var x = req.body.x;
        var y = req.body.y;
        var orderId = uuid.v1();
        var delay = gameContext.getSettings(1).shipConstructionTime; // seconds
        
        // Calculate total.
        gameContext.TechManager.userHasTechs(user, ["Improved Attack I", "Improved Attack II", "Improved Attack III", "Science I", "Terraform Planets", "Space Probes", "Mining I"], function (techs) {
            var subtotal = 0;
            for (var i = 0; i < order.length; i++) {
                var b = gameContext.ShipManager.find(order[i].name);
                if (b && b.tech == '' || techs.indexOf(b.tech) >= 0) {
                    subtotal += b.cost * parseFloat(order[i].quantity);
                    order[i].total = b.cost * parseFloat(order[i].quantity);
                }
            }
            
            // Verify the user can afford this transaction.
            gameContext.UserManager.getUser(user, function (objUser) {
                if (parseFloat(objUser.iron) >= subtotal) {
                    // They can afford it!
                    gameContext.UserManager.debitResources(user, 0, subtotal, 0, 0);
                    
                    // Iterate over the orders and add some properties.
                    for (var i = 0; i < order.length; i++) {
                        var gShip = gameContext.ShipManager.find(order[i].name);
                        if (gShip) {
                            if ( gShip.tech == '' || techs.indexOf(gShip.tech) >= 0 ) {
                                gameContext.ShipManager.getBuildTime(user, order[i].name, parseFloat(order[i].quantity), order[i].x, order[i].y, order[i], function (buildTime, ctx) {
                                    // Put the fabrication order onto the queueue.
                                    gameContext.QueueManager.enqueue('buyShips', { user: user, x: ctx.x, y: ctx.y, quantity: parseFloat(ctx.quantity), name: ctx.name }, buildTime, function () {
                                        gameContext.SocketManager.updateNotifications(user);                                                       
                                    });
                                });
                            }
                        }
                    }
                    
                    // Retrieve the historical records.
                    res.send({ success: true });
                    res.end();
                } else {
                    res.send({ success: false, msg: 'You have insufficient iron to complete this transaction.' });
                    res.end();
                }
            });

        });
    });



};