﻿var uuid = require('node-uuid'),
    thrust = require('../common/thrust'),
    combat = require('../common/combat');
module.exports = function (app, gameContext) {
    
    // Easing function.
    function ease(t) { return t < .5 ? 2 * t * t : -1 + (4 - 2 * t) * t };

    app.post('/fleet/positions', function (req, res) {
        var user = req.body.user;
        var x = req.body.x;
        var y = req.body.y;
        var sector = gameContext.Map.getQuadrant(x, y);
        
        gameContext.db0.getList("world_fleets", function (fleets) {
            var result = [];
            for (var i = 0; i < fleets.length; i++) {
                // Calculate where this fleet is.
                var fleet = fleets[i];
                result.push(fleet);
            }
            res.send({ success: true, positions: result });
            res.end();
        }, function () {
            res.send({ success: false });
            res.end(); 
        });

    });
    
    app.post('/fleet/modify', function (req, res) {
        var user = req.body.user;
        var fleet = req.body.fleet;
        var display = req.body.display;

        gameContext.FleetManager.getFleet(user, fleet, function (objFleet) {
            objFleet.displayTitle = display;
            gameContext.FleetManager.updateFleet(user, fleet, objFleet, function () {
                res.send({ success: true });
                res.end();
            });
        }, function () {
            res.send({ success: false, msg: 'Fleet does not exist.' });
            res.end(); 
        });
    });
    
    // Capture a defensless planet.
    app.post('/fleet/capture', function (req, res) {
        thrust('capturePlanet', gameContext, { x: req.body.px, y: req.body.py, user: req.body.user, fleet: req.body.fleet }, req, res, null, function (distance, time) {
            gameContext.TimerManager.spawn("capture", req.body.user, time, "Fleet arrives to attempt a planetary capture.", "yellow", { x: req.body.px, y: req.body.py });
        });
    });
    
    app.post('/fleet/merge', function (req, res) {
        thrust('fleetMerge', gameContext, { user: req.body.user, fleet1: req.body.px, fleet2: req.body.py }, req, res, null, function (distance, time) { });
    });

    app.post('/fleet/thrust', function (req, res) {
        thrust('', gameContext, {}, req, res, null);
    });
    
    app.post('/fleet/station', function (req, res) {
        thrust('defendPlanet', gameContext, { x: req.body.px, y: req.body.py, user: req.body.user, fleet: req.body.fleet }, req, res, null, function () {

        });
    });
    
    app.post('/fleet/colonize', function (req, res) {
        thrust('fleetDeployTerraformer', gameContext, { x: req.body.px, y: req.body.py, user: req.body.user, fleet: req.body.fleet }, req, res, null, function () {});
    });
    
    // This function takes two fleets. An attacker and defender.
    // It then calculates the current position of each fleet and 
    // derives an intercept course between the two. Then it spawns
    // off a thrust vector change request set with combat at the end.
    //
    // NOTE: this needs to happen server-side to prevent hacking.
    app.post('/fleet/combat', function (req, res) {
        combat(gameContext, req, res);
    });
    
    app.post('/fleet/capturestats', function (req, res) {
        var user = req.body.user;
        var fleet = req.body.fleet;
        var planetX = req.body.x;
        var planetY = req.body.y;
        gameContext.FleetManager.getFleet(user, fleet, function (objFleet) {
            gameContext.Map.getInfo(planetX, planetY, function (planet) {
                // Calculate the attack power.
                var report = gameContext.ShipManager.getCombatReport(objFleet.ships, [], parseFloat(planet.shields), parseFloat(planet["Suborbital ArrayOwned"] || 1));
                res.send({
                    success: true,
                    report: report
                });
                res.end();
            });
        }, function () {
            res.send({ success: false });
            res.end(); 
        });
    });
    
    app.post('/fleet/battlestats', function (req, res) {
        var fleet1User = req.body.fleet1User;
        var fleet1Name = req.body.fleet1Name;
        var fleet2User = req.body.fleet2User;
        var fleet2Name = req.body.fleet2Name;

        // Get the fleets.
        gameContext.FleetManager.getAllFleets(function (fleets) {
            var f1ships = [];
            var f2ships = [];
            var planetX, planetY;

            for (var i = 0; i < fleets.length; i++) {
                if (fleets[i].user == fleet1User && fleets[i].title == fleet1Name) {
                    f1ships = fleets[i].ships;
                } else if (fleets[i].user == fleet2User && fleets[i].title == fleet2Name) {
                    planetX = fleets[i].planetX;
                    planetY = fleets[i].planetY;
                    f2ships = fleets[i].ships;
                }
            }
            
            if (f1ships.length == 0 || f2ships.length == 0) {
                res.send({ success: false, msg: 'One or more of the fleets does not exist.' });
                res.end();
            } else {
                // Get the combat report.
                if (planetX && planetY) {
                    gameContext.Map.getInfo(planetX, planetY, function (planet) {
                        // Calculate the attack power.
                        var report = gameContext.ShipManager.getCombatReport(f1ships, f2ships, parseFloat(planet.shields), parseFloat(planet["Suborbital ArrayOwned"]));
                        res.send({
                            success: true,
                            report: report
                        });
                        res.end();
                    });
                } else {
                    var report = gameContext.ShipManager.getCombatReport(f1ships, f2ships);
                    res.send({
                        success: true,
                        report: report
                    });
                    res.end();
                }
            }
        });
    }); 

    app.post('/fleet/create', function (req, res) {
        var user = req.body.user;
        var title = req.body.title;
        
        // These should be FLEET COORDINATES
        // not sector coordinates.
        var x = parseFloat(req.body.x);
        var y = parseFloat(req.body.y);
        
        // These are actually map coordinates.
        var sx = parseFloat(req.body.sx);
        var sy = parseFloat(req.body.sy);
        var ships = req.body.ships;
        
        gameContext.FleetManager.createFleet(user, title, x, y, sx, sy, {}, function (guid) {
            gameContext.VictoryManager.fireEvent(user, "fleet", 1);
            gameContext.FleetManager.addShips(user, guid, ships, sx, sy, function () {
                res.send({ success: true });
                res.end(); 
            });
        });
    });

};