﻿var thrust = require('../common/thrust');
module.exports = function (app, gameContext) {
    
    // New fleet system.
    app.post('/asteroid/defend', function (req, res) {
        var ax = req.body.px;
        var ay = req.body.py;
        thrust('defendAsteroid', gameContext, { x: ax, y: ay, user: req.body.user, fleet: req.body.fleet }, req, res, null, function (distance, time) {
            gameContext.TimerManager.spawn("defending", req.body.user, time, "Fleet arrives to defend an asteroid in sector " + gameContext.Map.getQuadrant(ax, ay), "blue", { x: ax, y: ay });
        });
    });

    app.post('/asteroid/structures', function (req, res) {
        res.send(gameContext.AsteroidManager.getStructures());
    });
    
    app.post('/asteroid/deploy', function (req, res) {
        var x = req.body.x;
        var y = req.body.y;
        var user = req.body.user;
        var type = req.body.type;
        var platform = gameContext.AsteroidManager.find(type);

        if (platform) {
            // Validate the user resources.
            gameContext.UserManager.getUser(user, function (objUser) {
                var errorMsg = null;
                if (objUser.h3 < platform.h3Cost)
                    errorMsg = 'Insufficient H3 to complete this transaction.';
                if (objUser.gold < platform.goldCost)
                    errorMsg = 'Insufficient gold to complete this transaction.';
                if (objUser.iron < platform.ironCost)
                    errorMsg = 'Insufficient iron to complete this transaction.';
                if (parseFloat(objUser.credits) < platform.creditCost)
                    errorMsg = "Insufficient credits to complete this transaction.";

                if (errorMsg) {
                    res.send({ success: false, msg: errorMsg });
                    res.end();
                } else {
                    // Debit the resources and deploy the building.
                    gameContext.UserManager.debitResources(user, platform.goldCost, platform.ironCost, platform.h3Cost, 0);
                    gameContext.UserManager.debitCredits(user, platform.creditCost);
                    
                    var offset = 0;
                    var uid = gameContext.TimerManager.spawn('platform', user, gameContext.getSettings(1).asteroidDeployTime - offset, "Until " + type + " is built on the asteroid in sector " + gameContext.Map.getQuadrant(x, y), "brown", { x: x, y: y });
                    var end = gameContext.TimerManager.find(uid).end

                    gameContext.Map.updateProperty(x, y, "platform" + type, end, function () {
                        gameContext.QueueManager.enqueue('deployAsteroid', { x: x, y: y, user: user, type: type }, gameContext.getSettings(1).asteroidDeployTime - offset);
                        res.send({ success: true });
                    });
                }
            });
        }
    });
    
    app.post('/asteroid/report', function (req, res) {
        var x = req.body.x;
        var y = req.body.y;
        var user = req.body.user;
        var ships = req.body.ships;
        
        // Get the current ships.
        gameContext.Map.getInfo(x, y, function (asteroid) {
            if (asteroid.ships) {
                var probability = gameContext.ShipManager.getCombatReport(ships, asteroid.ships);
                res.send({ probability: probability });
                res.end();
            } else {
                res.send({ success: false });
                res.end();
            }
        });
    });

    app.post('/asteroid/protect', function (req, res) {
        var x = req.body.x;
        var y = req.body.y;
        var user = req.body.user;
        var ships = req.body.ships;
        var mission = y + '-' + x;

        // Protect.
        gameContext.ShipManager.deployFleet('protect', mission, ships, x, y, user, function () {
            var uid = gameContext.TimerManager.spawn('protect', user, gameContext.getSettings(1).asteroidProtectTime, 'Until your fleet arrives to protect an asteroid.', "brown", { x: x, y: y, dismiss: true });
            gameContext.QueueManager.enqueue('protectAsteroid', { x: x, y: y, user: user, ships: ships }, gameContext.getSettings(1).asteroidProtectTime);
            gameContext.Map.pushProperty(x, y, "protectTimers", uid, function () {
                res.send({ success: true });
                res.end();
            }); 
        });
    });
    
    app.post('/asteroid/mine', function (req, res) {
        var x = req.body.x;
        var y = req.body.y;
        var user = req.body.user;

        // Get the asteroid so we can see which fleet is defending it.
        gameContext.AsteroidManager.getAsteroid(x, y, function (objAsteroid) {
            var fleet = objAsteroid.defenderFleet;
            // Verify the fleet has a mining vessel.
            gameContext.FleetManager.getFleet(user, fleet, function (objFleet) {
                // Iterate over the list of ships and 
                var hasMiner = false;
                objFleet.ships = objFleet.ships || [];
                for (var i = 0; i < objFleet.ships.length; i++) {
                    var shipName = objFleet.ships[i][0];
                    var g_ship = gameContext.ShipManager.find(shipName);

                    // Check the properties of the ship.
                    if (g_ship) {
                        if (g_ship.miningIronMultiplier > 0 
                        || g_ship.miningGoldMultiplier > 0 
                        || g_ship.miningH3Multiplier > 0) {
                            hasMiner = true;
                        }
                    }
                }
                
                // If they don't have a miner, abort.
                if (!hasMiner) {
                    res.send({ success: false, msg: 'The fleet protecting this asteroid does not have any mining vessels and cannot harvest any resources.' });
                    res.end();
                } else {                    
                    // Figure out the duration.
                    var duration = gameContext.getSettings(1).asteroidMine1;
                    var uid = gameContext.TimerManager.spawn("mineAsteroid", user, duration, "Until your mining ships collect resources from an asteroid in sector " + gameContext.Map.getQuadrant(x, y) + ".", "brown", { x: x, y: y, dismiss: true });
                    gameContext.FleetManager.putProperty(user, fleet, "miningUID", uid, function () {
                        gameContext.QueueManager.enqueue("mineAsteroid", { x: x, y: y, user: user, fleet: fleet }, duration, function () {
                            res.send({ success: true, uid: uid });
                            res.end();
                        });
                    });
                }
            }, function () {
                res.send({ success: false, msg: 'The specified fleet does not exist.' });
                res.end();         
            });
        });
    });

    app.post('/asteroid/recall', function (req, res) {
        var x = req.body.x;
        var y = req.body.y;
        var user = req.body.user;
        
        gameContext.QueueManager.enqueue('recallAsteroid', { x: x, y: y, user: user }, gameContext.getSettings(1).asteroidRecallTime);
        var uid = gameContext.TimerManager.spawn('recall', user, gameContext.getSettings(1).asteroidRecallTime, "Until your fleet returns from the asteroid in sector " + gameContext.Map.getQuadrant(x, y), "brown", { x: x, y: y, dismiss: true });
        gameContext.Map.pushProperty(x, y, "recallTimers", uid, function () {
            gameContext.SocketManager.updateNotifications(user);
            res.send({ success: true });
            res.end();
        });
    });

};