﻿
module.exports = function (app, gameContext) {
    
    app.post('/buildings/get', function (req, res) {
        res.send(gameContext.BuildingManager.getBuildings());
    });
    
};