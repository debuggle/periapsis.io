﻿module.exports = function (app, gameContext) {
    app.post('/science/structures', function (req, res) {
        res.send(gameContext.ScienceManager.getStructures());
        res.end();
    });
    
    app.post('/science/fix', function (req, res) {
        var x = req.body.x;
        var y = req.body.y;
        var sector = gameContext.Map.getQuadrant(x, y);

        gameContext.QueueManager.enqueue('destroyAnomaly', { sector: sector }, 1);
        res.send({ success: true });
        res.end();
    });

    app.post('/science/stabilize', function (req, res) {
        var x = req.body.x;
        var y = req.body.y;
        var user = req.body.user;
        var sector = gameContext.Map.getQuadrant(x, y);
        
        // Generate a stabilize timer.
        // Check if the user has any mythos.
        gameContext.ShipManager.getUserShips(user, function (ships) {
            var availableMythos = false;
            
            // Iterate over the ships and try to find a Mythos.
            for (var i = 0; i < ships.length; i++) {
                if (ships[i][0] == 'Mythos' && parseFloat(ships[i][1]) > 0) {
                    availableMythos = true;
                }
            }

            // If they have an available ship, then use it.
            if (availableMythos) {
                var uid = gameContext.TimerManager.spawn('stabilize', user, gameContext.getSettings(1).anomalyStabilizeTime, "Until your Mythos arrive to stabilize the anomaly.", "purple");
                gameContext.ScienceManager.push(x, y, 'stabilizeTimer', uid, function () {
                    
                    // Now we need to send a mtyhos.
                    gameContext.ShipManager.deployFleet('stabilize', 'mythos-' + sector, [['Mythos', 1]], x, y, user, function () {
                        gameContext.QueueManager.enqueue('stabilizeAnomaly', { sector: sector, x: x, y: y }, gameContext.getSettings(1).anomalyStabilizeTime);
                        res.send({ success: true, uid: uid });
                        res.end();
                    });
                }, function () {
                    res.send({ success: false });
                    res.end();
                });
            } else {
                // Otherwise, throw an error.
                res.send({ success: false, msg: 'This action requires a Mythos ship to complete. You do not have any available.' });
                res.end();
            }            
        });

        
    });
    
    app.post('/science/protect', function (req, res) {
        var x = req.body.x;
        var y = req.body.y;
        var user = req.body.user;
        var ships = req.body.ships;
        
        // Generate a protection timer.
        var uid = gameContext.TimerManager.spawn('protect', user, gameContext.getSettings(1).anomalyProtectTime, "Until your fleet arrives to guard the anomaly.", "purple");
        gameContext.ScienceManager.push(x, y, "protectTimer", uid, function () {
            var sector = gameContext.Map.getQuadrant(x, y);
            gameContext.ShipManager.deployFleet('protect', 'protect-' + sector, ships, x, y, user, function () {
                gameContext.QueueManager.enqueue('protectAnomaly', { x: x, y: y, ships: ships, user: user }, gameContext.getSettings(1).anomalyProtectTime);
                res.send({ success: true });
                res.end();
            });
        }, function () {
            res.send({ success: false });
            res.end();
        });
    });

    app.post('/science/deploy', function (req, res) {
        var x = req.body.x;
        var y = req.body.y;
        var user = req.body.user;
        var type = req.body.type;

        // Find the structure.
        var structure = gameContext.ScienceManager.findStructure(type);
        if (structure != null) {
            
            // Debit the resources.
            gameContext.UserManager.debitResources(user, structure.goldCost, structure.ironCost, structure.h3Cost, 0);

            // Generate a timer.
            var uid = gameContext.TimerManager.spawn('structure', user, gameContext.getSettings(1).anomalyStructureTime, 'Until ' + type + ' structure is deployed into the anomaly.', "purple");
            gameContext.QueueManager.enqueue('deployAnomaly', { x: x, y: y, type: type }, gameContext.getSettings(1).anomalyStructureTime);
            gameContext.ScienceManager.updateAnomaly(x, y, structure.type + "Action", gameContext.TimerManager.find(uid).end, function () {
                gameContext.SocketManager.updateNotifications(user);
                gameContext.SocketManager.updateAnomaly(user, x, y);
                res.send({ success: true });
                res.end();
            });
        } else {
            res.send({ success: false });
            res.end();
        }
    });
};