﻿
module.exports = function (app, gameContext) {
    
    app.post('/diplomacy/get', function (req, res) {
        var user = req.body.username;
        gameContext.DiplomacyManager.getTradeRoutes(user, function (routes) {
            res.send(routes);
            res.end();
        });
    });
    
    app.post('/diplomacy/requestTrade', function (req, res) {
        var x = req.body.x;
        var y = req.body.y;
        var user = req.body.username;
        var target = req.body.target;

        gameContext.DiplomacyManager.establishTradeRoute(x, y, user, target, function () {
            res.send({ success: true });
            res.end();
        });

    });

    app.post('/diplomacy/updateRequest', function (req, res) {
        var x = req.body.x;
        var y = req.body.y;
        var user = req.body.username;
        var target = req.body.target;
        var accepted = req.body.accepted;

        gameContext.DiplomacyManager.updateTradeRequest(x, y, user, target, accepted, function () {
            res.send({ success: true });
            res.end();
        });
    });

};