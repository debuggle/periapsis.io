﻿module.exports = function (app, gameContext) {
    
    app.post('/planet/shieldboost', function (req, res) {
        var x = req.body.x;
        var y = req.body.y;
        var user = req.body.user;

        gameContext.UserManager.getUser(user, function (objUser) {
            var credits = parseFloat(objUser.credits);
            if (credits < 5) {
                res.send({ success: false, msg: 'You do not have enough credits to complete this action.' });
                res.end();
            } else {
                // Boost the shields.
                gameContext.Map.getInfo(x, y, function (planet) {
                    if (parseFloat(planet.shields) >= 100) {
                        res.send({ success: false, msg: 'This planet is already at maximum shields!' });
                        res.end();
                    } else {
                        gameContext.UserManager.debitCredits(user, 5, function () {
                            planet.shields = Math.max(parseFloat(planet.shields || 50) + 25, 150);
                            gameContext.Map.updateProperty(x, y, "shields", planet.shields, function () {
                                res.send({ success: true });
                                res.end();
                            }, function () {
                                res.send({ success: false, msg: 'Oops! Please try that again.' });
                                res.end();
                            });
                        });
                    }
                });
            }
        });
    });
    
    app.post('/planet/productionboost', function (req, res) {
        var x = req.body.x;
        var y = req.body.y;
        var user = req.body.user;

        gameContext.UserManager.getUser(user, function (objUser) {
            var credits = parseFloat(objUser.credits);
            if (credits < 5) {
                res.send({ success: false, msg : 'You do not have enough credits to complete this action.' });
                res.end();
            } else {
                gameContext.Map.getInfo(x, y, function (planet) {
                    gameContext.UserManager.debitCredits(user, 5, function () {
                        var uid = gameContext.TimerManager.spawn("productionBoost", user, 24 * 60 * 60, "Production boost on planet " + planet.name, "blue", { x: x, y: y });
                        gameContext.Map.updateProperty(x, y, "productionBoost", uid, function () {
                            gameContext.Map.updateProperty(x, y, "productionBoostEnd", gameContext.TimerManager.find(uid).end, function () {
                                res.send({ success: true, uid: uid });
                                res.end();
                            });
                        });
                    });
                });
            }
        });
    });

    app.post('/planet/colonize/:x/:y/:user', function (req, res) {
        var x = req.params.x;
        var y = req.params.y;
        var user = req.params.user;
        
        
        gameContext.ShipManager.getUserShips(user, function (ships) {
            var canColonize = false;
            ships = ships || [];
            for (var i = 0; i < ships.length; i++) {
                if (ships[i][0] == 'Terraformer') {
                    canColonize = true;
                    break;
                }
            }

            if (!canColonize) {
                res.send({
                    success: false,
                    msg: 'You need a terraformer before you can colonize this planet.'
                });
            } else {
                gameContext.ShipManager.deployFleet('colonize', "terraform-" + y + '-' + x, [['Terraformer', 1]], 0, 0, user, function () {
                    gameContext.Map.getInfo(x, y, function (planet) {
                        var uid = gameContext.TimerManager.spawn('colonize', user, gameContext.getSettings(1).colonizeTime, "Colonizing planet " + planet.name, "blue", { x: x, y: y });
                        
                        gameContext.Map.updateProperty(x, y, "beingColonized", true, function () {
                            gameContext.Map.updateProperty(x, y, "colonizeStart", gameContext.now(), function () {
                                gameContext.Map.updateProperty(x, y, "colonizeEnd", gameContext.TimerManager.find(uid).end, function () {
                                    gameContext.Map.updateProperty(x, y, "colonizeAction", uid, function () {
                                        gameContext.QueueManager.enqueue('colonize', { x: x, y: y, user: user, }, gameContext.getSettings(1).colonizeTime);
                                        res.send({ success: true, uid: uid });
                                        res.end();                                                                                                                                 
                                    });
                                });
                            });
                        });
                    });         
                });
            }
        });
    });
    
    app.post('/planet/scan/:x/:y', function (req, res) {
        var x = req.params.x;
        var y = req.params.y;
        var user = req.body.username;
        var uid = gameContext.TimerManager.spawn('scan', user, gameContext.getSettings(1).scanTime, "Scanning planet " + req.body.planetName, "blue", { x: x, y: y });
        
        // Invoke the necessary things.
        gameContext.Map.updateProperty(x, y, "scanAction", uid);
        gameContext.QueueManager.enqueue('scan', { x: x, y: y, user: user }, gameContext.getSettings(1).scanTime);

        setTimeout(function () {
            res.send(gameContext.TimerManager.find(uid));
            res.end();
        }, 100);
    });

    app.post('/planet/construct', function (req, res) {
        var x = req.body.x;
        var y = req.body.y;
        var user = req.body.username;
        var type = req.body.type;
        var duration = gameContext.getSettings(1).baseBuildTime * gameContext.BuildingManager.find(type).timeManipulator;

        var cost = gameContext.BuildingManager.findCost(type);
        gameContext.UserManager.debitResources(user, cost, 0, 0, 0);
        gameContext.QueueManager.enqueue('construct', { x: x, y: y, type: type, user: user }, duration);
        var uid = gameContext.TimerManager.spawn('construct', user, duration, "Building " + type, "blue", { x: x, y: y });
        gameContext.Map.updateProperty(x, y, type + "Action", uid);
        
        setTimeout(function () {
            res.send(gameContext.TimerManager.find(uid));
            res.end();
        }, 100);
    });

    app.post('/planet/upgrade', function (req, res) {
        var x = req.body.x;
        var y = req.body.y;
        var user = req.body.username;
        var type = req.body.type;

        // Get the current info.
        gameContext.Map.getInfo(x, y, function (planet) {
            
            // Calculate the cost.
            var building = gameContext.BuildingManager.find(type);
            if (building) {
                var level = parseFloat(planet[type + "Owned"] || 0);
                var cost = (level + 1) * building.cost;
                var time = gameContext.getSettings(1).upgradeTime * (level + 1) * building.timeManipulator;
                gameContext.UserManager.debitResources(user, cost, 0, 0, 0);
                gameContext.QueueManager.enqueue('upgrade', { x: x, y: y, type: type, level: level }, time);
                
                var uid = gameContext.TimerManager.spawn('upgrade', user, time, "Upgrading " + building.title + " to level " + (level + 1), "blue", { x: x, y: y });
                gameContext.Map.updateProperty(x, y, type + "Action", uid, function () {
                    gameContext.SocketManager.updatePlanet(user);         
                });

                // Respond with the timer UID.
                res.send(gameContext.TimerManager.find(uid));
                res.end();
            } else {
                res.end();
            }
            
        });
    });

};