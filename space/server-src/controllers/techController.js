﻿module.exports = function (app, gameContext) {

    app.post('/tech/tree', function (req, res) {
        gameContext.TechManager.getAll(req.body.user, function (tree) {
            res.send(tree); 
        });
    });

    app.post('/tech/buy', function (req, res) {
        var user = req.body.user;
        var title = req.body.tech;
        gameContext.TechManager.userHasTech(user, title, function (has) {
            if (has) {
                res.send({ success: false });
            } else {
                var tech = gameContext.TechManager.find(title, gameContext.TechManager.tree);
                if (tech) {
                    // Get the user info.
                    gameContext.UserManager.getUser(user, function (objUser) {
                        // Check if they can afford it.
                        if (parseFloat(objUser.science || 0) >= parseFloat(tech.cost)) {
                            gameContext.UserManager.debitResources(user, 0, 0, 0, parseFloat(tech.cost));
                            gameContext.TechManager.giveTech(user, title, function () {
                                gameContext.UserManager.addNotification(user, "yellow", "You have acquired '" + title + "' ");
                                res.send({ success: true });                             
                            });
                        } else {
                            res.send({ success: false, msg: 'You do not have enough science to buy this upgrade.' });
                        }
                    }, function () {
                        res.send({ success: false });
                    });
                } else {
                    res.send({ success: false });
                }
            }
        });
    });

};