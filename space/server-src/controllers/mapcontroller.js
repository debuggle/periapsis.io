﻿var thrust = require('../common/thrust');

module.exports = function (app, gameContext) {

    app.post('/map/sector/:x/:y/:user', function (req, res) {
        var x = req.params.x;
        var y = req.params.y;
        var user = req.params.user;
        var sector = gameContext.Map.getQuadrant(x, y);

        // Check if the user has explored this sector before.
        gameContext.UserManager.getUser(user, function (objUser) {
            gameContext.FleetManager.getAllFleets(function (fleets) {
                var accessible = (objUser.sector || {})[sector];
                var explored = (objUser.explored || {})[sector] || false;
                
                // If it's not accessible but they've explored it then
                // check if they have ships in the area.
                if (explored && !accessible) {
                    for (var i = 0; i < fleets.length; i++) {
                        if (fleets[i].user == user && fleets[i].sector == sector) {
                            accessible = true;
                        }
                    }
                }

                if (accessible) {
                    gameContext.Map.scan(x, y, function (map) {
                        
                        // Create an array
                        var prop_array = [];
                        var map_array = [];
                        var members = [];
                        for (var prop in map) {
                            prop_array.push(prop);
                        }
                        
                        for (var i = 0; i < prop_array.length; i++) {
                            var item = map[prop_array[i]];
                            if (members.indexOf(item.owner || item.protector || '') >= 0)
                                item.guild = true
                            else
                                item.guild = false;
                            map_array.push(item);//{ type: item.type, x: item.x, y: item.y, owner: item.owner });
                        }
                        
                        
                        // Define the logic for building the explored graph.
                        var visited = {};
                        function buildGraph(gX, gY, depth) {
                            depth = depth || 0;
                            var size = gameContext.Map.getQuadrantSize();
                            var sector = gameContext.Map.getQuadrant(gX, gY);
                            var explored = (objUser.explored || {}).hasOwnProperty(sector);
                            var colonized = (objUser.sector || {}).hasOwnProperty(sector);
                            
                            var node = {
                                sector: sector,
                                mapX: gX,
                                mapY: gY,
                                x: Math.round((parseFloat(req.params.x) - gX) / size),
                                y: Math.round((parseFloat(req.params.y) - gY) / size),
                                explored: explored,
                                colonized: colonized,
                                children: []
                            };
                            
                            // If we have explored this sector, show all the adjacent ones.
                            if (visited.hasOwnProperty(sector)) return {};
                            visited[sector] = true;
                            if (explored || colonized) {
                                node.children.push(buildGraph(gX, gY - size, depth));
                                node.children.push(buildGraph(gX, gY + size, depth));
                                node.children.push(buildGraph(gX - size, gY, depth));
                                node.children.push(buildGraph(gX + size, gY, depth));                                
                            }
                            return node;
                        };
                        
                        // Actually construct the result.
                        var graph = buildGraph(parseFloat(req.params.x), parseFloat(req.params.y));
                        
                        res.send({
                            success: true, 
                            quadrant: sector,
                            normalized: gameContext.Map.normalizeSector(sector),
                            width: gameContext.Map.getQuadrantSize(), 
                            height: gameContext.Map.getQuadrantSize(), 
                            map: map_array,
                            graph: graph
                        });
                        res.end();
                    });
                } else {
                    res.send({
                        success: false,
                        probeRequired: true,
                        msg: 'You have not explored this sector before.'
                    });
                    res.end();
                }
            });
        });        
    });
    
    app.post('/map/explore', function (req, res) {
        var user = req.body.user;
        var px = req.body.px;
        var py = req.body.py;
        thrust('revealSector', gameContext, { user: user, x: px, y: py }, req, res, null, function (distance, time) {
            gameContext.TimerManager.spawn("explore", user, time, "Until your fleet arrives to explore sector " + gameContext.Map.getQuadrant(px, py) + ".", "yellow", { x: px, y: py });
        });
    });
    
    //app.post('/map/probe', function (req, res) {
    //    var x = req.body.x;
    //    var y = req.body.y;
    //    var user = req.body.user;
    //    var fleet = req.body.fleet;
    //    var sector = gameContext.Map.getQuadrant(x, y);
        
        
    //    // Get the user ships.
    //    gameContext.ShipManager.getUserShips(user, function (ships) {
    //        var found = false;
    //        for (var i = 0; i < ships.length; i++) {
    //            if (ships[i][0] == "Space Probe" && parseFloat(ships[i][1]) > 0) {
    //                found = true;
    //            }
    //        }

    //        if (!found) {
    //            res.send({
    //                success: false,
    //                msg: 'You do not have any space probes.'
    //            });
    //            res.end();
    //        } else {
    //            // Add this sector to the user.
    //            gameContext.UserManager.getUser(user, function (objUser) {
    //                objUser.sector = objUser.sector || {};
    //                objUser.sector[sector] = true;
    //                gameContext.UserManager.putValue(user, "sector", objUser.sector);
    //            });
                
    //            // Trigger the quest stuff.
    //            gameContext.AdventureManager.processEvent(user, { newSector: true });

    //            // They have one!
    //            gameContext.ShipManager.killShip("Space Probe", false, undefined, user, function () {
    //                res.send({
    //                    success: true
    //                });
    //                res.end();
    //            });
    //        }
    //    });
    //});

    app.post('/map/get/:x/:y', function (req, res) {
        gameContext.Map.getInfo(req.params.x, req.params.y, function (planet) {
            if (planet.owner) {
                gameContext.UserManager.getUser(planet.owner, function (objUser) {
                    
                    // Get information about the owner.
                    planet.ownerGuild = objUser.guild;
                    planet.ownerTag = objUser.tag || '';
                    planet.ownerScore = objUser.score || 0;

                    res.send({ map: planet });
                    res.end();
                }, function () {
                    res.send({ map: planet });
                    res.end();         
                });
            } else {
                res.send({ map: planet });
                res.end();
            }
            
        });
    });

};