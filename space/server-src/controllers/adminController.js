﻿module.exports = function (app, gameContext) {
    
    app.get('/admin/offline', function (req, res) {
        gameContext.db0.takeOffline();
        gameContext.SocketManager.takeOffline();
        res.send({ success: true });
    });
    
    app.post('/admin/broadcast', function (req, res) {
        var msg = req.body.msg;
        gameContext.UserManager.getAllUsers(function (users) {
            for (var i = 0; i < users.length; i++) {
                gameContext.UserManager.addNotification(users[i], "yellow", "SERVER MESSAGE: " + msg + "   - " + (new Date().toTimeString()), {});
            }
            res.send({ success: true });
            res.end();
        });
    });

    app.post('/admin/giveresources', function (req, res) {
        var user = req.body.user;
        var iron = req.body.iron || 0;
        var gold = req.body.gold || 0;
        var h3 = req.body.h3 || 0;
        var science = req.body.science || 0;

        gameContext.UserManager.addResources(user, iron, gold, h3, science);
        gameContext.UserManager.addNotification(user, "yellow", "You have been given some resources by an administrator.", {});
        res.send({ success: true });
        res.end();
    });

    app.post('/admin/giveplanet', function (req, res) {
        var user = req.body.user;
        var x = req.body.x;
        var y = req.body.y;
        var sector = gameContext.Map.getQuadrant(x, y);

        gameContext.Map.changeOwner(x, y, user, function () {
            gameContext.SocketManager.updateMap(user);
            gameContext.UserManager.addNotification(user, "yellow", "You have been given a planet in sector " + sector + " by an administrator.", {});
            res.send({ success: true });
            res.end();
        });
    });

};