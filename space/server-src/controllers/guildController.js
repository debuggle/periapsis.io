﻿module.exports = function (app, gameContext) {
    app.post('/guild/create', function (req, res) {
        var user = req.body.user;
        var guildName = req.body.guild;
        
        gameContext.GuildManager.getGuild(guildName, function () {
            res.send({ success: false, msg: 'There is already a guild with this name!' });
            res.end();
        }, function () {
            gameContext.UserManager.getUser(user, function (objUser) {
                if ((objUser.guild || '').length > 0) {
                    res.send({ success: false, msg: 'You are already in a guild!' });
                    res.end();
                } else {
                    gameContext.GuildManager.create(guildName, user, function () {
                        res.send({ success: true });
                    }, function () {
                        res.send({ success: false, msg: 'There was a problem creating your guild.' });
                        res.end();
                    });
                }
            });
        });
    });

    app.post('/guild/join', function (req, res) {
        var user = req.body.user;
        var guild = req.body.guild;

        gameContext.UserManager.getUser(user, function (objUser) {
            if ((objUser.guild || '').length > 0) {
                res.send({ success: false, msg: 'You are already in a guild!' });
                res.end();
            } else if ((objUser.guild_invites || []).indexOf(guild) >= 0) {
                gameContext.GuildManager.join(guild, user, function () {
                    gameContext.UserManager.putValue(user, "guild_invites", []);
                    res.send({ success: true });
                    res.end();
                });
            } else {
                res.send({ success: false, msg: 'You have not been invited to join that guild.' });
                res.end();
            }
        });
    });
    
    app.post('/guild/leave', function (req, res) {
        var user = req.body.user;
        var guild = req.body.guild;

        gameContext.GuildManager.leave(guild, user, function () {
            res.send({ success: true });
        });
    });
    
    app.post('/guild/decline', function (req, res) {
        var user = req.body.user;
        var guild = req.body.guild;

        gameContext.GuildManager.getGuild(guild, function (objGuild) {
            gameContext.UserManager.getUser(user, function (objUser) {
                objUser.guild_invites = objUser.guild_invites || [];
                if (objUser.guild_invites.indexOf(guild) >= 0)
                    objUser.guild_invites.splice(objUser.guild_invites.indexOf(guild), 1);
                
                gameContext.UserManager.putValue(user, "guild_invites", objUser.guild_invites, function () {
                    gameContext.UserManager.addNotification(objGuild.founder, gameContext.GuildManager.color, "The play '" + user + "' has declined to join your guild.");
                    res.send({ success: true });
                    res.end();
                });
            });            
        });
    });
    
    app.post('/guild/give', function (req, res) {
        var username = req.body.user;
        var target = req.body.target;
        
        // Resources to give
        var iron = req.body.iron || 0;
        var gold = req.body.gold || 0;
        var h3 = req.body.h3 || 0;
        
        gameContext.UserManager.findUser(username, function (user) {
            gameContext.UserManager.getUser(user, function (objUser) {
                // Verify they have the resources to give.
                if (objUser.gold >= gold && objUser.iron >= iron && objUser.h3 >= h3) {
                    gameContext.GuildManager.isSameGuild(user, target, function (same) {
                        if (same) {
                            gameContext.UserManager.debitResources(user, gold, iron, h3, 0);
                            gameContext.UserManager.addResources(target, iron, gold, h3, 0);
                            gameContext.UserManager.addNotification(target, gameContext.GuildManager.color, user + " has given you " + gold + " gold, " + iron + " iron, " + h3 + " h3.");
                            gameContext.UserManager.addNotification(user, gameContext.GuildManager.color, "You have given some resources to " + target);
                            
                            // Update resources.
                            gameContext.SocketManager.updateResources(user);
                            gameContext.SocketManager.updateResources(target);
                            
                            res.send({ success: true });
                            res.end();
                        } else {
                            res.send({ success: false, msg: 'You are not members of the same guild!' });
                            res.end();
                        }
                    });
                } else {
                    res.send({ success: false, msg: 'You do not have sufficient resources.' });
                    res.end();
                }
            });
        }, function () {
            res.send({ success: false, msg: 'That user does not exist.' });
            res.end();   
        });
    });
    
    app.post('/guild/invite', function (req, res) {
        var user = req.body.user;
        var guild = req.body.guild;
        var target = req.body.target;
        
        gameContext.GuildManager.getGuild(guild, function (objGuild) {
            // Make sure this user is an admin.
            if (objGuild.admins.indexOf(user) >= 0) {
                gameContext.UserManager.findUser(target, function (username) {
                    gameContext.UserManager.getUser(username, function (objUser) {
                        // Make sure they have not already been invited.
                        if ((objUser.guild_invites || []).indexOf(guild) >= 0) {
                            // They have already been invited.
                            res.send({ success: false, msg: 'This player has already been invited to the guild.' });
                            res.end();
                        } else {
                            // Actually invite them.
                            gameContext.UserManager.pushValue(username, "guild_invites", guild, function () {
                                gameContext.UserManager.addNotification(username, gameContext.GuildManager.color, "You have been invited to the guild '" + guild + "'");
                                gameContext.SocketManager.updateGuild(username);
                                res.send({ success: true });
                                res.end();
                            });
                        }
                    });
                }, function () {
                    res.send({ success: false, msg: 'That player does not exist.' });
                    res.end();           
                });
            } else {
                res.send({ success: false, msg: 'Only the founder can invite new members to the guild.' });
                res.end();
            }
        });
    });
    
    app.post('/guild/message', function (req, res) {
        var user = req.body.user;
        var message = req.body.message;
        
        gameContext.UserManager.getUser(user, function (objUser) {
            if ((objUser.guild || '').length > 0) {
                gameContext.GuildManager.message(objUser.guild, user, user + " says '" + message + "'", function () {
                    res.send({ success: true });
                    res.end();
                });
            } else {
                res.send({ success: false, msg: 'You do not have a guild!' });
                res.end();
            }
        });
        
    });
    
    app.post('/guild/disband', function (req, res) {
        var user = req.body.user;
        var guild = req.body.guild;

        gameContext.GuildManager.disband(guild, user, function () {
            res.send({ success: true });
            res.end();
        }, function () {
            res.send({ success: true, msg: 'You are not authorized to disband this guild.' });
            res.end(); 
        });
    });

    app.get('/guild/get/:guild', function (req, res) {
        var guild = req.params.guild;
        gameContext.GuildManager.getGuild(guild, function (objGuild) {
            res.send(objGuild);
            res.end();
        });
    });
};