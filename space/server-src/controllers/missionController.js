﻿module.exports = function (app, gameContext) {
    
    app.post('/mission/report', function (req, res) {

        var ships = req.body.ships;
        var x = req.body.x;
        var y = req.body.y;

        gameContext.MissionManager.getAttackReport(ships, x, y, function (report) {
            res.send(report);
            res.end();
        });

    });

    app.post('/mission/list', function (req, res) {

        var user = req.body.username;
        res.send([]);
        res.end();
        //gameContext.AdventureManager.getAll(user, function (quests) {
        //    res.send(quests);
        //    res.end();
        //});

    });

    app.post('/mission/accept', function (req, res) {
        var user = req.body.username;
        var quest = req.body.quest;
        
        gameContext.UserManager.acceptQuest(user, quest);
        res.end();
    });

};