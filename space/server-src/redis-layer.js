﻿var redis = require('redis'),
    copy = require('deep-copy'),
    uuid = require('node-uuid'),
    fs = require('fs'),
    client;

if (process.argv[2] == 'DEBUG') {
    client = redis.createClient({
        host: '192.168.0.103'
    });
} else {
    client = redis.createClient({
        host: 'periapsis.cloudapp.net',
        port: 1337
    });
    client.auth('Kc7osjkl!1IWantToWork@TSalesForce!)(PrettyGoodJustAdding3939');
}

function isComplex(obj) {
    return (typeof (obj) == typeof ({}) || Array.isArray(obj));
}

Database = function () {
    this.canWrite = true;
    // Setup the error logger.
    this.threadId = process.argv[3] || 'Master';
};

Database.prototype.throw = function (err) {
    fs.appendFile(__dirname + "/redis-thread[" + this.threadId + "].log", JSON.stringify(err) + "\n", function (err) {});
};

Database.prototype.takeOffline = function () {
    this.canWrite = false;
    var self = this;
};

Database.prototype.create = function (id, obj, success) {
    // Buffer the object.
    for (var prop in obj) {
        if (isComplex(obj[prop]))
            obj[prop] = JSON.stringify(obj[prop]);
    }
    var self = this;
    client.hmset(id, obj, function (err) {
        if ( err ) self.throw(err)
        if (success) success.call();  
    });
};

Database.prototype.createStringified = function (id, obj, success, failure) {
    var json = JSON.stringify(obj);
    var self = this;
    client.set(id, json, function (err) {
        if (err) { self.throw(err); if (failure) failure.call(); }
        else if(success) success.call();
    });
}

Database.prototype.exists = function (id, success, failure) {
    var self = this;
    client.exists(id, function (error) {
        if (error) {
            self.throw(error);
            if (failure) failure.call();
        } else {
            if (success) success.call();
        }
    });
}

Database.prototype.put = function (record, field, value, success, failure) {
    var self = this;
    if (isComplex(value)) value = JSON.stringify(value);    
    client.hset(record, field, value, function (err) {
        if (err) { self.throw(err); if (failure) failure.call(); }
        else if (success) success.call();
    });
};

Database.prototype.addList = function (id, objs, success) {
    var self = this;
    client.rpush(id, JSON.stringify(objs), function (err, reply) {
        if (err) self.throw(err);
        if (!err && success) success.call();
    });
}

Database.prototype.getClient = function () {
    return client;
};

Database.prototype.deleteList = function (id, success) {
    var self = this;
    client.del(id, function (err) {
        if (err) self.throw(err);
        if (success) success.call();
    });
}

Database.prototype.getList = function (id, success, failure) {
    var self = this;
    client.lrange(id, 0, -1, function (err, objs) {
        if (err) {
            if (err) self.throw(err);
            if (failure)
                failure.call(this, err);
            return;
        }
        if (!objs) {
            if (failure)
                failure.call(this.err);
            return;
        }
        if (success) {
            var result = [];
            for (var i = 0; i < objs.length; i++) {
                try {
                    result.push(JSON.parse(objs[i]));
                } catch (ex) { 
                }

            }
            success.call(this, result);
        }
    });
};

// Remove at a specified index.
Database.prototype.remAt = function (id, index, success, failure) {
    var self = this;
    var guid = uuid.v1();
    client.lset(id, index, guid, function (err) {
        if (err) { self.throw(err); if (failure) failure.call(); }
        else {
            client.lrem(id, 0, guid, function (err) {
                if (err) { if (failure) failure.call() }
                else if (success) success.call();
            });
        }
    });
}

Database.prototype.setAt = function (id, index, object, success, failure) {
    var self = this;
    var json = JSON.stringify(object);
    client.lset(id, index, json, function (err) {
        if (err) { self.throw(err); if (failure) failure.call(); }
        else if (success) success.call();
    });
};

Database.prototype.retrieveString = function (id, success, failure) {
    // Try regular get.
    var self = this;
    client.get(id, function (e, o) {
        if (e || !o) {
            if (e) self.throw(e);
            if (failure)
                failure.call(this, e);
        } else {
            if (success && o)
                success.call(this, JSON.parse(o));
        }
    });
};

Database.prototype.retrieve = function (id, success, failure) {
    var self = this;
    client.hgetall(id, function (err, obj) {
        if (err) {
            self.throw(err);
            if (failure)
                failure.call(this, err);
            return;
        }
        if (!obj) {
            if (failure)
                failure.call(this, err);
            return;
        }
        
        for (var prop in obj) {
            try {
                var fixed = JSON.parse(obj[prop]);
                obj[prop] = fixed;
            } catch (ex) { }
        }
        success.call(this, obj);
    });
};

Database.prototype.redis_retrieveString = function (id, success, failure) {
    // Try regular get.
    var self = this;
    client.get(id, function (e, o) {
        if (e || !o) {
            if (e) self.throw(e);
            if (failure)
                failure.call(this, e, id);
        } else {
            if (success && o)
                success.call(this, JSON.parse(o), id);
        }
    });
};

Database.prototype.redis_retrieve = function (id, success, failure) {
    var self = this;
    client.hgetall(id, function (err, obj) {
        if (err) {
            self.throw(err);
            if (failure)
                failure.call(this, err, id);
            return;
        }
        if (!obj) {
            if (failure)
                failure.call(this, err, id);
            return;
        }
        
        for (var prop in obj) {
            try {
                var fixed = JSON.parse(obj[prop]);
                obj[prop] = fixed;
            } catch (ex) { }
        }
        success.call(this, obj, id);
    });
};

Database.prototype.redis_getList = function (id, success, failure) {
    var self = this;
    client.lrange(id, 0, -1, function (err, objs) {
        if (err) {
            self.throw(err);
            if (failure)
                failure.call(this, err, id);
            return;
        }
        if (!objs) {
            if (failure)
                failure.call(this, err, id);
            return;
        }
        if (success) {
            var result = [];
            for (var i = 0; i < objs.length; i++) {
                try {
                    result.push(JSON.parse(objs[i]));
                } catch (ex) { 
                }
            }
            success.call(this, result, id);
        }
    });
};

Database.prototype.update = function (id, obj, success) {
    var self = this;
    this.create(id, obj, function (err) {
        if (err) self.throw(err);
        if (success) success.call();
    });
};

Database.prototype.delete = function (id, success) {
    var self = this;
    client.del(id, function (err) {
        if (err) self.throw(err);
        if (success) success.call();
    }, success);
};

module.exports = Database;