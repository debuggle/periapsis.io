﻿var NameHelper = function () { };

NameHelper.prototype.generatePlanetName = function (sector) {

    var roots = [
        'Achelous', 'Aether', 'Alastor', 'Apollo',
        'Ares', 'Atlas', 'Castor', 'Cerus', 'Chaos',
        'Kronos', 'Dionysus', 'Eros', 'Eurybia', 'Gaia', 'Hades', 'Helios',
        'Heracles', 'Hermes', 'Hypnos', 'Kratos', 'Leto', 'Lelantos', 'Momus',
        'Morpheus', 'Notus', 'Nemesis', 'Oceanus', 'Ophion', 'Plutus', 'Poseidon',
        'Periapsis', 'Pricus', 'Porteus', 'Tartarus', 'Thantos',
        'Triton', 'Typhon', 'Zeus'
    ];
    
    var number = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X'];
    var vowels = 'aeiou';
    var post = '';
    var pre = roots[Math.round(Math.random() * (roots.length - 1))];
    
    // Get a different name to merge with.
    while (post == "" || post == pre)
        post = roots[Math.round(Math.random() * (roots.length - 1))];
    
    var preVowels = [];
    var postVowels = [];
    
    // Get the index of each vowl in each word.
    for (var i = 0; i < pre.length; i++)
        if (vowels.indexOf(pre.charAt(i)))
            preVowels.push(i);
    
    for (var r = 0; r < post.length; r++)
        if (vowels.indexOf(post.charAt(r)))
            postVowels.push(r);
    
    // Now split the first word on the middle vowel.
    var preIndex = preVowels[Math.round(preVowels.length / 2)];
    var postIndex = postVowels[Math.round(postVowels.length / 2)];
    
    // Then find a random vowel.
    var vowel = vowels.charAt(Math.round(Math.random() * (vowels.length - 1)));
    
    // Put it all together.
    var name = pre.substr(0, preIndex) + vowel + post.substr(postIndex + 1);
    
    // Add a little number to the end for fun.
    if (Math.random() > 0.75) {
        name += "-" + (sector || Math.round(Math.random() * 999));
    } else {
        name += " " + number[Math.round(Math.random() * (number.length - 1))];
    }
    
    return name;
}

NameHelper.prototype.generateAnomalyName = function (sector) {
    var l1 = ['X', 'XR', 'TX', 'BX', 'Z','T'];
    var l2 = ['L', 'M', 'Q'];
    var number = ['I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X'];

    var s2 = (Math.random() > 0.5);
    var s3 = (Math.random() > 0.5);
    var s1Index = Math.round(Math.random() * (l1.length - 1));
    var s2Index = Math.round(Math.random() * (l2.length - 1));

    // Generate.
    var result = l1[s1Index];
    if (s2) result += l2[s2Index];
    else result += Math.round(Math.random() * 10000);
    if (s3) result += '-' + sector;
    else result += '-' + number[Math.round(Math.random() * (number.length - 1))];
    return result;
};

String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

NameHelper.prototype.generatePirateFleetName = function() {
    var adjectives = [
        "pirate", "war", "fierce", "corrupt", "destructive", "nefarious",
        "raider", "marauder", "privateer", "thief", "bandit", "criminal", "crook",
        "swindler"
    ];
    
    var nouns_singular = [
        "brigade", "squadron", "militia", "troop", "squad", "fleet",
        "armada", "force", "flotilla", "battalion", "corps", "contingent",
        "regiment", "legion", "rangers", "caravan", "cavalcade", "company",
        "crew", "crusade", "excursion", "patrol", "gang", "team", "frontier",
        "expedition", "guild", "federation", "society"
    ];
    
    var suffix = [
        "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X",
        "alpha", "bravo", "charlie", "delta", "echo", "foxtrot", "gamma",
        "india", "juliet", "kilo", "lima", "mike", "november", "oscar", "quebec",
        "romeo", "sierra", "tango", "victor", "zulu"
    ];
    
    var first = adjectives[Math.round(Math.random() * (adjectives.length - 1))];
    var second = nouns_singular[Math.round(Math.random() * (nouns_singular.length - 1))];
    var third = suffix[Math.round(Math.random() * (suffix.length - 1))];
    
    // Return.
    return first.capitalize() + " " + second.capitalize();// + " " + third.capitalize();
};


module.exports = new NameHelper();
