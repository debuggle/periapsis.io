﻿var Map = require('./logic/map'),
    AI = require('./logic/aiManager.js'),
    BuildingManager = require('./logic/buildingManager'),
    QueueManager = require('./logic/queueManager.js'),
    MissionManager = require('./logic/missionManager.js'),
    UserManager = require('./logic/userManager.js'),
    GuildManager = require('./logic/guildManager.js'),
    ShipManager = require('./logic/shipManager.js'),
    PirateManager = require('./logic/pirateManager.js'),
    TimerManager = require('./logic/timerManager'),
    CataclysmicManager = require('./logic/cataclysmicManager.js'),
    DiplomacyManager = require('./logic/diplomacyManager.js'),
    Logger = require('./logic/logger.js'),
    SocketManager = require('./logic/socketManager.js'),
    AdventureManager = require('./logic/adventureManager.js'),
    ScienceManager = require('./logic/scienceManager.js'),
    TechManager = require('./logic/techManager.js'),
    FleetManager = require('./logic/fleetManager.js'),
    AsteroidManager = require('./logic/asteroidManager.js'),
    VictoryManager = require('./logic/victoryLogManager.js'),
    NetTable = require('./logic/netTables.js'),
    redis = require('./redis-layer.js');

// To Seconds
function tos(minutes) {
    return minutes * 60;
}

Context = function (callback) {
    var ctx = this;
    this.db0 = new redis();
    this.lock = require('redis-lock')(this.db0.getClient(), 10);

    this.setHomeQueue = [];
    this.settings = {
        // Difficulty level 0. The easiest. Infact, mostly used
        // for testing.
        0: {
            fleetMoveTime: 65, // Per square in a sector.
            shipConstructionTime: tos(0.05),
            cataclysmicEvent: tos(5),
            baseBuildTime: tos(0.1),
            scanTime: tos(0.5),
            attackTime: tos(0.25),
            warpTime: tos(0.25),
            returnTime: tos(0.5),
            allocateTime: tos(15),
            colonizeTime: tos(5),
            constructTime: tos(0.5),
            regenerateTime: tos(0.5),
            upgradeTime: tos(0.5),
            tradeRouteTime: tos(5),
            pirateTime: tos(30),
            sylmarTime: tos(5),
            sylmarAwakenTime: tos(2),
            mythosTime: tos(5),
            tauntTime: tos(0.08),
            sectorHelperTime: tos(1),
            asteroidDeployTime: tos(0.5),
            asteroidSpawnTime: tos(1),
            asteroidProtectTime: tos(0.5),
            asteroidRecallTime: tos(0.5),
            asteroidMine1: tos(0.3),
            asteroidMine2: tos(0.2),
            asteroidMine3: tos(0.15),
            anomalyStabilizeTotal: tos(5),
            anomalySpawnTime: tos(0.5),
            anomalyDecayTime: tos(2),
            anomalyProtectTime: tos(0.5),
            anomalyStabilizeTime: tos(0.5),
            anomalyStructureTime: tos(1)
        },
        1: {
            fleetMoveTime: 65, // Per square in a sector.
            shipConstructionTime: tos(1.5),
            cataclysmicEvent: tos(180),
            baseBuildTime: tos(1),
            scanTime: tos(2),
            attackTime: tos(2),
            warpTime: tos(10),
            returnTime: tos(1),
            allocateTime: tos(15),
            colonizeTime: tos(5),
            constructTime: tos(30),
            regenerateTime: tos(1),
            upgradeTime: tos(5),
            tradeRouteTime: tos(20),
            pirateTime: tos(55),
            sylmarTime: tos(5),
            sylmarAwakenTime: tos(2),
            mythosTime: tos(30),
            tauntTime: tos(0.08),
            sectorHelperTime: tos(7),
            asteroidDeployTime: tos(1),
            asteroidSpawnTime: tos(5),
            asteroidProtectTime: tos(0.5),
            asteroidRecallTime: tos(2),
            asteroidMine1: tos(3),
            asteroidMine2: tos(2),
            asteroidMine3: tos(0.5),
            anomalyStabilizeTotal: tos(7),
            anomalySpawnTime: tos(15),
            anomalyDecayTime: tos(4),
            anomalyProtectTime: tos(0.5),
            anomalyStabilizeTime: tos(0.5),
            anomalyStructureTime: tos(5)
        }
    }; 
    
    this.AIManager = new AI(this);
    this.CataclysmicManager = new CataclysmicManager(this);
    this.AdventureManager = new AdventureManager(this);
    this.UserManager = new UserManager(this);
    this.TimerManager = new TimerManager(this);
    this.MissionManager = new MissionManager(this);
    this.GuildManager = new GuildManager(this);
    this.PirateManager = new PirateManager(this);
    this.BuildingManager = new BuildingManager();
    this.ShipManager = new ShipManager(this);
    this.Logger = new Logger(this);
    this.DiplomacyManager = new DiplomacyManager(this);
    this.SocketManager = new SocketManager(this);
    this.ScienceManager = new ScienceManager(this);
    this.TechManager = new TechManager(this);
    this.VictoryManager = new VictoryManager(this);
    this.AsteroidManager = new AsteroidManager(this);
    this.FleetManager = new FleetManager(this);
    this.NetTable = new NetTable(this);
    this.Map = new Map(this);

    this.QueueManager = new QueueManager(this, function () {
        // Initialize data.
        ctx.init(callback);
    });
    
};

Context.prototype.now = function () {
    return Date.now();
};

// Use this to register all the things that have an initialization function.
Context.prototype.init = function (callback) {
    var gameContext = this;
    
    // Initialize the default building blocks of an instance of the game.
    gameContext.BuildingManager.init();
    gameContext.ShipManager.init();
    gameContext.AdventureManager.init();
    gameContext.ScienceManager.init();
    gameContext.TechManager.init();
    gameContext.AsteroidManager.init();
    gameContext.GuildManager.init();
    gameContext.FleetManager.init();
    
    // Setup the A.I. factions.
    gameContext.UserManager.initPirates();
    if (callback) callback.call();
};

Context.prototype.getSettings = function (index) {
    if (process.argv[2] == 'DEBUG') index = 0;
    return this.settings[index];
}

module.exports = Context;