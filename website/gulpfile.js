var gulp = require('gulp'),
    concat = require('gulp-concat'),
    react = require('gulp-react'),
	uglify = require('gulp-uglify'),
	bump = require('gulp-bump'),
    less = require('gulp-less');

gulp.task('default', function() {
	
  gulp.src('./web-src/manifest.json')
	.pipe(bump({ key: 'version' }))
	.pipe(gulp.dest('./web-src/'))
	.pipe(gulp.dest('../space/compiled/'));
	
  gulp.src(['./web-src/*.htm'])
    .pipe(gulp.dest('../space/compiled/'));
		
  gulp.src(['./web-src/tools/*.htm'])
    .pipe(gulp.dest('../space/compiled/tools/'));

  gulp.src(['./web-src/css/*.css'])
	.pipe(gulp.dest('../space/compiled/css/'));

  gulp.src(['./web-src/fonts/*'])
	.pipe(gulp.dest('../space/compiled/fonts'));
	
  gulp.src(['./web-src/css/*.less', './web-src/*.less'])
    .pipe(less())
	.pipe(concat('site.css'))
    .pipe(gulp.dest('../space/compiled/'));

  gulp.src(['./web-src/*.css'])
    .pipe(gulp.dest('../space/compiled/'));

  gulp.src(['./web-src/assets/*.*'])
    .pipe(gulp.dest('../space/compiled/assets/'));

  gulp.src('./web-src/libs/*.js')
   .pipe(gulp.dest('../space/compiled/libs/'));

  gulp.src('./web-src/js/*.js')
     .pipe(react())
     .pipe(concat('tetra.js'))
	 .pipe(uglify())
     .pipe(gulp.dest('../space/compiled/'));
});
