String.prototype.capitalize = function(){
  return this.charAt(0).toUpperCase() + this.slice(1);
};

function generateFleetName() {
  var adjectives = [
    "first", "final", "planetary", "galactic", "star", "nebula", "eagle",
    "prowler", "comanche", "reaper", "chinook", "kestrel", "osprey", "green",
    "deep", "lion", "eagle", "grizzly", "parsec", "sun", "moon", "orbit",
    "galaxy", "impossible", "starbound", "red", "orange", "blue", "deepspace",
    "lightspeed", "orbital", "freelance", "privateer"
  ];

  var nouns_singular = [
    "brigade", "squadron", "militia", "troop", "squad", "fleet",
    "armada", "force", "flotilla", "battalion", "corps", "contingent",
    "regiment", "legion", "rangers", "caravan", "cavalcade", "company",
    "crew", "crusade", "excursion", "patrol", "gang", "team", "frontier",
    "expedition", "guild", "federation", "society"
  ];

  var suffix = [
    "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X",
    "alpha", "bravo", "charlie", "delta", "echo", "foxtrot", "gamma",
    "india", "juliet", "kilo", "lima", "mike", "november", "oscar", "quebec",
    "romeo", "sierra", "tango", "victor", "zulu"
  ];

  var first = adjectives[Math.round(Math.random() * (adjectives.length - 1))];
  var second = nouns_singular[Math.round(Math.random() * (nouns_singular.length - 1))];
  var third = suffix[Math.round(Math.random() * (suffix.length - 1))];

  // Return.
  return first.capitalize() + " " + second.capitalize() + " " + third.capitalize();
};
