function pickFleetFixed(callback, type) {
  User.fireEvent('updateShipType', { type: type });
  User.updateResources(function() {
    $("#btnSubmitFleet").unbind('click');
    $("#btnSubmitFleet").bind('click', function(){

        var data = [];
        var ships = $(".ship-row");
        for ( var i = 0; i < ships.length; i++ ) {
            var ship = $(ships[i]);
            var shipName = ship.find(".ship-name").val();
            var shipQty = ship.find(".ship-qty").val();
            var availQty = ship.find(".ship-available-qty").text();

            if ( parseFloat(shipQty) > 0 ) {
              if ( parseFloat(shipQty) > parseFloat(availQty)) {
                // Error condition.
                alert("Please make sure you do not select more ships than you own.");
                return;
              } else{
                data.push([shipName, shipQty]);
              }
            }
        }

        // Verify they selected something.
        if ( data.length == 0 ) {
         alert("Please make sure you have at least one ship selected.");
         return;
       }

      if ( callback ) callback(data);
      $("#fleetModal").modal('hide');
      $(".ship-qty").val('');
    });
    $("#btnSubmitFleet").text("Launch Fleet");
    $("#fleetModal").modal();
  });
}

var FleetPicker = React.createClass({
  getInitialState: function() {
    return {
      type: ''
    };
  },
  componentDidMount: function() {
    var self = this;
    User.addListener("updateShips", function() {
      self.setState({});
    });
    User.addListener("updateShipType", function(obj) {
      obj = obj || {};
      self.setState({ type: obj.type || '' });
    });
  },
  render: function() {
    var userShips = User.getShips();
    var shipArray = [];
    for ( var i = 0; i < userShips.length; i++ ) {
      var shipName = userShips[i][0];
      var shipQty = userShips[i][1];

      if ( this.state.type == 'attack' && (userShips[i][2] || 0 ) <= 0 ) continue;
      if ( userShips[i][1] == 0 ) continue;

      shipArray.push({
        name: shipName,
        attack: userShips[i][2] || 0,
        quantity: shipQty
      });
    }

    return (
      <div className="modal fade" id="fleetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 className="modal-title" id="myModalLabel">Create Fleet</h4>
            </div>
            <div className="modal-body" id="myModalBody">
              <p>
                Select the ships you would like in this fleet. Note: the more war ships you select, the
                safer your travel will be. Pirates are numerous in this world, so choose carefully!
              </p>
              <table className="table">
                <thead>
                  <tr>
                    <th>Ship Name</th>
                    <th>Available</th>
                    <th>Health</th>
                    <th>Attack</th>
                    <th>Quantity</th>
                  </tr>
                </thead>
                <tbody>
                  {shipArray.map(function(ship) {
                    return (
                      <tr className="ship-row">
                        <td>{ship.name}</td>
                        <td className="ship-available-qty">{ship.quantity}</td>
                        <td>{ship.hp || 0}</td>
                        <td>{ship.attack || 0}</td>
                        <td><input type="hidden" value={ship.name} className="ship-name" /> <input type="text" className="ship-qty" /></td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary" id="btnSubmitFleet">Action</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
});
