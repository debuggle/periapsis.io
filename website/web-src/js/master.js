var Site = React.createClass({
  render: function() {
      return (
        <div>
          <Sidebar />
          <FleetPicker ref="pickFleet" />
          <CreateFleetControl />
          <SelectFleetControl />
          <EditFleetControl />
          <MessageBox />
          <div id="debug" className="black"></div>
          <div id="wrappers">
            <div className="container-fluid">
                {this.props.children}
            </div>
          </div>
        </div>
      );
  }
});

var WelcomeUser = React.createClass({
    render: function() {
      var username = User.getUsername();
      var messageCount = User.getUnreadMessages().length;
      if ( username == null ) return (<div></div>);
      this.props.username = username;
      return (
        <div>
        </div>
      );
    }
});

var ResourcePanel = React.createClass({
  render: function() {
    if ( User.getUsername() == null ) return (<div></div>);

  }
});

var ResourceControl = React.createClass({
  componentDidMount: function() {
    var self = this;
    User.addListener("updatedResources", function(resources) {
      self.setState(resources);
    });

    User.updateResources(function(resources) {
      self.forceUpdate();
    });
  },
  render: function() {
    if ( User.getUsername() == null ) return (<div></div>);

    var credits = User.getObject().credits || 200;

    var sciencek = '';
    var science = User.getResources().science || 0;

    var ironk = '';
    var iron = User.getResources().iron;

    var goldk = '';
    var gold = User.getResources().gold;

    var h3k = '';
    var h3 = User.getResources().h3;

    if ( science > 10000 ) {
      science = Math.round((science*100)/1000)/100;
      sciencek = 'k';
    }

    if ( iron > 10000 ) {
      iron = Math.round((iron * 100) / 1000) / 100;
      ironk = 'k';
    }

    if ( gold > 10000 ) {
      gold = Math.round((gold * 100) / 1000) / 100;
      goldk = 'k';
    }

    if ( h3 > 10000 ) {
      h3 = Math.round((h3 * 100) / 1000) / 100;
      h3k = 'k';
    }

    return (
      <div>
        <div className="resource-bar">
          <div className="resource-item">
            <div className="resource-value">
              <a href="#" className="normal resource-a" data-toggle="tooltip" data-placement="bottom" title="Use Iron to fabricate ships!">
                {iron}<span className="k">{ironk}</span> Iron
              </a>
            </div>
          </div>
          <div className="resource-item">
            <div className="resource-value">
              <a href="#" className="normal resource-a" data-toggle="tooltip" data-placement="bottom" title="Use Gold to construct and upgrade buildings!">
                {gold}<span className="k">{goldk}</span> Gold
              </a>
            </div>
          </div>
          <div className="resource-item">
            <div className="resource-value">
              <a href="#" className="normal resource-a" data-toggle="tooltip" data-placement="bottom" title="Use H3 to perform ship-based actions!">
                {h3}<span className="k">{h3k}</span> H3
              </a>
            </div>
          </div>
          <div className="resource-item">
            <div className="resource-value">
            <a href="#" className="normal resource-a" data-toggle="tooltip" data-placement="bottom" title="Use Science to upgrade technologies!">
              {science + sciencek} Sci
            </a>
            </div>
          </div>
          <div className="resource-item">
            <div className="resource-value">
              <a href="#" className="normal resource-a" data-toggle="tooltip" data-placement="bottom" title="Use credits to get a major boost!">
                ({credits} <i className="fa fa-bolt"></i>)
              </a>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

var Footer = React.createClass({
  render: function() {
    return (
      <div>
        <footer className="footer">
          <div className="container">
            <p className="footer-text">
              Boldly going where no one has gone before.
              (Note: send feedback to <a className="white" href="mailto:support@periapsis.io">support@periapsis.io</a>)
            </p>
          </div>
        </footer>
      </div>
    )
  }
});

var Sidebar = React.createClass({
  navigate: function(page) {
    $("#wrapper").toggleClass("toggled");
    mobileNavDeployed = $("#wrapper").hasClass("toggled");
    PanelManager.activate(page);
    if (drawingEngine) drawingEngine.openScene("mapView");
  },
  openGalaxyView: function() {
    $("#wrapper").toggleClass("toggled");
    mobileNavDeployed = $("#wrapper").hasClass("toggled");
    PanelManager.activate("map");
    if (drawingEngine) drawingEngine.openScene("galaxyView");
  },
  render: function() {
    var self = this;
    return (
      <div id="mobile">
        <div id="wrapper">
          <div id="sidebar-wrapper">
              <ul className="sidebar-nav">
                  <li className="sidebar-brand">
                      <a href="#">
                          periapsis.io
                      </a>
                  </li>

                  <li><a className="nav-item" href="#map" onClick={self.navigate.bind(this, "map")}><i className="fa fa-map-marker"></i> Map</a></li>
                  <li><a className="nav-item" href="#map" onClick={self.openGalaxyView}><i className="fa fa-map-marker"></i> Galaxy</a></li>
                  <li><a className="nav-item" href="#fabricate" onClick={self.navigate.bind(this, "fabricate")}><i className="fa fa-rocket"></i> Ships</a></li>
                  <li><a className="nav-item" href="#quests" onClick={self.navigate.bind(this, "quests")}><i className="fa fa-book"></i> Quest</a></li>
                  <li><a className="nav-item" href="#diplomacy" onClick={self.navigate.bind(this, "diplomacy")}><i className="fa fa-language"></i> Guilds</a></li>
                  <li><a className="nav-item" href="#tech" onClick={self.navigate.bind(this, "tree")}><i className="fa fa-flask"></i> Tech</a></li>
                  <li><a className="nav-item" href="#profile" onClick={self.navigate.bind(this, "profile")}><i className="fa fa-user"></i> Profile</a></li>
                  <li><a className="nav-item" href="#timer" onClick={self.navigate.bind(this, "timerPanel")}><i className="fa fa-clock-o"></i> Timers</a></li>
                  <br/><br/>
                  <li><a className="nav-item" href="logout.htm"><i className="fa fa-times"></i> Log Out</a></li>

              </ul>
          </div>
        </div>
      </div>
    );
  }
});

var mobileNavDeployed = false;
var SiteHeader = React.createClass({
  openMap: function() {
    User.fireEvent("reloadMap", {});
    PanelManager.activate('map');
    if (drawingEngine) drawingEngine.openScene("mapView");
  },
  openGalaxyView: function() {
    PanelManager.activate("map");
    if (drawingEngine) drawingEngine.openScene("galaxyView");
  },
  openFabricate: function() {
    PanelManager.activate('fabricate');
  },
  openDiplomacy: function() {
    PanelManager.activate('diplomacy');
  },
  openQuests: function() {
    PanelManager.activate('quests');
  },
  openTimer: function() {
    Notifications.toggle();
  },
  openProfile: function() {
    PanelManager.activate('profile');
  },
  openTechTree: function() {
    PanelManager.activate('tree');
  },
  componentDidMount: function() {
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        mobileNavDeployed = $("#wrapper").hasClass("toggled");
    });
  },
  render: function() {
    var self = this;
    function alertDisplay() {
      var usr = User.getObject();
      if ( usr ) {
        if ( usr.topMessageRead === true || usr.topMessageBody === undefined ) {
          return (<div></div>);
        } else {
          ViewHelper.setData({
            x: usr.topMessageX,
            y: usr.topMessageY
          });

          return (
            <div className="alert alert-success center" role="alert">
              <a href="./viewplanet.htm">
                {usr.topMessageBody}
              </a>
            </div>
          );
        }
      } else {
        return (<div></div>);
      }
    };

    function conditionalDisplay() {
      if ( User.getUsername() == null ) return (<span>
        <div>
          <a className="nav-item" href="about.htm"><i className="fa fa-info"></i> About</a>
          <a className="nav-item" href="login.htm"><i className="fa fa-server"></i> Login</a>
          <a className="nav-item" href="register.htm"><i className="fa fa-user-plus"></i> New User</a>
        </div>
        </span>);

      return (
        <div>
          <div className="desktop">
            <a className="nav-item" href="#map" onClick={self.openMap}><i className="fa fa-map-marker"></i> Map</a>
            <a className="nav-item" href="#map" onClick={self.openGalaxyView}><i className="fa fa-globe"></i> Galaxy</a>
            <a className="nav-item" href="#fabricate" onClick={self.openFabricate}><i className="fa fa-rocket"></i> Ships</a>
            <a className="nav-item" href="#quests" onClick={self.openQuests}><i className="fa fa-book"></i> Quest</a>
            <a className="nav-item" href="#diplomacy" onClick={self.openDiplomacy}><i className="fa fa-language"></i> Guilds</a>
            <a className="nav-item" href="#tech" onClick={self.openTechTree}><i className="fa fa-flask"></i> Tech</a>
            <a className="nav-item" href="#profile" onClick={self.openProfile}><i className="fa fa-user"></i> Profile</a>
            <a className="nav-item" href="logout.htm"><i className="fa fa-times fa-2x"></i></a>
            <a className="nav-item" href="#timer" onClick={self.openTimer}><i className="fa fa-clock-o fa-2x"></i></a>
          </div>
          <div className="mobile">
            <a className="nav-item" id="menu-toggle" href="#"><i className="fa fa-bars"></i></a>
          </div>
      </div>);
    }

    return (
      <div>
        <div className="row">
          <div className="md-col-12 site-header">
            <div className="site-header-text">
              <img className="site-logo" src="./assets/logo.png" />
            </div>
            <div className="site-header-nav">
              {conditionalDisplay()}
            </div>
          </div>
          <TimerWindow />
          <ResourceControl ref="resources" />
        </div>
        {this.props.children}
      </div>
    )
  }
});
