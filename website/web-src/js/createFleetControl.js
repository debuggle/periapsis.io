function createFleetDialog(planetName, x, y, callback) {
  User.fireEvent("updatePlanetName", { name: planetName });
  User.fireEvent("updateShips", { x: x, y: y });
    $("#btnCreateFleet").unbind('click');
    $("#btnCreateFleet").bind('click', function(){
        var fleetName = $("#txtFleetName").val();
        var data = [];
        var ships = $(".c-ship-row");
        for ( var i = 0; i < ships.length; i++ ) {
            var ship = $(ships[i]);
            var shipName = ship.find(".c-ship-name").val();
            var shipQty = ship.find(".c-ship-qty").val();
            var availQty = ship.find(".c-ship-available-qty").text();

            if ( parseFloat(shipQty) > 0 ) {
              if ( parseFloat(shipQty) > parseFloat(availQty)) {
                // Error condition.
                alert("Please make sure you do not select more ships than you own.");
                return;
              } else{
                data.push([shipName, shipQty]);
              }
            }
        }

        // Verify they selected something.
        if ( data.length == 0 ) {
         alert("Please make sure you have at least one ship selected.");
         return;
       }

       if ( $.trim(fleetName).length == 0 ) {
         alert("Please specify a fleet name.");
         return;
       }

       // Check if the fleet already exists.
       if (User.doesFleetExist(fleetName)) {
         alert("Please specify a unique fleet name.");
         return;
       }

       // Convert the map coordinate to fleet coordinates.
       var fx = $("#mapX").val();
       var fy = $("#mapY").val();

       $.ajax({
         type: 'post',
         dataType: 'json',
         contentType: 'application/json',
         url: '/fleet/create',
         data: JSON.stringify({
           x: fx,
           y: fy,
           sx: x,
           sy: y,
           title: fleetName,
           user: User.getUsername(),
           ships: data
         }),
         success: function(result) {
           if ( result && result.success ) {
             showMessageBox("Success", "Your fleet has been created and is stationed close to this planet.");
            if ( callback ) callback.call();
          } else if ( result && result.msg ) {
            showMessageBox("Cannot complete action", result.msg);
          }
         }
       });

      $("#createFleetModal").modal('hide');
      $(".ship-qty").val('');
    });

    $("#createFleetModal").modal();
}

var CreateFleetControl = React.createClass({
  getInitialState: function() {
    return {
      type: '',
      mapX: 0,
      mapY: 0,
      planetName: '',
      ships: []
    };
  },
  componentDidUpdate: function() {
    $("#txtFleetName").val(generateFleetName());
  },
  componentDidMount: function() {
    var self = this;
    User.addListener("updateShips", function(obj) {
      if ( obj && obj.x && obj.y ) {
        User.queryShips(obj.x,obj.y,function(ships) {
          self.setState({ ships: ships });
          $(".c-ship-qty").val("");
        });
      }
    });
    User.addListener("updatePlanetName", function(obj) {
      obj = obj || {};
      self.setState({ planetName: obj.name || '' });
    });
    User.addListener("setMapCoord", function(obj) {
      self.setState({ mapX: obj.x, mapY: obj.y });
    });
  },
  render: function() {
    var userShips = this.state.ships;
    var shipArray = [];
    for ( var i = 0; i < userShips.length; i++ ) {
      var shipName = userShips[i][0];
      var shipQty = userShips[i][1];

      if ( this.state.type == 'attack' && (userShips[i][2] || 0 ) <= 0 ) continue;
      if ( userShips[i][1] == 0 ) continue;

      shipArray.push({
        name: shipName,
        attack: userShips[i][2] || 0,
        quantity: shipQty
      });
    }

    function renderShipTable() {
      if ( shipArray.length == 0 ) {
        return (<div className="bg-danger padded"><h4>You do not have any ships stationed on this planet :(</h4></div>);
      }
      return (
        <div>
          <div className="form-group">
             <label for="txtFleetName">Fleet Title</label>
             <input type="text" className="form-control" id="txtFleetName" placeholder="An awesome name for your fleet!" />
           </div>
          <table className="table">
            <thead>
              <tr>
                <th>Ship Name</th>
                <th>Available</th>
                <th>Health</th>
                <th>Attack</th>
                <th>Quantity</th>
              </tr>
            </thead>
            <tbody>
              {shipArray.map(function(ship) {
                return (
                  <tr className="c-ship-row">
                    <td>{ship.name}</td>
                    <td className="c-ship-available-qty">{ship.quantity}</td>
                    <td>{ship.hp || 0}</td>
                    <td>{ship.attack || 0}</td>
                    <td>
                      <input type="hidden" value={ship.name} className="c-ship-name" /> <input type="text" className="c-ship-qty" />
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      );
    }

    var disabled = shipArray.length == 0;

    return (
      <div className="modal fade" id="createFleetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h3 className="modal-title" id="myModalLabel">Create Fleet <small>(stationed on {this.state.planetName})</small></h3>
            </div>
            <div className="modal-body" id="myModalBody">
              <p>
                Select the ships you would like in this fleet. Note: the more war ships you select, the
                safer your travel will be. Pirates are numerous in this world, so choose carefully!
              </p>
               {renderShipTable()}
            </div>
            <input type="hidden" value={this.state.mapX} id="mapX" />
            <input type="hidden" value={this.state.mapY} id="mapY" />

            <div className="modal-footer">
              <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
              <button type="button" disabled={disabled} className="btn btn-primary" id="btnCreateFleet">Create Fleet</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
});
