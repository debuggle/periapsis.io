var m_ox = 0;
var m_oy = 0;
function getXY(evt) {
  m_ox = $("#ctxMap").offset().left;
  m_oy = $("#ctxMap").offset().top;
    if ( evt.pageX ) return [evt.pageX,evt.pageY];
    if ( evt.originalEvent ) {
      evt = evt.originalEvent;
      if ( evt.changedTouches && evt.changedTouches.length > 0 ) {
        var x = evt.changedTouches[0].clientX;
        var y = evt.changedTouches[0].clientY;
        var radiusX = 0;//evt.changedTouches[0].radiusX / 2;
        var radiusY = 0;//evt.changedTouches[0].radiusY / 2;
        return [x + radiusX, y + radiusY];
      }
    }
    return [0,0];
}

var mx = 0,
    my = 0;
$(window).bind("mousemove touchmove", function(evt) {
  if ( modal ) return;
  if ( mobileNavDeployed ) return;
  if ( PanelManager.getActivePanel() !== "map" ) return;
  if ( $("#ctxMap").length > 0 && drawingEngine ) {
    var coords = getXY(evt);
    var x = coords[0] - m_ox;
    var y = coords[1] - m_oy;
    mx = x;
    my = y;
    if ( x < 0 || y < 0 ) return;
    if ( y > 0 ) {
      evt.preventDefault();
      drawingEngine.onMouseMove(x,y);
    }
  }
});

$(window).bind("mousedown touchstart", function(evt) {
  if ( modal ) return;
  if ( mobileNavDeployed ) return;
  if ( PanelManager.getActivePanel() !== "map" ) return;
  if ( $("#ctxMap").length > 0 && drawingEngine ) {
    var coords = getXY(evt);
    var x = coords[0] - m_ox;
    var y = coords[1] - m_oy;
    mx = x;
    my = y;
    if ( x < 0 || y < 0 ) return;
    if ( y > 0 ) {
      evt.preventDefault();
      drawingEngine.onMouseDown(x,y);
    }
  }
});

$(window).bind("mouseup touchend touchcancel", function(evt) {
  if ( modal ) return;
  if ( mobileNavDeployed ) return;
  if ( PanelManager.getActivePanel() !== "map" ) return;
  if ( $("#ctxMap").length > 0 && drawingEngine ) {
    var coords = getXY(evt);
    var x = coords[0] - m_ox;
    var y = coords[1] - m_oy;
    if ( x < 0 || y < 0 ) return;
    if ( my > 0 ) {
      drawingEngine.onMouseUp(mx,my);
      evt.preventDefault();
    }
  }
});

EasingFunctions = {
  // no easing, no acceleration
  linear: function (t) { return t },
  // accelerating from zero velocity
  easeInQuad: function (t) { return t*t },
  // decelerating to zero velocity
  easeOutQuad: function (t) { return t*(2-t) },
  // acceleration until halfway, then deceleration
  easeInOutQuad: function (t) { return t<.5 ? 2*t*t : -1+(4-2*t)*t },
  // accelerating from zero velocity
  easeInCubic: function (t) { return t*t*t },
  // decelerating to zero velocity
  easeOutCubic: function (t) { return (--t)*t*t+1 },
  // acceleration until halfway, then deceleration
  easeInOutCubic: function (t) { return t<.5 ? 4*t*t*t : (t-1)*(2*t-2)*(2*t-2)+1 },
  // accelerating from zero velocity
  easeInQuart: function (t) { return t*t*t*t },
  // decelerating to zero velocity
  easeOutQuart: function (t) { return 1-(--t)*t*t*t },
  // acceleration until halfway, then deceleration
  easeInOutQuart: function (t) { return t<.5 ? 8*t*t*t*t : 1-8*(--t)*t*t*t },
  // accelerating from zero velocity
  easeInQuint: function (t) { return t*t*t*t*t },
  // decelerating to zero velocity
  easeOutQuint: function (t) { return 1+(--t)*t*t*t*t },
  // acceleration until halfway, then deceleration
  easeInOutQuint: function (t) { return t<.5 ? 16*t*t*t*t*t : 1+16*(--t)*t*t*t*t }
};

function thrust(url,x,y,sx,sy,offsetX,offsetY,angle,fleet,self,px,py) {
  // Let's commit this change.
  $.ajax({
    type: 'post',
    dataType: 'json',
    contentType: 'application/json',
    url: url,
    data: JSON.stringify({
      sx: sx,
      sy: sy,
      px: px, // PlanetX (in sector coordinates)
      py: py, // PlanetY (in sector coordinates)
      x: offsetX + x,
      y: offsetY + y,
      angle: angle,
      user: User.getUsername(),
      fleet: fleet
    }),
    success: function(result) {
      if ( result && result.success ) {
        // Reload the map
        User.updateResources(function() {
          var coords = fleetToMap(sx * 10, sy * 10);
          if ( self )
            self.navigateSafe(coords[0], coords[1]);
          else
            User.fireEvent("refreshMap", { sector: User.getSector(coords[0], coords[1]) });
          User.getTimers();
        });
      } else if ( result && result.msg ) {
        alert(result.msg);
      }
    }
  });
}

function switchName(name, owner) {
  if ( owner == "Sylmar" ) return "planet-silmar.png";
  if ( name == "planet-1" ) return "planet0.png";
  else if ( name == "planet-2" ) return "planet3.png";
  else if ( name == "planet-3" ) return "planet4.png";
  else if ( name == "planet-4" ) return "planet4.png";
  else if ( name == "planet-5" ) return "planet5.png";
  else if ( name == "anomaly" ) return "anomaly2.png";
  else if ( name == "asteroid" ) return "asteroid.png";
  else if ( name == "planet_silver" ) return "planet_silver.png";
  else if ( name == "planet_sylmar" ) return "planet-silmar.png";
  else if ( name == "planet-home" ) return "planet_home.png";
  else if ( name == "planet-6" ) return "planet6.png";
  else if ( name == "planet-7" ) return "planet7.png";
  else if ( name == "planet-8" ) return "planet8.png";
  else return "planet2.png";
}

var down = false;
var start = {};
var end = {};

var drawingEngine = null;
var BetaMap = React.createClass({
  getInitialState: function() {
    return {
      x: 0,
      y: 0,
      width: 0,
      height: 0,
      space: {},
      graph: {},
      normalized: [],
      sector: 133.7
    };
  },
  refresh: function() {
    User.fireEvent("reloadMap");
  },
  componentDidMount: function() {
    var self = this;
    // Register the drawing engine
    Assets.Initialize(function() {
      if ( drawingEngine == null ) {
        drawingEngine = new JSCanvas('ctxMap');
        drawingEngine.setOnDragChange(function(dd) {
          if (!dd) {
            // Check if we want to do a hard reload.
            if (drawingEngine.hardReload) {
              drawingEngine.hardReload = false;
              drawingEngine.softReload = false;

              // Check if we're currently looking at the sector in question.
              var x = ViewHelper.getMap().x;
              var y = ViewHelper.getMap().y;
              self.navigateSafe(x,y,function() { },drawingEngine.getActiveScene());
            } else if (drawingEngine.softReload) {
              drawingEngine.softReload = false;
              self.drawMap(drawingEngine.getActiveScene());
            }
          }
        });
        // Navigate.
        // Get the last X/Y and verify we have access to view it.
        var x = ViewHelper.getMap().x;
        var y = ViewHelper.getMap().y;
        self.navigateSafe(x,y);
      }
    });

    User.addListener("reloadMap", function() {
      if ( drawingEngine ) {
        if (  drawingEngine.getActiveScene() == "mapView") {
          self.drawMap();
        } else {
          self.drawMap('galaxyView');
        }
      }
    });

    User.addListener("reloadPlanet", function(obj) {
      $.ajax({
        type: 'post',
        dataType: 'json',
        url: '/map/get/' + obj.x + '/' + obj.y,
        success: function(result) {
          User.fireEvent("planetUpdated", result.map);
        }
      });
    });

    User.addListener("clickMap", function(obj) {
      var x = obj.x;
      var y = obj.y;
      self.navigateSafe(x,y,function() {
        self.clickObject(x,y);
      });
    });

    User.addListener("fleetsUpdated", function() {
      if (drawingEngine) {
        drawingEngine.softReload = true;
        if (!drawingEngine.dd) {
          // Force the state change.
          drawingEngine.changeDragState.call(drawingEngine, false);
        }
      }
    });

    User.addListener("refreshMap", function(obj) {
      if ( drawingEngine ) {
        var x = ViewHelper.getMap().x;
        var y = ViewHelper.getMap().y;
        var currentSector = User.getSector(x,y);
        if (currentSector == obj.sector) {
          drawingEngine.hardReload = true;
          if (!drawingEngine.dd) {
            // Force the state change.
            drawingEngine.changeDragState.call(drawingEngine, false);
          }
        }
      }
    });

    function resize() {
      var w_height = $(window).height();
      $("#ctxMap").prop("height", w_height - 80);
    }

    $(window).resize(resize);
    User.addListener("timerTrayChanged", resize);
    resize();

  },
  navigateSafe: function(x,y,callback,activeScene) {
    // Check if we have access here. Otherwise,
    // check the surrounding area. Lastly, go home.
    var self = this;
    activeScene = activeScene || drawingEngine.getActiveScene();
    User.updateFleets(x,y,function() {
      var homeX = User.getObject().x;
      var homeY = User.getObject().y;
      if ( User.hasAccess(x,y)) {
        // Good
      } else {
        if ( User.hasAccess(x+10,y)) {
          x = x + 10;
        } else if ( User.hasAccess(x,y+10)) {
          y = y + 10;
        } else if (User.hasAccess(x-10,y)) {
          x = x - 10;
        } else if (User.hasAccess(x,y-10)) {
          y = y - 10;
        } else if (User.hasAccess(homeX,homeY)) {
          x = homeX;
          y = homeY;
        } else {
          // Fatal error.
          alert("There was an error with your account. Please contact the developer to get it resolved.");
          return;
        }
      }

      self.navigate(x,y,callback,false,activeScene);
    });
  },
  navigate: function(x, y, callback, skipDraw, activeScene) {
    var self = this;
    skipDraw = skipDraw || false;
    ViewHelper.setMap(x,y);
    $.ajax({
      type: 'post',
      dataType: 'json',
      contentType: 'application/json',
      url: '/map/sector/' + x + '/' + y + '/' + User.getUsername(),
      success: function(result) {
        if ( result.success ) {
          var newState = {
            x: x,
            y: y,
            width: result.width,
            height: result.height,
            space: result.map,
            graph: result.graph,
            normalized: result.normalized,
            sector: result.quadrant
          };

          User.updateFleets(x,y,function() {
            self.setState(newState);
            if ( !skipDraw ) {
              drawingEngine.clearAll();
              self.updateMap(newState, activeScene);
            }
            if ( callback ) callback.call(this, newState);
          });
        } else if ( result.probeRequired == true ) {
          var sector = User.getSector(x, y);
          var baseCoords = User.getSectorNormalized(sector);
          var coords = mapToFleet(baseCoords[0]*10,baseCoords[1]*10);
          var offsetX = coords[0];
          var offsetY = coords[1];
          selectFleet(offsetX, offsetY, "Please select the fleet you would like to deploy to explore this uncharted area of space.", null, function(fleetName) {

            // Get the fleet in question.
            var fleets = User.getFleets();
            for ( var i = 0; i < fleets.length; i++ ) {
              if (fleets[i].title == fleetName) {
                var fleet = fleets[i];
                // Logic here.
                var newX = 250;
                var newY = 250;
                var fx = offsetX + newX;
                var fy = offsetY + newY;
                var sx = fleet.dx || fleet.sx;
                var sy = fleet.dy || fleet.sy;
                var angle = Math.atan2(fy - sy, fx - sx);
                thrust('/map/explore',newX,newY,sx,sy,offsetX,offsetY,angle,fleet.title,self,x,y);
                track('Exploring the map');
                return;
              }
            }
          });
        }
      }
    });
  },
  clickObject: function(x,y,type) {
    if (x && y && type !== "planet_silver") {
      // Load the page with the relevant X/Y
      $.ajax({
        type: 'post',
        dataType: 'json',
        url: '/map/get/' + x + '/' + y,
        success: function(result) {
          result.map["ships"] = result.map["ships"] || [];
          if ( result.map.type == "anomaly" )
            PanelManager.activate('anomaly');
          else if ( result.map.type == "asteroid" )
            PanelManager.activate('asteroid');
          else if ( result.map.isPlanet )
            PanelManager.activate('planet');
          User.fireEvent("planetUpdated", result.map);
        }
      });
    }
  },
  updateMap: function(state, activeScene) {
    drawingEngine.state = state;
    this.drawMap(activeScene);
  },
  drawMap: function(activeScene) {
    var self = this;
    var state = drawingEngine.state;
    if ( drawingEngine ) {
      drawingEngine.clearAll(function() {
        var screen = new Viewport({
          name: 'Viewport'
        });

        var galaxyView = new Viewport({
          name: 'gViewport'
        });

        // Create some globals.
        var g_fx = 1.0;
        var g_fy = 1.0;

        // If we're in desktop mode, render differently.
        var txtTitleX = 20, txtTitleY = 25;
        var vp = 800;
        var hexCenterX = vp / 2;
        var hexCenterY = vp / 2;
        var orbitCenterX = vp / 2;
        var orbitCenterY = vp / 2 + 50;
        var fleetScaleX = 0, fleetScaleY = 0;

        var fleetCoords = mapToFleet(state.normalized[0] * 10, state.normalized[1] * 10);
        var planets = [];
        var offsetX = fleetCoords[0];
        var offsetY = fleetCoords[1];

        // Do some offsetting
        var ratio = Math.min($(window).width() / vp, $(window).height() / vp);
        var desktop = $(window).width() > vp;

        drawingEngine.gZoom = Math.min(ratio, 1.0);

        if ( !desktop ) {
          drawingEngine.gZoom = .77;
          orbitCenterX = vp - 355
          orbitCenterY -= 70;
          hexCenterX = vp - 555;
          hexCenterY -= 150;
        }

        var slotWidth = orbitCenterX / parseFloat(state.width);
        // Colors
        var blue = "#06DEFF";
        var orange = "#FFA100";
        var red = "#FF0000";
        var yellow = "#FFF";

        // Random number.
        var rand = new xor4096(state.sector || '133.7');
        var arand = new xor4096(rand());

        // Create a set.
        var num = rand(), used = 0;
        function getRandAngle(){
          var result = num + 25 * used;
          used = used + 1;
          return result;
        }

        var asteroidRandom = [];
        var asteroidUsed = [];
        var a_dist = 45;
        var a_iteration_x = 730 / 2 / a_dist; // Iterations
        var a_iteration_y = 630 / a_dist; // Iterations

        // Plot out the list.
        for ( var i = (100 / a_dist); i < a_iteration_x; i++ ) {
          for ( var r = (50 / a_dist); r < a_iteration_y; r++ ) {
            asteroidRandom.push([i*a_dist,r*a_dist]);
          }
        }

        function getAsteroidRandom(guid) {
          var arand = new xor4096(guid || rand());
          var index = Math.round(arand() * (asteroidRandom.length - 1));
          var number = asteroidRandom[index];
          if ( asteroidUsed.indexOf(index) >= 0 ) {
            return getAsteroidRandom();
          } else {
            asteroidUsed.push(index);
            return number;
          }
        }

        // Add the sector.
        var txtTitle = new Drawable({
          x: txtTitleX,
          y: txtTitleY
        });
        txtTitle.addDrawingLogic(function(ctx,gZoom) {
          ctx.save();
          ctx.font = "30px Exo2";
          ctx.fillStyle = "rgba(255, 255, 255, 0.5)";
          ctx.fillText("Sector " + state.sector, this.x, this.y);
          ctx.restore();
        });
        screen.addAsset(txtTitle);

        // Create the sun.
        var sun = new Drawable({
          x: orbitCenterX,
          y: orbitCenterY,
          fx: g_fx,
          fy: g_fy,
          zoomMod: 1.0,
          //zoomMod: 8.0,
          image: 'star.png'
        });

        // Draw the map.
        screen.addAsset(sun);
        var target = null;

        // Iterate over the planets.
        for ( var i = 0; i < state.space.length; i++ ) {
          var planet = state.space[i];
          if ( planet.isPlanet ) {

            // Get some properties about the planet.
            var owner = planet.owner;
            var owned = owner == User.getUsername();
            var attacking = User.isAttacking(planet.x, planet.y);
            var war = User.isAtWar(owner) || owner == "Pirates" || owner == "Sylmar";
            var guildMember = User.isGuildMember(planet.owner);

            // Derive the orbit color.
            var orbitColor = "#686868";
            if ( owned ) orbitColor = blue;
            else if ( war ) orbitColor = red;
            else if ( guildMember ) orbitColor = orange;
            else if (planet.owner !== undefined ) orbitColor = "#FFF";

            // Draw it to the map.
            var orbit = new Orbit({
              color: orbitColor,
              x: orbitCenterX,
              y: orbitCenterY,
              fx: g_fx,
              fy: g_fy,
              radius: 150 + (planet.layer * 55)
            });

            // Add the planet.
            var objPlanet = new Drawable({
              image: switchName(planet.type, planet.owner),
              mu: function(x,y) {
                if ( drawingEngine.dd ) return false;
                var gZoom = this.gZoom || 1.0;
                var dist = this.distance(x,y);
                if ( dist < 30 * gZoom) {
                  var mapX = offsetX + x;
                  var mapY = offsetY + y;
                  User.fireEvent("setMapCoord", {x: mapX, y: mapY});
                  self.clickObject(this.planet.x, this.planet.y, this.planet.type);
                  this.hover = false;
                  return true;
                }
                return false;
              },
              mm: function(x,y) {
                if (this.distance(x,y) < 30 * (this.gZoom || 1.0)) {
                  this.hover = true;
                } else {
                  this.hover = false;
                }
              },
              planet: planet,
              font: '15px Exo2',
              text: planet.name.toUpperCase(),
              scale: 0.18,
              tinted: owner == undefined,
              fx: g_fx,
              fy: g_fy
            });

            objPlanet.addDrawingLogic(function(ctx) {
              // Check if the mouse is hovering.
              if ( this.hover ) {
                ctx.save();
                ctx.fillStyle = "rgba(255,255,255,0.25)";
                ctx.beginPath();
                ctx.arc(this.x, this.y, (this.width / 2) * (this.gZoom || 1.0),0,2*Math.PI);
                ctx.closePath();
                ctx.fill();
                ctx.restore();
              }

              if ($.trim(this.planet.defendingFleet).length !== 0) {
                // Colors
                var owned = this.planet.owner == User.getUsername();
                var enemy = User.isAtWar(this.planet.owner);
                var color = "#FFF";
                var blue = "#06DEFF";
                if ( owned ) color = blue;
                else if ( enemy ) color = "#FF0000";
                // Draw a circle.
                ctx.beginPath();
                ctx.arc(this.x,this.y,40,0,2*Math.PI);
                ctx.setLineDash([5]);
                ctx.strokeStyle = color;
                ctx.stroke();
                ctx.closePath();
              }
            });

            planets.push(objPlanet);
            var planetAngle = 80 - getRandAngle();
            orbit.addObject(objPlanet, planetAngle);
            if ( target == null ) target = objPlanet;
            else if ( war ) target = objPlanet;

            // Add it to the engine.
            screen.addAsset(orbit);
          } else if ( planet.type == "asteroid" ) {
            var coord = getAsteroidRandom(planet.guid);
            var asteroid = new Drawable({
              planet: planet,
              x: coord[0],
              y: coord[1],
              scale: 0.18,
              image: 'asteroid.png',
              mu: function(x,y) {
                if ( drawingEngine.dd ) return false;
                var gZoom = this.gZoom || 1.0;
                var dist = this.distance(x,y);
                if ( dist < (this.width/3) * gZoom) {
                  // Navigate to the view asteroid page.
                  var mapX = offsetX + x;
                  var mapY = offsetY + y;
                  User.fireEvent("setMapCoord", {x: mapX, y: mapY});
                  self.clickObject(this.planet.x,this.planet.y,this.planet.type);
                  return true;
                }
              },
              mm: function(x,y) {
                if ( this.distance(x,y) < (this.width/3) * (this.gZoom || 1.0)) {
                  this.hover = true;
                } else {
                  this.hover = false;
                }
              }
            });

            asteroid.addDrawingLogic(function(ctx) {
              // Check if the mouse is hovering.
              if ( this.hover ) {
                ctx.save();
                ctx.fillStyle = "rgba(255,255,255,0.25)";
                ctx.beginPath();
                ctx.arc(this.x, this.y, (this.width / 3) * (this.gZoom || 1.0),0,2*Math.PI);
                ctx.closePath();
                ctx.fill();
                ctx.restore();
              }

              if ($.trim(this.planet.defenderPlayer).length !== 0) {

                // Colors
                var owned = this.planet.defenderPlayer == User.getUsername();
                var enemy = User.isAtWar(this.planet.defenderPlayer);
                var guild = User.isGuildMember(this.planet.owner);

                var color = "#FFF";
                var blue = "#06DEFF";

                if ( owned ) color = blue;
                else if ( enemy ) color = "#FF0000";
                else if ( guild ) color = orange;

                // Draw a circle.
                ctx.beginPath();
                ctx.arc(this.x,this.y,20,0,2*Math.PI);
                ctx.setLineDash([5]);
                ctx.strokeStyle = color;
                ctx.stroke();
                ctx.closePath();
              }
            });

            planets.push(asteroid);
            screen.addAsset(asteroid);
          }
        }

        // This needs to be run with the context of a drawable.
        function processSpaceCombat() {
          // We may want to attack it!
          var defendingUser = this.context.user;
          var defendingFleet = this.context.title;

          // Build the HTML table for showing what is in this fleet.
          var html = '<br/><table class="table"><thead><tr><td>Ship Name</td><td>Quantity</td></tr></th><tbody>';
          for ( var i = 0; i < this.context.ships.length; i++ ) {
            html += '<tr><td>' + this.context.ships[i][0] + '</td><td>' + this.context.ships[i][1] + '</td></tr>';
          }
          html += '</tbody></table>';

          selectFleet(offsetX, offsetY, "Would you like to send a fleet to intercept this player?", html, function(fleetName){
            // Now we need to spawn the combat thread.
            $.ajax({
              type: 'post',
              dataType: 'json',
              contentType: 'application/json',
              url: '/fleet/combat',
              data: JSON.stringify({
                attackingUser: User.getUsername(),
                attackingFleet: fleetName,
                defendingUser: defendingUser,
                defendingFleet: defendingFleet
              }),
              success: function(result) {
                if ( result && result.success ) {
                  track('Intercepting another player', { attacker: User.getUsername(), defender: defendingUser });
                  // Update timers.
                  User.getTimers();
                } else if ( result && result.msg ) {
                  showMessageBox("Cannot complete action", result.msg);
                }
              }
            });
          });
          this.dd_x = 0;
          this.dd_y = 0;
          this.dd = false;
        }

        var drawableFleets = [];
        var fleets = User.getFleets();
        for ( var i = 0; i < fleets.length; i++ ) {
          var fleet = fleets[i];
          var f_angle = 0;

          // Calculate the mapX
          var mapX = Math.round(parseFloat(fleet.dx || fleet.x) - offsetX);
          var mapY = Math.round(parseFloat(fleet.dy || fleet.y) - offsetY);
          var enemyFleet = fleet.user !== User.getUsername();
          var isSelf = fleet.user == User.getUsername();
          var ai = fleet.ai;
          var guild = User.isGuildMember(fleet.user);

          // If we should be pointing at something, do that now.
          var d_fleet = new Drawable({
            planet: planets,
            context: fleet,
            x: mapX,
            y: mapY,
            hasAngles: true,
            angle: fleet.angle || 0,
            font: '12px Exo2',
            text: fleet.displayTitle,
            textColor: (ai) ? "#FF0000" : (guild) ? orange : (isSelf) ? blue : null,
            ty: 15,
            scale: 0.5,
            image: (ai) ? "pirate-ship.png" : (enemyFleet) ? 'ship-single-big-enemy.png' : 'ship-single-big.png',
            md: function(x,y) {
              var gZoom = this.gZoom || 1.0;
              var dist = this.distance(x + (this.width/2),y + (this.height/2));
              if ( dist < (this.width/2) ) {
                // If this isn't our fleet, wtf?
                if ( this.context.user !== User.getUsername()) {
                  return processSpaceCombat;
                }

                // We want to engage the drag+drop
                this.dd = true;
                drawingEngine.changeDragState(true);
                return true;
              }
              return false;
            },
            mu: function(x,y) {
              var gZoom = this.gZoom || 1.0;

              // Check if we clicked on the fleet.
              var dist = this.distance(x + (this.width/2),y + (this.height/2));
              if (dist < (this.width/2) && !drawingEngine.dd) {
                // Clicked
                this.dd_x = 0;
                this.dd_y = 0;
                this.dd = false;
                drawingEngine.changeDragState(false);
                editFleet(this.context.title, this.context.displayTitle, function() {
                  User.fireEvent("reloadMap");
                });
                return true;
              }

              // Check if we're trying to encrouch on the space dragons.
              if ( x > orbitCenterX && this.dd ) {
                this.dd_x = 0;
                this.dd_y = 0;
                this.dd = false;
                drawingEngine.changeDragState(false);
                showMessageBox("Cannot complete action", "It is a well known fact that space dragons thrive on the far side of the solar system. We are incapable of traveling there...");
                return;
              }

              if ( this.dd ) {
                // We are creating a dragging vector.
                // Check the distance.
                var sx = offsetX + this.x;
                var sy = offsetY + this.y;
                var fleet = this.context.title;
                var angle = Math.atan2(y - this.cy*gZoom, x - this.cx*gZoom);

                // Check if we're on a planet.
                for ( var i = 0; i < this.planet.length; i++ ) {
                  var p_dist = this.planet[i].distance(x,y);
                  if ( p_dist < (this.planet[i].width / 2) * gZoom ) {

                    // Don't interact with guild member planets.
                    if (User.isGuildMember(this.planet[i].owner)) {
                      this.dd_x = 0;
                      this.dd_y = 0;
                      this.dd = false;
                      drawingEngine.changeDragState(false);
                      return;
                    }


                    // We are! Now let's determine our relationship
                    // with this planet.
                    var planet = this.planet[i].planet;
                    var owner = planet.owner;
                    var owned = planet.owner == User.getUsername();
                    var war = User.isAtWar(owner) || owner == "Pirates" || owner == "Sylmar";
                    var guildMember = User.isGuildMember(planet.owner);
                    var px = planet.x;
                    var py = planet.y;

                    // Check the capabilities of this fleet.
                    var canMine = false;
                    var canAttack = false;

                    if ( planet.owner !== undefined && planet.owner !== User.getUsername() && !guildMember ) {
                      // Check if the planet has anyone defending it.
                      if (planet.defendingFleet) {
                        // Get the battle report.
                        var attackingUser = User.getUsername();
                        var attackingFleet = fleet;
                        var defendingUser = planet.owner;
                        var defendingFleet = planet.defendingFleet;

                        $.ajax({
                          type: 'post',
                          dataType: 'json',
                          contentType: 'application/json',
                          url: '/fleet/battlestats',
                          data: JSON.stringify({
                            fleet1User: attackingUser,
                            fleet1Name: attackingFleet,
                            fleet2User: defendingUser,
                            fleet2Name: defendingFleet
                          }),
                          success: function(result) {
                            // We'll attack the defending fleet.
                            if ( result && result.success && result.report ) {
                              var report = result.report;
                              var msg = '';

                              // Build the message.
                              msg += "<br/><h4>Success probability: " + report + "%</h4>";
                              showMessageBox("War", "This planet is defended by a fleet. Would you like to attack them and attempt to capture the planet?" + msg, "Attack", function() {
                                // Now we need to spawn the combat thread.
                                $.ajax({
                                  type: 'post',
                                  dataType: 'json',
                                  contentType: 'application/json',
                                  url: '/fleet/combat',
                                  data: JSON.stringify({
                                    attackingUser: User.getUsername(),
                                    attackingFleet: fleet,
                                    defendingUser: defendingUser,
                                    defendingFleet: defendingFleet
                                  }),
                                  success: function() {
                                    track('Attacking a defended planet', { attacker: User.getUsername(), defender: defendingUser });
                                    // Update timers.
                                    $("#messageModal").modal('hide');
                                  }
                                });
                              });
                            } else {
                              alert("There was an error");
                            }
                          }
                        });
                      } else {
                        // Otherwise, we'll try to capture it.
                        $.ajax({
                          type: 'post',
                          url: '/fleet/capturestats',
                          dataType: 'json',
                          contentType: 'application/json',
                          data: JSON.stringify({
                            user: User.getUsername(),
                            fleet: fleet,
                            x: px,
                            y: py
                          }),
                          success: function(result) {
                            if ( result && result.report ) {
                              var msg = '<br/><h4>Success probability: ' + result.report + '%</h4>';
                              showMessageBox("Capture", "This planet is unprotected. Would you like to attempt a capture?" + msg, "Capture Planet", function() {
                                thrust('/fleet/capture',(x-15)/gZoom,(y-15)/gZoom,sx,sy,offsetX,offsetY,angle,fleet,self,px,py);
                                $("#messageModal").modal('hide');
                              });
                            } else if ( result && result.msg ) {
                              alert(result.msg);
                            }
                          }
                        });
                      }

                    } else if ( owned ) {
                      showMessageBox("Defend", "Would you like to station your fleet near this planet to boost its defenses?", "Defend", function() {
                        thrust('/fleet/station',x/gZoom,y/gZoom,sx,sy,offsetX,offsetY,angle,fleet,self,px,py);
                        track('Stationing fleet around a planet');
                        $("#messageModal").modal('hide');
                      });

                      this.dd_x = 0;
                      this.dd_y = 0;
                      this.dd = false;
                      drawingEngine.changeDragState(false);
                      return true;
                    } else if ( planet.type == "asteroid" ) {
                      // Check if this fleet has any mining vessels.
                      var hasMiners = (parseFloat(this.context.miners) || 0) > 0;
                      var additional = '';
                      if ( !hasMiners )
                        additional = "<div class='padded bg-danger'>Warning: this fleet does not have any mining vessels.</div>";

                      // Don't interact with guild member asteroids.
                      if (User.isGuildMember(this.planet[i].defenderPlayer)) {
                        this.dd_x = 0;
                        this.dd_y = 0;
                        this.dd = false;
                        drawingEngine.changeDragState(false);
                        return;
                      }

                      showMessageBox("Mine", additional + "<b>Mining</b><p>Would you like to defend this asteroid?</p>", "Defend Asteroid", function() {
                        thrust('/asteroid/defend',(x-25)/gZoom,(y-25)/gZoom,sx,sy,offsetX,offsetY,angle,fleet,self,px,py);
                        $("#messageModal").modal('hide');
                      });
                    } else if ( planet.owner === undefined ) {
                      // Check if we have a terraformer.
                      var f = this.context;
                      for ( var i = 0; i < f.ships.length; i++ ) {
                        if ( f.ships[i][0] == "Terraformer" && parseFloat(f.ships[i][1]) > 0 ) {
                          // We can terraform this planet.
                          showMessageBox("Colonize", "Would you like to use a terraformer and colonize this planet?", "Colonize", function() {
                            thrust('/fleet/colonize',x/gZoom,y/gZoom,sx,sy,offsetX,offsetY,angle,fleet,self,px,py);
                            track('Terraformed planet');
                            $("#messageModal").modal('hide');
                          });
                          break;
                        }
                      }
                    }

                    this.dd_x = 0;
                    this.dd_y = 0;
                    this.dd = false;
                    drawingEngine.changeDragState(false);
                    return;
                  }
                }

                // Otherwise, let's check if we dropped on an enemy fleet.
                for (var r = 0; r < drawableFleets.length; r++) {
                    // Don't interact with guild fleets.
                    var f_dist = Math.sqrt(Math.pow(drawableFleets[r].cx * gZoom - x, 2) + Math.pow(drawableFleets[r].cy * gZoom - y, 2));
                    if ( f_dist < 50 * gZoom ) {

                      if ( User.isGuildMember(drawableFleets[r].context.user)) {
                        this.dd_x = 0;
                        this.dd_y = 0;
                        this.dd = false;
                        drawingEngine.changeDragState(false);
                        return;
                      }

                      if ( drawableFleets[r].context.user !== User.getUsername()) {
                        this.dd_x = 0;
                        this.dd_y = 0;
                        this.dd = false;
                        drawingEngine.changeDragState(false);

                        // Now we need to spawn the combat thread.
                        var defendingUser = drawableFleets[r].context.user;
                        var defendingFleet = drawableFleets[r].context.title;

                        // We dropped on an enemy fleet.
                        var fleetName = this.context.title;
                        $.ajax({
                          type: 'post',
                          dataType: 'json',
                          contentType: 'application/json',
                          url: '/fleet/battlestats',
                          data: JSON.stringify({
                            fleet1User: User.getUsername(),
                            fleet1Name: fleet,
                            fleet2User: defendingUser,
                            fleet2Name: defendingFleet
                          }),
                          success: function(result) {
                            if ( result && result.success && result.report ) {
                              var msg = '<br/><h4>Success probability: ' + result.report + '%</h4>';
                              showMessageBox("Attack Fleet", "Would you like to intercept this fleet?" + msg, "Attack", function() {
                                $.ajax({
                                  type: 'post',
                                  dataType: 'json',
                                  contentType: 'application/json',
                                  url: '/fleet/combat',
                                  data: JSON.stringify({
                                    attackingUser: User.getUsername(),
                                    attackingFleet: fleet,
                                    defendingUser: defendingUser,
                                    defendingFleet: defendingFleet
                                  }),
                                  success: function(result) {
                                    $("#messageModal").modal('hide');
                                    if ( result && result.success ) {
                                      track('Intercepting another player', { attacker: User.getUsername(), defender: defendingUser });
                                    } else if ( result && result.msg ) {
                                      alert(result.msg);
                                    }
                                  }
                                });
                              });
                            }
                          }
                        });
                        return true;
                      } else {
                        // Oh, we dropped on our own fleet! Make sure it's not the same one.
                        if ( drawableFleets[r].context.title !== fleet) {
                          this.dd_x = 0;
                          this.dd_y = 0;
                          this.dd = false;
                          drawingEngine.changeDragState(false);

                          // Ask if we want to merge the two fleets.
                          showMessageBox("Merge fleets", "Would you like to merge these two fleets?", "Merge", function() {
                            thrust('/fleet/merge',x/gZoom,y/gZoom,sx,sy,offsetX,offsetY,angle,fleet,self,fleet,drawableFleets[r].context.title);
                            track('Merged fleets');
                            $("#messageModal").modal('hide');
                          });
                          return true;
                        }
                      }
                    }
                }

                if ( dist > 50 * gZoom ) {
                  this.angle = angle;
                  this.dx = offsetX + x;
                  this.dy = offsetY + y;
                  thrust('/fleet/thrust',x/gZoom,y/gZoom,sx,sy,offsetX,offsetY,angle,fleet,self);
                }

                this.dd_x = 0;
                this.dd_y = 0;
                this.dd = false;
                drawingEngine.changeDragState(false);
              }
            },
            mm: function(x,y) {
              // If this isn't our fleet, wtf?
              if ( this.context.user !== User.getUsername()) return;
              var gZoom = this.gZoom || 1.0;

              if ( this.dd ) {
                // Let's temporarily draw the movement vector.
                this.dd_x = x;
                this.dd_y = y;
              }
            }
          });

          d_fleet.addDrawingLogic(function(ctx) {
            if ( this.dd && this.dd_x && this.dd_y ) {
              var gZoom = this.gZoom || 1.0;
              ctx.save();
              ctx.beginPath();
              ctx.strokeStyle = "rgba(135,135,135,0.5)";
              var w = (this.width / 2);
              var lx = this.cx;// * gZoom;
              var ly = this.cy;// * gZoom;
              ctx.moveTo(lx,ly);
              ctx.lineTo((this.dd_x - 10) / gZoom, (this.dd_y - 10) / gZoom);
              ctx.closePath();
              ctx.lineWidth = 5;
              ctx.stroke();
              ctx.restore();
            }
          });

          d_fleet.addLogic(target, function(ctx) {
            var gZoom = this.gZoom || 1.0;
            if ( ctx ) {
              if ( this.dd ) {
                this.angle = Math.atan2(this.dd_y - this.cy*gZoom, this.dd_x - this.cx*gZoom);
              } else {

                //calculate the angle.
                var newAngle = Math.atan2(this.context.dy - this.context.y, this.context.dx - this.context.x);
                if ( newAngle != 0 )
                  this.angle = newAngle;

                // If we are in the process of moving.
                var oldImage = this.image;
                var newImage = this.image;
                if ( this.context.thrustEnd > User.now()) {
                  // Calculate how far we've gone.
                  var totalTime = (this.context.thrustEnd || 0) - (this.context.thrustStart || 0);
                  var elapsed = User.now() - this.context.thrustStart;
                  var x = Math.round(parseFloat(this.context.x) - offsetX);
                  var y = Math.round(parseFloat(this.context.y) - offsetY);
                  var diffX = Math.round(parseFloat(offsetX - this.context.dx) + x);
                  var diffY = Math.round(parseFloat(offsetY - this.context.dy) + y);

                  // Find the distance we have traveled.
                  var traveled = (elapsed) / (totalTime);
                  var eased = EasingFunctions.easeInOutQuad(traveled);

                  this.x = x - (diffX * eased);
                  this.y = y - (diffY * eased);

                  var imgSrc = (this.context.ai) ? "pirate-ship.png" : (this.context.user !== User.getUsername()) ? "ship-single-move-enemy.png" : "ship-single-move.png";
                  newImage = Assets.Get(imgSrc);
                  this.width = this.image.width * this.scale;
                  this.height = this.image.height * this.scale;
                } else {
                  var imgSrc = (this.context.ai) ? "pirate-ship.png" : (this.context.user !== User.getUsername()) ? "ship-single-big-enemy.png" : "ship-single-big.png";
                  newImage = Assets.Get(imgSrc);
                  this.width = this.image.width * this.scale;
                  this.height = this.image.height * this.scale;
                }

                this.image = newImage;
              }
            }
          });

          drawableFleets.push(d_fleet);
          screen.addAsset(d_fleet);
        }

        // Build the galaxy view.
        var root = state.graph;
        if ( root ) {
          var maxDepth = 0;
          function getWidth(node, prev) {
            prev = prev || 0;
            if ( prev > maxDepth ) maxDepth = prev;
            if (!node.sector) return;
            var children = node.children || [];
            if ( children.length > 0 ) {
              for ( var i = 0; i < children.length; i++ ) {
                getWidth(children[i], prev + 1);
              }
            }
          }

          // Get the width.
          getWidth(root, 1);

          function buildBoard(node) {
            if (!node.sector) return {};
            var hex = new Hexagon({
              explored: node.explored,
              colonized: node.colonized,
              text: node.sector,
              x: hexCenterX - ((maxDepth * 36) * .25),
              y: hexCenterY - ((maxDepth * 36) * .25),
              home: node.isRoot == true,
              bx: -node.x,
              by: -node.y,
              ctx: node,
              radius: 45,
              md: function() {
                var mapX = this.ctx.mapX;
                var mapY = this.ctx.mapY;

                // Peek at the effects of navigating here.
                self.navigate(mapX, mapY, function(state) {
                  drawingEngine.fade(true, function() {
                    drawingEngine.fade(false);
                    self.updateMap(state, '');
                  });
                }, true);
              }
            });
            // Add some logic to watch for fleet movement.
            hex.addDrawingLogic(function(ctx) {
              // Get fleets.
              var fleets = User.getFleets();
              for ( var i = 0; i < fleets.length; i++ ) {
                if ( fleets[i].sector == this.text ) {
                  ctx.save();
                  ctx.beginPath();
                  ctx.font = "15px FontAwesome";
                  ctx.fillStyle = "#FFF";

                  var self = (fleets[i].user == User.getUsername());
                  var enemy = User.isAtWar(fleets[i].user);
                  var other = fleets[i].user != User.getUsername() && !enemy;

                  if ( self ) ctx.fillStyle = blue;
                  else if ( enemy ) ctx.fillStyle = "#FF0000";
                  else if ( other ) ctx.fillStyle = "#FFA100";

                  ctx.fillText("\uf135", this.cx - (this.radius/2), this.cy + (this.radius/2));
                  ctx.fill();
                  ctx.closePath();
                  ctx.restore();
                }
              }
            });

            galaxyView.addAsset(hex);
            var children = node.children || [];
            for ( var i = 0; i < children.length; i++ ) {
              buildBoard(children[i]);
            }

            return hex;
          }

          root.isRoot = true;
          buildBoard(root);
        }

        // Add the viewport.
        drawingEngine.addScene("galaxyView", galaxyView);
        drawingEngine.addScene("mapView", screen);
        drawingEngine.openScene(activeScene || "mapView");
      });
    }
  },
  render: function() {
    return (
      <div className="container">
        <div className="row">
          <div className="solar-map">
            <canvas width="800" height="800" id="ctxMap" ref="ctxMap" />
          </div>
        </div>
      </div>
    );
  }
});
