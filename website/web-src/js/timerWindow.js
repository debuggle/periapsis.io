var __TimerHelper = function() { this.hidden = true; this.neverShow = false; };
__TimerHelper.prototype.registerWindow = function(component) {
  var self = this;
  this.ref = component;
  setTimeout(function(){
    if ( User.getUsername() != null )
      self.show();
  }, 150);
};
__TimerHelper.prototype.never = function() {
  this.neverShow = true;
};
__TimerHelper.prototype.show = function() {
    if ( this.neverShow ) return;
    this.hidden = false;
    $("#timerWindow").addClass("timer-window-active");
    $(".js-panel").addClass("js-toggled");
    User.fireEvent("panel-deployed", { hidden: false });
};
__TimerHelper.prototype.hide = function() {
    this.hidden = true;
    $("#timerWindow").removeClass("timer-window-active");
    $(".js-panel").removeClass("js-toggled");
    User.fireEvent("panel-deployed", { hidden: true });
};
__TimerHelper.prototype.toggle = function() {
  User.fireEvent("timerTrayChanged");
  if ( this.hidden ) this.show();
  else this.hide();
};
__TimerHelper.prototype.deployed = function() {
  return this.hidden == false;
};
__TimerHelper.prototype.refresh = function() {
  if (this.ref && this.ref.refresh )
    this.ref.refresh();
};

var Notifications = new __TimerHelper();

// PROPS INCLUDE
// guid, context, message, color, dismissed, end
var NotificationCard = React.createClass({
    getInitialState: function() {
      return {
        guid: this.props.guid,
        context: this.props.context,
        message: this.props.message,
        color: this.props.color,
        dismissed: this.props.dismissed,
        end: this.props.end || 0
      };
    },
    reload: function() {
      User.fireEvent("refreshNotificationBar");
    },
    componentDidMount: function() {
      this.reload();
    },
    componentWillReceiveProps: function(newProps) {
      this.setState(newProps);
    },
    dismiss: function() {
      var end = this.state.end;
      var guid = this.state.guid;
      var dismiss = false;
      var self = this;
      if ( end && end < User.now())
        dismiss = true;
      else if ( !end || end == 0 )
        dismiss = true;

      if ( dismiss ) {
        $("#" + guid).addClass("timer-card-dismissed");
        setTimeout(function(){
          User.dismiss(guid, function() {
            self.reload();
          });
        }, 250);
      }
    },
    handleContext: function(e) {
      e.preventDefault();
      var obj = this.state.context;
      if ( obj && obj.x && obj.y ) {
        // Navigate to the planet.
        User.fireEvent("clickMap", { x: obj.x, y: obj.y });
      }
    },
    render: function() {
      var css = '';
      var self = this;
      if ( this.state.dismissed == 'true' ) css += ' timer-card-dismissed';

      function renderMessage() {
        if ( self.state.end ) {
          return (<Timer onFinish={self.reload} UID={self.state.guid} keep={true} message={self.state.message} />);
        } else {
          return (<div>{self.state.message}</div>)
        }
      }

      var color = this.state.color;
      if ( this.state.end && this.state.end > 0 && this.state.end < User.now())
        color = 'green';

      return (
        <div id={this.state.guid}
              key={this.state.guid}
              className={"miniTimer timer-card timer-card-" + (color || 'blue') + css}
              onClick={this.dismiss}
              onContextMenu={this.handleContext}>
          <div className="card-message">
            <div className="card-trigger">
            </div>
            {renderMessage()}
          </div>
        </div>
      );
    }
});

var TimerWindow = React.createClass({
  getInitialState: function() {
    return {
      timers: [],
      notifications: []
    };
  },
  componentDidMount: function() {
    var self = this;
    Notifications.registerWindow(this);
    User.addListener("refreshNotificationBar", function() {
      self.refresh();
    });
  },
  componentDidUpdate: function() {
    $(".timer-card").addClass("timer-card-active");
  },
  refresh: function() {
    this.setState({
      timers: User.getTempTimers()
    });
  },
  clearAll: function() {
    var self = this;
    var timers = User.getTempTimers();
    var notes = User.getNotifications();

    // Clear the timers, if applicable.
    var terminate = [];
    for ( var i = 0; i < timers.length; i++ ) {
      var end = timers[i].end;
      if (end && end < User.now()){
        $("#" + timers[i].uid).addClass("timer-card-dismissed");
        terminate.push(timers[i].uid);
      }
    }

    // Clear the notes.
    for ( var i = 0; i < notes.length; i++ ) {
      $("#" + notes[i].guid).addClass("timer-card-dismissed");
      terminate.push(notes[i].guid);
    }

    setTimeout(function() {
      for ( var i = 0; i < terminate.length; i++ ) {
        User.dismiss(terminate[i], function() {
          self.refresh();
        });
      }
    }, 250);
  },
  render: function() {
    var self = this;


    function addTimer(timer) {
      if ( timer == undefined ) return (<span></span>);

      return (
        <NotificationCard
          guid={timer.uid}
          key={timer.uid}
          message={timer.note}
          color={timer.color}
          dismissed={false}
          context={timer.context}
          end={timer.end}  />
      );
    }

    function addNotification(obj) {
      if ( obj == undefined ) return (<span></span>);

      return (
        <NotificationCard
          key={obj.guid}
          guid={obj.guid}
          message={obj.message}
          color={obj.color}
          dismissed={obj.dismissed}
          context={obj.context}
          end={obj.end}  />
      );
    }

    var msg = User.getTempTimers().map(addTimer);
    var notes = User.getNotifications();
    //notes.sort(function(a,b) { if ( a.date && b.date ) return a.date - b.date; return 0; });
    if ( this.state.timers.length == 0 && notes.length == 0 ) {
      msg = <div className="timerEmptyMessage">No activity :(</div>;
    }

    return (
      <div id="timerWindow" className="desktop">
        {notes.map(addNotification)}
        {msg}
        <div id="timerWindowButtons" onClick={this.clearAll}>
          Clear All
        </div>
      </div>
    );
  }
});
