// Props include:
//   UID
//   keep
//   message
//   end (OPTIONAL)
// Events
//   onTick
//   onFinish
function calculateTime(ticks) {

  // Ticks = seconds
  var hours = Math.max(Math.floor(ticks / 60 / 60), 0);
  var minutes = Math.max(Math.floor((ticks - (Math.round(hours) * 60 * 60)) / 60), 0);
  var seconds = Math.max(Math.floor((ticks - (Math.round(hours) * 60 * 60) - (Math.round(minutes) * 60))), 0);
  return [hours,minutes,seconds];
};

var Timer = React.createClass({
  getInitialState: function() {
    var uid = this.props.UID;
    var endDate = 0;
    if (uid !== null && uid !== undefined ) {
      endDate = User.getTimerEnd(uid);
    } else if ( this.props.end ) {
      endDate = this.props.end;
    }

    return {
      remaining: 0,
      endDate: endDate,
      uid: this.props.UID,
      keep: this.props.keep || false,
      message: this.props.message || ''
    };
  },
  componentWillReceiveProps: function(props) {
    var uid = props.UID;
    var endDate = 0;
    var self = this;
    if ( uid != null ) endDate = User.getTimerEnd(uid);
    else if ( props.end ) endDate = props.end;

    var remaining = Math.round((endDate - User.now()) / 1000 );

    this.setState({
      remaining: remaining,
      endDate: endDate,
      keep: props.keep || false,
      uid: uid,
      message: props.message
    }, function() {
      if ( remaining > 0 && self.state.stopped ) {
        self.setState({ stopped: false });
        self.tick();
      }
    });
  },
  tick: function() {
    //if ( this.state.uid == null && this.state. ) return;
    var remaining = Math.round((this.state.endDate - User.now()) / 1000);
    if ( remaining < 0 ) {
      this.setState({
        remaining: 0,
        stopped: true,
        keep: this.state.keep,
        endDate: this.state.endDate
      });

      var fn = this.props.onFinish;
      if ( fn ) {
        fn.call();
      }

      // Check if we should automatically dismiss this timer.
      if (User.shouldTimerDismiss(this.state.uid)) {
        var guid = this.state.uid;
        $("#" + guid).addClass("timer-card-dismissed");
        setTimeout(function(){
          User.dismiss(guid, function() {
            Notifications.refresh();
          });
        }, 250);
      }

      return;
    }

    this.setState({
      remaining: remaining,
      endDate: this.state.endDate
    });

    if ( this.props.onTick )
      this.props.onTick.call();

    setTimeout(this.tick, 100);
  },
  componentDidMount: function() {
    this.tick.call(this);
  },
  render: function() {
    var show = (User.getTimerEnd(this.props.UID) != null) || (this.props.keep || false);
    var r = calculateTime(this.state.remaining);
    var text = '';

    if ( r[0] && r[0] > 0) {
      text += r[0] + ' H ';
    }
    if ( r[1] && r[1] > 0 ) {
      text += r[1] + ' Min ';
    }
    if ( r[2] && r[2] > 0 ) {
      text += r[2] + ' Sec ';
    }

    if ( !show && this.state.remaining < 0 )
      return ( <div></div> );
    else if ( this.state.remaining <= 0 )
      text = 'Done';


    return (
      <span className="timer">
        <div className="timer-remaining">{text}</div>
        <div className="timer-message">{this.state.message || ''}</div>
      </span>
    );
  }
});
