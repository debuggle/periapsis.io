var __PanelHelper = function() {
  this.panels = {};
  this.components = {};
  this.stack = [];
  this.root = 0;
  this.active = 0;
  this.id = 0;
};

__PanelHelper.prototype.getActivePanel = function() {
  return this.active;
};

__PanelHelper.prototype.getPanel = function(panelId) {
  if ( this.panels[panelId] ) {
    return this.panels[panelId];
  }
  return null;
};

__PanelHelper.prototype.registerPanel = function(ref, id, active, av) {
  this.panels[id] = {
    panelId: id,
    ref: ref
  };
  if ( active ) {
    this.root = id;
    this.active = id;

    // Do the thing.
    var deployed = '';
    if ( Notifications.deployed() ) deployed += '-toggled';
    ref.setAnimation('js-panel-left' + deployed);
  }
  return id;
};

__PanelHelper.prototype.registerComponent = function(id, ref) {
  this.components[id] = ref;
};

__PanelHelper.prototype.activate = function(panelId, state) {
  // Push the current one to the stack.
  if ( panelId !== this.active ) {
    var self = this;
    var goBack = false;
    var past = this.root;
    if ( this.stack.length > 0 ) {
      for ( var i = 0; i < this.stack.length; i++ ) {
        if ( this.stack[i] == panelId ){
          past = this.stack[i];
          this.stack = this.stack.splice(i,1);
          break;
        }
      }
    }


    if ( past == panelId ) {
      goBack = true;
    } else {
      this.stack.push(past);
      this.stack.push(this.active);
    }

    // Get the info
    var current = this.getPanel(this.active);
    var target = this.getPanel(panelId);
    if ( current && target ) {
      var deployed = '';
      if ( Notifications.deployed() ) deployed += '-toggled';

      if ( goBack ) {
        current.ref.setAnimation('js-panel-right-most' + deployed);
        target.ref.setAnimation('js-panel-right' + deployed);
      } else {
        current.ref.setAnimation('js-panel-left-most' + deployed);
        target.ref.setAnimation('js-panel-left' + deployed);
      }

      current.ref.setState({ active: false });
      target.ref.setState({ active: true });

      React.Children.map(target.ref.props.children, function(child) {
        if ( child.ref ) {
          if ( self.components[child.ref] ) {
            self.components[child.ref].replaceState(state||{});
          }
        }
      });

      window.location.hash = '#' + panelId;
      this.active = panelId;
    }
  }
};


var PanelManager = new __PanelHelper();

// Add in the url watcher.
var hash = window.location.hash;
setInterval(function(){
    if (window.location.hash != hash) {
        hash = window.location.hash;
        var active = PanelManager.getActivePanel();
        var loc = hash.substr(1) || 'map';
        if ( loc !== active ) {
          PanelManager.activate(loc);
        }
    }
}, 100);
