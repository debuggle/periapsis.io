var Structure = React.createClass({
  deploy: function() {
    var self = this;
    $.ajax({
      type: 'post',
      dataType: 'json',
      contentType: 'application/json',
      url: '/science/deploy',
      data: JSON.stringify({
        x: this.props.x,
        y: this.props.y,
        type: this.props.title,
        user: User.getUsername()
      }),
      success: function(result) {
        track('Deployed ' + self.props.title + ' into anomaly.');
        inc("Deployed structure into anomaly");
        User.fireEvent('completeAnomalyRefresh');
        User.getTimers(function() {
          Notifications.refresh();
        });
      }
    });
  },
  render: function() {
    var self = this;
    var disabled = this.props.disabled;
    var icon = 'fa-flask';
    var end = this.props.end;

    // Check if there's an action.
    function renderTimer() {
      if ( end > 0 ) {
        return (<Timer end={end} />);
      } else {
        return (<input disabled={disabled} className="btn btn-success" type="button" onClick={self.deploy} value="Deploy" />)
      }
    }

    var reward = this.props.building.reward;
    var ironCost = 0, h3Cost = 0, goldCost = 0;
    var costText = '';
    if ( this.props.building.ironCost ) {
      ironCost = parseFloat(this.props.building.ironCost);
      costText = ironCost + ' Iron';
    } else if ( this.props.building.goldCost ) {
      goldCost = parseFloat(this.props.building.goldCost);
      costText = goldCost + ' Gold';
    } else if ( this.props.building.h3Cost ) {
      h3Cost = parseFloat(this.props.building.h3Cost);
      costText = h3Cost + ' H3';
    }

    // Verify we have the resources.
    var resources = User.getResources();
    if ( resources.gold < goldCost ) disabled = true;
    if ( resources.h3 < h3Cost ) disabled = true;
    if ( resources.iron < ironCost ) disabled = true;

    return (
      <div className={"building " + ((disabled) ? "action-disabled" : "")}>
        <i className={"anomaly-building building-icon fa fa-4x " + icon}></i>
        <div className="building-title">{this.props.title} ({costText + ' -> ' + reward} Science)</div>
        <div className="building-description">{this.props.description}</div>
        {renderTimer()}
      </div>
    );
  }
});

var StructurePanel = React.createClass({
  getInitialState: function() {
    return {
      buildings: []
    };
  },
  componentDidMount: function() {
    var self = this;
    $.ajax({
      type: 'post',
      url: '/science/structures',
      success: function(result) {
        self.setState({
          buildings: result
        });
      }
    });
  },
  render: function() {
    var self = this;
    var actionsHappening = false;
    for ( var i = 0; i < this.state.buildings.length; i++ ) {
      var type = this.state.buildings[i].type;
      if ( this.props.anomaly[type + "Action"] !== null && this.props.anomaly[type + "Action"] !== undefined ) {
        actionsHappening = true;
      }
    }

    function renderBuilding(building) {
      return (
        <Structure building={building} end={self.props.anomaly[building.type + "Action"]} x={self.props.x} y={self.props.y} disabled={self.props.disabled || actionsHappening} title={building.type} description={building.description} />
      );
    }

    function renderNote() {
      if ( self.props.protector !== User.getUsername()) {
        return (
          <h4>You must secure this anomaly before you can create a temporary structure.</h4>
        );
      } else if ( self.props.disabled ) {
        return (
          <h4>You must stabilize this anomaly before you can create a temporary structure.</h4>
        );
      }
      return (<div></div>);
    }

    return (
      <div>
        <h3>Temporary Structures</h3>
        {renderNote()}
        {this.state.buildings.map(renderBuilding)}
      </div>
    );
  }
});

var AnomalyViewer = React.createClass({
    getInitialState: function() {
      return {
        x: 0,
        y: 0,
        description: '',
        protector: ''
      };
    },
    componentDidMount: function() {
      var self = this;
      User.addListener("planetUpdated", function(obj) {
        if ( obj.type == 'anomaly' )
          self.replaceState(obj);
      });
      User.addListener("completeAnomalyRefresh", function() {
        self.refresh();
      });
    },
    leavePage: function() {
      if ( PanelManager.getActivePanel() == "anomaly" ) {
        User.updateResources(function() {
          PanelManager.activate("map");
        });
      }
    },
    stabilize: function() {
      var self = this;
      $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        url: '/science/stabilize',
        data: JSON.stringify({
          x: this.state.x,
          y: this.state.y,
          user: User.getUsername()
        }),
        success: function(result) {
          if ( result && result.success ) {
            track('Stabilized anomaly');
            inc("anomaliesStabilized");
            User.getTimers(function() {
              Notifications.refresh();
              self.refresh();
            });
          } else if ( result.msg ) {
            showMessageBox("Cannot Complete Action", result.msg);
          }
        }
      });
    },
    refresh: function() {
      var self = this;
      $.ajax({
        type: 'post',
        dataType: 'json',
        url: '/map/get/' + this.state.x + '/' + this.state.y,
        success: function(result) {
          if (result) {
            self.replaceState(result.map);
          }
        }
      });
    },
    protect: function() {
      var self = this;
      if ( User.getIdleShips().length == 0 ) {
        showMessageBox("Error", "You don't have any idle ships! Go build more to protect this anomaly.");
        return;
      }
      pickFleetFixed(function(data) {
        $.ajax({
          type: 'post',
          dataType: 'json',
          contentType: 'application/json',
          url: '/science/protect',
          data: JSON.stringify({
            user: User.getUsername(),
            x: self.state.x,
            y: self.state.y,
            ships: data
          }),
          success: function(result) {
            $("#fleetModal").modal('hide');
            if ( result && result.success ) {
              track('Protected anomaly');
              User.getTimers(function() {
                Notifications.refresh();
                self.refresh();
              });
            }
          }
        });

      }, 'attack');
    },
    tick: function() {
      this.setState({});
    },
    render: function() {
      var self = this;
      var volatileNote;
      var protectText = 'Protect';
      var protectTimer = '';
      var protectDisabled = false;
      var stabilizeDisabled = false;
      var stabilizeTimer = '';

      if ( this.state.decay < User.now()) {

        // Check if we need to fix it. *sigh*
        if ( Math.abs(this.state.decay - User.now()) > 3000 ) {
          $.ajax({
            type: 'post',
            dataType: 'json',
            contentType: 'application/json',
            url: '/science/fix',
            data: JSON.stringify({
              x: this.state.x,
              y: this.state.y,
            }),
            success: function(result) {
              track('Fixed anomaly');
            }
          });
        }

        // Now GTFO.
        User.fireEvent("reloadMap");
        this.leavePage();
      }

      // Derive the timers.
      if ( this.state.stabilizeTimer && Array.isArray(this.state.stabilizeTimer)) {
        for(var i = 0; i < this.state.stabilizeTimer.length; i++ ) {
          if ( User.getTimerEnd(this.state.stabilizeTimer[i]) > 0 ) {
            stabilizeTimer = this.state.stabilizeTimer[i];
            break;
          }
        }
      }

      if ( this.state.protectTimer && Array.isArray(this.state.protectTimer)) {
        for ( var i = 0; i < this.state.protectTimer.length; i++ ) {
          if ( User.getTimerEnd(this.state.protectTimer[i]) > 0 ) {
            protectTimer = this.state.protectTimer[i];
            break;
          }
        }
      }

      function showOwner() {
        if ( self.state.protector == User.getUsername() ) {
          return (<span className="anomaly-protector">You are currently protecting this anomaly.</span>);
        } else if ( self.state.protector ) {
          return (<span className="anomaly-protector">Protected by <b>{self.state.protector}</b></span>);
        }else{
          return (<span></span>);
        }
      }

      if ( self.state.protector && self.state.protector !== User.getUsername()){
        protectText = 'Attack';
      } else if ( self.state.protector == User.getUsername() ) {
        protectDisabled = true;
      }

      if ( User.getIdleShips().length == 0) protectDisabled = true;
      if ( this.state.decay - User.now() < 30 * 1000 ) {
         stabilizeDisabled = true;
         protectDisabled = true;
         volatileNote = (<div className="anomaly-note"><h4>This anomaly is too volatile to stabilize.</h4></div>);
       } else {
         volatileNote = (<div></div>);
       }

      return (
          <div className="container">
            <div className="row">
              <div className="col-sm-9 col-sm-offset-2">
                <div className="planet-page">
                  <br />
                  <div className={"planet map-slot-anomaly"}></div>
                  <div className="anomaly-details">
                    <div className="anomaly-text">
                      <h3>Anomaly ({this.state.title}) <br/> <Timer onTick={self.tick} onFinish={self.leavePage} end={this.state.decay} /></h3>
                    </div>
                    <div className="planet-description">{this.state.description}</div>
                    <h5>{showOwner()}</h5>
                    {volatileNote}
                  </div>
                  <h3>Actions</h3>
                  <div>
                    <div>
                      <ActionControl
                        iconClass="fa fa-share-alt"
                        textClass="planet-action"
                        text="Stabilize"
                        handler={this.stabilize}
                        disabled={stabilizeDisabled}
                        ref="stabilizeTimerControl"
                        UID={stabilizeTimer} />
                    </div>
                    <div>
                      <ActionControl
                        iconClass="fa fa-street-view"
                        textClass="planet-action"
                        text={protectText}
                        handler={this.protect}
                        disabled={protectDisabled}
                        ref="protectTimerControl"
                        UID={protectTimer} />
                      </div>
                    </div>
                    <StructurePanel anomaly={this.state} protector={this.state.protector} disabled={this.state.protector !== User.getUsername() || (this.state.decay < (User.now() + 5 * 60 * 1000))} x={this.state.x} y={this.state.y} />
                </div>
              </div>
            </div>
          </div>
      );
    }
});
