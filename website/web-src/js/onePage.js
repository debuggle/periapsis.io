var OnePage = React.createClass({
  render: function() {
    return (
      <div id="page-content-wrappers" className="container-fluid">
        <div className="panel-wrapper">
          <Panel id="map" active="true" css="space-bg">
              <BetaMap ref={'map'} />
          </Panel>
          <Panel id="planet">
              <Planet ref={'planet'} x={ViewHelper.getData().x} y={ViewHelper.getData().y} />
          </Panel>
          <Panel id="fabricate">
            <FabricateShips ref={'fabricate'} />
          </Panel>
          <Panel id="diplomacy">
            <DiplomacyView ref={'diplomacy'} />
          </Panel>
          <Panel id="anomaly">
            <AnomalyViewer ref={'anomaly'} />
          </Panel>
          <Panel id="tree">
            <TechTree ref={'tree'} />
          </Panel>
          <Panel id="asteroid">
            <AsteroidViewer ref={'asteroid'} />
          </Panel>
          <Panel id="profile">
            <ProfileControl ref={'profile'} />
          </Panel>
          <Panel id="quests">
            <QuestHub ref={'quests'} />
          </Panel>
          <Panel id="timerPanel">
            <TimerPanel ref={'timerPanel'} />
          </Panel>
        </div>
      </div>
    );
  }
});
