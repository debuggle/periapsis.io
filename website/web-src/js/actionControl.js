// This control is meant to provide an action link, with a handler.
// And a timer component should the action be clicked.
// Props:
//  iconClass
//  textClass
//  text
//  handler
//  disabled
//  UID
//  disableImmediately (OPTIONAL)
//  onFinish (OPTIONAL)
//  end (OPTIONAL)
var ActionControl = React.createClass({
  getInitialState: function() {
    return {
      UID: this.props.UID,
      tempDisabled: false
    };
  },
  timerFinished: function() {
    this.setState({ UID: undefined });
    if ( this.props.onFinish )
      this.props.onFinish();
  },
  invokeHandler: function() {
    var self = this;
    var fn = this.props.handler;

    // To prevent spam clicking, some things want to be disabled
    // immediately.
    if ( this.props.disableImmediately ) {
      self.setState({ tempDisabled: true }, function() {
        fn.call(this, function(timer) {
          self.setState({ UID: timer.uid });
        });
      });
    } else {
      fn.call(this, function(timer) {
        self.setState({ UID: timer.uid });
      });
    }
  },
  componentWillReceiveProps: function(props) {
    props["tempDisabled"] = false;
    this.setState(props);
  },
  render: function() {
    // Check if the timer is triggered.
    var endTime = User.getTimerEnd(this.state.UID) || this.state.end;
    var showTimer = endTime > User.now();
    var self = this;

    function isDisabled() {
      if ( self.state.tempDisabled || self.props.disabled )
        return "action-disabled";
      return "";
    }

    if ( showTimer ){
      return (
        <span key={this.state.UID} className={this.props.textClass}>
          <i className={this.props.iconClass}></i>&nbsp;
          <span className="action-disabled">({this.props.text})</span>
          <Timer UID={this.state.UID} end={this.state.end} onFinish={this.timerFinished} />
        </span>
      );
    } else {
      if ( self.props.disabled ) {
        return (<span key={this.state.UID} className={this.props.textClass}>
          <i className={this.props.iconClass + " action-disabled"}></i>&nbsp;
          <span className={"action-disabled"}>{this.props.text}</span>
        </span>);
      } else {
        return (
          <span key={this.state.UID} onClick={this.invokeHandler} className={this.props.textClass}>
            <i className={this.props.iconClass}></i>&nbsp;
            <span>{this.props.text}</span>
          </span>
        );
      }

    }

  }
});
