var DEBUG = (window.location.hostname == 'localhost');

// Coordinate helper
function fleetToMap(x,y) {
  return [Math.round(x / 800), Math.round(y / 800)];
}

// Coordinate helper
function mapToFleet(x,y) {
  return [Math.round((x / 10) * 800), Math.round((y / 10) * 800)];
}

function track(event, options) {
  if ( DEBUG ) return;
  options = options || {};
  if ( $.trim(User.getUsername()).length > 0 )
    options["Name"] = User.getUsername();
  mixpanel.track(event, options);
}

function inc(id) {
  if ( DEBUG ) return;
  mixpanel.people.increment(id);
}

function identify(options) {
  if ( DEBUG ) return;
  options = options || {};
  options.password = '';
  if ( $.trim(User.getUsername()).length > 0 ) {
    options["First Login Date"] = new Date();
    options["Name"] = User.getUsername();
    options["Sessions"] = 0;
  }

  mixpanel.people.set(options);
  mixpanel.people.increment("Sessions");
}

var User = (function() {
  // Setup ajax
  $.ajaxSetup({
    cache: false
  });

  // Check for debug mode.
  $.ajax({
    dataType: 'json',
    url: '/debug',
    success: function(result) {
      if ( result ) {
          DEBUG = result.debug;
      }
    }
  });

  // Check for versioning mismatches.
  $.ajax({
    dataType: 'json',
    url: './manifest.json',
    success: function(result) {
      if ( result && result.version && localStorage) {
        // Compare with local storage.
        var version = localStorage.getItem('v');
        if ( version != result.version ) {
          localStorage.setItem('v', result.version);
          if ( !DEBUG ) {
            window.location.reload(true);
          }
        }
      }
    }
  });

  // Create an object
  var usr = function() { this.getTimers(); this.identify(); };
  var notifications = [];
  var userCtx = {
    loggedIn: false
  };

  if ( localStorage.getItem('user') !== null ) {
    try {
      userCtx = JSON.parse(localStorage.getItem('user'));
    } catch ( ex ) { }
  }

  if ( localStorage.getItem('notifications') !== null ) {
    try {
      notifications = JSON.parse(localStorage.getItem('notifications'));
    }catch(ex) {}
  }

  // Do an initial load.
  // Socket.io stuff.
  if ( userCtx && userCtx.loggedIn ) {
    try {
      var socket = io();
      socket.emit('identify', JSON.stringify({ user: userCtx.username }));
      socket.on('reconnect', function() {
        socket.emit('identify', JSON.stringify({ user: userCtx.username }));
      });

      // Events
      socket.on('updateNotifications', function() {
        User.updateResources(function() {
          User.updateNotifications(function() {
            User.getTimers(function() {
              Notifications.refresh();
            });
          });
        });
      });
      socket.on('offline', function() {
        window.location.assign("./offline.htm");
      });
      socket.on('updateResources', function() {
        User.updateResources(function() {
          User.getTimers(function() {
            Notifications.refresh();
          });
        });
      });
      socket.on('updateTrade', function() {
        User.fireEvent("updateTrade");
      });
      socket.on('updateShips', function(){
        User.updateResources(function() {
          User.fireEvent("updateShips");
        });
      });
      socket.on('updateFleet', function() {
        User.updateResources(function() {
          User.updateFleets(ViewHelper.getMap().x, ViewHelper.getMap().y, function() {
            User.fireEvent("fleetsUpdated");
          });
        });
      });
      socket.on('updateQuests', function() {
        User.fireEvent("updateQuests");
      });
      socket.on('updateMap', function() {
        User.fireEvent("reloadMap");
      });
      socket.on('updatePlanet', function() {
        User.updateResources(function() {
          User.fireEvent("planetUpdated");
        });
      });
      socket.on('updateAnomaly', function(obj){
        User.fireEvent('reloadPlanet', obj);
      });
      socket.on('updateAsteroid', function(obj) {
        User.fireEvent('refreshAsteroid');
      });
      socket.on('updateGuild', function() {
        User.fireEvent("updateGuild");
      });
      socket.on('refreshMap', function(obj) {
        var sector = obj.x;
        User.fireEvent("refreshMap", {sector: sector });
      });
      socket.on('relocated', function() {
        window.location.reload();
      });
    }catch(ex) {}

    // Add the hammertime.
    var hammertime = new Hammer(document.getElementById('area'));
    hammertime.on('swipe', function(evt) {
      if ( evt.direction == Hammer.DIRECTION_RIGHT ) {
        window.history.back();
      }
    });
  }

  usr.prototype.hasAccess = function(x,y) {
    // Check if we have access to this sector.
    var sector = this.getSector(x,y);
    var objUser = this.getObject();
    var hasAccess = false;
    if ( objUser.sector[sector] ) {
      hasAccess = true;
    } else {
      // Check if there's a fleet here.
      var fleets = this.getFleets();
      for( var i = 0; i < fleets.length; i++ ) {
        if ( fleets[i].user == this.getUsername() && fleets[i].sector == sector ) {
          hasAccess = true;
        }
      }
    }
    return hasAccess;
  }

  usr.prototype.getSector = function(x,y) {
    var quadrant_size = 10;
    var result = (Math.trunc(x / quadrant_size));
    result += (Math.trunc(y / quadrant_size) / 1000);
    result = Math.trunc(result * 1000);
    result = result / 1000;
    return result
  }

  usr.prototype.getSectorNormalized = function(sector) {
    var x = Math.trunc(sector);
    var y = Math.trunc(Math.round((sector - x) * 1000));
    return [x, y];
}


  usr.prototype.identify = function() {
    if ( userCtx && userCtx.userObj && userCtx.userObj.username ) {
      // Ping the newest data to identify.
      $.ajax({
        dataType: 'json',
        contentType: 'application/json',
        url: '/user/ping',
        type: 'post',
        data: JSON.stringify({
          username: userCtx.userObj.username
        }),
        success: function(result) {
          if ( result && result.success && result.user ) {
            if ( !DEBUG ) identify(result.user);
          }
        }
      });
    }
  };

  usr.prototype.addListener = function(evt, callback) {
    this.listeners = this.listeners || {};
    this.listeners[evt] = this.listeners[evt] || [];
    this.listeners[evt].push(callback);
  };

  usr.prototype.fireEvent = function(evt, args) {
    if ( this.listeners && this.listeners[evt] )
      this.listeners[evt].forEach(function(event){ event.call(this, args || []); });
  };

  // Define the logic for logging in.
  usr.prototype.login = function(username, resources, userObj) {
    resources = resources || {};
    resources = {
         credits: userObj.credits || 0,
         gold: resources.gold || 0,
         iron: resources.iron || 0,
         h3: resources.h3 || 0
    };

    var ctx = {
     loggedIn: true,
     username: username,
     resources: resources || {},
     userObj: userObj || {}
    };

    userCtx = ctx;
    localStorage.setItem('user', JSON.stringify(ctx));
    ViewHelper.setMap(userObj.x, userObj.y);
    this.fireEvent("login");
  }

  usr.prototype.setGuildInfo = function(obj) {
    if ( localStorage && localStorage.setItem ) {
      localStorage.setItem('guild', JSON.stringify(obj));
    }
  };

  usr.prototype.getGuildInfo = function() {
    if ( localStorage && localStorage.getItem ) {
      try {
        return JSON.parse(localStorage.getItem('guild'));
      } catch (ex) {
        return {};
      }
    }
  };

  usr.prototype.isGuildMember = function(member) {
    var guild = User.getGuildInfo();
    var myGuild = false;
    var isGuildMember = false;

    if ( guild.members ) {
      for ( var i = 0; i < guild.members.length; i++ ) {
        if ( guild.members[i] == member && member !== User.getUsername() ) {
          isGuildMember = true;
        } else if ( guild.members[i] == User.getUsername()) {
          myGuild = true;
        }
      }
    }
    return myGuild && isGuildMember;
  };

  usr.prototype.getTauntTime = function(target) {
    if ( userCtx && userCtx.userObj && userCtx.userObj.taunts ) {
      return userCtx.userObj.taunts[target] || 0;
    }
    return 0;
  }

  usr.prototype.findQuest = function(title) {
    var quests = userCtx.userObj.quests || {};
    for(var prop in quests) {
      if ( prop == title ) {
        return quests[prop];
      }
    }

    return undefined;
  };

  usr.prototype.now = function() {
    var now = new Date().getTime();
    if ( userCtx && userCtx.userObj && userCtx.userObj.utcOffset )
      now -= userCtx.userObj.utcOffset;
    return now;
  };

  usr.prototype.getObject = function() {
    if ( userCtx )
      return userCtx["userObj"];
    return undefined;
  };

  usr.prototype.logOut = function() {
    userCtx.loggedIn = false;
    localStorage.setItem('user', JSON.stringify(userCtx));
  };

  usr.prototype.getUnreadMessages = function() {
    return [];
  }

  usr.prototype.scanComplete = function(x,y) {
    if ( userCtx && userCtx.loggedIn && userCtx.userObj && userCtx.userObj.scanAction ) {
      userCtx.userObj.scanAction.push([x,y]);
    }
  };

  usr.prototype.canScan = function(x,y) {
    if ( userCtx.loggedIn) {
      if ( userCtx.userObj.scanAction ) {
        for (var i = 0; i < userCtx.userObj.scanAction.length; i++) {
          var coords = userCtx.userObj.scanAction[i];
          if ( coords.x == x && coords.y == y )
            return false;
        }
      }
    }
    return true;
  };

  usr.prototype.canColonize = function() {
    if ( userCtx.loggedIn ) {
      if ( userCtx.userObj.ships ){
        for ( var i = 0; i < userCtx.userObj.ships.length; i++ ) {
          var ship = userCtx.userObj.ships[i];
          if ( ship[0] == "Terraformer") {
            return true;
          }
        }
      }
    }
    return false;
  };

  usr.prototype.addTempTrade = function(x,y) {
      if ( userCtx && userCtx.loggedIn ) {
        userCtx.userObj.tradeRoutes = userCtx.userObj.tradeRoutes || [];
        userCtx.userObj.tradeRoutes.push({
          x: x,
          y: y,
          pending: true,
          accepted: false
        });
        localStorage.setItem('user', JSON.stringify(userCtx));
        this.fireEvent("planetUpdated", {});
      }
  };

  usr.prototype.canTrade = function(x,y) {
    if ( userCtx.loggedIn ) {
      if ( userCtx.userObj.tradeRoutes ) {
        for ( var i = 0; i < userCtx.userObj.tradeRoutes.length; i++ ) {
          var obj = userCtx.userObj.tradeRoutes[i];
          if ( obj.x == x && obj.y == y ) {
            return false;
          }
        }
      }
    }
    return true;
  };

  usr.prototype.hasAcceptedOnPlanet = function(x,y) {
    if ( userCtx.loggedIn ) {
      if ( userCtx.userObj.tradeRoutes ) {
        for ( var i = 0; i < userCtx.userObj.tradeRoutes.length; i++ ) {
          var obj = userCtx.userObj.tradeRoutes[i];
          if ( obj.x == x && obj.y == y && obj.accepted == true ) {
            return true;
          }
        }
      }
    }
    return false;
  }

  usr.prototype.hasAcceptedTradeAgreement = function(user) {
    if ( userCtx.loggedIn ) {
      if ( userCtx.userObj.tradeRoutes ) {
        for ( var i = 0; i < userCtx.userObj.tradeRoutes.length; i++ ) {
          var obj = userCtx.userObj.tradeRoutes[i];
          if ( (obj.user1 == user || obj.user2 == user) && obj.accepted == true) {
            return true;
          }
        }
      }
    }
    return false;
  };

  usr.prototype.getX = function() {
    if ( userCtx.loggedIn ) {
      if ( userCtx.userObj.planets && userCtx.userObj.planets.length > 0 ) {
        return parseFloat(userCtx.userObj.planets[0][0]) || userCtx.userObj.x;
      }
      return userCtx.userObj.x;
    }
    return 0;
  }

  usr.prototype.getY = function() {
    if ( userCtx.loggedIn ) {
      if ( userCtx.userObj.planets && userCtx.userObj.planets.length > 0 ) {
        return parseFloat(userCtx.userObj.planets[0][1]) || userCtx.userObj.y;
      }
      return userCtx.userObj.y;
    }
    return 0;
  }

  usr.prototype.queryShips = function(x,y,callback) {
    $.ajax({
      type: 'post',
      dataType: 'json',
      contentType: 'application/json',
      url: '/ships/query',
      data: JSON.stringify({
        x: x,
        y: y,
        user: User.getUsername()
      }),
      success: function(result) {
        if ( result && result.success && result.ships ) {
           if ( callback ) {
             callback.call(this, result.ships);
           }
        } else if ( result && result.msg) {
          showMessageBox("Cannot complete action", result.msg);
        }
      }
    });
  };

  usr.prototype.getShips = function(x,y) {
    if ( userCtx && userCtx.userObj && userCtx.userObj.ships )
      return userCtx.userObj.ships || [];
    return [];
  }

  usr.prototype.getDeployedShips = function() {
    if ( userCtx && userCtx.userObj && userCtx.userObj.deployed ) {
      return userCtx.userObj.deployed || [];
    }
    return [];
  }

  usr.prototype.getIdleShips = function() {
    if ( userCtx && userCtx.userObj && userCtx.userObj.ships ) {
      return userCtx.userObj.ships || [];
    }
    return [];
  };

  usr.prototype.useProbe = function() {
    if ( userCtx && userCtx.userObj && userCtx.userObj.ships ) {
      var ships = userCtx.userObj.ships;
      for ( var i = 0; i < ships.length; i++ ) {
        if ( ships[i].name == "Space Probe" ) {
          userCtx.userObj.ships = userCtx.userObj.ships.splice(i,1);
          break;
        }
      }
    }
    localStorage.setItem('user', JSON.stringify(userCtx));
  };

  usr.prototype.attack = function(x,y){
    if ( userCtx && userCtx.userObj && userCtx.userObj.ships ) {
      var ships = userCtx.userObj.ships;
      for ( var i = 0; i < ships.length; i++ ) {
        if ( ships[i].deployed == false && parseFloat(ships[i].attack) > 0 ) {
          ships[i].deployed = true;
          ships[i].tx = x;
          ships[i].ty = y;
          this.fireEvent("attacking");
          this.fireEvent("planetUpdated");
          break;
        }
      }
    }
  }

  usr.prototype.isAtWar = function(player) {
    if ( userCtx && userCtx.userObj && userCtx.userObj.wars ) {
      if ( userCtx.userObj.wars.indexOf(player) >= 0 ) {
        return true;
      }
    }
    return false;
  };

  usr.prototype.isAttacking = function(x,y) {
    if ( userCtx && userCtx.userObj && userCtx.userObj.ships ) {
      var ships = userCtx.userObj.ships;
      for ( var i = 0; i < ships.length; i++ ) {
        if ( ships[i].deployed && ships[i].tx == x && ships[i].ty == y && ships[i].missionType == 'attack' ) {
          return true;
        }
      }
    }
    return false;
  }

  usr.prototype.reconcileTimers = function(timers) {
    if ( !timers || !userCtx || !userCtx.timers ) return null;
    var t = userCtx.timers || [];
    for ( var i = 0; i < timers.length; i++ ) {
      if ( User.getTimerEnd(timers[i]) > 0 )
        return timers[i];
    }
    return null;
  }

  usr.prototype.addTempTimer = function(uid, end) {
    userCtx.timers.push({ uid: uid, end: end });
    this.getTimers();
  };

  usr.prototype.getTempTimers = function() {
    var timers = userCtx.timers || [];
    if ( Array.isArray(timers)) {
      timers.sort(function(a,b) {
        return a.end - b.end;
      });
      return timers;
    } else {
      return [];
    }
  };

  usr.prototype.shouldTimerDismiss = function(uid) {
    var timers = userCtx.timers || [];
    for ( var i = 0; i < timers.length; i++ ) {
      if ( timers[i].uid == uid ) {
        return false || (timers[i].context || {}).dismiss;
      }
    }
    return false;
  }

  usr.prototype.getTimers = function(callback) {
    var self = this;
    if ( userCtx && userCtx.loggedIn ) {
      $.ajax({
        type: 'post',
        dataType: 'json',
        url: '/user/timers/' + this.getUsername(),
        success: function(result) {
          userCtx.timers = result;
          localStorage.setItem('user', JSON.stringify(userCtx));
          if ( callback )
            callback.call(this, result);
          self.fireEvent("updatedTimers", result);
          Notifications.refresh();
        }
      });
    }
  }

  usr.prototype.getTimerEnd = function(key) {
    if ( userCtx && userCtx.timers ) {
      for ( var i = 0; i < userCtx.timers.length; i++ ) {
        if ( userCtx.timers[i].uid == key ) {
          return userCtx.timers[i].end;
        }
      }
    }
    return 0;
  }

  usr.prototype.getTimer = function(key, id) {
    return null;
  }

  usr.prototype.getUsername = function() {
    if ( userCtx.loggedIn )
      return userCtx.username;
    return null;
  }

  usr.prototype.getResources = function() {
    if ( userCtx.loggedIn )
      return userCtx.resources || {};
    return {};
  }

  usr.prototype.setResources = function(data) {
    userCtx.resources = data;
    localStorage.setItem('user', JSON.stringify(userCtx));
  }

  usr.prototype.getUID = function() {
    return null;
  }

  usr.prototype.updateNotifications = function(callback) {
    var self = this;
    $.ajax({
      type: 'post',
      dataType: 'json',
      url: '/user/notifications',
      contentType: 'application/json',
      data: JSON.stringify({
          username: self.getUsername()
      }),
      success: function(result) {
        notifications = result || [];
        localStorage.setItem('notifications', JSON.stringify(notifications));
        Notifications.refresh();
        if ( callback ) callback.call();
      }
    });
  };

  usr.prototype.getNotifications = function() {
    if ( userCtx && userCtx.loggedIn ) {
      var title = "Periapsis - A Galaxy Torn";
      if ( notifications.length > 0 ) {
        document.title = "(" + notifications.length + ") " + title;
      } else {
        document.title = title;
      }
      return notifications.sort(function(a,b) { if ( a.date && b.date ) return b.date - a.date; return 0; });
    }
    return [];
  };

  usr.prototype.doesFleetExist = function(title) {
    var fleets = this.getFleets();
    return fleets.indexOf(title) >= 0;
  }

  usr.prototype.getFleets = function() {
    if ( userCtx && userCtx.userObj ) {
      var jsonFleets = localStorage.getItem('fleets');
      if ( jsonFleets ) {
        return JSON.parse(jsonFleets);
      }
      return [];
    }
    return [];
  };

  usr.prototype.updateFleets = function(x, y, callback) {
    if ( userCtx && userCtx.userObj ) {
      var username = this.getUsername();
      $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        url: '/fleet/positions',
        data: JSON.stringify({
          user: username,
          x: x,
          y: y
        }),
        success: function(result) {
          if ( result && result.success && result.positions ) {
            localStorage.setItem('fleets', JSON.stringify(result.positions));
            if ( callback ) callback.call(this, result.positions);
          }
        }
      });
    } else {
      if ( callback ) callback.call(this, []);
    }
  };

  usr.prototype.dismiss = function(guid, callback) {
    $.ajax({
      type: 'post',
      dataType: 'json',
      contentType: 'application/json',
      url: '/user/dismiss/' + guid,
      data: JSON.stringify({
        user: User.getUsername()
      }),
      success: function() {
      },
      complete: function() {
        if ( userCtx && userCtx.timers ) {
          for ( var i = 0; i < userCtx.timers.length; i++ ) {
            if ( userCtx.timers[i].uid == guid ) {
              userCtx.timers.splice(i,1);
              if ( callback ) callback.call();
              return;
            }
          }
        }

        if ( userCtx && userCtx.userObj ) {
          for(var i = 0; i < notifications.length; i++ ) {
            if ( notifications[i].guid == guid ) {
              notifications.splice(i,1);
              if ( callback ) callback.call();
              return;
            }
          }
        }
      }
    });
  };

  usr.prototype.updateResources = function(callback) {
    var self = this;
    if ( userCtx.loggedIn ) {
      $.ajax({
        type: 'get',
        url: '/user/get/' + userCtx.username,
        cache: false,
        dataType: 'json',
        success: function(result) {
          var now = new Date().getTime();
          if ( result && result.success !== undefined && result.success.toString() == 'false' ) {
              // Log out.
              User.logOut();
              window.location.assign("./logout.htm");
              return;
          }

          resources = result || {};
          resources = {
              gold: resources.gold || 0,
              iron: resources.iron || 0,
              h3: resources.h3 || 0,
              science: resources.science || 0
          };
          self.setResources(resources);
          userCtx.userObj = result;
          userCtx.userObj.password = '';
          userCtx.userObj.utcOffset = now - parseFloat(result.now);

          localStorage.setItem('user', JSON.stringify(userCtx));

          // Invoke the callback.
          if ( callback ) {
            callback.call(this, resources);
            Notifications.refresh();
          }

          // Check for offline.
          if ( (userCtx.userObj.offline || false).toString() == 'true' ) {
            window.location.assign("./offline.htm");
          }

          // Check for relocation.
          if ( (userCtx.userObj.relocated || '').toString() == 'true' ) {
            $.ajax({
              type: 'post',
              dataType: 'json',
              contentType: 'application/json',
              url: '/user/ackrelocation',
              data: JSON.stringify({
                user: userCtx.userObj.username
              }),
              success: function(result) {
                var x = userCtx.userObj.x, y =userCtx.userObj.y;
                ViewHelper.setMap(x,y);
                User.fireEvent("reloadMap", {x: x, y: y });
                track('Civilization obliterated');
              }
            })
          }

          self.fireEvent("updateShips");
          self.fireEvent("updatedResources", resources);
        }
      });
    }
  }

  // Return the new object.
  return new usr();
})();

var ViewHelper = (function() {
  var view = function() { };

  view.prototype.setMap = function(x,y) {
    localStorage.setItem("view-data-map", JSON.stringify({x:x,y:y}));
  }

  view.prototype.getMap = function() {
    if ( localStorage.getItem('view-data-map') !== null ) {
      try {
        return JSON.parse(localStorage.getItem("view-data-map"));
      } catch(ex){ }
    }
    return { x: User.getX(), y: User.getY() };
  }

  view.prototype.setData = function(data) {
    localStorage.setItem('view-data', JSON.stringify(data));
  }

  view.prototype.getData = function() {
    if ( localStorage.getItem('view-data') !== null ) {
      try {
        return JSON.parse(localStorage.getItem('view-data'));
      } catch ( ex ) { }
    }
    return { x: 0, y: 0 };
  }

  return new view();
})();
