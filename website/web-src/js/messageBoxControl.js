function showMessageBox(title, description, action_text, action) {
  modal = true;
  $("#btnSubmitAction").unbind('click');
  $("#btnSubmitAction").bind('click', action);
  $("#modalTitle").text(title);
  $("#modalDescription").html(description);

  if ( !action_text )
    $("#btnSubmitAction").css("display", "none");
  else
    $("#btnSubmitAction").css("display", "inline-block");

  $("#btnSubmitAction").text(action_text || 'Okay');
  $("#messageModal").modal();
  $("#messageModal").on('hide.bs.modal', function() {
    modal = false;
  });
};

function showInputBox(title, description, action_text, action) {
  $("#btnInputSubmitAction").unbind('click');
  $("#btnInputSubmitAction").bind('click', function() {
    if (action) action.call(this, $("#inputMessage").val());
    $("#inputModal").modal('hide');
  });
  $("#inputModalTitle").text(title);
  $("#inputModalDescription").html(description);

  if ( !action_text )
    $("#btnInputSubmitAction").css("display", "none");
  else
    $("#btnInputSubmitAction").css("display", "inline-block");

  $("#btnInputSubmitAction").text(action_text || 'Okay');
  $("#inputModal").modal();
};

var MessageBox = React.createClass({
  render: function(){
    return (
      <div>
        <div className="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 className="modal-title" id="modalTitle">Create Fleet</h4>
              </div>
              <div className="modal-body" id="myModalBody">
                <p id="modalDescription">
                </p>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary" id="btnSubmitAction">Action</button>
              </div>
            </div>
          </div>
        </div>

        <div className="modal fade" id="inputModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title" id="inputModalTitle"></h4>
              </div>
              <div className="modal-body" id="inputModalBody">
                <p id="inputModalDescription">
                </p>
                <div className="form-group">
                  <label for="inputMessage">Message</label>
                  <input type="text" className="form-control" id="inputMessage" placeholder="Message" />
                </div>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" className="btn btn-primary" id="btnInputSubmitAction">Submit</button>
              </div>
            </div>
          </div>
        </div>

      </div>
    );
  }
});
