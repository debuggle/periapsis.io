var UserForm = React.createClass({
  handleSubmit: function(e) {
    e.preventDefault();

    // Get the values of the form.
    var username = React.findDOMNode(this.refs.username).value.trim();
    var password = React.findDOMNode(this.refs.password).value.trim();

    // Make the url request.
    if ( this.props.onUserSubmit ) {
      this.props.onUserSubmit({
        username: username,
        password: password
      });
    }
  },
  render: function() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="container">
          <div className="row">
            <div className="col-sm-12">
              <h1> {this.props.title} </h1>
              <p>
                {this.props.description || ""}
              </p>
              <div className="form-group">
                <label for="username">Username</label> <br />
                <input className="form-control" ref="username" id="username" type="text" />
              </div>
              <div className="form-group">
                <label for="password">Password</label> <br />
                <input className="form-control" ref="password" id="password" type="password" />
              </div>
              <input className='btn btn-success' type="submit" />
            </div>
          </div>
        </div>
      </form>
    );
  }
});

var LoginForm = React.createClass({
  getInitialState: function() {
    return {
      msg: ''
    };
  },
  onSubmit: function(config) {
    config = config || {};

    // Get the credentials.
    var username = config.username;
    var password = config.password;
    var self = this;

    // Submit the request.
    $.ajax({
      type: 'post',
      dataType: 'json',
      url: '/user/login',
      contentType: 'application/json',
      data: JSON.stringify({
        username: username,
        password: sha256(password)
      }),
      success: function(result) {
        if ( result ) {
          if ( result.success == false ) {
            self.setState({
              msg: result.msg
            });
          } else {
            var resources = {
                gold: result.user.gold,
                iron: result.user.iron,
                h3: result.user.h3
            };

            User.login(result.user.username, resources, result.user);

            // Clear the password. We don't need to track that.
            result.user.password = '';
            track('User logged in', { username: result.user.username });

            // Reload the website.
            window.location.assign("./index.htm");
          }
        }
      }
    });

  },
  render: function() {
      return (
        <div>
          <div className="user-form-error">{this.state.msg}</div>
          <UserForm title="Login"
            description="Use this form to login to periapsis.io"
            onUserSubmit={this.onSubmit} />
        </div>
      );
  }
});

var RegisterForm = React.createClass({
  getInitialState: function() {
    return {
      msg: ''
    };
  },
  onSubmit: function(config) {
    config = config || {};

    // Get the credentials.
    var username = config.username;
    var password = config.password;
    var self = this;

    // Submit the request.
    $.ajax({
      type: 'post',
      dataType: 'json',
      url: '/user/register',
      contentType: 'application/json',
      data: JSON.stringify({
        username: username,
        password: sha256(password)
      }),
      success: function(result) {
        if ( result ) {
          if ( result.success == false ) {
            self.setState({
              msg: result.msg
            });
          } else {
            var resources = {
                gold: result.user.gold,
                iron: result.user.iron,
                h3: result.user.h3
            };

            track('new account registered', { user: username });
            User.login(username, resources, result.user);
            window.location.assign("./index.htm");
          }
        }
      }
    });

  },
  render: function() {
      return (
        <div>
          <div className="user-form-error">{this.state.msg}</div>
          <UserForm title="Register"
            description="Use this form to register a new (free) account with periapsis.io and begin your journey through space!"
            onUserSubmit={this.onSubmit} />
        </div>
      );
  }
});
