var modal = false;
function selectFleet(targetX, targetY, description, table, callback) {
  modal = true;
  User.fireEvent("updateFleetPicker", {x: targetX, y: targetY });
  $("#txtPickFleetMsg").text(description);
  $("#select-fleet-table").html(table);
  $("#btnMoveFleet").unbind('click');
  $("#btnMoveFleet").bind('click', function(){
    var fleetName = $("#txtFleetToMove").val();
    if ( callback ) callback.call(this, fleetName);
    $("#moveFleetModal").modal('hide');
  });
  $("#moveFleetModal").modal();
  $("#moveFleetModal").on('hide.bs.modal', function() {
    modal = false;
  });
}

var SelectFleetControl = React.createClass({
  getInitialState: function() {
    return {
      x: 0,
      y: 0
    };
  },
  componentDidMount: function() {
    var self = this;
    User.addListener("updateFleetPicker", function(obj) {
      self.setState({ x: obj.x, y: obj.y });
    });
  },
  render: function() {
    var fleets = [];
    var list = User.getFleets();
    for ( var i = 0; i < list.length; i++ ) {
      // Calculate the distance.
      var distance = Math.sqrt(Math.pow(this.state.x - list[i].x, 2) + Math.pow(this.state.y - list[i].y,2));
      var hops = Math.round(distance / 801);
      var txt = (hops == 1) ? "(1 hop)" : "(" + hops + " hops)";
      list[i].displayTitle += " " + txt;
      list[i].distance = distance;
      fleets.push(list[i]);
    }

    fleets.sort(function(a,b) {
      return a.distance - b.distance;
    });

    return (
      <div className="modal fade" id="moveFleetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h3 className="modal-title">Move Fleet</h3>
            </div>
            <div className="modal-body">
              <p id="txtPickFleetMsg">
              </p>
              <select className="form-control" id="txtFleetToMove">
                {fleets.map(function(fleet){
                  if (fleet.user !== User.getUsername())  return;
                  return (
                    <option value={fleet.title}>
                      {fleet.displayTitle}
                    </option>
                  );
                })}
              </select>
              <div id="select-fleet-table"></div>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-default" id="btnCloseMoveFleet" data-dismiss="modal">Close</button>
              <button type="button" className="btn btn-primary" id="btnMoveFleet">Move Fleet</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
});
