var Panel = React.createClass({
  getInitialState: function() {
    return {
      panelId: 0,
      active: this.props.active == 'true',
      animation: '',
      newState: {}
    };
  },
  setAnimation: function(animation) {
    this.setState({ animation: animation });
  },
  activate: function() {
    PanelManager.activate(this.state.panelId)
  },
  componentDidMount: function() {
    var self = this;
    var panelId = PanelManager.registerPanel(this, this.props.id, this.state.active);

    // Handle the animation for panel deployment.
    User.addListener("panel-deployed", function(obj) {
      var hidden = obj.hidden;
      var state = self.state;
      if ( hidden ) {
        state.animation = state.animation.replace("-toggled", "");
      } else {
        state.animation = state.animation.replace("-toggled", "") + "-toggled";
      }
      self.setState(state)
    });

    // Handle window resize.
    var width_threshold = 960;
    var width = width_threshold;
    $(window).resize(function() {
      if ( $(window).width() >= width_threshold && width < width_threshold ) {
        // Change happened.
        self.setState({});
      } else if ( $(window).width() <= width_threshold && width > width_threshold ) {
        // Change happened.
        self.setState({});
      }
      width = $(window).width();
    });

    // Update the children to allow them the ability to register with the panel system.
    this.setState({ panelId: panelId });
  },
  componentDidUpdate: function() {
    $("#" + this.state.panelId).height($(document).height());
  },
  render: function() {
    var css = 'js-panel';
    if (this.state.animation ) {
      css += ' ' + this.state.animation;
    }

    if ( this.props.css ) {
      css += ' ' + this.props.css;
    }

    // Validate our size.
    if ($(window).width() <= 960 ) css = css.replace("-toggled", "");

    if (!this.state.active){
      css += ' js-panel-off';
    } else {
      css += ' js-panel-on';
    }

    if ( Notifications.deployed())
      css += " js-toggled";

    return (
      <div id={this.state.panelId} className={css} key={this.state.panelId}>
        {this.props.children}
      </div>
    );
  }
});
