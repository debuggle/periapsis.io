var Quest = React.createClass({
  getInitialState: function() {
    return {
      accepted: false || (User.findQuest(this.props.title) !== undefined)
    };
  },
  acceptQuest: function() {
    track('accepted quest', { title: this.props.title });
    var self = this;
    $.ajax({
      type: 'post',
      url: '/mission/accept',
      dataType: 'json',
      contentType: 'application/json',
      data: JSON.stringify({
        username: User.getUsername(),
        quest: self.props.title
      }),
      success: function(result) {
        self.setState({ accepted: true });
      }
    });
  },
  expand: function() {

  },
  render: function() {
    var self = this;
    if ( this.props.completed ) return (<div></div>);

    function renderAction() {
      if ( self.props.accepted ) {
        return (<div className="quest-accepted-note">Accepted!</div>);
      } else {
        return (<button type="button" onClick={self.acceptQuest} className="btn btn-warning">Accept Quest</button>);
      }
    }

    return (
      <div className="quest-item" onClick={this.expand}>
        <div className="quest-title">
          {this.props.title}
        </div>
        <div id={sha256(this.props.title)} className="quest-description">
          {this.props.description}
          <div className="quest-action">
            {renderAction()}
          </div>
        </div>
      </div>
    );
  }
});

var QuestHub = React.createClass({
  getInitialState: function() {
    return {
      quests: []
    };
  },
  refresh: function() {
    var self = this;
    $.ajax({
      type: 'post',
      url: '/mission/list',
      dataType: 'json',
      contentType: 'application/json',
      data: JSON.stringify({
        username: User.getUsername()
      }),
      success: function(result) {
        self.setState({ quests: result });
      }
    });
  },
  componentDidMount: function() {
    var self = this;
    this.refresh();
    User.addListener("updateQuests", function() {
      self.refresh();
    });
  },
  render: function() {

    function renderQuest(quest) {
      return (
        <Quest completed={quest.completed} accepted={quest.accepted} title={quest.title} description={quest.description} issuer={quest.issuer} />
      );
    }

    return (
    <div className="container">
      <div className="row">
        <div className="col-sm-9 col-sm-offset-2">
          <div className="page-header">
            <h1>Quests</h1>
            <p>This is the quest hub. Strike up a conversation with those who may
            need your help.</p>
            <div className="quest-panel">
              {this.state.quests.map(renderQuest)}
            </div>
          </div>
        </div>
      </div>
    </div>
    );

  }
});
