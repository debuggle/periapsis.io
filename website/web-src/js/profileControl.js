var ProfileControl = React.createClass({
  getInitialState: function() {
    return {
      username: '',
      email: '',
      tag: '',
      score: 0,
      rank: '',
      type: "Type I"
    };
  },
  componentDidMount: function() {
    var t = Math.round((User.getObject().score / 100000));
    var ct = (t == 0 ) ? "I" : (t == 1 ) ? "II" : (t == 3 ) ? "III" : "IV";
    this.setState({
      username: User.getUsername(),
      score: User.getObject().score || 0,
      email: User.getObject().email || '',
      tag: User.getObject().tag || '',
      rank: User.getObject().rank || '',
      type: "Type " + ct
    })
  },
  refresh: function() {

  },
  submit: function() {
    var email = $("#txtEmailAccount").val();
    var tag = $("#txtTag").val();
    var pass = $("#txtPassword").val();
    var newPass = $("#txtPassword2").val();

    if ( $.trim(newPass) == '' ) newPass = pass;

    if ( User.getObject().tag !== tag ) {
      track("Changing player tag");
    }

    $.ajax({
      url: '/user/update',
      dataType: 'json',
      contentType: 'application/json',
      type: 'post',
      data: JSON.stringify({
        user: User.getUsername(),
        pass: sha256(pass),
        newPass: sha256(newPass),
        email: email,
        tag: ''
      }),
      success: function(result) {
        if ( result && result.success ) {
          track("Updated profile");
          showMessageBox("Success!", "You have successfully updated your profile.");
        } else if ( result && result.msg ) {
          showMessageBox("Failure", result.msg);
        }
      }
    });
  },
  componentDidUpdate: function() {
    $("#txtEmailAccount").val(this.state.email);
  },
  render: function() {
    return (

<div className="container">
  <div className="row">
        <div className="col-sm-9 col-sm-offset-2">
          <br /><br />
            <div className="panel panel-primary">
              <div className="panel-heading">
                <h3 className="panel-title">
                  Profile for <b>{this.state.username}</b>
                </h3>
              </div>
              <div className="panel-body">
                <div className="form-group">
                  <label for="txtUsername">Username</label>
                  <input type="text" className="form-control" disabled="true" id="txtUsername" placeholder="Username" value={this.state.username} />
                </div>
                <div className="form-group">
                  <label for="txtTag">Player Rank</label>
                  <input type="text" className="form-control" disabled="true" id="txtTag" placeholder="" value={this.state.rank}></input>
                </div>
                <div className="form-group">
                  <label for="txtEmailAccount">Email Account</label>
                  <input type="email" className="form-control" id="txtEmailAccount" placeholder="Email"></input>
                </div>
                <div className="form-group">
                  <label for="txtPassword">Current Password</label>
                  <input type="password" className="form-control" id="txtPassword" placeholder="password" />
                </div>
                <div className="form-group">
                  <label for="txtPassword2">New Password</label>
                  <input type="password" className="form-control" id="txtPassword2" placeholder="password" />
                </div>
                <button type="submit" className="btn btn-primary" onClick={this.submit}>Submit</button>
              </div>
              <div className="panel-footer">
                <h4>Civilization Score: <b>{this.state.score} ({this.state.type})</b></h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});
