var TimerPanel = React.createClass({
  getInitialState: function() {
    return {
      timers: []
    };
  },
  componentDidMount: function() {
    var self = this;
    User.addListener("refreshNotificationBar", function() {
      self.refresh();
    });
  },
  refresh: function() {
      this.setState({
        timers: User.getTempTimers()
      });
  },
  componentDidUpdate: function() {
      $(".timer-card-o").addClass("timer-card-active");
  },
  dismiss: function(guid, end) {
    var dismiss = false;
    var self = this;
    if ( end && end < User.now())
      dismiss = true;
    else if ( !end || end == 0 )
      dismiss = true;

    if ( dismiss ) {
      $("#" + guid).addClass("timer-card-dismissed");
      setTimeout(function(){
        User.dismiss(guid, function() {
          self.refresh();
        });
      }, 250);
    }
  },
  handleClick: function(obj) {
    if ( obj ) {
      User.fireEvent("clickMap", obj);
    }
  },
  render: function() {
    var self = this;

    function addTimer(timer) {
      if ( timer == undefined ) return (<span></span>);
      var color = "timer-card-" + (timer.color || 'blue');
      if ( timer.end < (new Date()).getTime() ) color = "timer-card-green";
      return (
        <div id={timer.uid} key={timer.uid} className={"col-xs-12 timer-card-o " + color} onClick={self.dismiss.bind(self, timer.uid, timer.end)} onContextClick={self.handleClick.bind(self, timer.context)}>
          <div className="card-message-o">
            <div className="card-trigger">
            </div>
            <Timer onFinish={self.refresh} UID={timer.uid} keep={true} message={timer.note} />
          </div>
        </div>
      );
    }

    function addNotification(obj) {
      if ( obj == undefined ) return (<span></span>);
      var css = '';
      if ( obj.dismissed == 'true' ) css += ' timer-card-dismissed';

      return (
        <div id={obj.guid} key={obj.guid} className={"col-xs-12 timer-card-o timer-card-" + (obj.color || 'blue') + css}  onClick={self.dismiss.bind(self, obj.guid, 0)} onContextClick={self.handleClick.bind(self, obj.context)}>
          <div className="card-message">
            <div className="card-trigger">
            </div>
            {obj.message}
          </div>
        </div>
      );
    }


    var msg = User.getTempTimers().map(addTimer);
    var notes = User.getNotifications();
    //notes.sort(function(a,b) { if ( a.date && b.date ) return a.date - b.date; return 0; });
    if ( User.getTempTimers().length == 0 && notes.length == 0 ) {
      msg = <div className="timerEmptyMessage-o">No activity :(</div>;
    }

    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-9 col-sm-offset-2">
            {notes.map(addNotification)}
            {msg}
            </div>
        </div>
      </div>
    );
  }
});
