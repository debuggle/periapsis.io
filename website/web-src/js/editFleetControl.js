function editFleet(fleetName, fleetDisplayName, callback) {
  modal = true;
  User.fireEvent("updateEditFleetControl", {guid: fleetName, fleetName: fleetDisplayName});
  $("#txtEditFleetName").text(fleetName);
  $("#btnEditFleet").unbind('click');
  $("#btnEditFleet").bind('click', function(){
    var newFleetName = $("#txtEditFleetName").val();
    $.ajax({
      type: 'post',
      dataType: 'json',
      contentType: 'application/json',
      url: '/fleet/modify',
      data: JSON.stringify({
        user: User.getUsername(),
        fleet: fleetName,
        display: newFleetName
      }),
      success: function(result) {
        if ( result && result.success ) {
          if ( callback ) { callback.call(this, fleetName); }
        } else if ( result && result.msg ){
          alert(result.msg);
        }
        $("#editFleetModal").modal('hide');
      }
    });

  });
  $("#editFleetModal").modal();
  $("#editFleetModal").on('hide.bs.modal', function() {
    modal = false;
  });
}

var EditFleetControl = React.createClass({
  getInitialState: function() {
    return {
      guid: '',
      fleetName: ''
    };
  },
  componentDidMount: function() {
    var self = this;
    User.addListener("updateEditFleetControl", function(obj) {
      self.setState(obj);
    });
  },
  componentDidUpdate: function() {
    $("#txtEditFleetName").val(this.state.fleetName);
  },
  render: function() {
    var fleet = {};
    var ships = [];
    var list = User.getFleets();
    for ( var i = 0; i < list.length; i++ ) {
      if ( list[i].title == this.state.guid ) {
        fleet = list[i];
        ships = (fleet.ships || []);
        break;
      }
    }

    return (
      <div className="modal fade" id="editFleetModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h3 className="modal-title">Modify Fleet <small>{this.state.fleetName}</small></h3>
            </div>
            <div className="modal-body">
              <p>
                Edit the details regarding this fleet.
              </p>

              <div className="form-group">
               <label for="txtEditFleetName">Fleet Title</label>
               <input type="text" className="form-control" id="txtEditFleetName" placeholder="An awesome name for your fleet!" />
             </div>

             <table className="table">
             <thead>
                <tr>
                  <th>Ship Name</th>
                  <th>Quantity</th>
                </tr>
              </thead>
              <tbody>
                {ships.map(function(ship) {
                  if ( parseFloat(ship[1]) <= 0 ) return;
                  return (
                    <tr>
                      <td>{ship[0]}</td>
                      <td>{ship[1]}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            </div>
            <div className="modal-footer">
              <button type="button" className="btn btn-default" id="btnCloseEditFleet" data-dismiss="modal">Cancel</button>
              <button type="button" className="btn btn-primary" id="btnEditFleet">Edit Fleet</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
});
