var GuildAdminPanel = React.createClass({
  submit: function() {
    var self = this;
    var target = $("#txtGuildInviteName").val();

    $.ajax({
      type: 'post',
      dataType: 'json',
      contentType: 'application/json',
      url: '/guild/invite',
      data: JSON.stringify({
        user: User.getUsername(),
        guild: this.props.guild,
        target: target
      }),
      success: function(result) {
        if ( result && result.success ) {
          showMessageBox("Success!", "The player '" + target + "' has been invited to your guild.");
        } else if ( result && result.msg ) {
          showMessageBox("Cannot invite member", result.msg);
        }

        // Empty the text box.
        $("#txtGuildInviteName").val("");
      }
    })
  },
  disband: function() {
    if (confirm("Are you sure you want to disband this guild?")) {
      $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        url: '/guild/disband',
        data: JSON.stringify({
          user: User.getUsername(),
          guild: this.props.guild
        }),
        success: function(result) {
          if ( result && result.success ) {
            showMessageBox("Success", "You have successfully disbanded this guild.");
            User.fireEvent("updateGuild");
            track("Disbanded guild");
          } else if ( result && result.msg ) {
            showMessageBox("Cannot disband guild", result.msg);
          }
        }
      })
    }
  },
  render: function() {

    // Make sure we only render this to the founder.
    if ( this.props.founder !== User.getUsername()) return (<div></div>);

    return (
      <div>
        <button className="btn btn-danger" onClick={this.disband}>Disband Guild</button>
        <br/>
        <br/>
        <div className="panel panel-info">
          <div className="panel-heading">
            <h3 className="panel-title">
              Invite new Members
            </h3>
          </div>
          <div className="panel-body">
            <div className="form-group">
              <label for="txtGuildInviteName">Player Name</label>
              <input type="text" className="form-control" id="txtGuildInviteName" placeholder="Who shall join you in galactic domination?" />
            </div>
            <button type="submit" className="btn btn-info" onClick={this.submit}>Submit</button>
          </div>
        </div>
      </div>
    );
  }
});

var GuildCreator = React.createClass({
  submit: function() {
    var user = User.getUsername();
    var guild = $("#txtGuildName").val();
    if ( guild.length == 0 ) {
        showMessageBox("Could not create guild", "You must supply a guild name");
    } else {
      $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        url: '/guild/create',
        data: JSON.stringify({
          guild: guild,
          user: user
        }),
        success: function(result) {
          track("Created guild", { guild: guild });
          if ( result && result.success ) {
            track("Created guild", {guild: guild });
            User.fireEvent("updateGuild");
          } else if ( result && result.msg ) {
            showMessageBox("Could not create guild", result.msg);
          }
        }
      });
    }
  },
  render: function() {
    return (
      <div>
        <div className="panel panel-primary">
          <div className="panel-heading">
            <h3 className="panel-title">
              Create a new Guild
            </h3>
          </div>
          <div className="panel-body">
            <div className="form-group">
              <label for="txtGuildName">Guild Name</label>
              <input type="text" className="form-control" id="txtGuildName" placeholder="An awesome name for your new guild!" />
            </div>
            <button type="submit" className="btn btn-primary" onClick={this.submit}>Submit</button>
          </div>
        </div>
      </div>
    );
  }
});

GuildInvitesHub = React.createClass({
  getInitialState: function() {
    return {
      invites: []
    };
  },
  refresh: function() {
    var self = this;
    User.updateResources(function() {
      if ( $.trim(User.getObject().guild).length == 0 ) {
        // If they aren't a member of any guild, then we can render invites.
        self.replaceState({ invites: User.getObject().guild_invites });
      } else {
        self.replaceState({ invites: [] });
      }
    });
  },
  componentDidMount: function() {
    var self = this;
    self.refresh();
    User.addListener("updateGuild", function() {
      self.refresh();
    });
  },
  join: function(guild) {
    $.ajax({
      type: 'post',
      dataType: 'json',
      contentType: 'application/json',
      url: '/guild/join',
      data: JSON.stringify({
        user: User.getUsername(),
        guild: guild
      }),
      success: function(result) {
        if ( result && result.success ){
          User.fireEvent("updateGuild");
          track("Joined a guild");
        } else if ( result && result.msg ) {
          showMessageBox("Failed to join guild", result.msg);
        }
      }
    });
  },
  decline: function(guild) {
      $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        url: '/guild/decline',
        data: JSON.stringify({
          user: User.getUsername(),
          guild: guild
        }),
        success: function(result) {
          if ( result && result.success ){
            User.fireEvent("updateGuild");
            track("Declined a guild");
          } else if ( result && result.msg ) {
            showMessageBox("Error", result.msg);
          }
        }
      });
  },
  render: function() {
    if ( (this.state.invites || []).length == 0 ) return (<div></div>);
    var self = this;

    function renderButton(guild) {
      return (
        <div>
          <h4>Guild {guild}
            &nbsp;
            <button className="btn btn-success" onClick={self.join.bind(this, guild)}>Join</button>
            &nbsp;
            <button className="btn btn-danger" onClick={self.decline.bind(this, guild)}>Decline</button>
          </h4>
        </div>
      );
    }

    return (
      <div>
        <h3> Active Invites </h3>
        {this.state.invites.map(renderButton)}
      </div>
    );
  }
});

GuildResourceHubRow = React.createClass({
  give: function(member) {
    var gold = $("#" + member + "-gold").val();
    var iron = $("#" + member + "-iron").val();
    var h3 = $("#" + member + "-h3").val();

    $.ajax({
      type: 'post',
      dataType: 'json',
      contentType: 'application/json',
      url: '/guild/give',
      data: JSON.stringify({
        user: User.getUsername(),
        target: member,
        gold: gold,
        iron: iron,
        h3: h3
      }),
      success: function(result) {
        if ( result && result.success ) {
          $("#" + member + "-gold").val("");
          $("#" + member + "-iron").val("");
          $("#" + member + "-h3").val("");
          track("Gave resources to a guild member");
        } else if ( result && result.msg ) {
          showMessageBox("Failed to give resources", result.msg);
        }
      }
    });
  },
  render: function() {
    var disabled = { disabled: (this.props.member == User.getUsername()) };
    return (
      <tr>
          <td><b>{this.props.member}</b></td>
          <td><input className="form-control" id={this.props.member + "-iron"} type="text" {...disabled} /></td>
          <td><input className="form-control" id={this.props.member + "-gold"} type="text" {...disabled} /></td>
          <td><input className="form-control" id={this.props.member + "-h3"} type="text" {...disabled} /></td>
          <td><button className="btn btn-default" type="submit" onClick={this.give.bind(this, this.props.member)}>Give</button></td>
      </tr>
    );
  }
});

GuildResourceHub = React.createClass({
  render: function() {
    var founder = this.props.founder;
    var guild = this.props.guild;
    var members = this.props.members;

    return (
      <div>
        <div className="page-header">
          <h2>Members <small>in {guild}</small></h2>
        </div>
        <table className="table">
          <thead>
            <tr>
              <td>Member</td>
              <td>Iron</td>
              <td>Gold</td>
              <td>H3</td>
              <td></td>
            </tr>
          </thead>
          <tbody>
            {members.map(function(member) {return (<div><GuildResourceHubRow founder={founder} member={member} /></div>)})}
          </tbody>
        </table>
      </div>
    );
  }
});

GuildMessageHub = React.createClass({
  send: function() {
    var msg = $("#txtGuildMessage").val();
    $.ajax({
      type: 'post',
      dataType: 'json',
      contentType: 'application/json',
      url: '/guild/message',
      data: JSON.stringify({
        user: User.getUsername(),
        message: msg
      }),
      success: function(result) {
        if ( result && result.msg ) {
          showMessageBox("Error", result.msg);
        } else {
          track("Sent a guild message");
          $("#txtGuildMessage").val("");
        }
      }
    });
  },
  render: function() {
    return (
      <div>
        <div className="panel panel-primary">
          <div className="panel-heading">
            <h3 className="panel-title">
              Broadcast a Message
            </h3>
          </div>
          <div className="panel-body">
            <div className="form-group">
              <label for="txtGuildMessage">Message</label>
              <input type="text" className="form-control" id="txtGuildMessage" placeholder="Say something memorable!" />
            </div>
            <button type="submit" className="btn btn-success" onClick={this.send}>Submit</button>
          </div>
        </div>
      </div>
    );
  }
});

DiplomacyView = React.createClass({
  getInitialState: function() {
    return {
      guild: '',
      guildDetails: {}
    };
  },
  refresh: function() {
    var self = this;
    User.updateResources(function() {
      var guild = (User.getObject().guild || '');
      if ( guild.length > 0 ) {
        $.ajax({
          type: 'get',
          dataType: 'json',
          url: '/guild/get/' + guild,
          success: function(result) {
              if ( result ) {
                User.setGuildInfo(result);
                self.setState({ guild: guild, guildDetails: result });
              } else {
                self.setState({ guild: guild, guildDetails: {} });
              }
          }
        });
      } else {
        self.setState({ guild: '', guildDetails: {} });
      }

    });
  },
  leave: function() {
    $.ajax({
      type: 'post',
      dataType: 'json',
      contentType: 'application/json',
      url: '/guild/leave',
      data: JSON.stringify({
        user: User.getUsername(),
        guild: this.state.guild
      }),
      success: function(result) {
        if ( result && result.success ) {
          User.fireEvent("updateGuild");
          track("Left a guild");
        } else if ( result && result.msg ) {
          showMessageBox("Error", result.msg);
        }
      }
    });
  },
  componentDidMount: function() {
    var self = this;
    this.refresh();
    User.addListener("updateGuild", function() {
      self.refresh();
    });
  },
  render: function() {
    var self = this;

    function renderLeave() {
      if ( self.state.guildDetails.founder == User.getUsername()) return(<div></div>);
      return (
        <div>
          <button className="btn btn-danger" onClick={self.leave}>Leave Guild</button>
          <br/>
          <br/>
        </div>
      );
    }

    function renderGuild() {
      if ( (self.state.guild || '').length == 0 )  {
        return (<div><GuildCreator /><GuildInvitesHub /></div>);
      }
      return (
        <div>
          <h4>Guild: <b>{self.state.guild}</b></h4>
          <h4>Founder: <b>{self.state.guildDetails.founder}</b></h4>
          <h4>Members: <b>{self.state.guildDetails.members.length}</b></h4>

          {renderLeave()}
          <GuildAdminPanel founder={self.state.guildDetails.founder} guild={self.state.guild} />
          <GuildMessageHub guild={self.state.guild} />
          <GuildResourceHub founder={self.state.guildDetails.founder} guild={self.state.guild} members={self.state.guildDetails.members} />
        </div>
      );
    }

    return (
    <div className="container">
      <div className="row">
        <div className="col-sm-9 col-sm-offset-2">
          <div className="page-header">
              <h1>Guild Management</h1>
              <p>
                Manage your guild through this page. Create a new guild,
                view the guild inventory, send messages, etc.
              </p>
          </div>

          {renderGuild()}
          </div>
        </div>
      </div>
    );
  }
});
