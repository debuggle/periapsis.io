
var Building = React.createClass({
    getInitialState: function() {
      return this.props;
    },
    componentDidMount: function() {
      var self = this;
      // Register an event with the user object.
      User.addListener("updatedResources", function() {
        if (self.props.onUpdated ) self.props.onUpdated.call();
      });
    },
    doConstruction: function(upgrade) {
      var self = this;
      var buildingId = this.props.data.title;
      var username = User.getUsername();
      var x = this.props.x;
      var y = this.props.y;

      // Mixpanel integration for instrumentation
      if ( upgrade === true ) {
        track('upgrading ' + buildingId);
      } else {
        track('constructing ' + buildingId);
      }


      $.ajax({
        type: 'post',
        url: ((upgrade) ? '/planet/upgrade': '/planet/construct'),
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({
          username: username,
          type: buildingId,
          x: x,
          y: y
        }),
        success: function(result) {
          if ( result ) {
              var timerProp = buildingId + "Action";
              User.addTempTimer(result.uid, result.end);

              // Then update the resources!
              setTimeout(function() {
                User.updateResources(function() {
                  // Update the state.
                  var stateUpdate = {};
                  stateUpdate[timerProp] = result.uid;
                  User.fireEvent("planetUpdated", stateUpdate);
                });
              }, 150);
          }
        }
      });
    },
    componentWillReceiveProps: function(props) {
      this.setState(props);
    },
    refresh: function() {
      this.setState();
    },
    render: function() {

      // Determine the building type and associate it with an icon.
      var icon = 'fa-building';
      var category = this.state.data.category;

      switch(category) {
        case "space":
        icon = "fa-rocket";
        break;
        case "resources":
        icon = "fa-diamond";
        break;
        case "research":
        icon = "fa-flask";
        break;
        case "defense":
        icon = "fa-sitemap";
        break;
        case "array":
        icon = "fa-wifi";
        break;
      }

      var ownedProp = this.state.data.title + "Owned";
      var timerProp = this.state.data.title + "Action";
      var timer = User.getTimerEnd(this.state.planet[timerProp]);

      // See if we already own it.
      var owned = (this.state.planet[ownedProp] || 0) > 0;
      var level = (this.state.planet[ownedProp] || 0);

      // Check if this building has a timer attached.
      var disabled = (User.getResources().gold) < (this.state.data.cost * (level + 1));

      // Check if we even own this planet.
      // (This trumps everything).
      if (this.state.planet.owner !== User.getUsername())
        disabled = true;

      if ( timer !== null && (timer - User.now()) > 0 ) {
         return (
           <div className={"building " + ((disabled) ? "action-disabled" : "")}>
             <i className={"building-icon fa fa-4x " + icon}></i>
             <div className="building-title">{this.state.data.title} ({parseFloat(this.state.data.cost) * (level + 1)} gold)</div>
             <div className="building-description">{this.state.data.description}</div>
             <Timer onFinish={this.refresh.bind(this)} UID={this.state.planet[timerProp]} />
           </div>
          );
      }

      // Check if we own it technically.
      if ( timer !== null && timer > 0 && (timer - User.now()) <= 0 ) {
        owned = true;
      }

      if ( owned ) {
        return (
          <div className={"building " + ((disabled) ? "action-disabled" : "")}>
            <i className={"building-icon fa fa-4x " + icon}></i>
            <div className="building-title">{this.state.data.title} ({parseFloat(this.state.data.cost) * (level + 1)} gold)</div>
            <div className="building-description">{this.state.data.description}</div>
            <input disabled={disabled} className="btn btn-info" type="button" value={"Upgrade (" + (level + 1) + ")"} onClick={this.doConstruction.bind(this, true)} />
          </div>
        );
      } else {
        return (
          <div className={"building " + ((disabled) ? "action-disabled" : "")}>
            <i className={"building-icon fa fa-4x " + icon}></i>
            <div className="building-title">{this.state.data.title} ({parseFloat(this.state.data.cost)} gold)</div>
            <div className="building-description">{this.state.data.description}</div>
            <input disabled={disabled} className="btn btn-success" type="button" value="Construct (1)" onClick={this.doConstruction.bind(this, false)} />
          </div>
        );
      }
    }
});

var BuildingPanel = React.createClass({
  getInitialState: function() {
    return {
      buildings: []
    };
  },
  doUpdateAll: function() {
    this.setState({});
  },
  componentDidMount: function() {
    var self = this;
    $.ajax({
      type: 'post',
      url: '/buildings/get',
      success: function(result) {
        self.setState({
          buildings: result
        });
      }
    });
  },
  render: function() {
    //if ( this.props.disabled ) return (<div></div>);
    var self = this;
    var x = this.props.planet.x;
    var y = this.props.planet.y;
    var planet = this.props.planet;

    function renderBuilding(building) {
      return (
        <Building onUpdated={self.doUpdateAll} planet={planet} x={x} y={y} data={building} />
      );
    }

    return (
      <div>
        <h3>Construct Buildings</h3>
        {this.state.buildings.map(renderBuilding)}
      </div>
    );
  }
});

var PlanetActions = React.createClass({
  getInitialState: function() {
    return {
      planet: this.props.planet
    };
  },
  componentWillReceiveProps: function(props) {
    this.setState(props);
  },
  doTrade: function() {
    var self = this;
    var x = this.props.planet.x;
    var y = this.props.planet.y;
    var user = User.getUsername();
    var target = this.props.planet.owner;

    $.ajax({
      type: 'post',
      url: '/diplomacy/requestTrade',
      dataType: 'json',
      contentType: 'application/json',
      data: JSON.stringify({
        x: x,
        y: y,
        username: user,
        target: target
      }),
      success: function(result) {
        if ( result && result.success ) {
          track("Requested trade agreement");
          User.addTempTrade(x,y);
          showMessageBox("Trade Agreement", "Your trade agreement request has been sent. You will be notified once a decision is made.", null, function() {
            User.fireEvent("planetUpdated", { });
            self.setState({});
          });

        }
      }
    });

  },
  doAttack: function() {
    var self = this;
    var x = this.props.planet.x;
    var y = this.props.planet.y;
    var owner = this.props.planet.owner;

    pickFleetFixed(function(data) {
      // Get the attack estimate first.
      $.ajax({
        type: 'post',
        url: '/mission/report',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({
          x: x,
          y: y,
          ships: data
        }),
        success: function(result) {
          $("#fleetModal").modal("hide");

          // Mixpanel integration for instrumentation
          // track the probability of an attack. See who follows through.
          if ( result.attackProbability < 30 )
            track('attack probability < 30%');
          else if ( result.attackProbability < 40 )
            track('attack probability < 40%');
          else if ( result.attackProbability < 50 )
            track('attack probability < 50%');
          else if ( result.attackProbability < 60 )
            track('attack probability < 60%');
          else if ( result.attackProbability < 70 )
            track('attack probability < 70%');
          else if ( result.attackProbability < 80 )
            track('attack probability < 80%');
          else if ( result.attackProbability < 90 )
            track('attack probability < 90%');
          else if ( result.attackProbability < 100 )
            track('attack probability < 100%');
          else if ( result.attackProbability >= 100 && result.attackProbability < 200 )
            track('attack probability >= 100%');
          else if ( result.attackProbability >= 200 )
              track('attack probability >= 200%');

          var description = 'Here are the estimates for the war we are about to embark upon. <br /><br />';
          description += "&nbsp;&nbsp;&nbsp;&nbsp; H-3 Required: <b>" + result.h3 + "</b><br/>";
          description += "&nbsp;&nbsp;&nbsp;&nbsp; Planetary Shields: <b>" + result.planetShields + "%</b><br/>";
          description += "&nbsp;&nbsp;&nbsp;&nbsp; Success Probability: <b>" + result.attackProbability + "%</b><br/><br/>";
          description += "Would you like to send the fleet?";
          showMessageBox('Attack Estimates', description, 'Launch Attack', function() {

            if ( parseFloat(result.h3) > User.getResources().h3 ) {
              $("#messageModal").modal("hide");
              alert("Not enough H-3");
            } else {
              $.ajax({
                type: 'post',
                url: '/ships/startMission',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({ x: x, y: y, user: User.getUsername(), mission: 'attack', ships: data }),
                success: function(result) {
                  result = result || {};
                  if ( result && result.success ) {
                    // Mixpanel integration for instrumentation
                    if ( owner == "Pirates" )
                      track('attacking pirates');
                    else if ( owner == "Sylmar" )
                      track('attacking sylmar');
                    else
                      track('attacking another player');

                    User.attack(x,y);
                    $("#messageModal").modal("hide");
                    setTimeout(function() {
                      User.getTimers(function(){
                        Notifications.refresh();
                        User.updateResources(function(){
                          User.fireEvent("planetUpdated", { attackTimers: [result.uid] });
                        });
                      });
                    }, 500);
                  } else if ( result && result.msg ) {
                    showMessageBox("Cannot attack player", result.msg);
                  }
                }
              });
            }
          });
        }
      });
    }, 'attack');
  },
  doColonize: function(callback) {
    var x = this.props.planet.x;
    var y = this.props.planet.y;
    var username = User.getUsername();
    var self = this;

    $.ajax({
      type: 'post',
      url: '/planet/colonize/' + x + '/' + y + '/' + username,
      success: function(result) {
        // Mixpanel integration for instrumentation
        if ( result && result.success ) {
          track('colonized planet');
          User.getTimers(function() {
            if ( callback ) {
              callback.call(this, result);
            }
            Notifications.refresh();
            self.setState({ planet: { colonizeAction: result.uid } });
          });
        } else if ( result && result.msg ) {
          showMessageBox("Cannot complete action", result.msg);
        }
      }
    });

  },
  doTaunt: function() {
    var self = this;
    showInputBox("Send Message", "Enter the text you would like to send to another player.", "Submit", function(text){
      $.ajax({
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        url: '/user/taunt',
        data: JSON.stringify({
          user: User.getUsername(),
          target: self.props.planet.owner,
          msg: text
        }),
        success: function(result) {
          if ( result && result.success ) {
            track('User Taunt', { msg: text });
            User.updateResources(function() {
              self.setState({});
            });
          }
        }
      });
    });
  },
  doPeace: function() {
    var self = this;
    $.ajax({
      type: 'post',
      dataType: 'json',
      contentType: 'application/json',
      url: '/user/peace',
      data: JSON.stringify({
        user: User.getUsername(),
        target: this.props.planet.owner
      }),
      success: function(result) {
        if ( result && result.success ) {
          track("User made peace", { user: User.getUsername(), target: self.props.planet.owner });
          User.updateResources(function() {
            self.setState({});
          });
        }
      }
    });
  },
  doBoostShields: function() {
    var self = this;
    var x = this.props.planet.x;
    var y = this.props.planet.y;
    $.ajax({
      type: 'post',
      dataType: 'json',
      contentType: 'application/json',
      url: '/planet/shieldboost',
      data: JSON.stringify({
        x: this.props.planet.x,
        y: this.props.planet.y,
        user: User.getUsername()
      }),
      success: function(result) {
        if ( result && result.success ) {
          track("Shield boost");
          User.updateResources(function() {
            User.fireEvent("reloadPlanet", { x: x, y: y });
          });
        } else if ( result && result.msg ) {
          showMessageBox("Cannot complete action", result.msg);
        }
      }
    });
  },
  doBoostProduction: function() {
    var self = this;
    var x = this.props.planet.x;
    var y = this.props.planet.y;
    $.ajax({
      type: 'post',
      dataType: 'json',
      contentType: 'application/json',
      url: '/planet/productionboost',
      data: JSON.stringify({
        x: this.props.planet.x,
        y: this.props.planet.y,
        user: User.getUsername()
      }),
      success: function(result) {
        if ( result && result.success ) {
          track("Production boost");
          User.updateResources(function() {
            User.getTimers(function() {
              User.fireEvent("reloadPlanet", { x: x, y: y });
              self.setState({ planet: { productionBoost: result.uid } });
            });
          });
        } else if ( result && result.msg ) {
          showMessageBox("Cannot complete action", result.msg);
        }
      }
    });
  },
  doCreateFleet: function() {
    createFleetDialog(this.props.planet.name, this.props.planet.x, this.props.planet.y, function(data) {
      User.updateResources(function() {
        User.fireEvent("reloadMap");
      });
    });
  },
  colonizeFinish: function() {
    User.fireEvent("planetUpdated", { owner: User.getUsername() });
    User.fireEvent("colonizeUpdated", { x: this.props.planet.x, y: this.props.planet.y, owner: User.getUsername() });
  },
  refresh: function() {
    var self = this;
    User.updateResources(function(){
      self.setState({});
    });
  },
  render: function() {
    var owner = this.props.planet.owner;
    var owned = this.props.planet.owner != undefined && this.props.planet.owner != null;
    var mine = this.props.planet.owner == User.getUsername();
    var abandoned = !owned;
    var tauntEnd = User.getTauntTime(this.props.planet.owner);
    var productionBoost = this.state.planet.productionBoost;

    var atWar = User.isAtWar(this.props.planet.owner);
    var canTrade = !mine && !abandoned;
    var canColonize = !owned && abandoned;
    var canAttack = !mine && !abandoned;
    var canTaunt = !mine && !abandoned && this.props.planet.owner !== 'Pirates' && this.props.planet.owner !== 'Sylmar';

    if ( !User.canTrade(this.props.planet.x, this.props.planet.y))
      canTrade = false;

    if ( !User.canColonize())
      canColonize = false;

    if ( this.state.planet.owner == "Pirates" || this.state.planet.owner == "Sylmar" )
      canTrade = false;

    var attackText = 'Attack';
    var attackEnd = 0;
    if (parseFloat(this.props.planet.protection || 0) > User.now() && !owned) {
      attackText = 'Protected';
      canAttack = false;
      attackEnd = parseFloat(this.props.planet.protection || 0);
    }

    // Check if they're in your guild.
    if (this.props.planet.ownerGuild == User.getObject().guild  && $.trim(this.props.planet.ownerGuild).length > 0 ) {
      canAttack = false;
    }

    return (
      <div>
        <div>
          <ActionControl
              handler={this.doColonize.bind(this)}
              onFinish={this.colonizeFinish.bind(this)}
              iconClass="fa fa-users"
              textClass="planet-action"
              text="Colonize"
              ref="colonizeTimerControl"
              disabled={!canColonize}
              UID={this.state.planet.colonizeAction} />
        </div>
        <div>
          <ActionControl
              handler={this.doTaunt.bind(this)}
              onComplete={this.refresh.bind(this)}
              iconClass="fa fa-comments"
              textClass="planet-action"
              text="Send Message"
              disabled={!canTaunt}
              end={tauntEnd} />
        </div>
        <div>
          <ActionControl
              handler={this.doPeace.bind(this)}
              iconClass="fa fa-flag"
              textClass="planet-action"
              text="Declare Peace"
              disabled={!atWar || owner === "Pirates" || owner === "Sylmar"} />
        </div>
        <div>
          <ActionControl
              handler={this.doAttack.bind(this)}
              iconClass="fa fa-space-shuttle"
              textClass="planet-action"
              text={attackText}
              disabled={!canAttack}
              end={attackEnd} />
        </div>

        <h3>Administrative</h3>
        <div>
          <ActionControl
              handler={this.doCreateFleet.bind(this)}
              iconClass="fa fa-rocket"
              textClass="planet-action"
              text="Create Fleet"
              disabled={owner !== User.getUsername()} />
        </div>
        <div>
          <ActionControl
              handler={this.doBoostShields.bind(this)}
              iconClass="fa fa-bolt"
              textClass="planet-action"
              text="Boost Shields"
              disabled={owner !== User.getUsername()} />
        </div>
        <div>
          <ActionControl
              handler={this.doBoostProduction.bind(this)}
              iconClass="fa fa-bolt"
              textClass="planet-action"
              text="Boost Production"
              UID={productionBoost}
              disabled={owner !== User.getUsername()} />
        </div>
      </div>
    );
  }
});

var PlayerName = React.createClass({
  render: function() {
    function renderTag(tag) {
      if ( $.trim(tag).length == 0 ) {
        return (<span></span>);
      } else {
        return (<span className="tagName">[{tag}]</span>);
      }
    }
    function renderGuild(guild) {
      if ( $.trim(guild).length == 0 ) {
        return (<span></span>);
      } else {
        return (<span className="guildName">({guild})</span>);
      }
    }
    return (
      <div>
        {this.props.name} {renderTag(this.props.tag)} {renderGuild(this.props.guild)}
      </div>
    );
  }
});

var Planet = React.createClass({
  getInitialState: function() {
    return {
      rendered: false,
      x: this.props.x,
      y: this.props.y,
      colonizeAction: null,
      ships: [],
      name: ''
    };
  },
  componentDidMount: function() {
    var self = this;
    PanelManager.registerComponent("planet", this);
    User.addListener("planetUpdated", function(state) {
      self.setState(state || {});
    });
  },
  refresh: function() {
    var self = this;
    User.updateResources(function(){
      self.setState({});
    });
  },
  render: function() {
    var self = this;

    function displayOwner(owner, tag, guild) {
      if (owner == "Pirates" ) {
        return (
            <div className="infected-text">Pirates!</div>
        );
      } else if ( owner ) {
        return (
          <div className="planet-owner-name"><PlayerName name={owner} guild={guild} tag={tag} /></div>
        );
      } else {
        return (
          <div className="planet-owner-name">Abandoned!</div>
        );
      }
    }

    function displayAttackTimer(planet) {
      // Check for the attack timers.
      if ( planet && planet.attackTimers ) {
        for(var i = 0; i < planet.attackTimers.length; i++ ) {
          var timer = User.getTimerEnd(planet.attackTimers[i]);
          if ( timer > User.now() && timer != 0 ) {
            // This is our timer.
            return (
              <div>
                Attacking fleet will arrive in &nbsp;
                <Timer UID={planet.attackTimers[i]} onFinish={self.refresh} />
              </div>
            );
          }
        }
      }
      return (<div></div>);
    }

    var type = this.state.type;
    if ( this.state.owner == "Sylmar" )
      type = "planet-sylmar";

    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-9 col-sm-offset-2">
            <h1>{this.state.name}</h1>
            <div className="planet-attacking-info">
              {displayAttackTimer(this.state)}
            </div>
            <br />
            <div className={"planet map-slot-" + type}></div>
            <div className="planet-details">
              <div className="planet-owner">
                {displayOwner(this.state.owner, this.state.ownerTag, this.state.ownerGuild)}
                <div classname="planet-shields">(<i className="fa fa-rss"></i> Shields) <b>{this.state.shields}%</b></div>
              </div>
              <br />

              <div className="planet-description">{this.state.description}</div>
            </div>

            <h3>Actions</h3>
            <PlanetActions ref="planetActions" planet={this.state} />
            <BuildingPanel planet={this.state} disabled={(this.state.userId != User.getUID())} />
            <MessageBox />
          </div>
        </div>
      </div>
    );
  }
});
