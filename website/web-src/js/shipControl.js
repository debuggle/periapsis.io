var ShipAllocation = React.createClass({
  getInitialState: function() {
    return {
      stationed: []
    };
  },
  refresh: function() {
    var self = this;
    $.ajax({
      type: 'post',
      dataType: 'json',
      url: '/ships/inventory',
      contentType: 'application/json',
      data: JSON.stringify({
        user: User.getUsername()
      }),
      success: function(result) {
        if ( result && result.success && result.ships ) {
          self.setState({
            stationed: result.ships
          });
        } else if ( result && result.msg ) {
          alert(result.msg);
        }
      }
    });
  },
  componentDidMount: function() {
    var self = this;
    User.addListener("updateShips", function() {
      self.refresh();
    });
  },
  render: function() {
    // Serialize the data.
    var data = [];
    var fleets = User.getFleets();
    for (var i = 0; i < fleets.length; i++ ) {
      // Create the data.
      if (fleets[i].user !== User.getUsername()) continue;
      var location = fleets[i].sector;
      for ( var r = 0; r < fleets[i].ships.length; r++ ) {
        var ship = fleets[i].ships[r];
        var record = [location];
        record.push(fleets[i].displayTitle);
        record.push(ship[0]);
        record.push(ship[1])
        data.push(record);
      }
    }

    for ( var i = 0; i < this.state.stationed.length; i++ ) {
      var row = this.state.stationed[i];

      // determine whether it's a planet, to adjust the styling.
      if ( row[1].indexOf('Planet') >= 0 ) {
        row.push(true);
      } else {
        row.push(false);
      }

      data.push(row);
    }

    // Sort
    data.sort(function(a,b) { return parseFloat(a[0]) > parseFloat(b[0]); });

    return (
      <table className="table">
        <thead>
          <tr>
            <th>Location</th>
            <th>Fleet</th>
            <th>Name</th>
            <th>Qty</th>
          </tr>
        </thead>
        <tbody>
          {data.map(function(ship) {
            if ( parseFloat(ship[3]) <= 0 ) return (<span></span>);
            return (
              <tr>
                <td>{ship[0]}</td>
                <td className={(ship[4]) ? "cell-bold" : ""}>{ship[1]}</td>
                <td>{ship[2]}</td>
                <td>{parseFloat(ship[3])}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    );
  }
});

var FabricateShips = React.createClass({
  getInitialState: function() {
    return {
      ships: [],
      docks: []
    };
  },
  componentDidMount: function() {
    var self = this;
    this.refresh();
    User.addListener("updateShips", function() {
      self.refresh();
    });
  },
  componentDidUpdate: function() {
    var self = this;

    // Iterate over the ships.
    $('[data-toggle="tooltip"]').tooltip({
      container: 'body'
    });

    $("#btnSubmit").unbind('click');
    $('#btnSubmit').bind('click', function () {
      // business logic...
      // Iterate over each row.
      var order = {
        user: User.getUsername(),
        items: [],
        subtotal: 0
      };

      $(".order-row").each(function(index, row){
        var shipId = $(row).find("#shipId").val();
        var shipCost = $(row).find("#shipCost").val();
        var shipQuantity = $(row).find("#shipQuantity").val();
        var shipDestination = $(row).find("#shipDestination").val();

        // Lookup the destination and get the coordinates.
        var x = y = 0;
        var found = false;

        for ( var i = 0; i < self.state.docks.length; i++ ) {
          if ( self.state.docks[i].name == shipDestination ) {
            x = self.state.docks[i].x;
            y = self.state.docks[i].y;
            found = true;
            break;
          }
        }

        if ( !found ) return;

        if ( shipQuantity > 0 ) {
          order.subtotal += parseFloat(shipCost) * parseFloat(shipQuantity);
          order.items.push({
            name: shipId,
            cost: parseFloat(shipCost),
            quantity: parseFloat(shipQuantity),
            destination: shipDestination,
            x: parseFloat(x),
            y: parseFloat(y)
          });
        }
      }).promise().done(function() {

        // Place the orer.
        $.ajax({
          url: '/ships/fabricate',
          type: 'post',
          dataType: 'json',
          contentType: 'application/json',
          data: JSON.stringify(order),
          success: function(result) {

            if ( result !== undefined && result.success == false ) {
              showMessageBox("Error Completing Transaction", result.msg, null, function() {

              });
            } else if ( result !== undefined && result.success == true) {
              // Mixpanel integration for instrumentation
              track('fabricated ships');
              showMessageBox("Attention", "The fabrication process has begun. Please allow time for your ships to be built.");
              User.updateResources(function() {
                User.getTimers(function() {
                  $(".shipQuantity").prop('selectedIndex', 0);
                });
              });
            }
          },
          complete: function() {
            User.getTimers();
          }
        });
      });
    });
  },
  handleOrder: function() {

  },
  refresh: function() {
    var self = this;
    $.ajax({
      url: '/ships/getall/' + User.getUsername(),
      success: function(result) {
        self.setState(result);
      }
    });
  },
  render: function() {
    var docks = this.state.docks;
    if ( docks.length == 0 ) {
      return (

        <div className="container">
          <div className="row">
            <div className="col-sm-9 col-sm-offset-2">
              <br /><br />
              <div className="ship-yard-error">No Ship Yard found :( </div>
            </div>
          </div>
        </div>
        );
    }

    function renderShip(ship) {
      var css = "order-row ";
      var locked = false

      // Check if we even have this technology.
      var techRequired = ship.tech;
      if ( $.trim(techRequired).length > 0 )
        locked = ((User.getObject().technologies || []).indexOf(techRequired) < 0);

      if ( locked ) css += "order-row-disabled ";

      function renderQty() {
        if ( locked ) {
          return (<div>N/A</div>);
        } else {
          return(
            <select id="shipQuantity" className="shipQuantity">
              <option>0</option>
              <option>1</option>
              <option>2</option>
              <option>3</option>
              <option>4</option>
              <option>5</option>
              <option>6</option>
              <option>7</option>
              <option>8</option>
              <option>9</option>
              <option>10</option>
              <option>15</option>
              <option>20</option>
              <option>25</option>
              <option>50</option>
              <option>75</option>
              <option>100</option>
              <option>150</option>
              <option>175</option>
              <option>200</option>
            </select>
          );
        }
      }

      function renderDestination() {
        if ( locked ) return (<div>N/A</div>);
        else
          return (
            <select id="shipDestination">
              {docks.map(function(dock) {
                return (
                  <option>
                    {dock.name}
                  </option>
                )
              })}
            </select>
          );
      }

      return (
        <tr className={css}>
          <td>
            <input type="hidden" value={ship.name} id="shipId" />
            <input type="hidden" value={ship.cost} id="shipCost" />
            <span data-toggle="tooltip" data-placement="top" title={ship.description}>
              {ship.name}
            </span>
          </td>
          <td>
            {ship.attack || 0}
          </td>
          <td>
            {ship.defense || 0}
          </td>
          <td>
            {ship.cost}
          </td>
          <td>
            {renderQty()}
          </td>
          <td>
            {renderDestination()}
          </td>
        </tr>
      );
    }

    return (

      <div className="container">
        <div className="row">
          <div className="col-sm-9 col-sm-offset-2">
            <h1>Fabrication Order</h1>
            <div className="panel panel-default">
              <div className="panel-body">
                <div id="msgSuccess" className="order-success text-success bg-success">Order successfully placed!</div>

                <div>
                  Create new ships as part of your fleet. Each ship achieves a different goal,
                  so choose carefully! <i>Note: you must have a <b>Ship Yard</b> constructed before
                  you can build a ship!</i>
                </div>
              </div>
            </div>
            <table ref="table" className="table">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Attack</th>
                  <th>Defense</th>
                  <th>Iron</th>
                  <th>Qty</th>
                  <th>Location</th>
                </tr>
              </thead>
              <tbody>
                {this.state.ships.map(renderShip)}
              </tbody>
            </table>
            <div>
              <button id="btnSubmit" className="btn btn-info" type="button" value="Submit Order">
                Submit Order
              </button>
            </div>
            <br/><br/>
            <h3>Current Inventory</h3>
            <ShipAllocation ref="orderList" />
            <br/><br/><br/>
            <br/><br/><br/>
          </div>
        </div>
      </div>
    );
  }
});
