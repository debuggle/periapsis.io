var Platform = React.createClass({
  getInitialState: function() {
    return {
      title: this.props.title,
      disabled: false,
      description: this.props.description,
      defenderPlayer: this.props.defenderPlayer,
      icon: this.props.icon,
      goldCost: this.props.goldCost,
      ironCost: this.props.ironCost,
      h3Cost: this.props.h3Cost
    };
  },
  componentWillReceiveProps: function(props) {
    this.setState(props);
  },
  deploy: function() {
    var x = this.props.x;
    var y = this.props.y;
    var user = User.getUsername();
    var type = this.state.title;
    var self = this;

    $.ajax({
      type: 'post',
      dataType: 'json',
      contentType: 'application/json',
      url: '/asteroid/deploy',
      data: JSON.stringify({
        x: x,
        y: y,
        user: user,
        type: type
      }),
      success: function(result) {
        User.getTimers(function() {
          Notifications.refresh();
          User.fireEvent("refreshAsteroid");
        });

        if ( result && result.success ) {
          track("Deployed " + type + " into asteroid");
        } else if ( result && result.msg ) {
          showMessageBox("Cannot deploy building", result.msg);
        }
      }
    });
  },
  render: function() {
    var end = this.props.end || 0;

    // Calculate the cost stuff.
    var cost = (this.props.ironCost || this.props.h3Cost || this.props.goldCost || this.props.creditCost);
    var costType = (this.props.ironCost) ? 'iron' : (this.props.h3Cost) ? 'h3' : (this.props.creditCost) ? 'credits' : 'gold';
    var disabled = this.props.disabled;

    // Check if this should be disabled.
    if ( User.getResources()[costType] < cost ) disabled = true;
    if ( this.props.defenderPlayer !== User.getUsername() ) disabled = true;

    // Check if it has already been deployed.
    var self = this;

    // If this is a booby trap, don't let other people know about it.
    if ( this.props.defenderPlayer !== User.getUsername() && this.state.title == "Booby Trap" )
      end = 0;

    // GUI helper methods.
    function renderTimer() {
      if ( end > 0 ) {
        return (<Timer end={end} />);
      } else {
        return (<input disabled={disabled} onClick={self.deploy} className="btn btn-success" type="button" value="Deploy" />)
      }
    }

    return (
      <div className={"building " + ((disabled) ? "action-disabled" : "")}>
        <i className={"anomaly-building building-icon fa fa-4x " + this.state.icon}></i>
        <div className="building-title">{this.state.title} ({cost} {costType})</div>
        <div className="building-description">{this.state.description}</div>
        {renderTimer()}
      </div>
    );
  }
});

var PlatformPanel = React.createClass({
  render: function() {
    var self = this;
    function mapStructure(struct) {
      var disabled = self.props.disabled;
      if ( self.props.structures.indexOf(struct.name) >= 0  ) disabled = true;

      return (
        <Platform end={self.props.asteroid["platform" + struct.name]} disabled={disabled} defenderPlayer={self.props.defenderPlayer} title={struct.name} icon={struct.icon} goldCost={struct.goldCost} creditCost={struct.creditCost} ironCost={struct.ironCost} h3Cost={struct.h3Cost} description={struct.description} x={self.props.x} y={self.props.y} />
      );
    }

    return (
      <div>
        {this.props.platforms.map(mapStructure)}
      </div>
    );
  }
});

var AsteroidViewer = React.createClass({
  getInitialState: function() {
    return {
      x: 0,
      y: 0,
      mapX: 0,
      mapY: 0,
      mass: 0,
      protectTimers: [],
      platforms: [],
      structures: []
    };
  },
  doProtect: function() {
    var x = this.state.x;
    var y = this.state.y;
    var mx = this.state.mapX;
    var my = this.state.mapY;
    var f_coords = mapToFleet(x,y);

    var user = User.getUsername();
    var self = this;

    selectFleet(f_coords[0],f_coords[1],"Select which fleet you would like to protect this asteroid.", null, function(fleetName) {
      var fleet;
      var fleets = User.getFleets();
      for ( var i = 0; i < fleets.length; i++ ) {
        if ( fleets[i].title == fleetName && fleets[i].user == User.getUsername()) {
          fleet = fleets[i];
          break;
        }
      }
      if ( fleet ){
        var sx = fleet.x;
        var sy = fleet.y;
        var angle = Math.atan2(my - sy, mx - sx);
        thrust('/asteroid/defend',35,35,sx,sy,mx,my,angle,fleetName,null,x,y);
      }
    });
  },
  refresh: function() {
    var self = this;
    $.ajax({
      type: 'post',
      dataType: 'json',
      url: '/map/get/' + self.state.x + '/' + self.state.y,
      success: function(result) {
        self.setState(result.map);
      }
    });
  },
  componentDidMount: function() {
    var self = this;

    // Listen for planet updated on asteroid types.
    User.addListener("planetUpdated", function(obj) {
      if ( obj.type == 'asteroid' ) {

          // Fix the timers. This is because we are not replacing
          // empty ones. Fixes a bug with asteroids sharing timers basically.
          obj.platforms = self.state.platforms || [];
          obj.mapX = self.state.mapX;
          obj.mapY = self.state.mapY;
          self.replaceState(obj);
      }
    });

    User.addListener("setMapCoord", function(obj) {
      self.setState({ mapX: obj.x, mapY: obj.y });
    });

    User.addListener("refreshAsteroid", function() {
      self.refresh();
    });

    // Download all the structures.
    $.ajax({
      url: '/asteroid/structures',
      type: 'post',
      dataType: 'json',
      success: function(result) {
        if ( result ) {
          self.setState({ platforms: result });
        }
      }
    });
  },
  componentDidUpdate: function() {
    if ( PanelManager.getActivePanel() == "asteroid" && (this.state.type != 'asteroid' || this.state.mass <= 0 )) {
        PanelManager.activate('map');
    }
  },
  render: function() {
    var self = this;
    var owner = (<div></div>);

    // Timers
    var protectUID = User.reconcileTimers(self.state.protectTimers);

    // Actions enabled
    var canProtect = (self.state.defenderPlayer != User.getUsername());
    var canMine = (self.state.defenderPlayer == User.getUsername());
    var canBuild = canMine;

    // Text
    var protectText = "Protect";
    if ( self.state.defenderPlayer && self.state.defenderPlayer !== User.getUsername())
      protectText = "Attack";

    if ( self.state.defenderPlayer == User.getUsername()) {
      owner = (<div className="purple">
        <b>You</b> currently control this asteroid and it is in the process of being <b>mined.</b>
      </div>);
    } else if ( self.state.defenderPlayer ) {
      owner = (<div>This asteroid is protected by <b>{self.state.defenderPlayer}</b></div>);
    } else {
      owner = (<div>This asteroid is not protected. Secure it to unlock the mining operations.</div>);
    }

    // Do the mass adjustment thing.
    var mass = (Math.round(Math.round(this.state.mass) * 100) / 100) / 100;


    return (

        <div className="container">
          <div className="row">
            <div className="col-sm-9 col-sm-offset-2">
            <div className="planet-page">
              <br />
              <div className={"planet asteroid-planet map-slot-asteroid"}></div>
              <div className="anomaly-details">
                <div className="anomaly-text">
                  <h3>Asteroid ({mass} x 10<sup>7</sup> kg) </h3>
                  <h5><b>Primary Resource:</b> {this.state.resource}</h5>
                  <h5><b>Density: </b> {this.state.density}</h5>
                  <h4>{owner}</h4>
                </div>
              </div>
              <h3>Actions</h3>
                <div>
                  <ActionControl
                    iconClass="fa fa-street-view"
                    textClass="planet-action"
                    end={0}
                    disabled={!canProtect}
                    text={protectText}
                    handler={this.doProtect}
                    onFinish={this.refresh}
                    UID={protectUID} />
                </div>
                <div>
                  <ActionControl
                    iconClass="fa fa-diamond"
                    textClass="planet-action"
                    end={self.state.mineTimer || 0}
                    disabled={true}
                    text={"Mine"}
                    onFinish={this.refresh} />
                </div>

                <PlatformPanel disabled={!canBuild} asteroid={this.state} structures={this.state.structures} x={this.state.x} y={this.state.y} defenderPlayer={this.state.defenderPlayer} platforms={this.state.platforms} />
            </div>
          </div>
        </div>
      </div>
    );
  }
});
