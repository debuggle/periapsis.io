function renderTechNode(node) {
  if ( !node ) return (<div></div>);
  return (<TechNode techChildren={node.children} available={node.available} title={node.title} description={node.description} icon={node.icon} cost={node.cost} unlocked={node.unlocked||false} />);
}

var TechNode = React.createClass({
  getInitialState: function() {
    return {
      bought: false
    };
  },
  buy: function() {
    if ( this.props.available != true ) return;
    var self = this;
    $.ajax({
      url: '/tech/buy',
      type: 'post',
      dataType: 'json',
      contentType: 'application/json',
      data: JSON.stringify({
        user: User.getUsername(),
        tech: this.props.title
      }),
      success: function(result) {
        if ( result && result.success ) {
          track('Acquired ' + self.props.title);
          User.updateResources(function() {
            User.fireEvent("updateShips");
            User.fireEvent("updateTree");
          });

          self.setState({bought: true });
        } else if ( result && result.msg ) {
          showMessageBox("Cannot Purchase Upgrade", result.msg);
        }
      }
    });
  },
  render: function() {
    var available = this.props.available;
    var css = "tech-node ";
    if ( !available ) css += "tech-disabled ";
    if ( this.props.unlocked || this.state.bought ) css += "tech-acquired ";

    return (
      <div className="tech-node-parent">
        <div className={css} onClick={this.buy}>
          <div className="tech-title">{this.props.title} ({this.props.cost} Science)</div>
          <div className="tech-description">{this.props.description}</div>
        </div>
        <div className="tech-children">
          {this.props.techChildren.map(renderTechNode)}
        </div>
      </div>
    );
  }
});

var TechTree = React.createClass({
  getInitialState: function() {
    return {
      nodes: []
    };
  },
  componentDidMount: function() {
    this.refresh();
    var self = this;
    User.addListener("updateTree", function() {
      self.refresh();
    });
  },
  refresh: function() {
    var self = this;
    $.ajax({
      url: '/tech/tree',
      type: 'post',
      dataType: 'json',
      contentType: 'application/json',
      data: JSON.stringify({
        user: User.getUsername()
      }),
      success: function(result) {
        self.setState({ nodes: result });
      }
    });
  },
  render: function() {

    // Render a node.
    function renderWrapper(node) {
        return (
          <div className="tech-root">
            {renderTechNode(node)}
          </div>
        );
    }

    return (
      <div className="container">
        <div className="row">
          <div className="col-sm-9 col-sm-offset-2">
            <div className="page-header">
                <h1>Tech Tree</h1>
                <p>
                  Use this page to research technologies which can provide combat
                  bonuses, unlock ships, and improve your effectiveness as an empire.
                </p>
            </div>
            {this.state.nodes.map(renderWrapper)}
          </div>
        </div>
      </div>
    );
  }
});
